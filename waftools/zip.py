import os.path
import zipfile

from waflib.Task    import Task
from waflib.TaskGen import before, feature

import taskgen_utils


class zip(Task):
	color = "YELLOW"

	def keyword(self):
		return "Archiving"

	def __str__(self):
		return self.inputs[0].srcpath() + os.path.sep

	def scan(self):
		# Rebuild when any file under `inputs` changes.
		return self.inputs[0].ant_glob("**"), ()

	def run(self):
		with zipfile.ZipFile(self.outputs[0].abspath(), "w", zipfile.ZIP_DEFLATED) as z:
			# Obtain nodes returned by a scanner.
			for node in self.generator.bld.node_deps[self.uid()]:
				z.write(node.abspath(), os.path.join(self.prefix, node.path_from(self.inputs[0])))


@feature("zip")
@before("process_source")
def process_zip(tgen):
	sources = tgen.to_dir_nodes(tgen.source)
	targets = tgen.to_out_nodes(tgen.target)
	if len(sources) != 1:
		raise ValueError("Only a single source is allowed: %r" % tgen)
	if len(targets) != 1:
		raise ValueError("Only a single target is allowed: %r" % tgen)

	task = tgen.create_task("zip", sources, targets)
	task.prefix = getattr(tgen, "prefix", "")
	tgen.source = [ ]
