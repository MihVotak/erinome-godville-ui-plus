from waflib.TaskGen import taskgen_method


def _to_nodes(tgen, nodes, method):
	nodes = tgen.to_list(nodes)
	if not isinstance(nodes, list):
		return [nodes]
	return [method(node) if isinstance(node, str) else node for node in nodes]


@taskgen_method
def to_dir_nodes(tgen, dirs):
	return _to_nodes(tgen, dirs, tgen.path.find_dir)


@taskgen_method
def to_out_nodes(tgen, targets):
	return _to_nodes(tgen, targets, tgen.path.find_or_declare)
