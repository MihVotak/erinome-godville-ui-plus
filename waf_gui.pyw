#!/usr/bin/env python3

"""
This script is a thin GUI wrapper around `waf` command line tool, exposing its most needed features.
"""

import functools
import queue
import re
import signal
import subprocess
import sys
import threading
import webbrowser

import tkinter as tk
import tkinter.messagebox
from   tkinter import ttk


VERSION_FILE = "current_version"
COMMANDS     = ["configure", "build", "clean", "distclean", "upload"]
MAX_LINES    = 500

LINKS = [
	"https://addons.mozilla.org/en-US/developers/addons",
	"https://chrome.google.com/webstore/developer/dashboard",
]


class RootWindow(tk.Tk):
	def __init__(self, commands):
		super().__init__()
		self.title("eGUI+ packaging tool")
		# Make <Return> activate any buttons.
		self.bind_class("TButton", "<Return>", lambda e: e.widget.invoke())

		toolbar = ttk.Frame(self)
		self.btn_set_version = self._create_button(toolbar, "Set version")
		self.btn_set_version.focus_set()
		self.waf_buttons = { }
		for command in commands:
			self.waf_buttons[command] = self._create_button(toolbar, "waf %s" % command)
			setattr(self, "btn_%s" % command, self.waf_buttons[command])
		self.btn_open_amo = self._create_button(toolbar, "Open AMO")
		self.btn_open_dd_cws = self._create_button(toolbar, "Open DD-CWS")
		toolbar.pack(side=tk.LEFT, anchor=tk.N)

		scrollbar = ttk.Scrollbar(self)
		scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

		self.textarea = tk.Text(self, state=tk.DISABLED, yscrollcommand=scrollbar.set)
		self.textarea.pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)
		self.textarea.tag_config("c31", foreground="red")
		self.textarea.tag_config("c32", foreground="green")
		self.textarea.tag_config("c33", foreground="brown")
		self.textarea.tag_config("c34", foreground="blue")
		self.textarea.tag_config("c35", foreground="purple")
		self.textarea.tag_config("c36", foreground="magenta")

		scrollbar.config(command=self.textarea.yview)

	def _create_button(self, toolbar, text):
		button = ttk.Button(toolbar, text=text)
		button.pack(fill=tk.X)
		return button

	def append_text(self, text, max_lines=None):
		self.textarea.config(state=tk.NORMAL)
		for part in text.split("\x1B["):
			if not part:
				continue
			a = re.match(r"(?:\d+;)*(\d+)m", part)
			if a is not None:
				color = int(a.group(1))
				part = part[a.end():]
				if not part:
					continue
				if 31 <= color <= 36:
					self.textarea.insert(tk.END, part, "c%d" % color)
					continue
			self.textarea.insert(tk.END, part)

		if max_lines is not None:
			end_index = self.textarea.index(tk.END)
			last_line = int(end_index[:end_index.index('.')])
			if last_line > max_lines:
				self.textarea.delete("1.0", "%d.0" % (last_line - max_lines + 1))
		self.textarea.config(state=tk.DISABLED)
		self.textarea.yview_moveto(1) # Scroll to 100%.


class VersionDialog(tk.Toplevel):
	def __init__(self, master, current_version=""):
		super().__init__(master)
		self.transient(master)
		self.title("New version")
		self.bind("<Escape>", lambda e: self.destroy())
		self.new_version = None

		frame = ttk.Frame(self)
		ttk.Label(frame, text="New version:").pack(side=tk.LEFT)
		self.entry = ttk.Entry(frame)
		self.entry.insert(0, current_version)
		self.entry.bind("<Return>", self._accept)
		self.entry.pack(side=tk.LEFT)
		frame.pack()

		frame = ttk.Frame(self)
		ttk.Button(frame, text="OK", command=self._accept).pack(side=tk.LEFT)
		ttk.Button(frame, text="Cancel", command=self.destroy).pack(side=tk.LEFT)
		frame.pack(anchor=tk.E)

		self.grab_set()
		self.geometry("+%d+%d" % (master.winfo_rootx() + 50, master.winfo_rooty() + 50))
		self.entry.focus_set()
		self.wait_window(self)

	def _accept(self, event=None):
		self.new_version = self.entry.get()
		if self.new_version:
			self.destroy()
		else:
			tk.messagebox.showwarning("New version", "Please enter a version number.")
			self.entry.focus_set()


# A hack that makes functools.partial usable as a Tkinter callback.
def partial(func, *args, **kwargs):
	result = functools.partial(func, *args, **kwargs)
	result.__name__ = getattr(func, "__name__", "<unnamed>")
	return result


def stream_file(f, q, bufsize):
	while True:
		data = f.read(bufsize)
		if not data:
			break
		q.put(data)


def async_execute(*args, **kwargs):
	proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, **kwargs)
	q = queue.Queue()
	threading.Thread(target=stream_file, args=(proc.stdout, q, 1024)).start()
	return proc, q


g_root = None
g_active_process = None


def request_version():
	try:
		with open(VERSION_FILE, encoding="utf-8-sig") as f:
			current_version = f.read().strip()
	except (OSError, UnicodeDecodeError, MemoryError):
		current_version = ""

	new_version = VersionDialog(g_root, current_version).new_version
	if new_version:
		try:
			with open(VERSION_FILE, "w", encoding="utf-8") as f:
				print(new_version, file=f)
		except OSError as e:
			tk.messagebox.showwarning("New version", "Cannot save the version: %s." % e)


def on_timer_ticked(q):
	global g_active_process
	if not q.empty():
		chunks = [ ]
		try:
			while True:
				chunks.append(q.get_nowait())
		except queue.Empty:
			g_root.append_text(b"".join(chunks).decode(errors="replace"), max_lines=MAX_LINES)

	if g_active_process.poll() is None:
		g_root.after(50, partial(on_timer_ticked, q))
	else:
		g_active_process = None
		g_root.append_text('\n', max_lines=MAX_LINES)
		for button in g_root.waf_buttons.values():
			button.config(state=tk.NORMAL)


def on_button_clicked(clicked_button, command):
	global g_active_process
	if g_active_process is not None:
		g_active_process.send_signal(signal.SIGINT)
		return

	for button in g_root.waf_buttons.values():
		if button is not clicked_button:
			button.config(state=tk.DISABLED)
	g_active_process, q = async_execute(sys.executable, "waf", "--color=yes", command, bufsize=0)
	on_timer_ticked(q)


def main():
	global g_root
	g_root = RootWindow(COMMANDS)
	g_root.btn_set_version.config(command=request_version)
	for command, button in g_root.waf_buttons.items():
		button.config(command=partial(on_button_clicked, button, command))
	for button, url in zip((g_root.btn_open_amo, g_root.btn_open_dd_cws), LINKS):
		button.config(command=partial(webbrowser.open, url))

	g_root.mainloop()
	if g_active_process is not None:
		g_active_process.terminate()


if __name__ == "__main__":
	main()
