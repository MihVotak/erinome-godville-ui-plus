# По-русски (in Russian)

## Структура репозитория

    .
    |-build/ (упакованные расширения для браузеров и артефакты сборки)
    |-help_guide/ (картинки для окошка помощи)
    |-images/ (картинки: фон и лопатка)
    |-source/ (весь исходный код)
        |-chrome/ (специфичные для Chrome файлы)
        |-edge/ (специфичные для MS Edge файлы)
        |-firefox/ (специфичные для Firefox файлы)
        |-firefox-we/ (специфичные для Firefox/WebExtensions файлы)
        |-opera/ (специфичные для Opera 12 файлы)
        |-vendor/ (сторонние библиотеки)
        |-mixins/ (фрагменты, подключаемые в несколько файлов)
        |-loader/ (скрипты, подгружающие ресурсы расширения на страницы сайта)
        |-common/ (файлы, касающиеся нескольких разделов сайта)
        |-superhero/ (файлы, касающиеся страницы героя /superhero)
        |-forum/ (файлы, касающиеся форума /forums/*)
        |-*.js (разные скрипты)
        |-*.sass (разные файлы стилей)
    |-waftools/ (модули для сборки дополнения)
    |-* (разные файлы сборки дополнения, номера версии, файла обновления для FF и т. п.)

Обычно код редактируется только в `source/*`, `source/common/*`, `source/superhero/*`
и `source/forum/*`.

*Примечание:* внесение обновлений в репозиторий может происходить с задержкой до нескольких дней
относительно даты релиза новых версий.


## Сборка из исходных текстов

Скрипты сборки написаны на [Python][] (с использованием [Waf Build System][]), так что вам нужно
установить интерпретатор версии **>=3.4**. Далее предполагается, что он прописан в `PATH`.

Прежде всего выполните `pip install -Ur requirements.txt`, чтобы установить зависимости.

* `python waf configure` — подготавливает проект к сборке. Должна быть вызвана в начале работы и при
  добавлении новых модулей в `waftools/`.
* `python waf build` (или просто `python waf`) — (пере)собирает проект.
* `python waf clean` — удаляет артефакты, позволяя сделать чистую сборку.
* `python waf distclean` — удаляет артефакты и конфигурацию. После этого нужно снова вызвать
  `configure`.
* `python waf upload` — загружает релиз на сайт. *Вряд ли вам понадобится.*

Команды можно записывать подряд друг за другом: например, `python waf clean build` или
`python waf distclean configure build`.

Кроме того, есть `waf_gui.pyw` — надстройка над `waf` с графическим интерфейсом.


# In English (по-английски)

## Repository tree structure

    .
    |-build/ (packed browser add-ons and build artifacts)
    |-help_guide/ (images for help dialog)
    |-images/ (images: default background and shovel)
    |-source/ (all the source code)
        |-chrome/ (Chrome-specific files)
        |-edge/ (MS Edge-specific files)
        |-firefox/ (Firefox-specific files)
        |-firefox-we/ (Firefox/WebExtensions-specific files)
        |-opera/ (Opera 12-specific files)
        |-vendor/ (third-party libraries)
        |-mixins/ (code fragments included into several files)
        |-loader/ (scripts injecting add-on's resources into the pages)
        |-common/ (files concerning several sections of the site)
        |-superhero/ (files concerning /superhero page)
        |-forum/ (files concerning /forums/* pages)
        |-*.js (various scripts)
        |-*.sass (various style sheets)
    |-waftools/ (modules for project building)
    |-* (other files of project building, version number, update file for Firefox add-on, etc.)

Usually, only the code from `source/*`, `source/common/*`, `source/superhero/*`,
and `source/forum/*` should be edited.

*Note:* repository updates might be delayed for a few days after release of every new extension
version.


## Building from source

Build scripts are in [Python][] (and use the [Waf Build System][]) so you'll need an interpreter of
version **>=3.4**. Then, assuming it is in your `PATH`:

First, run `pip install -Ur requirements.txt` to install dependencies.

* `python waf configure` — (re)configure the project. Must be run at the start or when a new module
  is added under `waftools/`.
* `python waf build` (or just `python waf`) — (re)build the project.
* `python waf clean` — remove build artifacts (for performing a full rebuild).
* `python waf distclean` — remove artifacts and reset the configuration. After that, `configure`
  has to be called again.
* `python waf upload` — upload a release to the website. *It's a maintainer's command.*

Commands can be chained, e.g., `python waf clean build` or `python waf distclean configure build`.

There is also `waf_gui.pyw` — a wrapper around `waf` with a graphical interface.


[Python]: https://www.python.org
[Waf Build System]: https://waf.io
