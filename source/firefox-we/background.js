(function(window) {
'use strict';

browser.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
	switch (msg.type) {
		case 'playsound':
			try {
				new Audio(msg.content).play();
			} catch (e) { }
			break;
	}
	return true;
});

})(this);
