(function(window) {
'use strict';

var notifications = Object.create(null);

var destroyNotification = function(notid, notify) {
	clearTimeout(notify.timer);
	chrome.notifications.clear(notid);
};

var hideNotification = function(manual, notid) {
	var notify = notifications[notid];
	if (!notify) return;
	destroyNotification(notid, notify);
	notify.callback({
		type: 'notifyClosed',
		manual: manual,
		cid: notify.cid
	});
	delete notifications[notid];
};

chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
	switch (msg.type) {
		case 'playsound':
			try {
				var audio = new Audio(msg.content);
				audio.play();
			} catch (e) { }
			break;
		case 'makefocus':
			if (!sender.tab) break;
			if (msg.tab && sender.tab.id) {
				chrome.tabs.update(sender.tab.id, {active: true});
			}
			if (msg.window && sender.tab.windowId) {
				chrome.windows.update(sender.tab.windowId, {focused: true});
			}
			break;
		case 'notify':
			chrome.notifications.create(msg.notid, {
				type: 'basic',
				iconUrl: chrome.extension.getURL('icon64.png'),
				title: msg.title,
				message: msg.message,
				contextMessage: 'Erinome Godville UI+',
				requireInteraction: true
			},function(notid) {
				notifications[notid] = {
					cid: msg.cid,
					callback: sendResponse,
					timer: setTimeout(hideNotification, msg.timeout * 1e3, false, notid)
				};
			});
			break;
		case 'notifyHide':
			hideNotification(false, msg.notid);
			break;
		case 'notifyHideAll':
			var keys = Object.keys(notifications);
			for (var i = 0, len = keys.length; i < len; i++) {
				destroyNotification(keys[i], notifications[keys[i]]);
			}
			notifications = Object.create(null);
			break;
	}
	return true;
});

chrome.notifications.onClicked.addListener(hideNotification.bind(null, true));
chrome.notifications.onClosed.addListener(hideNotification.bind(null, false));

})(this);
