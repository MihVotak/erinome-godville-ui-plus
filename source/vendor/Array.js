// polyfills of several ES2015's array methods for Opera 12
(function() {
'use strict';

var arrayProto = Array.prototype,
	arrayProtoSlice = arrayProto.slice,
	arrayProtoIndexOf = arrayProto.indexOf,
	descriptor = {configurable: true, writable: true, value: null};

if (!Array.from) {
	descriptor.value = function from(arrayLike, mapFn, thisArg) {
		if (mapFn === undefined) {
			return arrayProtoSlice.call(arrayLike);
		}
		// cannot use Array.prototype.map because it passes 3 arguments to its callback
		var result = [];
		for (var i = 0, len = arrayLike.length >>> 0; i < len; i++) {
			result[i] = mapFn.call(thisArg, arrayLike[i], i);
		}
		return result;
	};
	Object.defineProperty(Array, 'from', descriptor);
}

if (!arrayProto.find) {
	descriptor.value = function find(pred, thisArg) {
		for (var i = 0, len = this.length >>> 0; i < len; i++) {
			var x = this[i];
			if (pred.call(thisArg, x, i, this)) {
				return x;
			}
		}
		// return undefined;
	};
	Object.defineProperty(arrayProto, 'find', descriptor);
}

if (!arrayProto.findIndex) {
	descriptor.value = function findIndex(pred, thisArg) {
		for (var i = 0, len = this.length >>> 0; i < len; i++) {
			if (pred.call(thisArg, this[i], i, this)) {
				return i;
			}
		}
		return -1;
	};
	Object.defineProperty(arrayProto, 'findIndex', descriptor);
}

if (!arrayProto.includes) {
	descriptor.value = function includes(searchElement, fromIndex) {
		if (searchElement === searchElement) {
			// .indexOf is defined in ES5, and Opera implements it natively
			return arrayProtoIndexOf.call(this, searchElement, fromIndex) !== -1;
		}
		// .indexOf(NaN) returns -1, but .includes(NaN) is required to return true if there are any
		var i = fromIndex | 0, len = this.length >>> 0;
		if (i < 0) {
			i = Math.max(len + i, 0);
		}
		for (; i < len; i++) {
			var x = this[i];
			if (x !== x) return true;
		}
		return false;
	};
	Object.defineProperty(arrayProto, 'includes', descriptor);
}

if (!arrayProto.fill) {
	descriptor.value = function fill(value, start, end) {
		var i = start | 0, j, len = this.length >>> 0;
		if (i < 0) {
			i = Math.max(len + i, 0);
		}
		if (end === undefined) {
			j = len;
		} else {
			j = end | 0;
			if (j < 0) {
				j += len;
			} else if (j > len) {
				j = len;
			}
		}
		for (; i < j; i++) {
			this[i] = value;
		}
		return this;
	};
	Object.defineProperty(arrayProto, 'fill', descriptor);
}

if (!arrayProto.copyWithin) {
	descriptor.value = function copyWithin(target, start, end) {
		var i = start | 0, j, k = target | 0, len = this.length >>> 0;
		if (i < 0) {
			i = Math.max(len + i, 0);
		}
		if (k < 0) {
			k = Math.max(len + k, 0);
		}
		if (end === undefined) {
			j = len;
		} else {
			j = end | 0;
			if (j < 0) {
				j += len;
			} else if (j > len) {
				j = len;
			}
		}
		var iters = Math.min(j - i, len - k);
		if (iters > 0) {
			if (i >= k || i + iters <= k) {
				do {
					this[k++] = this[i++];
				} while (--iters);
			} else {
				i += iters;
				k += iters;
				do {
					this[--k] = this[--i];
				} while (--iters);
			}
		}
		return this;
	};
	Object.defineProperty(arrayProto, 'copyWithin', descriptor);
}

var funcs = [
	'copyWithin', 'every', 'fill', 'find', 'findIndex', 'forEach', 'indexOf', 'join', 'lastIndexOf',
	'reduce', 'reduceRight', 'reverse', 'some', 'sort'
];
var protos = [
	Int8Array.prototype, Uint8Array.prototype, Uint8ClampedArray.prototype,
	Int16Array.prototype, Uint16Array.prototype,
	Int32Array.prototype, Uint32Array.prototype,
	Float32Array.prototype, Float64Array.prototype
];
var func, proto,
	i, j, len = funcs.length, jlen = protos.length;
for (i = 0; i < len; i++) {
	func = funcs[i];
	descriptor.value = arrayProto[func];
	if (!descriptor.value) continue;
	for (j = 0; j < jlen; j++) {
		proto = protos[j];
		if (!proto[func]) {
			Object.defineProperty(proto, func, descriptor);
		}
	}
}
descriptor.value = function map(callback, thisArg) {
	var len = this.length >>> 0,
		result = new this.constructor(len);
	for (var i = 0; i < len; i++) {
		result[i] = callback.call(thisArg, this[i], i, this);
	}
	return result;
};
for (j = 0; j < jlen; j++) {
	proto = protos[j];
	if (!proto.map) {
		Object.defineProperty(proto, 'map', descriptor);
	}
}

})();
