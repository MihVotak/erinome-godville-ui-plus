/**
 * @param {string} path
 * @param {string} method
 * @param {?(string|Object)} data
 * @param {string} encoding
 * @param {?function(!XMLHttpRequest)} [onsuccess]
 * @param {?function(!XMLHttpRequest)} [onfailure]
 */
var processXHR = function(path, method, data, encoding, onSuccess, onFailure) {
	var xhr = new XMLHttpRequest, f, key, value, i, len;

	xhr.open(method, path, true);
	xhr.onreadystatechange = function() {
		if (xhr.readyState < 4) {
			return;
		} else if (xhr.status >= 200 && xhr.status < 300) {
			if (onSuccess) {
				onSuccess(xhr);
			}
		} else if (onFailure) {
			onFailure(xhr);
		}
	};

	if (data == null) {
		xhr.send();
		return;
	}

	switch (encoding) {
		case 'url':
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			break;

		case 'form-data':
			f = new FormData;
			for (key in data) {
				value = data[key];
				if (Array.isArray(value)) {
					for (i = 0, len = value.length; i < len; i++) {
						f.append(key, value[i]);
					}
				} else {
					f.append(key, value);
				}
			}
			data = f;
			break;

		default:
			throw new Error("unknown POST XHR encoding '" + encoding + "'");
	}

	xhr.send(data);
};
