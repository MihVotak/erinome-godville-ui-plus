(function(worker) {
'use strict';

worker.GUIp = worker.GUIp || {};

var ui_log = worker.GUIp.log = {};

ui_log.customDomain = !/^https?:\/\/godville(?:\.net|game\.com)\/+duels\/+log\//.test(location.href);
ui_log.logID = (/\/duels\/+log\/+([^?#]+?)\/*(?:[?#]|$)/.exec(location.pathname) || [])[1];
// .startsWith relies on a polyfill in Opera 12 which may not be loaded yet
ui_log.isStream = location.pathname.indexOf('/reporter/') === 0;
ui_log.chronicles = {};
ui_log.directionlessMoveIndex = 0;
ui_log.wormholeMoveIndex = 0;
ui_log.directionlessMoveCombo = '';
ui_log.wormholeMoveCombo = [];
ui_log.corrections = {n: 'north', e: 'east', s: 'south', w: 'west'};

/** @namespace */
ui_log.storage = {
	_localPrefix: 'Log:' + ui_log.logID + ':',
	_localPrefixPattern: 'Log:\\w{5,9}:',

	_keyOf: function(id, local) {
		return 'eGUI_' + ui_log.godname + (local ? ':' + this._localPrefix : ':') + id;
	},

	/**
	 * @param {string} id
	 * @param {boolean=} local
	 * @returns {?string}
	 */
	get: function(id, local) {
		return localStorage.getItem(this._keyOf(id, local));
	},

	/**
	 * @param {string} id
	 * @param {(string|number|boolean)} value
	 * @param {boolean=} local
	 */
	set: function(id, value, local) {
		localStorage[this._keyOf(id, local)] = value;
	},

	/**
	 * @param {string} id
	 * @param {boolean=} local
	 */
	remove: function(id, local) {
		delete localStorage[this._keyOf(id, local)];
	},

	/**
	 * @param {string} id
	 * @param {boolean=} local
	 * @returns {boolean}
	 */
	getFlag: function(id, local) {
		return this.get(id, local) === 'true';
	},

	/**
	 * @param {string} id
	 * @param {boolean=} local
	 * @returns {!Array<string>}
	 */
	getList: function(id, local) {
		var s = this.get(id, local);
		return s ? s.split(',') : [];
	},

	/**
	 * @param {string} id
	 * @param {boolean=} local
	 * @returns {*}
	 */
	getJSON: function(id, local) {
		return GUIp.common.parseJSON(this.get(id, local));
	},

	/**
	 * @param {string} id
	 * @param {*} value
	 * @param {boolean=} local
	 */
	setJSON: function(id, value, local) {
		this.set(id, JSON.stringify(value), local);
	},

	clean: function() {
		if (this.get('Log:cleanup') === GUIp.common.formatTime(new Date(),'logger')) {
			return;
		}
		var current = this.get('Log:current'),
			general = new worker.RegExp(this._keyOf(this._localPrefixPattern)),
			specialized = new worker.RegExp(this._localPrefix + (current ? '|' + current : ''));
		for (var key in localStorage) {
			if (general.test(key) && !specialized.test(key)) {
				localStorage.removeItem(key);
			}
		}
		this.set('Log:cleanup',GUIp.common.formatTime(new Date(),'logger'));
	}
};

// TODO: implement more robust checks
ui_log.isOldDungeonLog = function() {
	return this.logID.length === 5;
};

ui_log.isDungeonLog = function() {
	return this.logID.length === 9;
};

ui_log.isSailingLog = function() {
	return this.logID.length === 7;
};

ui_log.clearDungeonPhrases = function() {
	for (var key in localStorage) {
		if (key.startsWith('LogDB:')) {
			localStorage.removeItem(key);
		}
	}
};

/**
 * @param {string} selector
 * @returns {?Element}
 */
ui_log.getChronicleLastLine = function(selector) {
	var items = document.querySelectorAll(selector);
	return items[location.href.includes('sort=desc') ? 0 : items.length - 1] || null;
};

/**
 * @param {string} blockID
 * @returns {string}
 */
ui_log.getArenaHeroName = function(blockID) {
	var heroBlock = document.getElementById(blockID), fields;
	if (!heroBlock) return '';
	fields = heroBlock.getElementsByClassName('field_content');
	return fields.length >= 2 && fields[1].textContent.trim() === this.godname ? fields[0].textContent.trim() : '';
};

/**
 * @returns {!Object<string, string>} godName: heroName
 */
ui_log.getDungeonHeroNames = function() {
	var links = document.querySelectorAll('#hero1_info .l_capt a'),
		result = Object.create(null),
		link;
	for (var i = 0, len = links.length; i < len; i++) {
		link = links[i];
		result[decodeURIComponent(/\/([^/]+)$/.exec(link.href)[1])] = link.textContent;
	}
	return result;
};

/**
 * @returns {number}
 */
ui_log.getApproximateEndDate = function() {
	var date = GUIp.common.parseDateTime(document.getElementsByClassName('ft')[0].textContent),
		startDate = +date,
		revCaptions = Array.from(document.querySelectorAll('#last_items_arena .d_capt')),
		caption, m, endDate;
	if (!location.href.includes('sort=desc')) {
		revCaptions.reverse();
	}
	for (var i = 0, len = revCaptions.length; i < len; i++) {
		if ((caption = revCaptions[i].textContent.trim())) {
			m = /(\d+):(\d+)/.exec(caption);
			endDate = date.setHours(+m[1], +m[2], 0, 0);
			return endDate >= startDate ? endDate : endDate + 86400e3; // 24h
		}
	}
	throw new Error('cannot determine end date of the chronicle');
};

/**
 * @param {string} msg
 * @param {string} heroName
 * @returns {boolean}
 */
ui_log.parseSparResult = function(msg, heroName) {
	var pos = msg.search(/ получает порцию опыта| gets experience points for today/i);
	return pos >= 0 && msg.endsWith(heroName, pos);
};

/**
 * @param {string} msg
 * @param {!Object<string, string>} names
 * @returns {number}
 */
ui_log.parseDungeonResult = function(msg, names) {
	var heroName = names[this.godname],
		otherNames = Object.values(names),
		endPos = 0,
		pos, logs;
	while ((pos = msg.indexOf(heroName, endPos)) >= 0) {
		pos += heroName.length;
		endPos = Math.min.apply(null, otherNames.map(function(name) {
			var i = msg.indexOf(name, pos);
			return i >= 0 ? i : msg.length;
		}));
		if ((logs = msg.slice(pos, endPos).match(/бревно для ковчега|ещё одно бревно|log for the ark/gi))) {
			return logs.length;
		}
	}
	return 0;
};

/**
 * @param {!GUIp.common.activities.Activity} act
 */
ui_log.addActivity = function(act) {
	if (act.date < Date.now() - GUIp.common.activities.storageTime) {
		return;
	}
	var activities = GUIp.common.activities.load(this.storage),
		existing = activities.find(function(existing) { return existing.logID === act.logID; });
	if (!existing) {
		activities.push(act);
	} else if (existing.result < 0) {
		existing.result = act.result;
		// should not update date here, since we might have inaccurate one
	} else {
		if (existing.result !== act.result) {
			GUIp.common.warn('activity result mismatch:', existing.result, '!=', act.result);
		}
		return;
	}
	GUIp.common.activities.save(this.storage, activities.sort(function(a, b) { return a.date - b.date; }));
};

ui_log.saveSparResults = function() {
	var entry = this.getChronicleLastLine('#last_items_arena .new_line .text_content'),
		heroName = this.getArenaHeroName('hero1_info') || this.getArenaHeroName('hero2_info');
	if (!entry || !heroName) return;
	this.addActivity({
		type: 'spar',
		date: this.getApproximateEndDate(),
		result: +this.parseSparResult(entry.textContent, heroName),
		logID: this.logID
	});
};

ui_log.saveDungeonResults = function() {
	var entry = this.getChronicleLastLine('#last_items_arena .new_line .text_content'),
		heroNames = this.getDungeonHeroNames();
	if (!entry || !heroNames[this.godname]) return;
	this.addActivity({
		type: 'dungeon',
		date: this.getApproximateEndDate(),
		result: this.parseDungeonResult(entry.textContent, heroNames),
		logID: this.logID
	});
};

ui_log.initColorMap = function() {
	if (localStorage.getItem('debugGVP') || ui_log.storage.getFlag('Option:enableDebugMode')) {
		GUIp.common['treasureChestRegExp'] = new worker.RegExp(localStorage.getItem('LogDB:treasureChestPhrases').replace(/(plunder the treasure trove and divide the loot|потрошат сокровищницу|делят награб)\|?/g,'').replace(/\|$/,''));
		GUIp.common['bossRegExp'] = new worker.RegExp(localStorage.getItem('LogDB:bossPhrases').replace(/(The heroes defeated|defeated the nasty creature|arrogant monster|the heroes are deciding what to do next|successfully defeating the boss|defeated the nasty boss|тяжело рухнул|победу над наглым|Собранные судьбой|предсмертными хрипами|золото и трофеи|порван на куски|над исчадием зла|побеждённого в столичный зоопарк)\|?/g,'').replace(/\|$/,''));
	}
	// finally process everything
	this.parseChronicles();
	if (!document.querySelector('#dmap')) {
		this.isCGM = true;
		this.buildMap();
	}
	if (document.querySelector('#dmap')) {
		this.prepareMap();
		this.describeMap();
		this.highlightTreasuryZone();
		if (GUIp.common.isAndroid) {
			GUIp.common.tooltipCells();
		}
		if ((ui_log.customDomain || ui_log.storage.getList('Option:dungeonMapSettings').includes('dims')) &&
			document.querySelector('#hero2 .block_h') && !document.querySelector('.dmapDimensions')
		) {
			var mapDimensions = document.createElement('span');
			mapDimensions.classList.add('dmapDimensions');
			mapDimensions.textContent = GUIp.common.dmapDimensions();
			document.querySelector('#hero2 .block_h').insertBefore(mapDimensions,null);
		}
	} else if (document.querySelector('div.trace_div')) {
		document.querySelector('div.trace_div').style.display = 'none';
	}
};

ui_log.initColorMapNC = function() {
	this.prepareMap();
	this.highlightTreasuryZone();
};

ui_log.prepareMap = function() {
	// make dmap feel a bit like normal map
	var dmap = document.querySelector('#dmap');
	if (dmap.classList.contains('e_prepared')) {
		return;
	}
	dmap.innerHTML = dmap.innerHTML.replace(/>\s{2,}</g, "><");
	var cells = document.querySelectorAll('.dml .dmc');
	for (var i = 0, len = cells.length; i < len; i++) {
		if (cells[i].textContent.includes('@')) {
			cells[i].classList.add('map_pos');
			break;
		}
	}
	// expand minimap
	try {
		var mw, styles, overhead = 0;
		styles = worker.getComputedStyle(dmap);
		overhead += parseInt(styles.paddingLeft) + parseInt(styles.paddingRight);
		styles = worker.getComputedStyle(dmap.parentNode);
		overhead += parseInt(styles.paddingLeft) + parseInt(styles.paddingRight) + 2;
		overhead = Math.max((overhead || 0), 20);
		if (worker.GUIp_browser !== 'Opera') {
			dmap.style.width = dmap.scrollWidth > (400 - overhead) ? (400 - overhead) + 'px' : 'initial';
		} else {
			dmap.style.width = dmap.scrollWidth > (400 - overhead) ? (400 - overhead) + 'px' : 'inherit';
			dmap.style.overflowY = 'auto';
			dmap.style.paddingBottom = '25px';
		}
		mw = Math.min((dmap.scrollWidth + overhead),400) + 'px';
		GUIp.common.addListener(worker, 'scroll', function() {
			dmap.parentNode.parentNode.style.width = mw;
		});
		dmap.parentNode.parentNode.style.width = mw;
	} catch (e) {
		GUIp.common.error('expanding minimap has failed:', e);
	}
	dmap.classList.add('e_prepared');
};

ui_log.buildMap = function() {
	var step, lastSentence, blocked, bounds, txmi, txma, pos = {x: 0, y: 0},
		mapData = {}, mapArray = [], params, tdest, noffset, dlMoveIndex = 0, whMoveIndex = 0,
		trapMoveLossCount = 0,
		steps = worker.Object.keys(this.chronicles),
		steps_max = steps.length,
		warning_text = '';
	if (worker.location.href.includes('?')) {
		params = this.parseQueryString(worker.location.href);
		this.directionlessMoveCombo = params['edm'] || '';
		if (params['ewh']) {
			tdest = params['ewh'].split(',');
			this.wormholeMoveCombo = [];
			for (var i = 0, len = tdest.length; i < len; i += 2) {
				this.wormholeMoveCombo.push([+tdest[i],+tdest[i+1]]);
			}
		}
		if (params['eno']) {
			noffset = params['eno'].split(',');
		}
	}
	var setCell = function(position, content) {
		if (!mapData[position.y]) {
			mapData[position.y] = {}
		}
		if ((content === '#' || content === ' ') && mapData[position.y][position.x] !== undefined) {
			return;
		}
		mapData[position.y][position.x] = content;
	}
	var matchPointers = function(cellPointers, matchPointers) {
		return cellPointers.includes(matchPointers[0]) && cellPointers.includes(matchPointers[1]);
	}
	for (step = 1; step <= steps_max; step++) {
		if (!this.chronicles[step]) {
			GUIp.common.warn('data from step #' + step + ' missing: map generation not possible!');
			warning_text = worker.GUIp_i18n.dungeon_map_failed;
			break;
		}
		if (this.chronicles[step].directionless) {
			if (tdest = this.directionlessMoveCombo[dlMoveIndex++]) {
				GUIp.common.moveCoords(pos, { direction: this.corrections[tdest] });
			} else if (this.chronicles[step].directionguess) {
				GUIp.common.info(
					'detected directionless move at step #' + step + ' with single guess: proceeding map generation...');
				GUIp.common.moveCoords(pos, { direction: this.chronicles[step].directionguess });
			} else {
				GUIp.common.warn('detected directionless move at step #' + step + ': map generation not possible!');
				setCell(pos, '@');
				warning_text = worker.GUIp_i18n.cgm_no_dlm;
				break;
			}
		} else {
			GUIp.common.moveCoords(pos, this.chronicles[step]);
		}
		if (this.chronicles[step].wormhole) {
			setCell(pos, '~');
			if (tdest = this.wormholeMoveCombo[whMoveIndex++]) {
				pos.y += tdest[0];
				pos.x += tdest[1];
			} else {
				warning_text = worker.GUIp_i18n.cgm_no_wm;
				break;
			}
		}
		if (this.chronicles[step].pointers.length) {
			var pchar = ' ';
			if (this.chronicles[step].pointers.length === 2) {
				if (matchPointers(this.chronicles[step].pointers, ['north', 'east'])) {
					pchar = '⌊';
				} else if (matchPointers(this.chronicles[step].pointers, ['north', 'west'])) {
					pchar = '⌋';
				} else if (matchPointers(this.chronicles[step].pointers, ['south', 'east'])) {
					pchar = '⌈';
				} else if (matchPointers(this.chronicles[step].pointers, ['south', 'west'])) {
					pchar = '⌉';
				} else if (matchPointers(this.chronicles[step].pointers, ['north_east', 'north_west'])) {
					pchar = '∨';
				} else if (matchPointers(this.chronicles[step].pointers, ['north_east', 'south_east'])) {
					pchar = '<';
				} else if (matchPointers(this.chronicles[step].pointers, ['south_east', 'south_west'])) {
					pchar = '∧';
				} else if (matchPointers(this.chronicles[step].pointers, ['north_west', 'south_west'])) {
					pchar = '>';
				}
			} else {
				switch (this.chronicles[step].pointers[0]) {
					case 'north_east': pchar = '↗'; break;
					case 'north_west': pchar = '↖'; break;
					case 'south_east': pchar = '↘'; break;
					case 'south_west': pchar = '↙'; break;
					case 'north':      pchar = '↑'; break;
					case 'east':       pchar = '→'; break;
					case 'south':      pchar = '↓'; break;
					case 'west':       pchar = '←'; break;
					case 'freezing': pchar = '✵'; break;
					case 'cold':     pchar = '❄'; break;
					case 'mild':     pchar = '☁'; break;
					case 'warm':     pchar = '♨'; break;
					case 'hot':      pchar = '☀'; break;
					case 'burning':  pchar = '✺'; break;
				}
			}
			setCell(pos, pchar);
		} else if (this.chronicles[step].marks.includes('boss')) {
			setCell(pos, '💀');
		} else if (this.chronicles[step].marks.join().includes('trap')) {
			setCell(pos, '🕳');
		} else {
			setCell(pos, ' ');
		}
		if (blocked = GUIp.common.calcBlocked(this.chronicles, step)) {
			if (blocked & 0x01) {
				setCell({y: pos.y - 1, x: pos.x}, '#');
			}
			if (blocked & 0x02) {
				setCell({y: pos.y + 1, x: pos.x}, '#');
			}
			if (blocked & 0x04) {
				setCell({y: pos.y, x: pos.x - 1}, '#');
			}
			if (blocked & 0x08) {
				setCell({y: pos.y, x: pos.x + 1}, '#');
			}
		}
	}
	if (noffset) {
		setCell({x: noffset[1], y: noffset[0]}, '✖');
	}
	setCell({x: 0, y: 0}, '🚪');
	if (step > steps_max) {
		setCell(pos, '@');
	}
	var bounds = {xmi: 0, xma: 0, ymi: 0, yma: 0};
	for (var y in mapData) {
		txmi = Math.min.apply(null,Object.keys(mapData[y]));
		txma = Math.max.apply(null,Object.keys(mapData[y]));
		if (txmi < bounds.xmi) {
			bounds.xmi = txmi;
		}
		if (txma > bounds.xma) {
			bounds.xma = txma;
		}
	}
	bounds.ymi = Math.min.apply(null,Object.keys(mapData));
	bounds.yma = Math.max.apply(null,Object.keys(mapData));
	txmi = false;
	txma = false;
	for (var y in mapData) {
		if (!txmi && mapData[y][bounds.xmi] && mapData[y][bounds.xmi] !== '#') {
			bounds.xmi--;
			txmi = true;
		}
		if (!txma && mapData[y][bounds.xma] && mapData[y][bounds.xma] !== '#') {
			txma = true;
			bounds.xma++;
		}
	}
	for (var x in mapData[bounds.ymi]) {
		if (mapData[bounds.ymi][x] !== '#') {
			bounds.ymi--;
			break;
		}
	}
	for (var x in mapData[bounds.yma]) {
		if (mapData[bounds.yma][x] !== '#') {
			bounds.yma++;
			break;
		}
	}
	for (var y = bounds.ymi, i = 0; y <= bounds.yma; y++, i++) {
		mapArray.push([]);
		for (var x = bounds.xmi; x <= bounds.xma; x++) {
			if (!mapData[y] || !mapData[y][x]) {
				mapArray[i].push('?');
			} else {
				mapArray[i].push(mapData[y][x]);
			}
		}
	}
	var stl_form = document.getElementById('send_to_LEM_form'),
		trace_div = document.querySelector('div.trace_div'),
		map_elem = '<div id="hero2"><div class="box"><div class="block"><div class="block_h">' + worker.GUIp_i18n.map + ' <span title="' + worker.GUIp_i18n.map_cgm + '">(CGM)</span></div><div id="dmap" class="new_line em_font">';
	for (var i = 0, ilen = mapArray.length; i < ilen; i++) {
		map_elem += '<div class="dml" style="width:' + (mapArray[0].length * 21) + 'px;">';
		for (var j = 0, jlen = mapArray[0].length; j < jlen; j++) {
			map_elem += '<div class="dmc' + (mapArray[i][j] === '#' ? ' dmw' : '') + '" style="left:' + (j * 21) + 'px">' + mapArray[i][j] + '</div>';
		}
		map_elem += '</div>';
	}
	map_elem += '</div>' + (warning_text.length ? '<div class="hint_bar">' + warning_text + '</div>' : '') + '</div></div></div>';
	document.getElementById('right_block').insertAdjacentHTML('beforeend', map_elem);
	if (trace_div) {
		document.querySelector('#hero2 .block').insertBefore(trace_div, null);
	}
	if (stl_form) {
		document.querySelector('#hero2 .block').insertBefore(stl_form, null);
	}
	worker.onscroll = GUIp.common.try2.bind(ui_log, ui_log.onscroll);
	GUIp.common.setTimeout(worker.onscroll, 500);
	ui_log.chronicleGeneratedMap = true;
};

ui_log.buildSM = function(data, type) {
	var map_elem, wtime, streamreload;
	switch (type) {
		case 'd':
			map_elem = '<div id="hero2"><div class="box"><div class="block"><div class="block_h">' + worker.GUIp_i18n.map + ' <span title="' + worker.GUIp_i18n.map_sm + '">(SM)</span><span class="e_sm_auto"><input id="streamreload" type="checkbox" title="' + worker.GUIp_i18n.sm_autoreload + '"></span></div><div id="dmap" class="new_line em_font">';
			for (var i = 0, ilen = data.map.length; i < ilen; i++) {
				map_elem += '<div class="dml" style="width:' + (data.map[0].length * 21) + 'px;">';
				for (var j = 0, jlen = data.map[0].length; j < jlen; j++) {
					map_elem += '<div class="dmc' + (data.map[i][j] === '#' ? ' dmw' : '') + '" style="left:' + (j * 21) + 'px">' + data.map[i][j] + '</div>';
				}
				map_elem += '</div>';
			}
			map_elem += '</div></div></div></div>';
			document.getElementById('right_block').insertAdjacentHTML('beforeend', map_elem);
			worker.onscroll = GUIp.common.try2.bind(ui_log, ui_log.onscroll);
			GUIp.common.setTimeout(worker.onscroll, 500);
			break;
		case 'f':
			map_elem = '<span class="e_sm_auto"><input id="streamreload" type="checkbox" title="' + worker.GUIp_i18n.sm_autoreload + '"></span>';
			document.querySelector('#right_block .block_h').insertAdjacentHTML('beforeend', map_elem);
			break;
		case 'fb':
			map_elem = '<div id="hero2"><div class="box"><div class="block"><div class="block_h">' + worker.GUIp_i18n.opps + ' <span class="e_sm_auto"><input id="streamreload" type="checkbox" title="' + worker.GUIp_i18n.sm_autoreload + '"></span></div><div class="new_line"></div></div></div></div>';
			document.getElementById('right_block').insertAdjacentHTML('beforeend', map_elem);
			break;
	}
	var bar = document.createElement('div');
	bar.id = 'timeout_bar';
	document.body.insertBefore(bar, document.body.firstChild);
	streamreload = document.getElementById('streamreload');
	GUIp.common.addListener(streamreload, 'click', function() {
		ui_log.storage.set('streamreload', this.checked, true);
		if (!this.checked) {
			bar.style.transitionDuration = '';
			bar.classList.remove('running');
			if (ui_log.smReloadTimer !== undefined) {
				worker.clearTimeout(ui_log.smReloadTimer);
			}
			if (ui_log.smWatchdogTimer !== undefined) {
				worker.clearTimeout(ui_log.smWatchdogTimer);
			}
		} else if (type === 'd') {
			wtime = data.rcvtime + data.delay * 1e3 - Date.now();
			if (wtime > 0) {
				bar.style.transitionDuration = wtime + 'ms';
				GUIp.common.setTimeout(function() { bar.classList.add('running'); }, 100);
				ui_log.smReloadTimer = GUIp.common.setTimeout(function() { ui_log.checkSM(data.step); }, wtime);
				ui_log.smWatchdogTimer = GUIp.common.setTimeout(function() { worker.location.reload(); }, 65e3);
			} else {
				worker.location.reload();
			}
		} else {
			bar.style.transitionDuration = '15s';
			GUIp.common.setTimeout(function() { bar.classList.add('running'); }, 100);
			ui_log.smReloadTimer = GUIp.common.setTimeout(function() { worker.location.reload(); }, 15e3);
		}
	});
	if (ui_log.storage.getFlag('streamreload', true)) {
		data.rcvtime = Date.now();
		streamreload.click();
	}
};

ui_log.checkSM = function(curstep) {
	var requeueCSM = function(delay) {
		var bar = document.getElementById('timeout_bar');
		bar.style.transitionDuration = '';
		bar.classList.remove('running');
		GUIp.common.setTimeout(function() { bar.style.transitionDuration = delay + 's'; bar.classList.add('running'); }, 100);
		ui_log.smReloadTimer = GUIp.common.setTimeout(ui_log.checkSM.bind(ui_log,curstep), delay * 1e3);
	}
	GUIp.common.getXHR(GUIp.common.erinome_url + '/mapStreamer/port?id=' + ui_log.logID + '&step=' + (curstep + 1) + '&lang=' + worker.GUIp_locale, function(xhr) {
		try {
			var data = JSON.parse(xhr.responseText);
			if (data && data.map) {
				data.rcvtime = Date.now();
				ui_log.storage.setJSON('datasm', data, true);
				worker.location.reload();
				return;
			}
		} catch (e) {}
		requeueCSM(15);
	}, requeueCSM.bind(null, 5));
};

ui_log.parseQueryString = function(query) {
	var query_string = {};
	var v1 = query.split("?");
	if (v1 && v1.length == 2) {
		var vars = v1[1].split("&");
		for (var i = 0; i < vars.length; i++) {
			var pair = vars[i].split("=");
			var key = decodeURIComponent(pair[0]);
			var value = decodeURIComponent(pair[1]);
			// If first entry with this name
			if (typeof query_string[key] === "undefined") {
				query_string[key] = decodeURIComponent(value);
			// If second entry with this name
			} else if (typeof query_string[key] === "string") {
				var arr = [query_string[key], decodeURIComponent(value)];
				query_string[key] = arr;
			// If third or later entry with this name
			} else {
			query_string[key].push(decodeURIComponent(value));
			}
		}
	}
	return query_string;
};

ui_log.traceMapProcess = function(direction) {
	var chronicle, coords2, currentCell, mapCells = document.querySelectorAll('#dmap .dml'),
		progressbar = document.getElementById('trace_progress');
	if (!mapCells.length) {
		return;
	}
	var highlightThis = function(traceCoords) {
		currentCell = mapCells[traceCoords.y] && mapCells[traceCoords.y].children[traceCoords.x];
		if (!currentCell) {
			ui_log.traceMapStop();
			return false;
		}
		currentCell.classList.add('dtrace');
		return true;
	}
	currentCell = document.querySelectorAll('.dmc.dtrace');
	for (var i = 0, len = currentCell.length; i < len; i++) {
		currentCell[i].classList.remove('dtrace');
	}
	if (direction !== -1) {
		if (this.chronicles[this.traceStep] && this.chronicles[this.traceStep].wormhole) {
			if (this.chronicles[this.traceStep].wormholedst) {
				this.traceCoords.y += this.chronicles[this.traceStep].wormholedst[0];
				this.traceCoords.x += this.chronicles[this.traceStep].wormholedst[1];
			} else {
				ui_log.traceMapStop();
				return false;
			}
		}
	} else {
		if (this.chronicles[this.traceStep - 1] && this.chronicles[this.traceStep - 1].wormhole) {
			coords2 = {x: this.traceCoords.x, y: this.traceCoords.y};
			GUIp.common.moveCoords(coords2, this.chronicles[this.traceStep], direction);
			if (!highlightThis(coords2)) {
				return;
			}
			if (this.chronicles[this.traceStep - 1].wormholedst) {
				this.traceCoords.y -= this.chronicles[this.traceStep - 1].wormholedst[0];
				this.traceCoords.x -= this.chronicles[this.traceStep - 1].wormholedst[1];
			} else {
				ui_log.traceMapStop();
				return false;
			}
		}
	}
	this.traceStep += direction;
	if (this.traceStep === 1) {
		this.traceCoords = GUIp.common.calculateExitXY();
	} else {
		chronicle = this.chronicles[this.traceStep + (direction === -1 ? 1 : 0)];
		if (chronicle) {
			GUIp.common.moveCoords(this.traceCoords, chronicle, direction);
			if (chronicle.wormholedst && direction !== -1) {
				if (!highlightThis({y: this.traceCoords.y + chronicle.wormholedst[0], x: this.traceCoords.x + chronicle.wormholedst[1]})) {
					return;
				}
			};
		}
	}
	progressbar.value = this.traceStep;
	progressbar.title = worker.GUIp_i18n.trace_map_progress_step + ' #' + this.traceStep;
	if (!highlightThis(this.traceCoords)) {
		return;
	}
	currentCell = document.querySelector('.new_line.dtrace');
	if (currentCell) {
		currentCell.classList.remove('dtrace');
	}
	mapCells = document.querySelectorAll('.new_line .d_turn');
	for (var i = 0, len = mapCells.length; i < len; i++) {
		if ((mapCells[i].textContent.match(/[0-9]+/) || [])[0] === this.traceStep.toString()) {
			mapCells[i].parentNode.parentNode.classList.add('dtrace');
			break;
		}
	}
	ui_log.traceDir = direction;
};
ui_log.traceMapProgressClick = function(targetStep,max) {
	if (ui_log.traceInt) {
		ui_log.traceMapPause();
	}
	ui_log.traceStep = 0;
	ui_log.traceDir = 1;
	ui_log.traceCoords = GUIp.common.calculateExitXY();
	ui_log.traceStep = Math.min(targetStep,max - 1);
	for (var i = 1, len = ui_log.traceStep; i <= len; i++) {
		if (ui_log.chronicles[i - 1] && ui_log.chronicles[i - 1].wormhole) {
			if (ui_log.chronicles[i - 1].wormholedst) {
				ui_log.traceCoords.y += ui_log.chronicles[i - 1].wormholedst[0];
				ui_log.traceCoords.x += ui_log.chronicles[i - 1].wormholedst[1];
			} else {
				return;
			}
		}
		GUIp.common.moveCoords(ui_log.traceCoords, ui_log.chronicles[i]);
	}
	ui_log.traceMapProcess(1);
};
ui_log.traceMapStart = function() {
	if (this.traceInt) {
		return;
	}
	this.traceInt = GUIp.common.setInterval(function() {
		if (ui_log.traceStep >= document.getElementById('trace_progress').max) {
			ui_log.traceStep = 0;
		}
		ui_log.traceMapProcess(1);
	},500);
	document.querySelectorAll('#trace_button_play img')[0].style.display = 'none';
	document.querySelectorAll('#trace_button_play img')[1].style.display = '';
	document.getElementById('trace_button_play').title = worker.GUIp_i18n.trace_map_pause;
};

ui_log.traceMapPause = function() {
	if (this.traceInt) {
		worker.clearInterval(this.traceInt);
	}
	delete this.traceInt;
	document.querySelectorAll('#trace_button_play img')[1].style.display = 'none';
	document.querySelectorAll('#trace_button_play img')[0].style.display = '';
	document.getElementById('trace_button_play').title = worker.GUIp_i18n.trace_map_start;
};

ui_log.traceMapStop = function() {
	if (this.traceInt) {
		worker.clearInterval(this.traceInt);
		document.querySelectorAll('#trace_button_play img')[1].style.display = 'none';
		document.querySelectorAll('#trace_button_play img')[0].style.display = '';
		document.getElementById('trace_button_play').title = worker.GUIp_i18n.trace_map_start;
	}
	delete this.traceInt;
	delete this.traceStep;
	delete this.traceCoords;
	var cell = document.querySelectorAll('.dmc.dtrace')
	for (var i = 0, len = cell.length; i < len; i++) {
		cell[i].classList.remove('dtrace');
	}
	cell = document.querySelector('.new_line.dtrace')
	if (cell) {
		cell.classList.remove('dtrace');
	}
	document.getElementById('trace_progress').value = 0;
	document.getElementById('trace_progress').title = worker.GUIp_i18n.trace_map_progress_stopped;
};

ui_log.parseChronicles = function() {
	var flc = document.getElementById('fight_log_capt') || document.querySelector('#last_items_arena .block_h, #fight_chronicle .block_h'),
		step;
	if (this.steps < 2) {
		return;
	}
	var lastNotParsed, texts = [], infls = [],
		matches = document.querySelector('#last_items_arena').innerHTML.match(/<div class="new_line[^"]*"[^>]*>[^]*?<div class="text_content .*?">[^]+?<\/div>/g),
		reversed = location.href.includes('sort=desc');
	if (!matches) {
		GUIp.common.warn('initial parsing chronicles failed!');
		return;
	}
	if (reversed) {
		matches.reverse();
	}
	step = 1;
	for (var i = 0; step <= this.steps; i++) {
		if (!matches[i]) {
			if (step !== this.steps) {
				GUIp.common.warn('not enough steps detected! (required: ' + this.steps + ', got: ' + step + ')');
			}
			break;
		}
		lastNotParsed = true;
		if (!matches[i].match(/<div class="text_content (opp_)?infl">/)) {
			texts.push(matches[i].match(/<div class="text_content ">([^]+?)<\/div>/)[1].trim().replace(/&#39;/g, "'"));
		} else {
			infls.push(matches[i].match(/<div class="text_content (?:opp_)?infl">([^]+?)(<span|<\/div>)/)[1].trim().replace(/&#39;/g, "'"));
		}
		if (!reversed && matches[i].match(/<div class="new_line[^"]*"[^>]*style="[^"]+"[^>]*>/) && (i > 0 || flc.id === 'fight_log_capt') ||
			 reversed && (!matches[i+1] || matches[i+1].match(/<div class="new_line[^"]*"[^>]*style="[^"]+"[^>]*>/) && !matches[i].match(/<div class="d_turn"><\/div>/))) {
			GUIp.common.parseSingleChronicle.call(ui_log, texts, infls, step);
			lastNotParsed = false;
			texts = [];
			infls = [];
			step++;
		}
	}
	if (lastNotParsed) {
		GUIp.common.parseSingleChronicle.call(ui_log, texts, infls, step);
	}
};

ui_log.enumerateSteps = function() {
	if (!this.customDomain || document.querySelector('.d_capt .d_turn') || this.logID.length === 6) return;
	var i, len, matches, step, stepholder, steplines = [], dcapt = false,
		chronobox = document.querySelector('#last_items_arena, #fight_chronicle, #m_fight_log'),
		reversed = location.href.includes('sort=desc'),
		flc = document.getElementById('fight_log_capt') || chronobox.querySelector('.block_h'),
		duel = !flc.textContent.match(/Хроника подземелья|Dungeon Journal/) || location.href.includes('boss=');
	if (!chronobox || !(matches = chronobox.getElementsByClassName('new_line'))) {
		return;
	}
	for (i = 0, len = matches.length; i < len; i++) {
		steplines.push(matches[i]);
	}
	if (reversed) {
		steplines.reverse();
	}
	for (i = 0, step = duel ? 0 : 1, len = steplines.length; i < len; i++) {
		stepholder = steplines[i].getElementsByClassName('d_capt')[0];
		stepholder.title = worker.GUIp_i18n.step_n+step;
		dcapt |= stepholder.textContent.length > 0;
		if ((!reversed && steplines[i].style.length > 0 || reversed && (!steplines[i+1] || steplines[i+1].style.length > 0)) && (!duel || dcapt)) {
			step++;
			dcapt = false;
		}
	}
};

ui_log.stoneEaterCompat = function(combo) {
	var result = '', step, steps = worker.Object.keys(this.chronicles), steps_max = steps.length;
	combo = combo.split('');
	for (step = 1; step <= steps_max && combo.length; step++) {
		if (this.chronicles[step].directionless || this.chronicles[step].directionfxd) {
			if (this.chronicles[step].directionguess) {
				combo.shift();
			} else {
				result += combo.shift();
			}
		}
	}
	return result;
};

ui_log.stoneEaterCompat2 = function(combo) {
	var result = [];
	for (var i = 0, len = combo.length; i < len; i++) {
		result.push(combo[i][1]);
		result.push(-combo[i][0]);
	}
	return result.join(',');
};

ui_log.describeMap = function() {
	var step, mapCells, currentCell, trapMoveLossCount = 0,
		dmap = document.getElementById('dmap'),
		coords = GUIp.common.calculateExitXY(),
		steps = worker.Object.keys(this.chronicles),
		steps_max = steps.length,
		descFailed = false;
	mapCells = document.querySelectorAll('#dmap .dml');
	if (dmap.classList.contains('e_described')) {
		return;
	}
	for (step = 1; step <= steps_max; step++) {
		if (this.chronicles[step].directionless) {
			var shortCorrection = (this.directionlessMoveCombo || ui_log.storage.get('corrections', true) || [])[this.directionlessMoveIndex++];
			if (shortCorrection) {
				this.chronicles[step].direction = this.corrections[shortCorrection];
			} else {
				var prevCorrections = this.directionlessMoveCombo || this.storage.get('corrections', true) || '',
					newCorrections = GUIp.common.calculateDirectionlessMove.call(ui_log, '#dmap', coords, step);
				this.chronicles[step].direction = this.corrections[newCorrections[0]];
				this.directionlessMoveCombo = prevCorrections + newCorrections;
				if (document.getElementById('stoneeater')) {
					document.getElementById('stoneeater').value = this.stoneEaterCompat(this.directionlessMoveCombo);
				}
				if (!this.customDomain) {
					this.storage.set('corrections', this.directionlessMoveCombo, true);
				}
			}
			this.chronicles[step].directionless = false;
			this.chronicles[step].directionfxd = true;
		}
		GUIp.common.moveCoords(coords, this.chronicles[step]);
		if (this.chronicles[step].wormhole) {
			if (this.chronicles[step].wormholedst === null) {
				this.wormholeMoveCombo = this.wormholeMoveCombo.length ? this.wormholeMoveCombo : ui_log.storage.getJSON('wormholes', true) || [];
				if (this.wormholeMoveCombo[this.wormholeMoveIndex]) {
					this.chronicles[step].wormholedst = this.wormholeMoveCombo[this.wormholeMoveIndex];
				} else {
					var result = GUIp.common.calculateWormholeMove.call(ui_log, '#dmap', coords, step, true);
					if (!result.wm.length) {
						GUIp.common.debug('wcheck disabled, trying again');
						result = GUIp.common.calculateWormholeMove.call(ui_log, '#dmap', coords, step, false);
					}
					if (result.wm.length) {
						GUIp.common.debug('found possible targets: [' + JSON.stringify(result) + ']');
						this.chronicles[step].wormholedst = result.wm[0];
						this.wormholeMoveCombo = this.wormholeMoveCombo.concat(result.wm);
						if (result.dm.length) {
							this.directionlessMoveCombo = (this.directionlessMoveCombo || this.storage.get('corrections', true) || '') + result.dm;
						}
						if (!this.customDomain) {
							ui_log.storage.setJSON('wormholes', this.wormholeMoveCombo, true);
							ui_log.storage.set('corrections', this.directionlessMoveCombo, true);
						}
					} else {
						GUIp.common.error('unknown wormhole destination!');
						descFailed = true;
					}
				}
				this.wormholeMoveIndex++;
			}
			if (this.chronicles[step].wormholedst !== null) {
				if (mapCells[coords.y] && mapCells[coords.y].children[coords.x]) {
					currentCell = mapCells[coords.y].children[coords.x];
					GUIp.common.describeCell(currentCell,step,steps_max,this.chronicles[step],trapMoveLossCount,true);
					if (!currentCell.classList.contains('e_clickable')) {
						currentCell.classList.add('e_clickable');
						GUIp.common.addListener(currentCell, 'click', function() { ui_log.cellClick(this); });
					}
				}
				GUIp.common.debug(
					'moving via wormhole to [' + this.chronicles[step].wormholedst.toString() + '] on step ' + step);
				coords.y += this.chronicles[step].wormholedst[0];
				coords.x += this.chronicles[step].wormholedst[1];
				if (document.getElementById('teleports')) {
					document.getElementById('teleports').value = this.stoneEaterCompat2(this.wormholeMoveCombo);
				}
			}
		}
		if (!mapCells[coords.y] || !mapCells[coords.y].children[coords.x]) {
			GUIp.common.error(
				'the map does not match parsed chronicle at step #' + step +
				': either direction ("' + this.chronicles[step].direction + '") is invalid or map is out of sync!');
			descFailed = true;
			break;
		}
		currentCell = mapCells[coords.y].children[coords.x];
		if (/[#?]/.test(currentCell.textContent)) {
			GUIp.common.error(
				'parsed chronicle does not match the map at step #' + step +
				': either direction ("' + this.chronicles[step].direction + '") is invalid or map is out of sync!');
			descFailed = true;
			break;
		}
		if (currentCell.textContent.trim() === '✖') {
			this.chronicles[step].chamber = true;
		}
		if (!currentCell.classList.contains('e_clickable')) {
			currentCell.classList.add('e_clickable');
			GUIp.common.addListener(currentCell, 'click', function() { ui_log.cellClick(this); });
		}
		if (this.chronicles[step].pointers.length > 0) {
			currentCell.dataset.pointers = this.chronicles[step].pointers.join(' ');
		}
		currentCell.classList.remove('dmv');
		currentCell.classList.remove('dmh');
		trapMoveLossCount = GUIp.common.describeCell(currentCell,step,steps_max,this.chronicles[step],trapMoveLossCount);
	}
	var heroesCoords = GUIp.common.calculateXY(document.getElementsByClassName('map_pos')[0]);
	if (heroesCoords.x !== coords.x || heroesCoords.y !== coords.y) {
		GUIp.common.error(
			'chronicle processing failed, coords diff: x: ' + (heroesCoords.x - coords.x) + ', y: ' + (heroesCoords.y - coords.y));
		descFailed = true;
	} else {
		dmap.classList.add('e_described');
	}
	if (descFailed) {
		descFailed = false;
		if (ui_log.storage.get('corrections', true)) {
			descFailed = true;
			ui_log.storage.remove('corrections', true)
		}
		if (ui_log.storage.get('wormholes', true)) {
			descFailed = true;
			ui_log.storage.remove('wormholes', true)
		}
		if (!document.querySelector('.hint_bar')) {
			dmap.insertAdjacentHTML('afterend','<div class="hint_bar">' + worker.GUIp_i18n.dungeon_map_failed + (descFailed ? worker.GUIp_i18n.dungeon_map_failed_reload : '') + '<div>');
			if (descFailed) {
				GUIp.common.addListener(document.getElementById('e_dungeon_map_reload'), 'click', function() { location.reload(); });
			}
		}
	}
};

ui_log.cellClick = function(cell) {
	var targetStep, maxSteps = worker.Object.keys(ui_log.chronicles).length,
		cellSteps = cell.title.match(/#(\d+)\b/g);
	if (!cellSteps) {
		return;
	}
	targetStep = parseInt(cellSteps[0].slice(1));
	for (var i = 0, len = cellSteps.length; i < len - 1; i++) {
		if (parseInt(cellSteps[i].slice(1)) === ui_log.traceStep) {
			targetStep = parseInt(cellSteps[i + 1].slice(1));
			break;
		}
	}
	document.getElementById('trace_progress').max = maxSteps;
	ui_log.traceMapProgressClick(targetStep - 1,maxSteps);
	ui_log.scrollTo(document.querySelector('div.dtrace'));
};

ui_log.highlightTreasuryZone = function() {
	GUIp.common.improveMap.call(ui_log,'dmap', ui_log.isCGM ? true : false);
};

ui_log.getNodeIndex = function(node) {
	var i = 0;
	while ((node = node.previousElementSibling)) {
		i++;
	}
	return i;
};

/**
 * Find an element suitable for inserting custom controls before it, or null.
 *
 * @param {boolean} [aboveDelimiter=false]
 * @returns {HTMLElement}
 */
ui_log.islandsMapGetAnchor = function(aboveDelimiter) {
	var mapAnchor = document.getElementsByClassName('c_center')[0] || document.getElementById('h_tbl');
	return mapAnchor && ((aboveDelimiter && mapAnchor.previousSibling) || mapAnchor);
};

/**
 * Insert the element before the chronicle anchor.
 *
 * @param {!HTMLElement} elem
 * @param {boolean} [aboveDelimiter=false]
 * @returns {boolean} true iff succeeds.
 */
ui_log.islandsMapInsertElement = function(elem, aboveDelimiter) {
	var mapAnchor = this.islandsMapGetAnchor(aboveDelimiter);
	return !!(mapAnchor && mapAnchor.parentNode.insertBefore(elem, mapAnchor));
};

/**
 * Insert the HTML before the chronicle anchor.
 *
 * @param {string} html
 * @param {boolean} [aboveDelimiter=false]
 * @returns {boolean} true iff succeeds.
 */
ui_log.islandsMapInsertHTML = function(html, aboveDelimiter) {
	var mapAnchor = this.islandsMapGetAnchor(aboveDelimiter);
	if (!mapAnchor) {
		return false;
	}
	mapAnchor.insertAdjacentHTML('beforebegin', html);
	return true;
};

ui_log.updateButton = function() {
	var isSail = ui_log.isSailingLog(),
		result = ui_log.checkLogLimits();
	if (result.allowed === false) {
		ui_log.button.innerHTML = worker.GUIp_i18n.send_log_to_LEMs_script + (isSail ? ' (' + worker.GUIp_i18n.till_next_try_s : '<br>' + worker.GUIp_i18n.till_next_try) + result.minutes + ':' + result.seconds + (isSail ? ')' : '');
		ui_log.button.setAttribute('disabled', 'disabled');
	} else {
		ui_log.button.innerHTML = worker.GUIp_i18n.send_log_to_LEMs_script + (isSail ? ' (' + worker.GUIp_i18n.tries_left_s : '<br>' + worker.GUIp_i18n.tries_left) + result.tries + (isSail ? ')' : '');
		ui_log.button.removeAttribute('disabled');
	}
};

ui_log.improveSailChronicles = function() {
	GUIp.common.sailing.describeBeastiesOnPage('.new_line, .d_msg:not(.parsed)', null, ui_log.storage.getList('Option:islandsMapSettings').includes('bhp'));
};

/** @namespace */
ui_log.islandsMap = {
	/** @type {?GUIp.common.islandsMap.Manager} */
	manager: null,

	/**
	 * @private
	 * @type {?GUIp.common.islandsMap.observer.Observer}
	 */
	_observer: null,

	/**
	 * @private
	 * @type {?function(!GUIp.common.islandsMap.conv.RawModel)}
	 */
	_redrawMap: null,

	/** @namespace */
	expansionType: {
		none:    0,
		borders: 1,
		fill:    2
	},

	/**
	 * @private
	 * @type {number}
	 */
	_curExpansion: 0,

	/**
	 * @private
	 * @type {!Array<number>}
	 */
	_expanded: [],

	/** @type {boolean} */
	get canRedraw() { return !!this._redrawMap; },

	_extract: function(regex, text) {
		var m = regex.exec(text);
		return m && GUIp.common.parseJSON(m[1]);
	},

	/**
	 * @private
	 * @param {!Object<string, !Array<string>>} parsed
	 * @returns {!Object<string, string>}
	 */
	_processTracks: function(parsed) {
		var keys = Object.keys(parsed).sort(), result = {};
		for (var i = 0, len = keys.length; i < len; i++) {
			var key = keys[i], positions = parsed[key];
			for (var j = 0, jlen = positions.length; j < jlen; j++) {
				result[positions[j]] = key;
			}
		}
		return result;
	},

	/**
	 * @private
	 * @returns {!Array} [?GUIp.common.islandsMap.conv.RawModel, !Object<string, string>]
	 */
	_extractData: function() {
		// HACK: inspect JavaScript sources embedded into the page
		var scripts = document.getElementsByTagName('script'),
			smth, map = [], arks = [], visited = [], pois = [], tracks = {};
		for (var i = 0, len = scripts.length; i < len; i++) {
			var scriptText = scripts[i].textContent;

			// map data
			smth = this._extract(/\bm\s*=\s*(\[[^\]]{50,}\])\s*;/, scriptText);
			if (!GUIp.common.isIntegralArray(smth)) {
				continue; // this isn't the script we're looking for
			}
			map = smth;

			// tracks
			if ((smth = this._extract(/\btr\s*=\s*(\{[^}]*\})\s*;/, scriptText))) {
				try {
					tracks = this._processTracks(smth);
				} catch (e) {
					GUIp.common.error('failed processing map tracks:', e);
				}
			}

			// points of interest
			smth = this._extract(/\bsmh\s*=\s*(\[[^\]]*\])\s*;/, scriptText);
			if (GUIp.common.isIntegralArray(smth)) {
				pois = smth;
			}

			break;
		}

		var checkLocalStorage = ui_log.storage.getList('Option:islandsMapSettings').includes('conv');
		if (checkLocalStorage) {
			if (!map.length) {
				// load a map from the local storage (translation page)
				smth = ui_log.storage.getJSON('map', true);
				if (GUIp.common.isIntegralArray(smth)) {
					map = smth;
				}
			}

			smth = ui_log.storage.getJSON('arks', true);
			if (GUIp.common.isIntegralArray(smth)) {
				arks = smth;
			}

			smth = ui_log.storage.getJSON('visited', true);
			if (GUIp.common.isIntegralArray(smth)) {
				visited = smth;
			}
		}

		if (!map.length) {
			return [null, tracks];
		}

		if (!pois.length) {
			// check if the Chronicle Archive tells us points of interest
			if (GUIp.common.isIntegralArray(worker.e_islandsMapPoints) && worker.e_islandsMapPoints.length) {
				pois = worker.e_islandsMapPoints;
			} else {
				if (checkLocalStorage) {
					// load points of interest from the local storage
					pois = ui_log.storage.getJSON('pois', true);
				}
				if (!GUIp.common.isIntegralArray(pois) || !pois.length) {
					// guess from the map
					pois = GUIp.common.islandsMap.conv.guessPOIsFromMap(map);
				}
			}
		}

		return [new GUIp.common.islandsMap.conv.RawModel(map, arks, visited, pois), tracks];
	},

	/**
	 * @private
	 * @param {!Object<string, string>} tracks
	 * @returns {?function(!GUIp.common.islandsMap.conv.RawModel)}
	 */
	_maybeMakeMapRedrawer: function(tracks) {
		if (!ui_log.customDomain) {
			return null; // do not ever try to redraw a map on the original domain
		} else if (typeof worker.exported_map_reload === 'function') {
			return function(rawModel) {
				worker.exported_map_reload(rawModel.map.slice()); // pass a copy of data as it is captured there
			};
		} else if (typeof worker.Hexer === 'function') {
			var hexer = new worker.Hexer;
			return function(rawModel) {
				hexer.draw_smap(worker.$('#sail_map'), rawModel.map.slice(), {}, 0, '', tracks, rawModel.pois);
			};
		} else if (typeof worker.make_map_log === 'function') {
			return function(rawModel) {
				worker.make_map_log(worker.$('#sail_map'), rawModel.map.slice(), {}, 0, '', tracks);
			};
		}
		return null;
	},

	_extractConditions: function() {
		// load from the local storage
		var conditions = ui_log.storage.get('conds', true);
		if (conditions != null) {
			this.manager.conditions = conditions ? GUIp.common.makeHashSet(conditions.split(',')) : Object.create(null);
			return;
		}

		// check if we're watching a stream
		var data = worker.gReporterClientData; // see superhero/improver.js#reporter.collect
		if (data && (data = data.erinome) && Array.isArray(data.conditions)) {
			this.manager.conditions = GUIp.common.makeHashSet(data.conditions);
			return;
		}

		// parse ourselves
		conditions = GUIp.common.sailing.tryExtractConditions();
		if (conditions != null) {
			this.manager.conditions = GUIp.common.sailing.parseConditions(conditions);
			return;
		}

		GUIp.common.warn('cannot detect sailing conditions, assuming there are none');
	},

	/**
	 * @private
	 * @param {!Array<string>} mapSettings
	 */
	_loadPOIColors: function(mapSettings) {
		if (!mapSettings.includes('rndc')) {
			return;
		}

		var colors = ui_log.storage.getJSON('poiColors', true),
			colorizer = GUIp.common.islandsMap.vtrans.poiColorizer;
		if (GUIp.common.isIntegralArray(colors)) {
			colorizer.colors = colors;
		} else {
			GUIp.common.shuffleArray(colorizer.colors);
		}
	},

	/**
	 * @param {!Array<string>} mapSettings
	 * @returns {number}
	 */
	getExpansionType: function(mapSettings) {
		if (!mapSettings.includes('mbc')) {
			return this.expansionType.none;
		}
		return mapSettings.includes('mfc') ? this.expansionType.fill : this.expansionType.borders;
	},

	/**
	 * @param {string} type
	 */
	changeHintDrawer: function(type) {
		var imap = GUIp.common.islandsMap;
		imap.vtrans.hintManager.drawer = imap.vtrans.createHintDrawer(
			this.manager.model,
			this.manager.view,
			type,
			{whirlpoolZoneRadius: imap.defaults.getWhirlpoolZoneRadius(this.manager.conditions)}
		);
	},

	_changeExpansion: function(expansion, mger) {
		var imap = GUIp.common.islandsMap;
		if (expansion < this._curExpansion) {
			imap.mtrans.unexpand(mger.model, this._expanded);
			this._curExpansion = this.expansionType.none;
			this._expanded.length = 0;
			mger.rawModel = null;
		}
		if (expansion > this._curExpansion) {
			var added = imap.mtrans.expand(
				mger.model,
				imap.defaults.predictBorderRadius(mger.model, mger.conditions),
				expansion === this.expansionType.fill
			);
			this._curExpansion = expansion;
			for (var i = 0, len = added.length; i < len; i++) {
				this._expanded.push(added[i]);
				if (mger.rawModel) {
					// instead of reencoding the whole map, just append new tiles
					mger.rawModel.map.push(imap.conv.encodeTilePosCode(added[i], 63 /*?*/));
				}
			}
		}
	},

	/**
	 * @param {!Element} mapContainer
	 * @param {number} [expansion]
	 */
	updateMap: function(mapContainer, expansion) {
		var svgLiveCollection = mapContainer.getElementsByTagName('svg'),
			newSVG = svgLiveCollection[0];
		if (!newSVG) {
			throw new Error('the SVG is gone');
		}

		if (this.canRedraw && expansion != null && expansion !== this._curExpansion) {
			var f0, f1;
			this.manager.onPreEncode.push((f0 = this._changeExpansion.bind(this, expansion)));
			this.manager.onPostEncode.push((f1 = function(mger) {
				// _observer, _redrawMap and rawModel are non-null here
				this._observer.withDisabled(this._redrawMap.bind(this, mger.rawModel));
				mger.svg = svgLiveCollection[0] || null;
				if (!mger.svg) {
					throw new Error('cannot find an SVG after redrawing');
				}
				mger.view = null;
			}.bind(this)));
			try {
				this.manager.replaceSVG(newSVG);
			} finally {
				// unregister immediately
				var notThis = function(x) { return x !== this; };
				GUIp.common.filterInPlace(this.manager.onPreEncode, notThis, f0);
				GUIp.common.filterInPlace(this.manager.onPostEncode, notThis, f1);
			}
		} else {
			this.manager.replaceSVG(newSVG);
		}
		var settings = ui_log.storage.getList('Option:islandsMapSettings');
		GUIp.common.islandsMap.defaults.vTransBindAll(
			this.manager.model, this.manager.view, this.manager.conditions, settings
		);
	},

	/**
	 * @param {!Element} mapContainer
	 */
	initMap: function(mapContainer) {
		// in streams, #s_map itself is replaced on each step, not just its contents
		var watched = ui_log.isStream ? mapContainer.parentNode : mapContainer;
		this._observer = GUIp.common.islandsMap.observer.create(watched, function() {
			try {
				var mapContainer = GUIp.common.sailing.tryFindMapBlock();
				if (!mapContainer) {
					throw 'the block is gone';
				}
				this.manager.rawModel = null; // force rescanning the map
				this.updateMap(mapContainer);
			} catch (e) {
				GUIp.common.error('cannot update the map:', e);
			}
		}, this);

		var mapSettings = ui_log.storage.getList('Option:islandsMapSettings');
		this._loadPOIColors(mapSettings);
		this.updateMap(mapContainer, this.getExpansionType(mapSettings));
	},

	/**
	 * @param {function(!Element)} callback
	 * @param {?Object} [thisArg]
	 */
	prepare: function(callback, thisArg) {
		var pair = this._extractData();
		this._redrawMap = this._maybeMakeMapRedrawer(pair[1]);
		this.manager = new GUIp.common.islandsMap.Manager;
		this.manager.rawModel = pair[0];
		this._extractConditions();

		this.manager.onPreUpdate.push(function(mger) {
			GUIp.common.islandsMap.defaults.vTransUnbindAll(mger.model, mger.view);
		});

		var mapContainer = GUIp.common.sailing.tryFindMapBlock();
		if (mapContainer) {
			// everything is already OK
			if (this.manager.rawModel) {
				// ignore arks and islands if they haven't been cleaned up yet
				this.manager.rawModel.arks.length = this.manager.rawModel.visited.length = 0;
			}
			try {
				callback.call(thisArg, mapContainer);
			} catch (e) {
				GUIp.common.error(e);
			}
		} else if (this.manager.rawModel) {
			// we're on a translation page - draw a map ourselves
			GUIp.common.loadDomainScript('sail_' + worker.GUIp_locale + '_packaged.js',
				function() {
					return typeof worker.HS2 === 'function';
				},
				function() {
					if (!ui_log.islandsMapInsertHTML('<div id="sail_map"></div>')) {
						throw new Error('cannot insert a map');
					}

					try {
						this.manager.rawModel.withArksOnMap(function(rawModel) {
							// (data, tracks, pois, hp150, step, hp, cargo)
							new worker.HS2().s(rawModel.map, {}, rawModel.pois, {}, 0, [], []);
						});
					} catch (e) {
						GUIp.common.error('map builder failed:', e);
						return;
					}

					var mapContainer = GUIp.common.sailing.tryFindMapBlock();
					if (!mapContainer) {
						throw new Error('#sail_map got lost after creating a map'); // evil HS2...
					}
					callback.call(thisArg, mapContainer);
				}.bind(this),
				function() {
					GUIp.common.error('cannot load the map builder');
				}
			);
		} else {
			// we're on a translation page but don't have a map
			GUIp.common.info('no data to build a map with');
		}
	}
};

ui_log.islandsMapCreateRuler = function() {
	var container = document.createElement('div');
	container.className = 'e_ruler_button_wrap';
	var button = document.createElement('span');
	button.id = 'e_ruler_button';
	button.title = worker.GUIp_i18n.sail_ruler;
	button.textContent = '📏';
	container.appendChild(button);
	GUIp.common.islandsMap.vtrans.rulerManager.init(button);
	return container;
};

/**
 * @class
 */
ui_log.IslandsMapOptionBox = function() {
	/** @type {!Element} */
	this.container = document.createElement('div');
	this.container.className = 'e_option_box';
	/**
	 * @private
	 * @type {!Object<string, !HTMLInputElement>}
	 */
	this._group = {};
	var mapSettings = ui_log.storage.getList('Option:islandsMapSettings');
	if (ui_log.islandsMap.canRedraw) {
		this.container.appendChild(this._createOption('mbc', mapSettings));
		this.container.appendChild(document.createTextNode(' '));
		this.container.appendChild(this._createOption('mfc', mapSettings));
		this.container.appendChild(document.createTextNode(' '));
	}
	this.container.appendChild(this._createOption('shh', mapSettings));
};

ui_log.IslandsMapOptionBox.prototype = {
	constructor: ui_log.IslandsMapOptionBox,

	/**
	 * @private
	 * @param {string} optionName
	 */
	_onClick: function(optionName) {
		var settings = ui_log.storage.getList('Option:islandsMapSettings'),
			checked = this._group[optionName].checked,
			needsRedrawing = false,
			index = -1;
		if (checked) {
			if (!settings.includes(optionName)) {
				settings.push(optionName);
			}
		} else {
			GUIp.common.linearRemove(settings, optionName);
		}

		switch (optionName) {
			case 'mbc':
				if (!checked && (index = settings.indexOf('mfc')) >= 0) {
					settings.splice(index, 1);
					this._group.mfc.checked = false;
				}
				needsRedrawing = true;
				break;

			case 'mfc':
				if (checked && !settings.includes('mbc')) {
					settings.push('mbc');
					this._group.mbc.checked = true;
				}
				needsRedrawing = true;
				break;

			case 'shh':
				ui_log.islandsMap.changeHintDrawer(
					checked ? 'polylinear' : settings.includes('newhints') ? 'tileMarking' : 'tileMarkingOld'
				);
				break;
		}

		settings = settings.join();
		ui_log.storage.set('Option:islandsMapSettings', settings);
		if (needsRedrawing) {
			var mapContainer = GUIp.common.sailing.tryFindMapBlock();
			if (!mapContainer) {
				GUIp.common.error('the map block is gone');
				return;
			}
			ui_log.islandsMap.updateMap(mapContainer, ui_log.islandsMap.getExpansionType(settings));
		}
	},

	/**
	 * @private
	 * @param {string} optionName
	 * @param {!Array<string>} mapSettings
	 * @returns {!Element}
	 */
	_createOption: function(optionName, mapSettings) {
		var checkbox = this._group[optionName] = document.createElement('input');
		checkbox.type = 'checkbox';
		if (mapSettings.includes(optionName)) {
			checkbox.checked = true;
		}
		GUIp.common.addListener(checkbox, 'click', this._onClick.bind(this, optionName));

		var label = document.createElement('label');
		label.textContent = worker.GUIp_i18n['islands_map_' + optionName];
		label.insertBefore(checkbox, label.firstChild);
		return label;
	}
};

/**
 * @returns {!Element}
 */
ui_log.islandsMapCreateOptionBox = function() {
	return new this.IslandsMapOptionBox().container;
};

/** @namespace */
ui_log.lemUploader = {
	_url: 'https://www.godalert.info' + (ui_log.isSailingLog() ? '/Sail' : '/Dungeons'),

	_form: document.createElement('form'),

	/**
	 * @readonly
	 * @type {?Object}
	 */
	restrictions: null,

	/** @type {!Array<function(!Object)>} */
	onupdate: [],

	_keyOf: function(id) {
		return 'LEMRestrictions:' + (ui_log.isSailingLog() ? id + 'S' : id);
	},

	_getOption: function(id) {
		return ui_log.storage.get(this._keyOf(id));
	},

	_setOption: function(id, value) {
		ui_log.storage.set(this._keyOf(id), value);
	},

	/**
	 * @private
	 * @returns {!Object<string, !Array<number>>}
	 */
	_loadTimestamps: function() {
		var result = ui_log.storage.getJSON(this._keyOf('Sent'));
		if (!result || typeof result !== 'object' || Array.isArray(result)) {
			return {};
		}
		var now = Date.now(), threshold = now - this.restrictions.timeFrame * 1e3;
		for (var logID in result) {
			var moments = result[logID];
			if (!Array.isArray(moments) || !GUIp.common.filterInPlace(moments, function(x) {
				return typeof x === 'number' && x > threshold && x <= now;
			}).length) {
				delete result[logID];
			}
		}
		return result;
	},

	_parseRestrictions: function(text) {
		try {
			var response = JSON.parse(text);
			if (!Number.isSafeInteger(response.first_request) || response.first_request < 0 ||
				typeof response.time_frame !== 'number' || response.time_frame < 0 ||
				!Number.isSafeInteger(response.request_limit) || response.request_limit < 0) {
				return null;
			}
			return {
				firstRequest: response.first_request,
				timeFrame: response.time_frame * 60,
				requestLimit: response.request_limit
			};
		} catch (e) {
			return null;
		}
	},

	/**
	 * @returns {{tries: number, disabledTill: number}} Exactly one of these equals to 0.
	 */
	getStatus: function() {
		if (!this.restrictions) {
			throw new Error("calling getStatus on an uninitialized LEM's uploader");
		}
		var timestamps = this._loadTimestamps();
		ui_log.storage.setJSON(this._keyOf('Sent'), timestamps);
		var moments = timestamps[ui_log.logID] || [];
		if (moments.length < this.restrictions.requestLimit) {
			return {
				tries: this.restrictions.requestLimit - moments.length,
				disabledTill: 0
			};
		} else {
			return {
				tries: 0,
				disabledTill: Math.min.apply(null, moments) + this.restrictions.timeFrame * 1e3
			};
		}
	},

	/**
	 * @returns {!Object}
	 */
	collect: function() {
		if (!ui_log.isSailingLog()) {
			throw new Error('not implemented');
		}
		var html =
			'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n' +
			'<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' +
			document.getElementsByTagName('html')[0].innerHTML
				.replace(/<(style|form|svg|i?frame)\b[^]*?<\/\1>|\bonclick="[^"]*"|\bbackground-image\s*:\s*url\s*\(\s*&quot;data:image[^)]*\)\s*;?|\t+/g, '')
				.replace(/<script\b[^>]*>/g, '<notascript>')
				.replace(/ {2,}/g, ' ')
				.replace(/\n{2,}/g, '\n') +
			'<!-- var m = [' + ui_log.islandsMap.manager.rawModel.withArksOnMap(function(rawModel) {
				// .toString is necessary since the map will be transformed back upon returning
				return rawModel.map.toString();
			}) + ']; -->' +
			'<!-- var points = [' + ui_log.islandsMap.manager.rawModel.pois + ']; -->' +
			'</html>';
		return {
			guip: 1,
			fight_text: html
		};
	},

	/**
	 * @param {!Object} data
	 */
	send: function(data) {
		for (var key in data) {
			var input = document.createElement('input');
			input.type = 'hidden';
			input.name = key;
			input.value = data[key];
			this._form.appendChild(input);
		}
		document.body.appendChild(this._form);
		this._form.submit();
		document.body.removeChild(this._form);
		this._form.textContent = '';

		var timestamps = this._loadTimestamps(),
			moments = timestamps[ui_log.logID];
		if (moments) {
			moments.push(Date.now());
		} else {
			timestamps[ui_log.logID] = [Date.now()];
		}
		ui_log.storage.setJSON(this._keyOf('Sent'), timestamps);
	},

	init: function() {
		this._form.action = this._url + (worker.GUIp_locale === 'en' ? '/index-eng.cgi' : '/index.cgi');
		this._form.method = 'POST';
		this._form.enctype = 'multipart/form-data';
		this._form.target = '_blank';
		this._form.className = 'hidden';

		this.restrictions = {
			firstRequest: parseInt(this._getOption('FirstRequest')) || (ui_log.isSailingLog() ? 5 : 12),
			timeFrame: +this._getOption('TimeFrame') || 20 * 60,
			requestLimit: parseInt(this._getOption('RequestLimit')) || (ui_log.isSailingLog() ? 8 : 5)
		};
		var lastUpdated = +this._getOption('Date');
		if (lastUpdated && Date.now() - lastUpdated < 24*60*60*1000) {
			return;
		}

		GUIp.common.getXHR(this._url + '/guip.cgi', function(xhr) {
			var restrictions = this._parseRestrictions(xhr.responseText);
			if (!restrictions) {
				GUIp.common.error("cannot parse LEM's restrictions: '" + xhr.responseText + "'");
				return;
			}

			this.restrictions = restrictions;
			this._setOption('FirstRequest', restrictions.firstRequest);
			this._setOption('TimeFrame', restrictions.timeFrame);
			this._setOption('RequestLimit', restrictions.requestLimit);
			this._setOption('Date', Date.now());

			for (var i = 0, len = this.onupdate.length; i < len; i++) {
				this.onupdate[i](restrictions);
			}
		}.bind(this), function(xhr) {
			GUIp.common.error("cannot fetch LEM's restrictions (" + xhr.status + '):', xhr.responseText);
		});
	}
};

/**
 * @class
 */
ui_log.LemUploaderUI = function() {
	if (!ui_log.isSailingLog()) {
		throw new Error('not implemented');
	}

	this._status = ui_log.lemUploader.getStatus();
	this._timer = 0;
	this._frequentTimer = false;
	this._startTimer();
	ui_log.lemUploader.onupdate.push(this.update.bind(this));

	this._button = document.createElement('button');
	GUIp.common.addListener(this._button, 'click', this._onSubmit.bind(this));
	this._updateButton();

	this.container = this._button;
};

ui_log.LemUploaderUI.prototype = {
	constructor: ui_log.LemUploaderUI,

	_getEnabledAnnotation: function(tries) {
		return (ui_log.isSailingLog() ? worker.GUIp_i18n.tries_left_s : worker.GUIp_i18n.tries_left) + tries;
	},

	_getDisabledAnnotation: function(cooldown) {
		var msg = ui_log.isSailingLog() ? worker.GUIp_i18n.till_next_try_s : worker.GUIp_i18n.till_next_try;
		return msg + Math.floor(cooldown / 60) + ':' + ('0' + cooldown % 60).slice(-2);
	},

	_formatButtonText: function() {
		var annotation =
			this._status.tries
			? this._getEnabledAnnotation(this._status.tries)
			: this._getDisabledAnnotation(Math.ceil((this._status.disabledTill - Date.now()) * 1e-3));
		annotation = ui_log.isSailingLog() ? ' (' + annotation + ')' : '<br />' + annotation;
		return worker.GUIp_i18n.send_log_to_LEMs_script + annotation;
	},

	_updateButton: function() {
		this._button.disabled = !this._status.tries;
		this._button.innerHTML = this._formatButtonText();
	},

	_requestStatus: function() {
		this._status = ui_log.lemUploader.getStatus();
	},

	_startTimer: function() {
		this._frequentTimer = !this._status.tries;
		this._timer = GUIp.common.setInterval(function(self) {
			if (self._status.tries || Date.now() >= self._status.disabledTill) {
				self._requestStatus();
				self._updateTimer();
			}
			self._updateButton();
		}, this._frequentTimer ? 1e3 : 10e3, this);
	},

	_updateTimer: function() {
		if (this._frequentTimer !== !this._status.tries) {
			worker.clearInterval(this._timer);
			this._startTimer();
		}
	},

	_onSubmit: function() {
		this._requestStatus();
		if (this._status.tries) {
			ui_log.lemUploader.send(ui_log.lemUploader.collect());
			this._requestStatus();
		}
		this._updateTimer();
		this._updateButton();
	},

	update: function() {
		this._requestStatus();
		this._updateTimer();
		this._updateButton();
	}
};

/**
 * @returns {!Node}
 */
ui_log.createLEMUploaderUI = function() {
	// TODO: check for firstRequest only when searching for a dungeon map in the database
	if (ui_log.steps < ui_log.lemUploader.restrictions.firstRequest && !ui_log.isStream) {
		return document.createDocumentFragment();
	}
	return new this.LemUploaderUI().container;
};

ui_log.getLEMRestrictions = function() {
	var postfix = this.isSailingLog() ? 'S' : '';
	this.firstRequest = ui_log.storage.get('LEMRestrictions:FirstRequest' + postfix) || (postfix ? 5 : 12);
	this.timeFrameSeconds = (ui_log.storage.get('LEMRestrictions:TimeFrame' + postfix) || (postfix ? 20 : 20))*60;
	this.requestLimit = ui_log.storage.get('LEMRestrictions:RequestLimit' + postfix) || (postfix ? 8 : 5);
	if (isNaN(this.storage.get('LEMRestrictions:Date' + postfix)) || Date.now() - this.storage.get('LEMRestrictions:Date' + postfix) > 24*60*60*1000) {
		GUIp.common.getXHR('https://www.godalert.info/' + (postfix ? 'Sail' : 'Dungeons') + '/guip.cgi', this.parseLEMRestrictions.bind(this, postfix));
	}
};

ui_log.parseLEMRestrictions = function(postfix, xhr) {
	var restrictions = JSON.parse(xhr.responseText);

	this.storage.set('LEMRestrictions:Date' + postfix, Date.now());
	this.storage.set('LEMRestrictions:FirstRequest' + postfix, restrictions.first_request);
	this.storage.set('LEMRestrictions:TimeFrame' + postfix, restrictions.time_frame);
	this.storage.set('LEMRestrictions:RequestLimit' + postfix, restrictions.request_limit);

	this.firstRequest = restrictions.first_request;
	this.timeFrameSeconds = restrictions.time_frame * 60;
	this.requestLimit = restrictions.request_limit;
};

ui_log.updateLogLimits = function() {
	for (var i = ui_log.requestLimit; i > 1; i--) {
		ui_log.storage.set('sentToLEM' + i, ui_log.storage.get('sentToLEM' + (i - 1), true), true);
	}
	ui_log.storage.set('sentToLEM1', Date.now(), true);
};

ui_log.checkLogLimits = function() {
	var i, stored = ui_log.storage.get('sentToLEM' + ui_log.requestLimit, true);
	if (!isNaN(stored) && Date.now() - stored < ui_log.timeFrameSeconds*1000) {
		var time = ui_log.timeFrameSeconds - (Date.now() - stored)/1000,
			minutes = Math.floor(time/60),
			seconds = Math.floor(time%60);
		seconds = seconds < 10 ? '0' + seconds : seconds;
		return {allowed: false, minutes: minutes, seconds: seconds};
	} else {
		var tries = 0;
		for (i = 0; i < ui_log.requestLimit; i++) {
			stored = ui_log.storage.get('sentToLEM' + i, true);
			if (isNaN(stored) || Date.now() - stored > ui_log.timeFrameSeconds*1000) {
				tries++;
			}
		}
		return {allowed: true, tries: tries};
	}
};

ui_log.restoreDungeonMap = function(map,streaming) {
		var map_elem = '<div id="hero2"><div class="box"><div class="block"><div class="block_h">' + worker.GUIp_i18n.map + (streaming ? ' <span title="' + worker.GUIp_i18n.map_sm + '">(SM)</span>' : '') + '</div><div id="dmap" class="new_line em_font">';
		for (var i = 0, ilen = map.length; i < ilen; i++) {
			map_elem += '<div class="dml" style="width:' + (map[0].length * 21) + 'px;">';
			for (var j = 0, jlen = map[0].length; j < jlen; j++) {
				map_elem += '<div class="dmc' + (map[i][j] === '#' ? ' dmw' : '') + '" style="left:' + (j * 21) + 'px">' + map[i][j] + '</div>';
			}
			map_elem += '</div>';
		}
		map_elem += '</div></div></div></div>';
		document.getElementById('right_block').insertAdjacentHTML('beforeend', map_elem);
};

ui_log.saverSendLog = function() {
	var i, div = document.createElement('div'), inputs = '<input type="hidden" name="bosses_count" value="' + ui_log.saverBossesCnt + '"><input type="hidden" name="log_id" value="' + (ui_log.saverURL.includes('gdvl.tk/') && ui_log.logID.length >= 8 ? ui_log.logID.slice(0,5) : ui_log.logID) + '">';
	for (i = 0; i < ui_log.saverPages.length; i++) {
		inputs += '<input type="hidden" name="' + i + '">';
	}
	if (ui_log.islandsMap.manager && ui_log.islandsMap.manager.rawModel && ui_log.islandsMap.manager.rawModel.pois.length) {
		inputs += '<input type="hidden" name="poi" value="' + ui_log.islandsMap.manager.rawModel.pois + '">';
	}
	inputs += '<input type="hidden" name="tzo" value="' + (new Date()).getTimezoneOffset() + '">';
	div.insertAdjacentHTML('beforeend', '<form method="post" action="' + ui_log.saverURL + '" enctype="multipart/form-data" accept-charset="utf-8">' + inputs + '</form>');
	for (i = 0; i < ui_log.saverPages.length; i++) {
		div.querySelector('input[name="' + i + '"]').setAttribute('value', ui_log.saverPages[i]);
	}
	document.body.appendChild(div);
	div.firstChild.submit();
	document.body.removeChild(div);
};

ui_log.saverFetchPage = function(boss_no) {
	GUIp.common.getDomainXHR(location.pathname + (boss_no ? '?boss=' + boss_no : ''), ui_log.saverProcessPage.bind(this, boss_no), ui_log.saverFetchFailed);
};

ui_log.saverProcessPage = function(boss_no, xhr) {
	if (!xhr.responseText.match(new worker.RegExp(worker.GUIp_i18n.saver_missing_log_c)) && xhr.responseText.includes('class="lastduelpl"')) {
		var page = xhr.responseText.replace(/<img[^>]+>/g, '')
								 .replace(/\.js\?\d+/g, '.js')
								 .replace(/\.css\?\d+/g, '.css')
								 .replace(/не менее 30 дней/, 'по мере возможности')
								 .replace(/for at least 30 days after the fight is over/, 'as far as possible')
		page = page.replace(/(<script[^>]*?>[^]*?<\/script>)/g, function(script) { return script.match(/Tracker|analytics/) ? '' : script.replace(/\.js\?\d+/g, '.js'); });
		// workaround inability of gdvl.tk to save 9-char logids
		if (ui_log.saverURL.includes('gdvl.tk/') && ui_log.logID.length >= 8) {
			page = page.replace(new worker.RegExp('/+duels/+log/+' + ui_log.logID,'g'),'/duels/log/' + ui_log.logID.slice(0,5));
		}
		if (!page.includes('text/html; charset=')) {
			page = page.replace('<head>', '<head>\n<meta http-equiv="content-type" content="text/html; charset=UTF-8">')
		}
		ui_log.saverPages.push(page);
		if (boss_no < ui_log.saverBossesCnt) {
			GUIp.common.setTimeout(function() { ui_log.saverFetchPage(boss_no + 1); }, 2e3);
		} else {
			ui_log.saverSendLog();
		}
	} else {
		ui_log.saverRemoveLoader();
		worker.alert(worker.GUIp_i18n.saver_error + ' (#1)');
	}
};

ui_log.saverFetchFailed = function() {
	ui_log.saverRemoveLoader();
	worker.alert(worker.GUIp_i18n.saver_error + ' (#2)');
};

ui_log.saverAddLoader = function() {
	document.body.insertAdjacentHTML('beforeend', '<div id="erinome_chronicle_loader" style="position: fixed; left: 50%; top: 50%; margin: -24px; padding: 8px; background: rgba(255,255,255,0.9);"><img src="' + (worker.GUIp_browser !== 'Opera' ? GUIp_getResource('images/loader.gif') : '//gv.erinome.net/images/loader.gif') + '"></div>');
};

ui_log.saverRemoveLoader = function() {
	if (document.getElementById('erinome_chronicle_loader')) {
		document.body.removeChild(document.getElementById('erinome_chronicle_loader'));
	}
};

ui_log.saverPrepareLog = function(foreign) {
	if (worker.GUIp_locale === 'ru') {
		ui_log.saverURL = '//gv.erinome.net/processlog';
	} else {
		ui_log.saverURL = '//gvg.erinome.net/processlog';
	}
	if (foreign) {
		ui_log.saverURL = '//gdvl.tk/upload.php';
	}
	try {
		ui_log.saverPages = [];
		if (!ui_log.logID) {
			throw worker.GUIp_i18n.saver_invalid_log;
		}
		if (document.getElementById('search_status') && document.getElementById('search_status').textContent.match(new worker.RegExp(worker.GUIp_i18n.saver_missing_log_c))) {
			throw worker.GUIp_i18n.saver_missing_log;
		}
		if (ui_log.isBroadcast) {
			throw worker.GUIp_i18n.saver_broadcast_log;
		}
		if (document.getElementById('erinome_chronicle_loader')) {
			worker.alert(worker.GUIp_i18n.saver_already_working);
			return;
		} else {
			ui_log.saverAddLoader();
		}
		ui_log.saverBossesCnt = document.querySelectorAll('a[href*="boss"]').length;
		ui_log.saverFetchPage(null);
	} catch (e) {
		ui_log.saverRemoveLoader();
		worker.alert(worker.GUIp_i18n.error_message_subtitle + ' ' + e);
	}
};

ui_log.onscroll = function() {
	var $box = document.querySelector('#hero2 .box'),
		isFixed = $box.style.position === 'fixed',
		resSM = (ui_log.lfExp || !ui_log.customDomain || !ui_log.isOldDungeonLog()) ? 0 : 100;
	if (worker.scrollY > $box.offsetTop && ($box.offsetHeight + resSM) < worker.innerHeight && !isFixed) {
		$box.style.position = 'fixed';
		$box.style.top = 0;
	} else if ((worker.scrollY <= $box.offsetTop || ($box.offsetHeight + resSM) >= worker.innerHeight) && isFixed) {
		$box.style.position = 'static';
	}
};

ui_log.scrollTo = function(target) {
	if (!target) {
		return;
	}
	if (typeof target.scrollIntoView === 'function') {
		// older versions of firefox throw errors with {block: 'center'}, newer versions implement it but use 'start' as default value
		// although in MDN there's clearly stated that center should be the default
		try {
			target.scrollIntoView({behavior: 'smooth', block: 'center'});
		} catch (e) {
			target.scrollIntoView({behavior: 'smooth'});
		}
	}
};

ui_log.starter = function() {
	// init mobile cookies
	GUIp.common.forceDesktopPage();

	// detect godname
	this.godname = GUIp.common.getCurrentGodname(this.customDomain);

	// check whether log is finished
	this.isBroadcast = document.getElementsByClassName('lastduelpl')[1] && (new worker.RegExp(worker.GUIp_i18n.saver_broadcast_log_c)).test(document.getElementsByClassName('lastduelpl')[1].textContent);

	// try to load streamed dungeon map
	if (this.logID.length === 9 && this.parseQueryString(worker.location.href)['estreaming'] === '1' && this.isBroadcast && !this.streamRequested) {
		this.streamRequested = true; // to use ONLY once per page load
		var steps = (((document.querySelector('#last_items_arena .block_h') || {}).textContent || '').match(/(?:шаг|step) (?:\d+ \/ )?(\d+)\)/) || [])[1] || 0;
		if (steps > 0 && !document.getElementById('dmap') && !document.getElementById('opps')) {
			GUIp.common.getXHR(GUIp.common.erinome_url + '/mapStreamer/port?id=' + this.logID + '&step=' + steps + '&lang=' + worker.GUIp_locale, function(xhr) {
				try {
					var data = JSON.parse(xhr.responseText);
					if (data && data.map) {
						ui_log.restoreDungeonMap(data.map,true);
					}
				} catch (e) {}
				GUIp.common.setTimeout(ui_log.starter.bind(ui_log), 100);
			}, function(xhr) {
				GUIp.common.setTimeout(ui_log.starter.bind(ui_log), 100);
			});
			return;
		}
	}

	// check for missing chronicle
	if (document.getElementById('search_status') && document.getElementById('search_status').textContent.match(new worker.RegExp(worker.GUIp_i18n.saver_missing_log_c))) {
		if (!ui_log.customDomain) {
			if (worker.GUIp_locale === 'ru') {
				document.getElementById('search_status').insertAdjacentHTML('beforeend','<br/><br/>Попробуйте поискать хронику:<br/><a href="//gdvl.tk/duels/log/' + ui_log.logID + '?from=search">gdvl.tk</a> или <a href="//gv.erinome.net/duels/log/' + ui_log.logID + '?from=search">gv.erinome.net</a>');
			} else {
				document.getElementById('search_status').insertAdjacentHTML('beforeend','<br/><br/>Try searching your chronicle in:<br/><a href="//gvg.erinome.net/duels/log/' + ui_log.logID + '?from=search">gvg.erinome.net</a>');
			}
		}
		return;
	}

	var fight_log_capt = document.querySelector('#fight_log_capt, #last_items_arena .block_h, #fight_chronicle .block_h, #m_fight_log .block_h, .step_capt');
	if (!fight_log_capt) {
		return;
	}

	// clean old entries
	this.storage.clean();

	// mark page as chronicle for use in custom CSS
	document.body.classList.add('chronicle');

	if (worker.GUIp_browser === 'Opera') {
		document.body.classList.add('e_opera');
	}

	// add some styles
	GUIp.common.addCSSFromString(this.storage.get('UserCss'));
	var background = this.storage.get('Option:useBackground');
	if (background) {
		document.querySelector('.lastduelpl').style.cssText = 'margin-top: 0; padding-top: 10px;';
		GUIp.common.setPageBackground(background);
	}

	// add save links (but NOT on mobile devices since now godville generates DIFFERENT code for them)
	if (!ui_log.customDomain && !this.isBroadcast) {
		var savelnk, savediv = document.createElement('div');
		if (!document.querySelector('.rpl.mb')) {
			savediv.insertBefore(document.createTextNode(worker.GUIp_i18n.save_log_to + ' '), null);
			savelnk = document.createElement('a');
			if (worker.GUIp_locale === 'ru') {
				if (!ui_log.isSailingLog()) {
					GUIp.common.addListener(savelnk, 'click', ui_log.saverPrepareLog.bind(ui_log, true));
					savelnk.textContent = 'gdvl.tk';
					savediv.insertBefore(savelnk, null);
					savediv.insertBefore(document.createTextNode(worker.GUIp_i18n.or), null);
				}
				savelnk = document.createElement('a');
				GUIp.common.addListener(savelnk, 'click', ui_log.saverPrepareLog.bind(ui_log, false));
				savelnk.textContent = 'gv.erinome.net';
			} else {
				GUIp.common.addListener(savelnk, 'click', ui_log.saverPrepareLog.bind(ui_log, false));
				savelnk.textContent = 'gvg.erinome.net';
			}
			savediv.insertBefore(savelnk, null);
		} else {
			savediv.textContent = worker.GUIp_i18n.save_log_fullpage;
		}
		var ldplf = document.getElementsByClassName('lastduelpl_f');
		ldplf[ldplf.length - 1].insertBefore(savediv,null);
	}

	// add usercss for custom domains
	if (ui_log.customDomain) {
		var uclink = document.querySelector('.lastduelpl_f a[href="#"]');
		if (uclink) {
			uclink.insertAdjacentHTML('afterend','<span>, <a id="user_css_edit" href="#">CSS ►</a><div id="user_css_form"><textarea></textarea></div></span>');
			GUIp.common.addListener(document.getElementById('user_css_edit'), 'click', function(ev) {
				ev.preventDefault();
				var ucform = document.getElementById('user_css_form');
				if (ucform.style.display === 'block') {
					this.textContent = 'CSS ►';
					ui_log.storage.set('UserCss',document.querySelector('#user_css_form textarea').value);
					GUIp.common.addCSSFromString(ui_log.storage.get('UserCss'));
					ucform.style.display = 'none';
				} else {
					this.textContent = 'CSS ▼';
					document.querySelector('#user_css_form textarea').value = ui_log.storage.get('UserCss');
					ucform.style.display = 'block';
				}
			});
		}
	// ally blacklisting
	} else {
		var abc, abb = '<span id="e_ally_blacklist_setup" class="em_font e_t_icon e_icon" title="' + worker.GUIp_i18n['lb_ally_blacklist_title'] + '">⚙</span>';
		if (abc = document.querySelector('#hero1_info .block_h, #fight_chronicle .block_h')) {
			abc.insertAdjacentHTML('beforeend',abb);
			abb = document.getElementById('e_ally_blacklist_setup');
			GUIp.common.addListener(abb, 'click', function() {
				GUIp.common.createLightbox('ally_blacklist',ui_log.storage,GUIp_words(),null);
				document.getElementById('optlightbox').classList.add('e_box');
			});
		}
	}

	if (document.getElementsByClassName('rpl')[0]) {
		// make replay's keydown hook less aggressive
		GUIp.common.addListener(worker, 'keydown', function(ev) {
			var target = ev.target, key = ev.keyCode;
			if ((target.type === 'text' || target.tagName === 'TEXTAREA') &&
				(key === 13 || key === 37 || key === 39) && !ev.ctrlKey && !ev.altKey && !ev.shiftKey) {
				ev.stopPropagation();
			}
		}, true);
	}

	if (location.search.length > 1 && location.search !== '?sort=desc') {
		var a = document.querySelector('.block_h > a');
		if (a) {
			if (location.search.includes('sort=desc')) {
				a.href = location.pathname + '?' + location.search.slice(1).split('&').filter(function(b) { return b !== 'sort=desc'; }).join('&');
			} else {
				a.href = location.href + '&sort=desc';
			}
		}
	}

	// add step numbers to chronicle log
	this.enumerateSteps();

	// save results for last fights list
	if (!this.customDomain && !this.isBroadcast) {
		try {
			if (/Тренировочный бой|Sparring Fight/.test(fight_log_capt.textContent)) {
				this.saveSparResults();
			} else if (/Хроника подземелья|Dungeon Journal/.test(fight_log_capt.textContent)) {
				this.saveDungeonResults();
			}
		} catch (e) {
			GUIp.common.error(e);
		}
	}

	if (location.href.includes('boss=') || (!/Хроника (подземелья|заплыва)|(Dungeon|Sail) Journal/.test(fight_log_capt.textContent) && !document.querySelector('#s_map'))) {
		return;
	}

	try {
		this.steps = +GUIp.common.matchRegex(/(?:шаг|step)\s*(?:\w*\s*\/\s*)?(\d+)/, fight_log_capt.textContent, 0);
		// update LEM restrictions
		this.getLEMRestrictions();
		// support other color themes
		GUIp.common.exposeThemeName(localStorage.ui_s || 'th_classic');
		// specific code for sailing
		if (this.isSailingLog()) {
			this.improveSailChronicles();
			this.lemUploader.init();
			this.islandsMap.prepare(function(mapContainer) {
				var controls = document.createElement('div');
				controls.className = 'e_sailing_controls';
				controls.appendChild(this.islandsMapCreateRuler());
				controls.appendChild(this.islandsMapCreateOptionBox());
				controls.appendChild(this.createLEMUploaderUI());
				mapContainer.parentNode.insertBefore(controls, mapContainer.nextSibling);
				this.islandsMap.initMap(mapContainer);
			}, this);
			return;
		}
		// add a pre-saved map for a translation-type chronicle if available
		if (this.isBroadcast && !document.getElementById('dmap') && this.steps === +this.storage.get('steps', true)) {
			this.restoreDungeonMap(this.storage.getJSON('map', true));
		}

		if (document.querySelector('#hero2 .box') && typeof window.$ !== 'function') {
			worker.onscroll = GUIp.common.try2.bind(ui_log, ui_log.onscroll);
			GUIp.common.setTimeout(worker.onscroll, 500);
		}
		// add some colors and other info to the map
		GUIp.common.setExtraDiscardData(Object.values(ui_log.getDungeonHeroNames()));
		GUIp.common.getDungeonPhrases(ui_log.initColorMap.bind(ui_log),ui_log.initColorMapNC.bind(ui_log));
		// check for missing exclamations
		var dmapSettings = ui_log.storage.getList('Option:dungeonMapSettings');
		if (dmapSettings.includes('excl') && ui_log.storage.get('excl',true)) {
			GUIp.common.dmapExclCache = ui_log.storage.getJSON('excl',true);
			GUIp.common.dmapExcl();
		}
		GUIp.common.dmapCoords();
		// send button and other stuff
		var $box = document.querySelector('#hero2 fieldset, #hero2 .block') || document.getElementById('right_block');
		// dungeon map tracer
		$box.insertAdjacentHTML('beforeend', '<div class="trace_div"><progress id="trace_progress" max="0" value="0" title="' + worker.GUIp_i18n.trace_map_progress_stopped + '"></progress><span class="trace_buttons"><button class="trace_button" id="trace_button_prev" title="' + worker.GUIp_i18n.trace_map_prev + '"><img src="' + worker.GUIp_getResource('images/trace_prev.png') + '"/></button><button class="trace_button" id="trace_button_stop" title="' + worker.GUIp_i18n.trace_map_stop + '"><img src="' + worker.GUIp_getResource('images/trace_stop.png') + '"/></button><button class="trace_button" id="trace_button_play" title="' + worker.GUIp_i18n.trace_map_start + '"><img src="' + worker.GUIp_getResource('images/trace_play.png') + '"/><img src="' + worker.GUIp_getResource('images/trace_pause.png') + '" style="display:none;"/></button><button class="trace_button" id="trace_button_next" title="' + worker.GUIp_i18n.trace_map_next + '"><img src="' + worker.GUIp_getResource('images/trace_next.png') + '"/></button></span></div>');
		if (worker.GUIp_browser === 'Opera') {
			worker.GUIp_getResource('images/trace_prev.png',document.querySelector('#trace_button_prev img'),true);
			worker.GUIp_getResource('images/trace_pause.png',document.querySelector('#trace_button_pause img'),true);
			worker.GUIp_getResource('images/trace_stop.png',document.querySelector('#trace_button_stop img'),true);
			worker.GUIp_getResource('images/trace_play.png',document.querySelectorAll('#trace_button_play img')[0],true);
			worker.GUIp_getResource('images/trace_pause.png',document.querySelectorAll('#trace_button_play img')[1],true);
			worker.GUIp_getResource('images/trace_next.png',document.querySelector('#trace_button_next img'),true);
		}
		GUIp.common.addListener(document.getElementById('trace_button_stop'), 'click', ui_log.traceMapStop.bind(ui_log));
		GUIp.common.addListener(document.getElementById('trace_button_play'), 'click', function() {
			if (!ui_log.traceCoords) {
				ui_log.traceStep = 0;
				ui_log.traceDir = 1;
				ui_log.traceCoords = GUIp.common.calculateExitXY();
				document.getElementById('trace_progress').max = worker.Object.keys(ui_log.chronicles).length;
			}
			if (ui_log.traceInt) {
				ui_log.traceMapPause();
			} else {
				ui_log.traceMapStart();
			}
		});
		GUIp.common.addListener(document.getElementById('trace_button_prev'), 'click', function() {
			if (ui_log.traceInt) {
				ui_log.traceMapPause();
			}
			if (!ui_log.traceCoords || ui_log.traceStep <= 1) {
				return;
			}
			ui_log.traceMapProcess(-1);
		});
		GUIp.common.addListener(document.getElementById('trace_button_next'), 'click', function() {
			if (ui_log.traceInt) {
				ui_log.traceMapPause();
			}
			if (!ui_log.traceCoords) {
				ui_log.traceStep = 0;
				ui_log.traceDir = 1;
				ui_log.traceCoords = GUIp.common.calculateExitXY();
				document.getElementById('trace_progress').max = worker.Object.keys(ui_log.chronicles).length;
			}
			if (ui_log.traceStep === worker.Object.keys(ui_log.chronicles).length) {
				return;
			}
			ui_log.traceMapProcess(1);
		});
		var traceProgTimer = null, traceProgCancel = false;
		GUIp.common.addListener(document.getElementById('trace_progress'), 'click', function(e) {
			if (traceProgCancel) {
				traceProgCancel = false;
				return;
			}
			this.max = worker.Object.keys(ui_log.chronicles).length;
			if (this.max === 0) {
				return;
			}
			ui_log.traceMapProgressClick(parseInt(e.offsetX * this.max / this.offsetWidth),this.max);
			ui_log.scrollTo(document.querySelector('div.dtrace'));
		});
		GUIp.common.addListener(document.getElementById('trace_progress'), 'mouseup', function(e) {
			if (traceProgTimer) {
				worker.clearTimeout(traceProgTimer);
			}
		});
		GUIp.common.addListener(document.getElementById('trace_progress'), 'mousedown', function(e) {
			var max = this.max = worker.Object.keys(ui_log.chronicles).length;
			if (this.max === 0) {
				return;
			}
			traceProgCancel = false;
			traceProgTimer = GUIp.common.setTimeout(function() {
				var step = parseInt(worker.prompt(worker.GUIp_i18n.trace_map_progress_prompt));
				if (step > 0 && step <= max) {
					ui_log.traceMapProgressClick(step - 1,max);
					ui_log.scrollTo(document.querySelector('div.dtrace'));
				}
				traceProgCancel = true;
			},7e2);
		});
		GUIp.common.addListener(document.getElementById('last_items_arena'), 'click', function(ev) {
			var target = ev.target;
			while (!target.classList.contains('d_capt')) {
				if (target === this) return;
				target = target.parentNode;
			}
			var turn = target.getElementsByClassName('d_turn')[0],
				m = (turn && /\d+/.exec(turn.textContent)) || /\d+/.exec(target.title);
			if (!m) return;
			ev.stopPropagation();
			var maxSteps = Object.keys(ui_log.chronicles).length;
			document.getElementById('trace_progress').max = maxSteps;
			ui_log.traceMapProgressClick(+m[0] - 1, maxSteps);
		}, true);
		// LEM's uploader
		if (location.href.includes('sort')) {
			$box.insertAdjacentHTML('beforeend', '<span id="send_to_LEM_form">' + worker.GUIp_i18n.wrong_entries_order + '</span>');
			return;
		}
		if (this.steps < this.firstRequest) {
			$box.insertAdjacentHTML('beforeend', '<span id="send_to_LEM_form">' + worker.GUIp_i18n.fmt('the_button_will_appear_after', this.firstRequest) + '</span>');
			return;
		}
		$box.insertAdjacentHTML('beforeend',
			'<form target="_blank" method="post" enctype="multipart/form-data" action="//www.godalert.info/Dungeons/index' + (worker.GUIp_locale === 'en' ? '-eng' : '') + '.cgi" id="send_to_LEM_form" style="padding-top: calc(0.5em + 3px);">' +
				'<input type="hidden" id="fight_text" name="fight_text">' +
				'<input type="hidden" name="map_type" value="map_graphic">' +
				'<input type="hidden" name="min" value="X">' +
				'<input type="hidden" name="partial" value="X">' +
				'<input type="hidden" name="room_x" value="">' +
				'<input type="hidden" name="room_y" value="">' +
				'<input type="hidden" name="Submit" value="' + worker.GUIp_i18n.get_your_map + '">' +
				'<input type="hidden" name="guip" value="1">' +
				'<div' + (ui_log.customDomain && ui_log.isOldDungeonLog() ? '' : ' style="display: none;"') + '><input type="checkbox" id="match" name="match" value="1"><label for="match">' + worker.GUIp_i18n.search_database + '</label></div>' +
				'<div id="search_mode" style="display: none;">' +
					'<input type="checkbox" id="match_partial" name="match_partial" value="1"><label for="match_partial">' + worker.GUIp_i18n.relaxed_search + '</label>' +
					'<div><input type="radio" id="exact" name="search_mode" value="exact"><label for="exact">' + worker.GUIp_i18n.exact + '</label></div>' +
					'<div><input type="radio" id="high" name="search_mode" value="high"><label for="high">' + worker.GUIp_i18n.high_precision + '</label></div>' +
					'<div><input type="radio" id="medium" name="search_mode" value="medium" checked=""><label for="medium">' + worker.GUIp_i18n.normal + '</label></div>' +
					'<div><input type="radio" id="low" name="search_mode" value="low"><label for="low">' + worker.GUIp_i18n.primary + '</label></div>' +
				'</div>' +
				'<table style="box-shadow: none; width: 100%;"><tr>' +
					'<td style="border: none; padding: 0;"><label for="stoneeater">' + worker.GUIp_i18n.corrections + '</label></td>' +
					'<td style="border: none; padding: 0 1.5px 0 0; width: 100%;"><input type="text" id="stoneeater" name="stoneeater" value="' + this.stoneEaterCompat(ui_log.storage.get('corrections', true) || ui_log.directionlessMoveCombo) + '" style=" width: 100%; padding: 0;"></td>' +
				'</tr><tr>' +
					'<td style="border: none; padding: 0;"><label for="teleports">' + worker.GUIp_i18n.wormholes + '</label></td>' +
					'<td style="border: none; padding: 0 1.5px 0 0; width: 100%;"><input type="text" id="teleports" name="teleports" value="' + this.stoneEaterCompat2(ui_log.storage.getJSON('wormholes', true) || ui_log.wormholeMoveCombo) + '" style=" width: 100%; padding: 0;"></td>' +
				'</tr></table>' +
				'<input type="checkbox" id="high_contrast" name="high_contrast" value="1"><label for="high_contrast">' + worker.GUIp_i18n.high_contrast + '</label>' +
				'<button id="send_to_LEM" style="font-size: 15px; height: 100px; width: 100%;">' +
			'</form>');
		document.querySelector('#fight_text').value = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" >\n<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' +
															document.getElementsByTagName('html')[0].innerHTML.replace(/<(?:script|style)[^]+?<\/(?:script|style)>/g, '')
																											  .replace(/background-image: url\(&quot;data:image[^)]+\);/g, '')
																											  .replace(/onclick="[^"]+?"/g, '')
																											  .replace(/"javascript[^"]+"/g, '""')
																											  .replace(/<form[^]+?<\/form>/g, '')
																											  .replace(/<iframe[^]+?<\/iframe>/g, '')
																											  .replace(/\t/g, '')
																											  .replace(/<div[^>]+class="dmc[^>]+>/g,'<div class="dmc">')
																											  .replace(/ {2,}/g, ' ')
																											  .replace(/\n{2,}/g, '\n') +
													  '</html>';
		if (this.chronicleGeneratedMap) {
			document.querySelector('#fight_text').value = document.querySelector('#fight_text').value.replace(/<div id="hero2"[^]+?<\/div><\/div><\/div><\/div>/g, '');
		}
		var match = document.getElementById('match'),
			search_mode = document.getElementById('search_mode'),
			high_contrast = document.getElementById('high_contrast');
		this.button = document.getElementById('send_to_LEM');
		GUIp.common.addListener(this.button, 'click', function(e) {
			e.preventDefault();
			ui_log.updateLogLimits();
			ui_log.updateButton();
			this.form.submit();
			document.getElementById('match').checked = false;
			document.getElementById('match_partial').checked = false;
			document.getElementById('medium').click();
			document.getElementById('search_mode').style.display = "none";
		});
		ui_log.updateButton();
		GUIp.common.setInterval(function() {
			ui_log.updateButton();
		}, 10e3);
		GUIp.common.addListener(match, 'change', function() {
			search_mode.style.display = search_mode.style.display === 'none' ? 'block' : 'none';
			ui_log.lfExp = search_mode.style.display === 'block';
			if (worker.onscroll) { worker.onscroll(); }
		});
		high_contrast.checked = localStorage.getItem('eGUI_highContrast') === 'true';
		GUIp.common.addListener(high_contrast, 'change', function() {
			localStorage.setItem('eGUI_highContrast', document.getElementById('high_contrast').checked);
		});
	} catch (e) {
		GUIp.common.error(e);
	}
};

//! include 'mixins/module.js';

registerModule('log', ui_log.starter, ui_log);

})(this);
