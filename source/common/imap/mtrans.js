/**
 * Model transformers.
 *
 * @alias GUIp.common.islandsMap.mtrans
 * @namespace
 */
ui_imap.mtrans = {};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 */
ui_imap.mtrans.guessPort = function(model) {
	model.port = model.tiles[0x0] || null;
};

ui_imap.mtrans._processTile = function(model, tile) {
	var dist = 0;
	switch (tile.code) {
		// ordered by popularity
		case 73: /*I*/ model.visited.push(tile); break;
		case 35: case 36: /*#$*/
			dist = ui_imap.vec.len(tile.pos);
			if (!model.borderRadius || dist < model.borderRadius) {
				model.borderRadius = dist;
			}
			break;
		case 64: /*@*/ model.whirlpools.push(tile); break;
		case 77: case 102: case 70: case 71: case 103: /*MfFGg*/ model.treasures.push(tile); break;
		case 116: /*t*/ model.thermos.push(new ui_imap.Thermo(tile, 0, 3)); break;
		case 121: /*y*/ model.thermos.push(new ui_imap.Thermo(tile, 3, 5)); break;
		case 117: /*u*/ model.thermos.push(new ui_imap.Thermo(tile, 5, 7)); break;
		case 111: /*o*/ model.thermos.push(new ui_imap.Thermo(tile, 7, 9)); break;
		case 91:  /*[*/ model.thermos.push(new ui_imap.Thermo(tile, 9, 11)); break;
		case 93:  /*]*/ model.thermos.push(new ui_imap.Thermo(tile, 11)); break;
		// qwe
		// a d
		// zxc
		case 113: /*q*/ model.arrows.push({tile: tile, dir: 0xFF00}); break;
		case 97:  /*a*/ model.arrows.push({tile: tile, dir: 0x00FF}); break;
		case 122: /*z*/ model.arrows.push({tile: tile, dir: 0x01FF}); break;
		case 101: /*e*/ model.arrows.push({tile: tile, dir: 0xFF01}); break;
		case 100: /*d*/ model.arrows.push({tile: tile, dir: 0x0001}); break;
		case 99:  /*c*/ model.arrows.push({tile: tile, dir: 0x0100}); break;
		// case 119: /*w*/ model.arrows.push({tile: tile, dir: 0xFE01}); break;
		// case 120: /*x*/ model.arrows.push({tile: tile, dir: 0x02FF}); break;
		case 112: /*p*/ model.port = tile; break;
	}
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!GUIp.common.islandsMap.Tile} tile
 */
ui_imap.mtrans.addTile = function(model, tile) {
	var pos = tile.pos;
	model.tiles[pos] = tile;
	// update map radius
	var dist = ui_imap.vec.len(pos);
	if (dist > model.radius) {
		model.radius = dist; // Math.max is slower
	}
	// update nonBorderRadius
	if (!tile.isFogged && !tile.isBorder && dist > model.nonBorderRadius) {
		model.nonBorderRadius = dist;
	}
	// add the tile to a group of points of interest if it is one of those
	var groupIndex = (model.poiGroupIndexAt[pos] + 1) | 0; // for type stability
	if (groupIndex) {
		for (var i = model.poiGroups.length; i < groupIndex; i++) {
			model.poiGroups[i] = [];
		}
		model.poiGroups[groupIndex - 1].push(tile);
	}
	// do various processing depending on its type
	this._processTile(model, tile);
};

/**
 * Migrate some special tiles.
 *
 * @private
 * @param {!Array<!GUIp.common.islandsMap.Tile>} oldArr
 * @param {!Array<!GUIp.common.islandsMap.Tile>} newArr
 * @param {!Object<GUIp.common.islandsMap.Vec, !GUIp.common.islandsMap.Tile>} newTiles
 */
ui_imap.mtrans._migrateTiles = function(oldArr, newArr, newTiles) {
	var tile, newPositions = GUIp.common.makeHashSet(newArr.map(function(tile) { return tile.pos; }));
	for (var i = 0, len = oldArr.length; i < len; i++) {
		var pos = oldArr[i].pos;
		if (!(pos in newPositions) && (tile = newTiles[pos])) {
			newArr.push(tile);
		}
	}
};

/**
 * Migrate objects shadowed by arks.
 *
 * @private
 * @param {!Object<GUIp.common.islandsMap.Vec, !GUIp.common.islandsMap.Tile>} oldTiles
 * @param {!GUIp.common.islandsMap.Model} newModel
 */
ui_imap.mtrans._migrateShadowed = function(oldTiles, newModel) {
	var tile;
	for (var i = 0, len = newModel.arks.length; i < len; i++) {
		var ark = newModel.arks[i];
		if (ark && ark.code === 32 /* */ && (tile = oldTiles[ark.pos]) && !tile.isFogged && tile.code !== 66 && tile.code !== 98 /*Bb*/) {
			ark.code = tile.code;
			this._processTile(newModel, ark);
		}
	}
};

/**
 * @private
 * @param {!Array<{tile: !GUIp.common.islandsMap.Tile}>} oldData
 * @param {!Array<{tile: !GUIp.common.islandsMap.Tile}>} newData
 * @param {!Object<GUIp.common.islandsMap.Vec, !GUIp.common.islandsMap.Tile>} newTiles
 */
ui_imap.mtrans._migrateObjs = function(oldData, newData, newTiles) {
	var tile, newTable = GUIp.common.makeHashSet(newData.map(function(obj) { return obj.tile.pos; }));
	for (var i = 0, len = oldData.length; i < len; i++) {
		var oldObj = oldData[i],
			pos = oldObj.tile.pos;
		if (!(pos in newTable) && (tile = newTiles[pos])) {
			var newObj = Object.assign({}, oldObj);
			newObj.tile = tile;
			newData.push(newObj);
		}
	}
};

/**
 * @private
 * @param {!GUIp.common.islandsMap.Model} from
 * @param {!GUIp.common.islandsMap.Model} to
 */
ui_imap.mtrans._migratePOIs = function(from, to) {
	var poiGroups = [], i, j, len, jlen, group, tile, target;
	for (i = 0, len = from.poiGroups.length; i < len; i++) {
		group = from.poiGroups[i];
		var newGroup = [];
		for (j = 0, jlen = group.length; j < jlen; j++) {
			tile = to.tiles[group[j].pos];
			if (tile) {
				to.poiGroupIndexAt[tile.pos] = i; // unmerge POIs of the same color but possibly different groups
				newGroup.push(tile);
			}
		}
		poiGroups[i] = newGroup;
	}
	// move newly created points of interest to new groups, even if they have an already known color
	for (i = 0, len = to.poiGroups.length; i < len; i++) {
		group = to.poiGroups[i];
		var found = false,
			targetIndex = poiGroups.length;
		for (j = 0, jlen = group.length; j < jlen; j++) {
			tile = group[j];
			if (tile.pos in from.poiGroupIndexAt) continue;
			// that point has just been created
			to.poiGroupIndexAt[tile.pos] = targetIndex;
			if (found) {
				target.push(tile);
			} else {
				poiGroups[targetIndex] = target = [tile];
				found = true;
			}
		}
	}
	to.poiGroups = poiGroups;
};

/**
 * @param {!GUIp.common.islandsMap.Model} from
 * @param {!GUIp.common.islandsMap.Model} to
 */
ui_imap.mtrans.migrate = function(from, to) {
	this._migrateTiles(from.visited, to.visited, to.tiles); // necessary for multipass seas
	this._migrateShadowed(from.tiles, to); // must be called prior to migrating treasures and arrows
	this._migrateTiles(from.treasures, to.treasures, to.tiles);
	this._migrateObjs(from.arrows, to.arrows, to.tiles);
	this._migratePOIs(from, to);
	// we do not attempt to migrate thermo hints.
	// the only way we can lose a hint is when new one is placed right onto the old one
	// (yeah, multipass seas can be surprising sometimes),
	// in which case we don't care about the old one anyway
	if (to.thermos.length) {
		// exclude thermo hints from visited so that they are not rendered semi-transparent
		var thPositions = GUIp.common.makeHashSet(to.thermos.map(function(th) { return th.tile.pos; }));
		GUIp.common.filterInPlace(to.visited, function(tile) { return !(tile.pos in thPositions); });
	}
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {number} radius
 * @param {boolean=} fill
 * @returns {!Array<GUIp.common.islandsMap.Vec>}
 */
ui_imap.mtrans.expand = function(model, radius, fill) {
	var i, j, len, to, pos, result = [];
	if (fill) {
		for (i = -radius; i <= radius; i++) {
			for (j = -radius - Math.min(i, 0), to = radius - Math.max(i, 0); j <= to; j++) {
				pos = ui_imap.vec.make(i, j);
				if (!(pos in model.tiles)) {
					// we bypass addTile here because it will not do anything else
					model.tiles[pos] = new ui_imap.Tile(pos, true, 63 /*?*/);
					result.push(pos);
				}
			}
		}
	} else {
		var vectors = ui_imap.vec.ofLen(radius);
		for (i = 0, len = vectors.length; i < len; i++) {
			pos = vectors[i];
			if (!(pos in model.tiles)) {
				model.tiles[pos] = new ui_imap.Tile(pos, true, 63 /*?*/);
				result.push(pos);
			}
		}
	}

	model.radius = Math.max(model.radius, radius);
	if (!model.port) {
		this.guessPort(model);
	}
	return result;
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!Array<GUIp.common.islandsMap.Vec>} added
 */
ui_imap.mtrans.unexpand = function(model, added) {
	for (var i = 0, len = added.length; i < len; i++) {
		delete model.tiles[added[i]];
	}
	// recalculate map radius. hoping this is not going to be called often, and O(n) is fine
	var positions = Object.keys(model.tiles), newRadius = 0;
	for (var i = 0, len = positions.length; i < len; i++) {
		var dist = ui_imap.vec.len(positions[i]);
		if (dist > newRadius) {
			newRadius = dist;
		}
	}
	model.radius = newRadius;
	if (model.port && !(model.port.pos in model.tiles)) {
		this.guessPort(model);
	}
};
