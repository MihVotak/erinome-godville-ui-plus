/**
 * View transformers.
 *
 * Many objects here have bind(model, view) and unbind(model, view) methods. bind changes the map currently used by a
 * transformer. It must not be called with the same map twice. The previously used map is left in a "dirty" state -
 * usually, we don't care about that and let the GC do its work. unbind undoes any changes introduced by a transformer.
 * It must be called with the same model and view that were passed to bind the last time. Moreover, relevant model
 * fields must stay immutable between bind and unbind. Calling unbind multiple times in a row is a no-op.
 *
 * @alias GUIp.common.islandsMap.vtrans
 * @namespace
 */
ui_imap.vtrans = {};

//! include './highlighter.js';
//! include './hint_drawers.js';
//! include './ruler.js';

/**
 * Orchestrates an IHintDrawer to mark thermo hints, the port, and, optionally, whirlpools.
 */
ui_imap.vtrans.hintManager = {
	/**
	 * If true, hints won't be drawn until a tile is clicked. Changes to this property take effect immediately.
	 *
	 * @type {boolean}
	 */
	requiresClick: false,

	/**
	 * 0 - hidden; 1 - shown; 2 - pinned. Be careful to treat undefined elements differently than 0.
	 *
	 * @private
	 * @type {!Object<GUIp.common.islandsMap.Vec, number>}
	 */
	_hintStates: {},

	/**
	 * @private
	 * @type {?Object<GUIp.common.islandsMap.Vec, !SVGElement>}
	 */
	_nodes: null,

	_drawer: null,

	_setDrawer: function(drawer) {
		if ((this._drawer = drawer)) {
			var newStates = {};
			for (var i = 0, len = drawer.processable.length; i < len; i++) {
				var pos = drawer.processable[i];
				this._nodes[pos].classList.add('e_clickable');
				if ((newStates[pos] = this._hintStates[pos] || 0)) {
					drawer.draw(pos);
				}
			}
			this._hintStates = newStates;
		}
	},

	_unsetDrawer: function() {
		if (this._drawer) {
			for (var i = 0, len = this._drawer.processable.length; i < len; i++) {
				this._nodes[this._drawer.processable[i]].classList.remove('e_clickable');
			}
			this._drawer.dispose();
			this._drawer = null;
		}
	},

	/** @type {?GUIp.common.islandsMap.vtrans.IHintDrawer} */
	get drawer() { return this._drawer; },

	set drawer(value) {
		if (!this._nodes) {
			throw new Error('attempting to set a drawer on an unbound hintManager');
		}
		this._unsetDrawer();
		this._setDrawer(value);
	},

	_onOver: function(pos) {
		if (!this.requiresClick && this._hintStates[pos] === 0) {
			if (this._drawer) {
				this._drawer.draw(pos);
			}
			this._hintStates[pos] = 1;
		}
	},

	_onOut: function(pos) {
		if (this._hintStates[pos] === 1) {
			if (this._drawer) {
				this._drawer.undraw(pos);
			}
			this._hintStates[pos] = 0;
		}
	},

	_onClick: function(pos) {
		var state = this._hintStates[pos];
		if (state === 2) {
			if (this._drawer) {
				this._drawer.undraw(pos);
			}
			this._hintStates[pos] = 0;
		} else if (state != null) {
			if (state === 0 && this._drawer) {
				this._drawer.draw(pos);
			}
			this._hintStates[pos] = 2;
		}
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 * @param {?GUIp.common.islandsMap.vtrans.IHintDrawer} drawer
	 */
	bind: function(model, view, drawer) {
		// note: new drawer already has its constructor run. this doesn't cause any problems
		// with current drawers' implementation, but it's worth keeping that in mind
		this._nodes = view.nodes;
		this._setDrawer(drawer);
		view.register('hintManager', 'mouseover', this._onOver.bind(this));
		view.register('hintManager', 'mouseout', this._onOut.bind(this));
		view.register('hintManager', 'click', this._onClick.bind(this));
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	unbind: function(model, view) {
		view.unregister('hintManager', 'mouseover');
		view.unregister('hintManager', 'mouseout');
		view.unregister('hintManager', 'click');
		this._unsetDrawer();
		this._nodes = null;
	}
};

/**
 * Forwards bind and unbind calls to the ruler object it holds.
 */
ui_imap.vtrans.rulerManager = {
	_ruler: null,

	/**
	 * A shortcut function that creates a ruler, binds it to a button and registers keyboard shortcuts.
	 *
	 * @param {!Element} button
	 */
	init: function(button) {
		if (this._ruler) {
			GUIp.common.error('attempting to create more than one ruler');
			return;
		}
		var ruler = this._ruler = new ui_imap.vtrans.Ruler;
		ruler.onstatechange.push(button.classList.toggle.bind(button.classList, 'active'));
		GUIp.common.addListener(button, 'click', ruler.toggle.bind(ruler));
		GUIp.common.addListener(worker, 'keydown', function(ev) {
			if ((ev.altKey && ev.keyCode === 82 /*R*/) || (ev.keyCode === 27 /*Esc*/ && ruler.active)) {
				ruler.toggle();
			}
		});
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	bind: function(model, view) {
		if (this._ruler) {
			this._ruler.bind(model, view);
		}
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	unbind: function(model, view) {
		if (this._ruler) {
			this._ruler.unbind();
		}
	}
};

/**
 * Adds some padding to the map edges.
 */
ui_imap.vtrans.mapExpander = {
	/**
	 * @private
	 * @param {!SVGElement} svg
	 * @param {?SVGElement} root
	 * @param {boolean} newState
	 */
	_expand: function(svg, root, newState) {
		// this seems to cause glitches in ancient opera browser
		if (!root || worker.GUIp_browser === 'Opera' || svg.classList.contains('e_expanded') === newState) {
			return;
		}

		// disable transitions when manipulating a map
		var oldTransition = svg.style.transition || 'unset',
			dx = newState ? 10 : -10,
			dy = newState ? 5 : -5;
		svg.style.transition = 'unset';
		svg.setAttribute('width',  (parseFloat(svg.getAttribute('width'))  + dx * 2) + 'px');
		svg.setAttribute('height', (parseFloat(svg.getAttribute('height')) + dy * 2) + 'px');
		var matrix = root.transform.baseVal.getItem(0).matrix;
		root.setAttribute('transform', 'translate(' + (matrix.e + dx) + ',' + (matrix.f + dy) + ')');
		svg.classList[newState ? 'add' : 'remove']('e_expanded');
		// restore previous transitions if someone'll ever need them
		GUIp.common.setTimeout(function() { svg.style.transition = oldTransition; }, 1);
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	bind: function(model, view) {
		this._expand(view.svg, view.root, true);
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	unbind: function(model, view) {
		this._expand(view.svg, view.root, false);
	}
};

/**
 * Suppresses original Godville behaviour of highlighting thermo hints.
 */
ui_imap.vtrans.originalHintHighlightingRemover = {
	/**
	 * @private
	 * @type {!Array<!SVGElement>}
	 */
	_backup: [],

	/**
	 * @private
	 * @param {!Object<GUIp.common.islandsMap.Vec, !SVGElement>} nodes
	 * @param {!Object<GUIp.common.islandsMap.Vec, number>} radiuses
	 * @param {!GUIp.common.islandsMap.Vec} pos
	 */
	_onClick: function(nodes, radiuses, pos) {
		var radius = radiuses[pos];
		if (radius) {
			var deltas = ui_imap.vec.ofLen(radius);
			for (var i = 0, len = deltas.length; i < len; i++) {
				var node = nodes[ui_imap.vec.add(pos, deltas[i])]
				if (node) {
					node.classList.remove('hl');
				}
			}
		}
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	bind: function(model, view) {
		var i, len;
		this._backup = Array.from((view.root || view.svg).getElementsByClassName('hl'));
		for (i = 0, len = this._backup.length; i < len; i++) {
			this._backup[i].classList.remove('hl');
		}

		var radiuses = {};
		for (i = 0, len = model.thermos.length; i < len; i++) {
			var thermo = model.thermos[i];
			radiuses[thermo.tile.pos] = thermo.yes || thermo.no;
		}
		view.register('originalHintHighlightingRemover', 'click', this._onClick.bind(this, view.nodes, radiuses));
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	unbind: function(model, view) {
		view.unregister('originalHintHighlightingRemover', 'click');

		for (var i = 0, len = this._backup.length; i < len; i++) {
			this._backup[i].classList.add('hl');
		}
	}
};

/**
 * Assigns CSS classes to points of interest.
 */
ui_imap.vtrans.poiColorizer = {
	/** @type {!Array<number>} */
	colors: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],

	/**
	 * @private
	 * @param {function(this: DOMTokenList, string)} f
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!Object<GUIp.common.islandsMap.Vec, !SVGElement>} nodes
	 */
	_apply: function(f, model, nodes) {
		if (!this.colors.length) {
			return;
		}
		for (var i = 0, len = model.poiGroups.length; i < len; i++) {
			var group = model.poiGroups[i],
				cls = 'e_poi_c' + this.colors[i % this.colors.length];
			for (var j = 0, jlen = group.length; j < jlen; j++) {
				var classes = nodes[group[j].pos].classList;
				f.call(classes, 'e_poi');
				f.call(classes, cls);
			}
		}
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	bind: function(model, view) {
		this._apply(DOMTokenList.prototype.add, model, view.nodes);
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	unbind: function(model, view) {
		this._apply(DOMTokenList.prototype.remove, model, view.nodes);
	}
};

/**
 * Highlights all treasures when any is hovered.
 */
ui_imap.vtrans.treasureHighlighter = new ui_imap.vtrans.TileGroupHighlighter('e_treasure_hover', function(model) {
	return [model.treasures];
});

/**
 * When any point of interest is hovered, highlights points of the same color.
 */
ui_imap.vtrans.poiHighlighter = new ui_imap.vtrans.TileGroupHighlighter('e_poi_hover', function(model) {
	return model.poiGroups;
});

/**
 * Assigns a CSS class to fogged tiles supposed to be a map border.
 */
ui_imap.vtrans.borderDrawer = {
	_radius: 0,
	_locked: false,

	/**
	 * @private
	 * @param {function(this: DOMTokenList, string)} f
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!Object<GUIp.common.islandsMap.Vec, !SVGElement>} nodes
	 */
	_apply: function(f, model, nodes) {
		var cls = this._locked ? 'e_border_n' : 'e_border',
			anchor = model.port ? model.port.pos : 0x0;
		for (var radius = this._radius; radius <= model.radius; radius++) {
			var deltas = ui_imap.vec.ofLen(radius);
			for (var i = 0, len = deltas.length; i < len; i++) {
				var pos = ui_imap.vec.add(anchor, deltas[i]),
					tile = model.tiles[pos];
				if (tile && tile.isFogged) {
					f.call(nodes[pos].classList, cls);
				}
			}
		}
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 * @param {number} radius
	 * @param {{locked: (boolean|undefined)}} conditions
	 */
	bind: function(model, view, radius, conditions) {
		this._radius = radius;
		this._locked = conditions.locked || false;
		this._apply(DOMTokenList.prototype.add, model, view.nodes);
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	unbind: function(model, view) {
		this._apply(DOMTokenList.prototype.remove, model, view.nodes);
	}
};

/**
 * Replaces anything that is written on arks with their numbers.
 */
ui_imap.vtrans.arkTextRewriter = {
	/**
	 * @private
	 * @type {!Array<(string|undefined)>}
	 */
	_backup: [],

	/**
	 * @private
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 * @param {function(!SVGElement, number)} callback
	 */
	_forEachArkText: function(model, view, callback) {
		for (var i = 0, len = model.arks.length; i < len; i++) {
			if (!model.arks[i]) {
				continue;
			}
			var node = view.nodes[model.arks[i].pos];
			node = node && node.getElementsByTagName('text')[0];
			if (node) {
				callback.call(this, node, i);
			}
		}
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	bind: function(model, view) {
		this._backup.length = model.arks.length;
		this._forEachArkText(model, view, function(node, i) {
			this._backup[i] = node.textContent;
			node.textContent = i + 1;
		});
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	unbind: function(model, view) {
		this._forEachArkText(model, view, function(node, i) {
			if (this._backup[i] != null) {
				node.textContent = this._backup[i];
			}
		});
		this._backup.length = 0;
	}
};

/**
 * Assigns a CSS class to islands visited by someone.
 */
ui_imap.vtrans.visitedFader = {
	/**
	 * @private
	 * @param {function(this: DOMTokenList, string)} f
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!Object<GUIp.common.islandsMap.Vec, !SVGElement>} nodes
	 */
	_apply: function(f, model, nodes) {
		for (var i = 0, len = model.visited.length; i < len; i++) {
			f.call(nodes[model.visited[i].pos].classList, 'e_visited');
		}
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	bind: function(model, view) {
		this._apply(DOMTokenList.prototype.add, model, view.nodes);
	},

	/**
	 * @param {!GUIp.common.islandsMap.Model} model
	 * @param {!GUIp.common.islandsMap.View} view
	 */
	unbind: function(model, view) {
		this._apply(DOMTokenList.prototype.remove, model, view.nodes);
	}
};
