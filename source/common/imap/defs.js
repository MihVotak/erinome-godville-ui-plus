/**
 * @class
 * @param {GUIp.common.islandsMap.Vec} pos
 * @param {boolean} isFogged
 * @param {number} code
 */
ui_imap.Tile = function(pos, isFogged, code) {
	/** @type {GUIp.common.islandsMap.Vec} */
	this.pos = pos;
	/** @type {boolean} */
	this.isFogged = isFogged;
	/** @type {number} */
	this.code = code;
};

ui_imap.Tile.prototype = {
	constructor: ui_imap.Tile,

	/** @type {boolean} */
	get isBorder() {
		return this.code === 35 /*#*/ || this.code === 36 /*$*/;
	}
};

/**
 * @typedef {Object} GUIp.common.islandsMap.Arrow
 * @property {!GUIp.common.islandsMap.Tile} tile
 * @property {GUIp.common.islandsMap.Vec} dir - A vector of minimal length to accurately represent that direction.
 */

/**
 * @class
 * @param {!GUIp.common.islandsMap.Tile} tile
 * @param {number}  no   - There are no treasures up to (and including) this distance.
 * @param {number} [yes] - There is at least one treasure up to (and including) this distance.
 */
ui_imap.Thermo = function(tile, no, yes) {
	/** @type {!GUIp.common.islandsMap.Tile} */
	this.tile = tile;
	/** @type {number} */
	this.no = no;
	/** @type {?number} */
	this.yes = yes || null;
};

ui_imap.Thermo.prototype = {
	constructor: ui_imap.Thermo,

	/**
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @returns {boolean} Whether a treasure can be in the given point, according to this hint.
	 */
	allows: function(pos) {
		return ui_imap.vec.dist(this.tile.pos, pos) > this.no;
	},

	/**
	 * @param {!GUIp.common.islandsMap.Thermo} another
	 * @returns {boolean} Whether the hints cross (but not when one is contained inside another).
	 */
	crosses: function(another) {
		var d = ui_imap.vec.dist(this.tile.pos, another.tile.pos),
			r0 = this.yes || this.no,
			r1 = another.yes || another.no;
		if (d >= r0 && d >= r1) {
			return d <= r0 + r1;
		} else {
			// one circle lays inside another one
			return d >= Math.abs(r0 - r1) - !!(this.yes && another.yes);
		}
	}
};

/**
 * @class
 * @classdesc Contains all essential information about the map and objects on it.
 */
ui_imap.Model = function() {
	/**
	 * Don't iterate through this dictionary. If you'd like to, then you probably should define an extra property here.
	 *
	 * @type {!Object<GUIp.common.islandsMap.Vec, !GUIp.common.islandsMap.Tile>}
	 */
	this.tiles = {};
	/** @type {number} */
	this.radius = 0;
	/** @type {number} */
	this.borderRadius = 0;
	/** @type {number} */
	this.nonBorderRadius = 0;
	/** @type {!Array<?GUIp.common.islandsMap.Tile>} */
	this.arks = [null, null, null, null];
	/** @type {?GUIp.common.islandsMap.Tile} */
	this.port = null;
	/** @type {!Array<!GUIp.common.islandsMap.Tile>} */
	this.visited = [];
	/** @type {!Array<!GUIp.common.islandsMap.Tile>} */
	this.whirlpools = [];
	/** @type {!Array<!GUIp.common.islandsMap.Tile>} */
	this.treasures = [];
	/** @type {!Array<!GUIp.common.islandsMap.Arrow>} */
	this.arrows = [];
	/** @type {!Array<!GUIp.common.islandsMap.Thermo>} */
	this.thermos = [];
	/** @type {!Array<!Array<!GUIp.common.islandsMap.Tile>>} */
	this.poiGroups = []; // points of interest
	/** @type {!Object<GUIp.common.islandsMap.Vec, number>} */
	this.poiGroupIndexAt = {};
};

/**
 * @class
 * @classdesc Represents an <svg> and maps DOM nodes to their positions and vice versa.
 * @param {!SVGElement} svg
 * @param {number} scale
 */
ui_imap.View = function(svg, scale) {
	/** @type {!SVGElement} */
	this.svg = svg;
	/** @type {?SVGElement} */
	this.root = null;
	/** @type {number} */
	this.scale = scale;
	/**
	 * Event listeners registered on the SVG node.
	 *
	 * @type {!Object<string, {listener: function(!Event), registry: !Object<string, function(GUIp.common.islandsMap.Vec, !Event)>, cache: !GUIp.common.FuncCache}>}
	 */
	this.handlers = Object.create(null);
	/** @type {!Object<GUIp.common.islandsMap.Vec, !SVGElement>} */
	this.nodes = {};
	/** @type {!WeakMap<!SVGElement, GUIp.common.islandsMap.Vec>} */
	this.positions = new WeakMap;
};

ui_imap.View.prototype = {
	constructor: ui_imap.View,

	/**
	 * @param {GUIp.common.islandsMap.Vec} pos
	 * @param {!SVGElement} node
	 */
	addNode: function(pos, node) {
		this.nodes[pos] = node;
		this.positions.set(node, pos);
	},

	/**
	 * @private
	 * @param {!GUIp.common.FuncCache>} callbacksCache
	 * @param {!Event} ev
	 */
	_dispatchEvent: function(callbacksCache, ev) {
		for (var node = ev.target; node && node !== this.svg; node = node.parentNode) {
			var pos = this.positions.get(node);
			if (pos != null) {
				var callbacks = callbacksCache.get();
				for (var i = 0, len = callbacks.length; i < len; i++) {
					GUIp.common.try2(callbacks[i], pos, ev);
				}
				return;
			}
		}
	},

	/**
	 * @param {string} key
	 * @param {string} type
	 * @param {function(GUIp.common.islandsMap.Vec, !Event)} callback
	 */
	register: function(key, type, callback) {
		var h = this.handlers[type];
		if (h) {
			h.registry[key] = callback;
			h.cache.invalidate();
		} else {
			var registry = {};
			registry[key] = callback;
			var cache = new GUIp.common.FuncCache(Object.values.bind(Object, registry)),
				listener = this._dispatchEvent.bind(this, cache);
			this.handlers[type] = {listener: listener, registry: registry, cache: cache};
			this.svg.addEventListener(type, listener);
		}
	},

	/**
	 * @param {string} key
	 * @param {string} type
	 */
	unregister: function(key, type) {
		var h = this.handlers[type];
		if (h) {
			delete h.registry[key];
			h.cache.invalidate();
		}
	},

	unregisterAll: function() {
		for (var type in this.handlers) {
			this.svg.removeEventListener(type, this.handlers[type].listener);
		}
		this.handlers = Object.create(null);
	}
};
