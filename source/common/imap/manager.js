/**
 * @class
 */
ui_imap.Manager = function() {
	/** @type {?GUIp.common.islandsMap.Model} */
	this.model = null;
	/** @type {?GUIp.common.islandsMap.conv.RawModel} */
	this.rawModel = null;
	/** @type {!Object<string, boolean>} */
	this.conditions = {};
	/** @type {?GUIp.common.islandsMap.View} */
	this.view = null;
	/** @type {?Element} */
	this.svg = null;
	/** @type {!Array<function(!GUIp.common.islandsMap.Manager)>} */
	this.onPreUpdate = [];
	/** @type {!Array<function(!GUIp.common.islandsMap.Manager)>} */
	this.onPreEncode = [];
	/** @type {!Array<function(!GUIp.common.islandsMap.Manager)>} */
	this.onPostEncode = [];
};

ui_imap.Manager.prototype = {
	constructor: ui_imap.Manager,

	_executeCallbacks: function(callbacks) {
		for (var i = 0, len = callbacks.length; i < len; i++) {
			callbacks[i](this);
		}
	},

	/**
	 * @param {!Element} svg
	 */
	replaceSVG: function(svg) {
		if (svg === this.svg) {
			// SVG stays the same - we need to clean it up first
			this._executeCallbacks(this.onPreUpdate);
		} else {
			this.svg = svg; // throw the old SVG away
		}

		var newModel;
		if (this.rawModel) {
			// faster
			newModel = ui_imap.conv.decode(this.rawModel);
			this.view = null; // will be set later
		} else {
			var pair = ui_imap.domParsers.mvFromSVG(this.svg, this.conditions);
			newModel = pair[0];
			this.view = pair[1];
		}
		if (this.model) {
			ui_imap.mtrans.migrate(this.model, newModel);
		}
		this.model = newModel;

		this._executeCallbacks(this.onPreEncode);

		if (!this.rawModel) {
			this.rawModel = ui_imap.conv.encode(this.model);
		}

		this._executeCallbacks(this.onPostEncode);

		if (!this.view) {
			this.view = ui_imap.domParsers.vFromSVG(this.svg);
		}
	}
};
