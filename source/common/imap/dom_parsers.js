/**
 * @alias GUIp.common.islandsMap.domParsers
 * @namespace
 */
ui_imap.domParsers = {};

/**
 * @param {!SVGElement} svg
 * @returns {number} A radius of a circle circumscribed around a tile.
 */
ui_imap.domParsers.detectMapScale = function(svg) {
	try {
		var p = svg.querySelector('g.tile polygon').getAttributeNS(null, 'points').split(/[\s,]+/),
			dx = +p[0] - +p[6],
			dy = +p[1] - +p[7];
		return Math.sqrt(dx * dx + dy * dy) * 0.5;
	} catch (e) {
		return 11;
	}
};

/**
 * @private
 * @param {!SVGElement} node
 * @param {number} scale
 * @returns {GUIp.common.islandsMap.Vec}
 */
ui_imap.domParsers._getNodePos = function(node, scale) {
	var matrix = node.transform.baseVal.getItem(0).matrix;
	return ui_imap.vec.fromCartesian(matrix.e, matrix.f, scale);
};

/**
 * @private
 * @const {!Object<string, number>}
 */
ui_imap.domParsers._symbolCodes = {
	'⁂': 44,  // ,
	'△': 59,  // ;
	'🗻': 59,  // ;
	'👾': 98,  // b
	'🐠': 66,  // B
	'?': 105, // i
	'🙏': 118, // v
	'🔧': 110, // n
	'🔦': 109, // m
	'🍴': 60,  // <
	'💡': 62,  // >
	'🌀': 64,  // @
	'♂': 77,  // M
	'✺': 116, // t
	'☀': 121, // y
	'♨': 117, // u
	'☁': 111, // o
	'❄': 91,  // [
	'✵': 93,  // ]
	// qwe
	// a d
	// zxc
	'↑': 119, // w
	'↗': 101, // e
	'→': 100, // d
	'↘': 99,  // c
	'↓': 120, // x
	'↙': 122, // z
	'←': 97,  // a
	'↖': 113  // q
};

/**
 * Parse info from the tile node and update the model and view with it.
 *
 * @private
 * @param {!GUIp.common.islandsMap.Model} model
 * @param {!GUIp.common.islandsMap.View} view
 * @param {!SVGElement} node
 * @param {!Object<string, boolean>} conditions
 */
ui_imap.domParsers._processTileNodeMV = function(model, view, node, conditions) {
	var pos = this._getNodePos(node, view.scale);
	if (pos in model.tiles) {
		GUIp.common.warn('duplicate tile:', ui_imap.conv.encodeTile(model.tiles[pos]), 'vs', node);
		return;
	}

	var classes = node.classList,
		isFogged = classes.contains('unknown'),
		isIsland = classes.contains('island'),
		code = isFogged ? 63 : isIsland ? 73 : 32 /*?I */,
		poiGroupIndex = -1,
		arkIndex = -1,
		titleNode = null,
		textNode = null,
		num = 0;

	// precalc children to avoid multiple calls to .getElementsByTagName
	for (var child = node.firstChild; child; child = child.nextSibling) {
		var tagName = child.tagName;
		if (tagName === 'title') {
			titleNode = child;
		} else if (tagName === 'text') {
			textNode = child;
		}
	}

	if (classes.contains('port')) {
		code = 112; // p
	} else if (conditions.locked) {
		if (titleNode) {
			// this code is executed for almost every tile, so don't use regexes
			var titleText = titleNode.textContent.trim().toLowerCase();
			if (titleText.startsWith('закрытая граница') || titleText.startsWith('closed edge')) {
				// note: isIsland === true && isBorder === true
				code = 36; // $
			}
		}
	} else if (classes.contains('border')) { // note: the port also has this class
		code = 35; // #
	}

	for (var i = 0, len = classes.length; i < len; i++) {
		var cls = classes[i], cls0 = cls[0];
		// avoid .startsWith here
		if (cls0 === 'p' && cls[1] === 'l' && (num = parseInt(cls.slice(2))) > 0) {
			// ark
			if (arkIndex !== -1) {
				GUIp.common.warn('multiple arks: #' + (arkIndex + 1) + ' vs #' + num, 'at', pos);
				continue;
			}
			arkIndex = num - 1;
		} else {
			var isFoggedPOI = cls0 === 'i';
			if ((isFoggedPOI || (cls0 === 'o' && cls[1] === 'i')) && (num = parseInt(cls.slice(2 - isFoggedPOI))) > 0) {
				// point of interest
				if (poiGroupIndex !== -1) {
					GUIp.common.warn('multiple POIs: #' + (poiGroupIndex + 1) + ' vs #' + num, 'at', pos);
					continue;
				}
				poiGroupIndex = num - 1;
				if (isFoggedPOI) {
					isFogged = true;
					code = ui_imap.conv.poiCodes[poiGroupIndex % ui_imap.conv.poiCodes.length];
				}
			}
		}
	}

	if (textNode) {
		var text = textNode.textContent.trim();
		if (text === '♀') {
			code = isIsland ? 102 : 70; // fF (yes, fenimals are supposed to live on islands too)
		} else if (text === '💰') {
			code = isIsland ? 71 : 103; // Gg
		} else {
			// cast to an int because it might be a property in the prototype chain
			var newCode = this._symbolCodes[text] | 0;
			if (newCode) code = newCode;
		}
	}

	var tile = new ui_imap.Tile(pos, isFogged, code);
	if (arkIndex !== -1) {
		model.arks[arkIndex] = tile;
	}
	if (poiGroupIndex !== -1) {
		model.poiGroupIndexAt[pos] = poiGroupIndex;
	}
	ui_imap.mtrans.addTile(model, tile);
	view.addNode(pos, node);
};

/**
 * @param {!SVGElement} svg
 * @returns {!GUIp.common.islandsMap.View}
 */
ui_imap.domParsers.vFromSVG = function(svg) {
	var view = new ui_imap.View(svg, this.detectMapScale(svg));
	view.root = svg.getElementsByTagName('g')[0] || null;
	// assume all tiles are siblings, thus avoiding slower querySelectorAll
	for (var node = svg.querySelector('g.tile'); node; node = node.nextSibling) {
		if (node.classList.contains('tile')) {
			view.addNode(this._getNodePos(node, view.scale), node);
		}
	}
	return view;
};

/**
 * @param {!SVGElement} svg
 * @param {!Object<string, boolean>} conditions
 * @returns {!Array<!Object>} [!GUIp.common.islandsMap.Model, !GUIp.common.islandsMap.View]
 */
ui_imap.domParsers.mvFromSVG = function(svg, conditions) {
	var model = new ui_imap.Model,
		view = new ui_imap.View(svg, this.detectMapScale(svg));
	view.root = svg.getElementsByTagName('g')[0] || null;
	// assume all tiles are siblings, thus avoiding slower querySelectorAll
	for (var node = svg.querySelector('g.tile'); node; node = node.nextSibling) {
		if (node.classList.contains('tile')) {
			this._processTileNodeMV(model, view, node, conditions);
		}
	}
	if (!model.port) {
		ui_imap.mtrans.guessPort(model);
	}
	return [model, view];
};
