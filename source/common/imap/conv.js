/**
 * Functions for converting data between different formats (internal and raw).
 *
 * @alias GUIp.common.islandsMap.conv
 * @namespace
 */
ui_imap.conv = {};

/** @const {!Array<number>} */
ui_imap.conv.poiCodes = [33, 96, 126, 94, 38, 40, 41]; // !`~^&()

/** @const {!Object<number, number>} */
ui_imap.conv.poiCodesDict = {33: 0, 96: 1, 126: 2, 94: 3, 38: 4, 40: 5, 41: 6};

/**
 * @param {GUIp.common.islandsMap.Vec} pos
 * @param {number} code
 * @returns {number}
 */
ui_imap.conv.encodeTilePosCode = function(pos, code) {
	// [q, -(q + r), r, code]
	// yes, the second byte is redundant
	return (pos & 0xFF) | ((~(pos + (pos << 8)) + 0x0100) & 0xFF00) | (pos & 0xFF00) << 8 | code << 24;
};

/**
 * @param {number} mask
 * @returns {GUIp.common.islandsMap.Vec}
 */
ui_imap.conv.decodeTilePos = function(mask) {
	return (mask & 0xFF) | (mask >> 8 & 0xFF00);
};

/**
 * @param {number} mask
 * @returns {number}
 */
ui_imap.conv.decodeTileCode = function(mask) {
	return mask >>> 24;
};

/**
 * @param {!GUIp.common.islandsMap.Tile} tile
 * @returns {number}
 */
ui_imap.conv.encodeTile = function(tile) {
	return this.encodeTilePosCode(tile.pos, tile.code);
};

/**
 * @param {number} mask
 * @returns {!GUIp.common.islandsMap.Tile}
 */
ui_imap.conv.decodeTile = function(mask) {
	var code = this.decodeTileCode(mask);
	return new ui_imap.Tile(this.decodeTilePos(mask), code === 63 /*?*/ || code in this.poiCodesDict, code);
};

/**
 * @class
 * @param {!Array<number>} map
 * @param {!Array<number>} arks
 * @param {!Array<number>} visited
 * @param {!Array<number>} pois - A flattened array of encoded tiles.
 */
ui_imap.conv.RawModel = function(map, arks, visited, pois) {
	/** @type {!Array<number>} */
	this.map = map;
	/** @type {!Array<number>} */
	this.arks = arks;
	/** @type {!Array<number>} */
	this.visited = visited;
	/** @type {!Array<number>} */
	this.pois = pois;
};

ui_imap.conv.RawModel.prototype = {
	constructor: ui_imap.conv.RawModel,

	/**
	 * @private
	 * @returns {number}
	 */
	_partitionMap: function() {
		var arkPositions = GUIp.common.makeHashSet(this.arks.map(ui_imap.conv.decodeTilePos));
		return GUIp.common.partitionArray(this.map, function(tile) {
			return !(ui_imap.conv.decodeTilePos(tile) in arkPositions);
		});
	},

	/**
	 * @param {function(!GUIp.common.islandsMap.conv.RawModel): *} action
	 * @param {?Object} [thisArg]
	 * @returns {*}
	 */
	withArksOnMap: function(action, thisArg) {
		if (!this.arks.length) {
			return action.call(thisArg, this);
		}
		// add arks to the map, replacing corresponding tiles
		var mid = this._partitionMap(),
			excluded = this.map.slice(mid);
		GUIp.common.replaceArrayTail(this.map, mid, this.arks);
		try {
			return action.call(thisArg, this);
		} finally {
			// put that tiles back into the map
			GUIp.common.replaceArrayTail(this.map, mid, excluded);
		}
	}
};

ui_imap.conv._encodeMap = function(model) {
	return Object.values(model.tiles).map(this.encodeTile, this);
};

ui_imap.conv._encodeArks = function(model) {
	var result = [];
	for (var i = 0, len = model.arks.length; i < len; i++) {
		if (model.arks[i]) {
			result.push(this.encodeTilePosCode(model.arks[i].pos, i + 49 /*1234*/));
		}
	}
	return result;
};

ui_imap.conv._encodeVisited = function(model) {
	return model.visited.map(function(tile) { return tile.pos; });
};

ui_imap.conv._encodePOIs = function(model) {
	var result = [];
	for (var i = 0, len = model.poiGroups.length; i < len; i++) {
		var group = model.poiGroups[i],
			// already revealed points of interest have their code replaced
			code = this.poiCodes[i % this.poiCodes.length];
		for (var j = 0, jlen = group.length; j < jlen; j++) {
			result.push(this.encodeTilePosCode(group[j].pos, code));
		}
	}
	return result;
};

/**
 * @param {!GUIp.common.islandsMap.Model} model
 * @returns {!GUIp.common.islandsMap.conv.RawModel}
 */
ui_imap.conv.encode = function(model) {
	return new this.RawModel(
		this._encodeMap(model),
		this._encodeArks(model),
		this._encodeVisited(model),
		this._encodePOIs(model)
	);
};

/**
 * @param {!Array<number>} rawMap
 * @returns {!Array<number>}
 */
ui_imap.conv.guessPOIsFromMap = function(rawMap) {
	var result = [], i, j, len;
outer:
	for (i = 0, len = rawMap.length; i < len; i++) {
		var offset = this.poiCodesDict[this.decodeTileCode(rawMap[i])] * 3;
		if (offset !== offset) continue;
		if (this.decodeTilePos(rawMap[i]) === 0x8080) {
			GUIp.common.warn('weird POI:', rawMap[i]);
			continue;
		}

		for (j = result.length; j < offset; j++) {
			result[j] = this.encodeTilePosCode(0x8080, this.poiCodes[Math.floor(j / 3)]);
		}
		// pick the first "empty" element
		for (j = 0; j < 3; j++) {
			if (result[offset + j] === undefined || this.decodeTilePos(result[offset + j]) === 0x8080) {
				result[offset + j] = rawMap[i];
				continue outer;
			}
		}
		GUIp.common.warn('too many same-colored POIs:', rawMap[i]);
	}
	// the last group may have less than 3 points of interest
	for (i = 2, len = result.length; i < len; i += 3) {
		if (this.decodeTilePos(result[i]) === 0x8080) {
			GUIp.common.warn('too few POIs of color #' + Math.floor(i / 3));
		}
	}
	return result;
};

/**
 * @param {!GUIp.common.islandsMap.conv.RawModel} rawModel
 * @returns {!GUIp.common.islandsMap.Model}
 */
ui_imap.conv.decode = function(rawModel) {
	var model = new ui_imap.Model, i, j, len, to, pos, tile, index;

	for (i = 0, len = rawModel.map.length; i < len; i++) {
		tile = this.decodeTile(rawModel.map[i]);
		if (tile.pos in model.tiles) {
			GUIp.common.warn('duplicate tile:', model.tiles[tile.pos], 'vs', mask);
			continue;
		}
		if (tile.code >= 49 && tile.code <= 52 /*1234*/) {
			index = tile.code - 49;
			tile.code = 32; /* */
			if (model.arks[index]) {
				GUIp.common.warn('duplicate ark:', model.arks[index], 'vs', mask);
			} else {
				model.arks[index] = tile;
			}
		}
		ui_imap.mtrans.addTile(model, tile);
	}

	if (!model.port) {
		ui_imap.mtrans.guessPort(model);
	}

	for (i = 0, len = rawModel.arks.length; i < len; i++) {
		tile = model.tiles[this.decodeTilePos(rawModel.arks[i])];
		if (!tile) {
			GUIp.common.warn('dangling ark:', rawModel.arks[i]);
			continue;
		}
		if (model.arks.includes(tile)) {
			GUIp.common.warn('multiple arks at the same position:', tile, 'vs', rawModel.arks[i]);
			continue;
		}
		index = this.decodeTileCode(rawModel.arks[i]) - 49; // 1234
		if (index < 0 || index >= 4) {
			GUIp.common.warn('pretending', tile, 'to be an ark #' + (index + 1));
			continue;
		}
		if (model.arks[index]) {
			GUIp.common.warn('duplicate ark:', model.arks[index], 'vs', rawModel.arks[i]);
			continue;
		}
		model.arks[index] = tile;
	}

	for (i = 0, len = rawModel.visited.length; i < len; i++) {
		tile = model.tiles[rawModel.visited[i]];
		if (!tile) {
			GUIp.common.warn('dangling visited island:', rawModel.visited[i]);
			continue;
		}
		model.visited.push(tile);
	}

	// assume it is a flattened array of 3-element groups
	for (i = 0, len = Math.floor((rawModel.pois.length + 2) / 3 + 1e-6); i < len; i++) {
		var group = [];
		for (j = i * 3, to = Math.min(i * 3 + 3, rawModel.pois.length); j < to; j++) {
			pos = this.decodeTilePos(rawModel.pois[j]);
			tile = model.tiles[pos];
			if (!tile) {
				GUIp.common.warn('dangling POI:', rawModel.pois[j]);
				continue;
			}
			model.poiGroupIndexAt[pos] = i;
			group.push(tile);
		}
		model.poiGroups[i] = group;
	}

	return model;
};
