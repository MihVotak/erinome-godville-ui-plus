/** @namespace */
ui_imap.observer = {};

// watch all changes in a subtree to keep working if Godville starts to update the map partially
ui_imap.observer._config = {childList: true, subtree: true};

ui_imap.observer._isRelevantNode = function(node) {
	var classes = node.classList;
	if (classes && (classes.contains('e_hint') || classes.contains('e_ruler_tooltip') || classes.contains('e_ruler_defs'))) {
		// ignore our own hints and ruler
		return false;
	}
	// the ruler has much stuff to ignore
	return node.id !== 'e_ruler' && node.nodeName.toLowerCase() !== '#text';
};

ui_imap.observer._isRelevantMutation = function(mutation) {
	if (mutation.target.classList.contains('dir_resp')) {
		// ignore native direction notice
		return false;
	}
	return Array.prototype.some.call(mutation.addedNodes, ui_imap.observer._isRelevantNode);
};

/**
 * @class
 * @param {!Element} container
 * @param {function()} callback
 * @param {?Object} [thisArg]
 */
ui_imap.observer.Observer = function(container, callback, thisArg) {
	/**
	 * @readonly
	 * @type {boolean}
	 */
	this.enabled = true;
	this._container = container;
	this._observer = GUIp.common.newMutationObserver(function(mutations) {
		if (mutations.some(ui_imap.observer._isRelevantMutation)) {
			// break an infinite loop
			this.withDisabled(callback, thisArg);
		}
	}.bind(this));
	this._observer.observe(container, ui_imap.observer._config);
};

/**
 * @param {function(): *} action
 * @param {?Object} [thisArg]
 * @returns {*}
 */
ui_imap.observer.Observer.prototype.withDisabled = function(action, thisArg) {
	if (!this.enabled) {
		return action.call(thisArg);
	}
	this.enabled = false;
	this._observer.disconnect();
	try {
		return action.call(thisArg);
	} finally {
		this._observer.observe(this._container, ui_imap.observer._config);
		this.enabled = true;
	}
};

/**
 * @param {!Element} container
 * @param {function()} callback
 * @param {?Object} [thisArg]
 * @returns {!GUIp.common.islandsMap.observer.Observer}
 */
ui_imap.observer.create = function(container, callback, thisArg) {
	return new this.Observer(container, callback, thisArg);
};
