/**
 * @class
 * @classdesc A simple binary min-heap.
 * @param {function(*, *): number} cmp
 * @param {?Array<*>} [data]
 */
GUIp.common.Heap = function(cmp, data) {
	this._cmp = cmp;
	this._data = data || [];
	this._heapify();
};

GUIp.common.Heap.prototype = {
	constructor: GUIp.common.Heap,

	/** @type {number} */
	get length() { return this._data.length; },

	/** @type {*} */
	get top() { return this._data[0]; },

	/**
	 * Move the element down the tree, given that both its subtrees are valid heaps.
	 *
	 * @private
	 * @param {*} x
	 * @param {number} i
	 * @param {number} n
	 */
	_moveDown: function(x, i, n) {
		for (var j; (j = i << 1 | 0x1) < n; i = j) {
			var next = this._data[j];
			if (j + 1 >= n) {
				// having only one (left) child
				if (this._cmp(next, x) >= 0) {
					this._data[i] = x;
				} else {
					this._data[i] = next;
					this._data[j] = x;
				}
				return;
			}
			var t = this._data[j + 1];
			if (this._cmp(t, next) < 0) {
				next = t;
				j++;
			}
			if (this._cmp(next, x) >= 0) {
				break;
			}
			this._data[i] = next;
		}
		this._data[i] = x;
	},

	_heapify: function() {
		for (var n = this._data.length, i = (n >>> 1) - 1; i >= 0; i--) {
			this._moveDown(this._data[i], i, n);
		}
	},

	/**
	 * @param {!Array<*>} data
	 * @returns {!Array<*>} Previously captured data.
	 */
	capture: function(data) {
		var old = this._data;
		this._data = data;
		this._heapify();
		return old;
	},

	/**
	 * @param {*} x
	 */
	push: function(x) {
		var i = this._data.length;
		for (var j; i; i = j) {
			j = (i - 1) >>> 1;
			var parent = this._data[j];
			if (this._cmp(x, parent) >= 0) {
				break;
			}
			this._data[i] = parent;
		}
		this._data[i] = x;
	},

	/**
	 * @returns {*}
	 */
	pop: function() {
		var result = this._data[0],
			n = this._data.length - 1;
		if (n > 0) {
			this._moveDown(this._data[n], 0, n);
			this._data.length = n;
		} else if (!n) {
			this._data.length = 0;
		}
		return result;
	}
};
