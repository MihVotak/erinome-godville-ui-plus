GUIp.common.debug = worker.console.log.bind(worker.console, '[eGUI+] debug:');
GUIp.common.info  = worker.console.info.bind(worker.console, '[eGUI+] info:');
GUIp.common.warn  = worker.console.warn.bind(worker.console, '[eGUI+] warning:');
GUIp.common.error = worker.console.error.bind(worker.console, '[eGUI+] error:');
