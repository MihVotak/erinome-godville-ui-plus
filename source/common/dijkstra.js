/**
 * @interface GUIp.common.IGraphTraverser
 */

/**
 * @function GUIp.common.IGraphTraverser.isFinal
 * @param {!*} state
 * @returns {boolean}
 */

/**
 * @function GUIp.common.IGraphTraverser.getHash
 * @param {!*} state
 * @returns {(number|string)}
 */

/**
 * @function GUIp.common.IGraphTraverser.cmp
 * @param {!*} stateA
 * @param {!*} stateB
 * @returns {number}
 */

/**
 * @function GUIp.common.IGraphTraverser.getAdj
 * @param {!*} state
 * @returns {!Array<!*>}
 */

/**
 * @param {!Array<!*>} states
 * @param {!GUIp.common.IGraphTraverser} tr
 * @returns {{final: ?*, visited: !Object, way: !Object}}
 */
GUIp.common.dijkstraSearch = function(states, tr) {
	var visited = Object.create(null),
		way = Object.create(null),
		i, len, cur, next, h, t;
	for (i = 0, len = states.length; i < len; i++) {
		cur = states[i];
		h = tr.getHash(cur);
		t = visited[h];
		if (t == null || tr.cmp(cur, t) < 0) {
			visited[h] = cur;
		}
	}
	var pq = new GUIp.common.Heap(tr.cmp.bind(tr), states);
	while ((cur = pq.pop()) != null) {
		if (tr.cmp(cur, visited[tr.getHash(cur)])) {
			continue;
		}
		if (tr.isFinal(cur)) {
			return {final: cur, visited: visited, way: way};
		}
		var adj = tr.getAdj(cur);
		for (i = 0, len = adj.length; i < len; i++) {
			next = adj[i];
			h = tr.getHash(next);
			t = visited[h];
			if (t == null || tr.cmp(next, t) < 0) {
				visited[h] = next;
				way[h] = cur;
				pq.push(next);
			}
		}
	}
	return {final: null, visited: visited, way: way};
};
