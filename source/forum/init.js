// base functions and variables initialization

var doc = document;
var $id = function(id) {
	return doc.getElementById(id);
};
var $C = function(classname) {
	return doc.getElementsByClassName(classname);
};
var $c = function(classname) {
	return doc.getElementsByClassName(classname)[0];
};
var $Q = function(sel) {
	return doc.querySelectorAll(sel);
};
var $q = function(sel) {
	return doc.querySelector(sel);
};
var storage = {
	_getKey: function(key) {
		return 'eGUI_' + this.god_name + ':' + key;
	},
	set: function(id, value) {
		localStorage[this._getKey(id)] = value;
	},
	get: function(id) {
		return localStorage.getItem(this._getKey(id));
	},
	getFlag: function(id) {
		return this.get(id) === 'true';
	},
	god_name: ''
};
var addSmallElements = function() {
	var temp = $Q('.c2');
	for (i = 0, len = temp.length; i < len; i++) {
		if (!temp[i].querySelector('small')) {
			temp[i].insertAdjacentHTML('beforeend', '<small></small>');
		}
	}
};
var followOnclick = function(e) {
	e.preventDefault();
	var topic = isTopic ? location.pathname.match(/\d+/)[0]
						: this.parentElement.parentElement.querySelector('a').href.match(/\d+/)[0],
		posts = isTopic ? +$c('subtitle').textContent.match(/\d+/)[0]
						: +this.parentElement.parentElement.nextElementSibling.nextElementSibling.textContent,
		dates = isTopic ? document.getElementsByTagName('abbr')
						: null,
		date =  isTopic ? document.getElementsByClassName('disabled next_page').length ? dates[dates.length - 1].title : 0
						: this.parentElement.parentElement.parentElement.getElementsByTagName('abbr')[0].title,
		name =  isTopic ? document.querySelector('.cur_nav').textContent
						: this.parentElement.parentElement.querySelector('a').textContent,
		by =  isTopic ? document.getElementsByClassName('disabled next_page').length ? document.querySelectorAll('.fn > span:first-child > a')[dates.length - 1].textContent : '-'
						: this.parentElement.parentElement.parentElement.querySelector('span.author').textContent,
		topics = JSON.parse(storage.get('ForumSubscriptions')) || {};
	if (worker.Object.keys(topics).length < 60) {
		topics[topic] = {posts: posts, name: name, by: by, date: +new Date(date)};
	} else {
		worker.alert(worker.GUIp_i18n.forum_subs_limit_exceeded + ' (60)');
		return;
	}
	storage.set('ForumSubscriptions', JSON.stringify(topics));
	this.style.display = 'none';
	this.parentElement.querySelector('.unfollow').style.display = 'inline';
	if (isSubs) {
		this.parentElement.querySelector('.mkrapid').style.display = 'inline';
		var checkboxes = this.parentElement.parentElement.parentElement.querySelectorAll('input[type=checkbox]');
		for (var i = 0, len = checkboxes.length; i < len; i++) {
			checkboxes[i].disabled = false;
		}
		updateSubscriptionsStats(topics,worker.Object.keys(topics));
	}
};
var addOnclickToFollow = function() {
	var follow_links = $Q('.follow');
	for (i = 0, len = follow_links.length; i < len; i++) {
		GUIp.common.addListener(follow_links[i], 'click', followOnclick);
	}
};
var unfollowOnclick = function(e) {
	e.preventDefault();
	var topic = isTopic ? location.pathname.match(/\d+/)[0]
						: this.parentElement.parentElement.querySelector('a').href.match(/\d+/)[0],
		topics = JSON.parse(storage.get('ForumSubscriptions')) || {},
		informers = JSON.parse(storage.get('ForumInformers')) || {};
	delete topics[topic];
	storage.set('ForumSubscriptions', JSON.stringify(topics));
	if (informers[topic]) {
		var notify = {tid: +topic, obsolete: true, ndate: Date.now()};
		storage.set('ForumInformersNotify' + Math.random(), JSON.stringify(notify));
	}
	this.style.display = 'none';
	this.parentElement.querySelector('.follow').style.display = 'inline';
	if (isSubs) {
		var tr = this.parentElement.parentElement.parentElement, icon, checkboxes;
		this.parentElement.querySelector('.mkrapid').style.display = 'none';
		this.parentElement.querySelector('.unmkrapid').style.display = 'none';
		if ((icon = tr.getElementsByClassName('icon')[0])) {
			icon.classList.add('grey');
			icon.classList.remove('green');
		}
		checkboxes = tr.querySelectorAll('input[type=checkbox]');
		for (var i = 0, len = checkboxes.length; i < len; i++) {
			checkboxes[i].checked = false;
			checkboxes[i].disabled = true;
		}
		updateSubscriptionsStats(topics,worker.Object.keys(topics));
	}
};
var addOnclickToUnfollow = function() {
	var unfollow_links = $Q('.unfollow');
	for (i = 0, len = unfollow_links.length; i < len; i++) {
		GUIp.common.addListener(unfollow_links[i], 'click', unfollowOnclick);
	}
};
var addOnclickToRapid = function() {
	var rapid_links = $Q('.mkrapid');
	for (i = 0, len = rapid_links.length; i < len; i++) {
		GUIp.common.addListener(rapid_links[i], 'click', function(e) {
			e.preventDefault();
			var topic = isTopic ? location.pathname.match(/\d+/)[0]
								: this.parentElement.parentElement.querySelector('a').href.match(/\d+/)[0],
				topics = JSON.parse(storage.get('ForumSubscriptions')) || {},
				keys = worker.Object.keys(topics),
				rapid = 0;
			for (var j = 0, len2 = keys.length; j < len2; j++) {
				if (topics[keys[j]].rapid) {
					rapid++;
				}
			}
			if (rapid >= 9) {
				worker.alert(worker.GUIp_i18n.forum_subs_rapid_exceeded + ' (9)');
				return;
			}
			if (topics[topic]) {
				topics[topic].rapid = true;
				storage.set('ForumSubscriptions', JSON.stringify(topics));
			}
			this.style.display = 'none';
			this.parentElement.querySelector('.unmkrapid').style.display = 'inline';
			if (isSubs) {
				updateSubscriptionsStats(topics,keys);
			}
		});
	}
};
var addOnclickToUnrapid = function() {
	var rapid_links = $Q('.unmkrapid');
	for (i = 0, len = rapid_links.length; i < len; i++) {
		GUIp.common.addListener(rapid_links[i], 'click', function(e) {
			e.preventDefault();
			var topic = isTopic ? location.pathname.match(/\d+/)[0]
								: this.parentElement.parentElement.querySelector('a').href.match(/\d+/)[0],
				topics = JSON.parse(storage.get('ForumSubscriptions')) || {};
			if (topics[topic]) {
				delete topics[topic].rapid;
				storage.set('ForumSubscriptions', JSON.stringify(topics));
			}
			this.style.display = 'none';
			this.parentElement.querySelector('.mkrapid').style.display = 'inline';
			if (isSubs) {
				updateSubscriptionsStats(topics,worker.Object.keys(topics));
			}
		});
	}
};
var addLinks = function() {
	var links_containers;
	if (isTopic) {
		links_containers = $Q('#content .crumbs');
		topic = location.pathname.match(/\d+/)[0];
		var isFollowed = topics[topic] !== undefined;
		if (links_containers[0]) {
			links_containers[0].insertAdjacentHTML('afterend',
				'\n<div id="ui_notify">' +
				'<a class="follow" href="#" style="display: ' + (isFollowed ? 'none' : 'inline') + '" title="' + worker.GUIp_i18n.subscribe_title + '">' + worker.GUIp_i18n.Subscribe + '</a>' +
				'<a class="unfollow" href="#" style="display: ' + (isFollowed ? 'inline' : 'none') + '" title="' + worker.GUIp_i18n.unsubscribe_title + '">' + worker.GUIp_i18n.Unsubscribe + '</a>' +
				'</div>'
			);
		}
	} else {
		links_containers = $Q('.c2 small');
		for (i = 0, len = links_containers.length; i < len; i++) {
			topic = links_containers[i].parentElement.getElementsByTagName('a')[0].href.match(/\d+/)[0];
			var isFollowed = topics[topic] !== undefined,
				isRapid = topics[topic] && topics[topic].rapid;
			links_containers[i].insertAdjacentHTML('beforeend',
				'\n<a class="follow" href="#" style="display: ' + (isFollowed ? 'none' : 'inline') + '" title="' + worker.GUIp_i18n.subscribe_title + '">' + worker.GUIp_i18n.subscribe + '</a>' +
				'<a class="unfollow" href="#" style="display: ' + (isFollowed ? 'inline' : 'none') + '" title="' + worker.GUIp_i18n.unsubscribe_title + '">' + worker.GUIp_i18n.unsubscribe + '</a>' +
				(isSubs ? ' <a class="mkrapid" href="#" style="display: ' + (isFollowed && !isRapid ? 'inline' : 'none') + '">☆</a>' + '<a class="unmkrapid" href="#" style="display: ' + (isFollowed && isRapid ? 'inline' : 'none') + '">★</a>' : '')
			);
		}
	}
	addOnclickToFollow();
	addOnclickToUnfollow();
	addOnclickToRapid();
	addOnclickToUnrapid();
};

var annotateExpensiveBingoItem = function(html) {
	// there is an artifact named "heart of the matter", so this regex must be case-sensitive
	return /^(?:золотой кирпич|бесценный дар|инвайт на Годвилль|старую шмотку|шкуру разыскиваемого |(?:(?:влюбл.нное )?сердце|глаз) босса |зубастого триббла|бонус за подряд|призовой сундук|пасхал(?:ку|ьное яйцо)|заморск.. |морскую (?:джемчужину|златоустрицу|суперзвезду)|морской приз|(?:ларец|сундучок|ящик) из моря|босскоин|gold(?:en)? brick|(?:really )?priceless gift|quest completion certificate|Godville invite|invite to Godville|old piece of equipment|[Pp]roof of a wanted |(?:heart|eye) of the [A-Z]|hungry tribble|side job bonus|prize chest|easter egg$|outlandish |overseas |chained crate|donation box|pirate coffer|exotic seashell|bosscoin)/.test(html.replace(/<[^]*?>/g, '')) ? (
		'<span class="e_bingo_expensive" title="' + GUIp_i18n.bingo_expensive_artifact + '">' + html + '</span>'
	) : html;
};

var highlightExpensiveInBingo = function() {
	var list = $id('b_inv'),
		html = list.innerHTML,
		replaced = html.replace(/[^\s:,.][^:,.]*/g, annotateExpensiveBingoItem);
	if (replaced !== html) {
		list.innerHTML = replaced;
	}
};

var prepareBingo = function() {
	var bingoItems = $Q('#bgn td span:not(.bgnk)'),
		bingoPattern = '',
		bingoTries = 0,
		node;
	if (bingoItems.length) {
		bingoPattern = Array.from(bingoItems, function(a) { return a.textContent; }).join('|');
		node = $id('l_clicks');
		if (node && node.style.display !== 'none') { // when filling the last time, #b_cnt is not updated
			node = $id('b_cnt');
			bingoTries = (node && parseInt(node.textContent)) || 0;
		}
	}
	storage.set('BingoItems', bingoPattern);
	storage.set('BingoTries', bingoTries);
	highlightExpensiveInBingo();
};

var updateForecast = function() {
	var fc = '', fcText = '', forecasts = Array.from(document.querySelectorAll('.fc > p'), function(a) { return a.textContent; }).join('\n');
	if (forecasts) {
		 fc = GUIp.common.parseForecasts(forecasts);
		 fcText = forecasts;
	}
	storage.set('DailyForecast', fc);
	storage.set('DailyForecastText', fcText);
};

var prepareSubscriptionsList = function() {
	var i, j, elm, topics, postdata, requestlist,
		subscriptions = JSON.parse(storage.get('ForumSubscriptions')) || {};
	if ((elm = document.querySelector('.crumbs .cur_nav'))) {
		elm.textContent = worker.GUIp_i18n.forum_subs + ' UI+';
	}
	elm = document.querySelectorAll('#search_block, #content .subtitle, #content .pagination, #content div a[href="/forums/new_topic/1"]');
	for (i = 0; i < elm.length; i++) {
		elm[i].style.display = 'none';
	}
	elm = document.querySelectorAll('tr.hentry');
	for (i = 0; i < elm.length; i++) {
		elm[i].parentNode.removeChild(elm[i]);
	}
	elm = document.querySelectorAll('table.topics th');
	elm[1].insertAdjacentHTML('beforebegin',
		'<th><abbr title="' + GUIp_i18n.forum_subs_notif_title + '">' + GUIp_i18n.forum_subs_notif_abbr + '</abbr></th>'
	);
	elm[2].parentNode.removeChild(elm[2]);

	topics = worker.Object.keys(subscriptions);
	if (topics.length === 0) {
		insertTopicRow('<tr><td colspan="5">' + worker.GUIp_i18n.forum_subs_no_subs + '</td></tr>');
		return;
	}

	var informers = JSON.parse(storage.get('ForumInformers'));
	topics.sort(function(a, b) { return subscriptions[b].date - subscriptions[a].date; });
	var tid, sub, page, date, content = '';
	for (var i = 0; i < topics.length; i++) {
		tid = topics[i];
		sub = subscriptions[tid];
		page = Math.ceil(sub.posts / 25);
		date = new Date(sub.date || 0);
		content += '<tr class="hentry">' +
			'<td style="padding:5px; width:16px;" class="c1"><img alt="Comment" class="icon ' + (tid in informers ? 'green' : 'grey') + ' " src="/images/forum/clearbits/comment.gif"></td>' +
			'<td class="c2">' +
				'<a href="/forums/show_topic/' + tid + '" class="entry-title" rel="bookmark">' + sub.name + '</a> ' +
				'<small><a href="/forums/show_topic/' + tid + '?page=' + page + '">' + worker.GUIp_i18n.forum_subs_last + '</a></small>' +
			'</td>' +
			'<td class="ca inv" style="padding: 0;" data-e-tid="' + tid + '">' +
				(worker.GUIp_browser !== 'Opera' ?
					'<input type="checkbox" data-e-mask="1" ' +
					(sub.notifications & 0x1 ? ' checked' : '') +
					' title="' + GUIp_i18n.forum_subs_desktop_notif + '" />'
				: '') +
				'<input type="checkbox" data-e-mask="2" ' +
					(sub.notifications & 0x2 ? ' checked' : '') +
					' title="' + GUIp_i18n.forum_subs_sound_notif + '" />' +
			'</td>' +
			'<td class="ca inv stat">' + sub.posts + '</td>' +
			'<td class="lp">' +
				'<abbr class="updated" title="' + GUIp.common.formatTime(date,'fakejson') + '">' + GUIp.common.formatTime(date,'forum') + '</abbr> ' +
				worker.GUIp_i18n.forum_subs_by + ' <span class="author"><strong class="fn">' + sub.by + '</strong></span> ' +
				'<span><a href="/forums/show_topic/' + tid + '?page=' + page + '#guip_' + (sub.posts + 25 - page*25) + '">' + worker.GUIp_i18n.forum_subs_view + '</a></span>' +
			'</td></tr>';
	}
	insertTopicRow(content);
	content = null;
	updateSubscriptionsStats(subscriptions,topics);
	addSmallElements();
	addLinks();
	if ((elm = document.querySelector('table.topics'))) {
		GUIp.common.addListener(elm, 'click', function(ev) {
			var checkbox = ev.target,
				mask = checkbox.dataset.eMask;
			if (!mask) return;

			var subscriptions = GUIp.common.parseJSON(storage.get('ForumSubscriptions')) || {},
				tid = checkbox.parentNode.dataset.eTid,
				sub = subscriptions[tid];
			if (!sub) return;

			// typeof mask === 'string'
			if (checkbox.checked) {
				sub.notifications |= mask;
			} else {
				sub.notifications &= ~mask;
			}
			storage.set('ForumSubscriptions', JSON.stringify(subscriptions));
		});
	}
};
var insertTopicRow = function(content) {
	var spinner;
	if ((spinner = document.getElementById('subs_spinner'))) {
		spinner.parentNode.removeChild(spinner);
	}
	document.querySelector('table.topics tbody').insertAdjacentHTML('beforeend',content);
};
var updateSubscriptionsStats = function(subscriptions, keys) {
	var topics, interval, rapid = 0, subtitle = document.querySelector('#content .subtitle');
	if (!subtitle) {
		return;
	}
	for (i = 0, len = keys.length; i < len; i++) {
		if (subscriptions[keys[i]].rapid) {
			rapid++;
		}
	}
	interval = Math.ceil((keys.length - rapid)/(20 - rapid)) * 3;
	subtitle.textContent = worker.GUIp_i18n.forum_subs_count + keys.length + (keys.length > 0 ? (rapid > 0 ? worker.GUIp_i18n.forum_subs_rapid + rapid : '') + worker.GUIp_i18n.forum_subs_intv + interval + ' ' + worker.GUIp_i18n.format_time_minute : '');
	subtitle.style.display = '';
};
var addSubscriptionsLink = function() {
	if (!document.getElementById('content')) {
		return;
	}
	var lnk = document.createElement('a'), div = document.createElement('div');
	lnk.href = '/forums/show/1/#guip_subscriptions';
	lnk.textContent = worker.GUIp_i18n.forum_subs.toLowerCase() + ' UI+';
	lnk.title = worker.GUIp_i18n.forum_subs + ' UI+';
	div.insertBefore(lnk,null);
	div.style.textAlign = 'center';
	div.style.marginTop = '0.5em';
	document.getElementById('content').insertBefore(div,null);
};
var getTotalPosts = function() {
	try {
		return +/\d+/.exec($q('#content .subtitle').textContent.replace(',', ''))[0];
	} catch (e) {
		return 0;
	}
};
