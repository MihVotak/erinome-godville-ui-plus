// topic formatting
var val = '', ss = 0, se = 0, pageSelection = '';

/**
 * @param {!HTMLTextAreaElement} editor
 */
var initEditor = function(editor) {
	val = editor.value;
	ss = editor.selectionStart;
	se = editor.selectionEnd;
	var selection = worker.getSelection();
	pageSelection = selection.isCollapsed ? '' : selection.toString().trim();
};

/**
 * @param {!HTMLTextAreaElement} editor
 */
var unsetSelection = function(editor) {
	if (editor.selectionDirection === 'backward') {
		se = ss;
	} else {
		ss = se;
	}
};

var trimSelection = function() {
	var c = '';
	while (ss !== se && ((c = val[ss]) === ' ' || c == '\t' || c === '\n')) {
		ss++;
	}
	while (ss !== se && ((c = val[se - 1]) === ' ' || c == '\t' || c === '\n')) {
		se--;
	}
};

var deselectListMarkerOrBlockTag = function() {
	if (ss && val[ss - 1] !== '\n') return;
	// cannot use /\s/ since it also matches '\xA0' (nbsp) and other stuff
	var regex = /(?:[*#]+|(?:[a-z][a-z0-9]*|[Цц][Тт])\.\.?) [ \t]*/g,
		m;
	regex.lastIndex = ss;
	if ((m = regex.exec(val)) && m.index === ss && regex.lastIndex <= se) {
		ss = regex.lastIndex;
	}
};

var expandSelectionToLine = function() {
	if (ss) ss = val.lastIndexOf('\n', ss - 1) + 1;
	var pos = val.indexOf('\n', se);
	se = pos >= 0 ? pos : val.length;
};

/**
 * @param {string} separator
 * @returns {string}
 */
var getSelectedText = function(separator) {
	if (ss === se) return pageSelection;
	var result = val.slice(ss, se);
	if (pageSelection) {
		// text is selected both in the textarea and on the page. why would someone do that?..
		// anyway, just concatenate them.
		result += separator;
		result += pageSelection;
	}
	return result;
};

/**
 * @param {!HTMLTextAreaElement} editor
 * @param {string} text
 */
var replaceSelectedText = function(editor, text) {
	editor.setSelectionRange(ss, se);
	editor.focus();
	try {
		// for Chrome and Edge; supports Ctrl+Z this way
		// one day it may work in Firefox too: https://bugzilla.mozilla.org/show_bug.cgi?id=1220696
		if (document.execCommand('insertText', false, text)) {
			return;
		}
	} catch (e) { }
	try {
		editor.setRangeText(text); // for Firefoxes; older ones support Ctrl+Z, while newer ones do not
	} catch (e) {
		editor.value = val.slice(0, ss) + text + val.slice(se); // for Opera
	}
};

/**
 * @param {string} text
 * @param {number} pos
 * @returns {number}
 */
var consumeLineBreak = function(text, pos) {
	var len = text.length,
		start = pos,
		c = '';
	do {
		if (pos === len) return pos - start; // assume implicit line break at the end
		c = text[pos++];
	} while (c === ' ' || c === '\t');
	return c === '\n' ? pos - start : -1;
};

/**
 * @param {string} text
 * @param {number} pos
 * @returns {boolean}
 */
var hasLineBreakBefore = function(text, pos) {
	var c = '';
	do {
		if (!pos) return true; // assume implicit line break at the start
		c = text[--pos];
	} while (c === ' ' || c === '\t');
	return c === '\n';
};

/**
 * @param {string} text
 * @param {number} pos
 * @returns {number}
 */
var consumeListMarker = function(text, pos) {
	var len = text.length,
		start = pos,
		c = '';
	if (pos === len) return -1;
	c = text[pos];
	if (c !== '*' && c !== '#') return -1;
	do {
		if (++pos === len) return -1;
		c = text[pos];
	} while (c === '*' || c === '#');
	return c === ' ' ? pos - start : -1;
};

/**
 * @param {string} text
 * @returns {!Array<number>}
 */
var findParaBreaks = function(text) {
	var result = [],
		rx = /[ \t\n]*\n(?:[*#]+ |[ \t]*\n(?:(?:[a-z][a-z0-9]*|[Цц][Тт])\.\.? )?)[ \t]*/g,
		m;
	while ((m = rx.exec(text))) {
		result.push(m.index, rx.lastIndex);
	}
	return result;
};

/**
 * @param {string} text
 * @returns {{hasNonList: boolean, positions: !Array<number>}}
 */
var findListBreaks = function(text) {
	var hasNonList = false,
		positions = [],
		insideList = false,
		len = text.length,
		pos = 0,
		step = 0;
	while (pos !== len) {
		if ((step = consumeListMarker(text, pos)) >= 0) { // this line has a list marker
			insideList = true;
			positions.push(pos, (pos += step));
		} else if ((step = consumeLineBreak(text, pos)) >= 0) { // this is a paragraph break
			insideList = false;
			pos += step;
			continue;
		} else if (!insideList) {
			hasNonList = true;
			positions.push(pos, pos);
		} // otherwise, this line is a part of the (multiline) list item seen earlier
		pos = text.indexOf('\n', pos) + 1;
		if (!pos) break;
	}
	return {hasNonList: hasNonList, positions: positions};
};

/**
 * @param {string} text
 * @param {number} pos
 * @returns {boolean}
 */
var isListItemAt = function(text, pos) {
	if (!pos++) {
		return consumeListMarker(text, 0) >= 0;
	}
	// search for a paragraph boundary backwards
	do {
		pos = text.lastIndexOf('\n', pos - 2);
		if (consumeListMarker(text, pos + 1) >= 0) {
			return true;
		}
	} while (pos >= 2 && !hasLineBreakBefore(text, pos));
	return false;
};

/**
 * @param {!Array<*>} leftAndRight
 * @param {!HTMLTextAreaElement} editor
 * @param {!Event} ev
 */
var insertInlineTag = function(leftAndRight, editor, ev) {
	ev.preventDefault();
	initEditor(editor);
	trimSelection();
	deselectListMarkerOrBlockTag();
	var selection = getSelectedText(' '),
		s = '',
		precedingChar = ss ? val[ss - 1] : '',
		// '"' is a delimiter as well, but we exclude it since text processors can silently
		// replace it with '“' or '”', which are not delimiters
		delimiters = ' \t\n`~!#$%^&()-=[]/\\|;:.,<?',
		restrictedTag = !!leftAndRight[2],
		needsIsolation = restrictedTag ? (
			!!precedingChar && precedingChar !== ' ' && precedingChar !== '\n'
		) : (
			// similarly, '...' counts as a delimiter, but '…' does not
			!delimiters.includes(precedingChar) || val.endsWith('...', ss)
		),
		needsIsolationRight = se !== val.length && (
			!delimiters.includes(val[se]) || val.startsWith('...', se) || (restrictedTag && val[se] === '\t')
		),
		paraBreaks = findParaBreaks(selection),
		i = 0,
		len = 0,
		pos = 0,
		lastBreak = '';
	if ((len = paraBreaks.length)) {
		// inline formatting, as its name suggests, cannot span across paragraphs, so we need to close and reopen it
		if (needsIsolation) {
			s = '['; // guaranteed not to be at the start of a paragraph
		}
		// i = 0;
		do {
			s += leftAndRight[0];
			s += selection.slice(pos, paraBreaks[i]);
			s += leftAndRight[1];
			if (needsIsolation) {
				// may only get here for the first paragraph
				s += ']';
				needsIsolation = false;
			}
			pos = paraBreaks[i + 1];
			s += lastBreak = selection.slice(paraBreaks[i], pos);
		} while ((i += 2) < len);
		// if we start a paragraph with a '[', that paragraph vanishes.
		// for those tags that allow unbalanced brackets, we omit the opening one.
		if (needsIsolationRight && restrictedTag) {
			// for those that do not, we prepend 'p. ' to it if we have to
			s += /[^ \t\n]/.test(lastBreak) ? '[' : 'p. [';
		}
		s += leftAndRight[0];
		s += selection.slice(pos);
	} else {
		// easy case: selected text does not span across paragraphs
		if (needsIsolation || needsIsolationRight) {
			if (restrictedTag) {
				// this tag requires balanced brackets
				s = ss && (precedingChar !== '\n' || !hasLineBreakBefore(val, ss - 1)) ? '[' : 'p. [';
			} else if (precedingChar && precedingChar !== '\n') {
				// we may omit opening bracket for this tag.
				// for consistency, omit after any newline, not only at paragraph boundaries.
				s = '[';
			}
		}
		s += leftAndRight[0];
		s += selection;
	}
	pos = ss + s.length;
	s += leftAndRight[1];
	if (needsIsolation || needsIsolationRight) {
		s += ']';
	}
	replaceSelectedText(editor, s);
	editor.setSelectionRange(pos, pos);
};

/**
 * @param {string} tagName
 * @param {!HTMLTextAreaElement} editor
 * @param {!Event} ev
 */
var insertBlockTag = function(tagName, editor, ev) {
	ev.preventDefault();
	initEditor(editor);
	trimSelection();
	if (!pageSelection) {
		expandSelectionToLine();
	}
	var selection = getSelectedText('\n\n'),
		s = '',
		extended = /\n[ \t]*\n/.test(selection),
		newCursorPos = 0,
		rx, m;
	if (ss) {
		// there should be a blank line before the block
		if (val[ss - 1] !== '\n') {
			s = '\n\n';
		} else if (!hasLineBreakBefore(val, ss - 1)) {
			s = '\n';
		}
	}
	s += tagName;
	s += extended ? '.. ' : '. ';
	s += selection;
	if (extended && (
		!(rx = /(?:[ \t\n]*\n)?(?:[a-z][a-z0-9]*|[Цц][Тт])\.\.? /g, rx.lastIndex = se, m = rx.exec(val)) ||
		m.index !== se
	)) {
		// close extended block unless it is already closed exactly at this place
		s += '\n\np. ';
		if (se !== val.length && val[se] === '\n') {
			se += Math.max(0, consumeLineBreak(val, se + 1)) + 1;
		}
		// put the cursor at the end of the block or after 'p. '
		newCursorPos = ss + s.length - (se !== val.length ? 5 : 0);
	} else {
		newCursorPos = ss + s.length;
		if (se !== val.length) {
			// there should be a blank line after the block
			if (val[se] !== '\n') {
				s += '\n\n';
			} else if (consumeLineBreak(val, se + 1) < 0) {
				s += '\n';
			}
		}
	}
	replaceSelectedText(editor, s);
	editor.setSelectionRange(newCursorPos, newCursorPos);
};

/**
 * @param {string} listMarker
 * @param {!HTMLTextAreaElement} editor
 * @param {!Event} ev
 */
var insertList = function(listMarker, editor, ev) {
	ev.preventDefault();
	initEditor(editor);
	trimSelection();
	if (!pageSelection) {
		expandSelectionToLine();
	}
	var selection = getSelectedText('\n'),
		s = '',
		inMidLine = false,
		lb = findListBreaks(selection),
		listBreaks = lb.positions,
		i = 0,
		len = listBreaks.length,
		pos = 0,
		markerPos = 0;
	if (ss && ((inMidLine = val[ss - 1] !== '\n') || !hasLineBreakBefore(val, ss - 1))) {
		// put a blank line before the list unless it's preceded by another list item
		switch (inMidLine - isListItemAt(val, ss - 1)) {
			case 0: s = '\n'; break;
			case 1: s = '\n\n'; break;
		}
	}
	if (!len) {
		// the cursor is placed on a blank line, and nothing is selected on the page
		s += listMarker;
		s += ' ';
	} else {
		if (!lb.hasNonList) {
			// every selected line has a marker. create an inner list.
			for (i = 1; i < len; i += 2) {
				markerPos = listBreaks[i];
				s += selection.slice(pos, markerPos);
				s += listMarker;
				pos = markerPos;
			}
		} else {
			// there are lines without a marker in selection. create an outer list.
			for (i = 0; i < len; i += 2) {
				markerPos = listBreaks[i];
				s += selection.slice(pos, markerPos);
				s += listMarker;
				if (markerPos === listBreaks[i + 1]) {
					s += ' ';
				}
				pos = markerPos;
			}
		}
		s += selection.slice(pos);
	}
	pos = ss + s.length;
	if (se !== val.length) {
		// put a blank line after the list unless it's followed by another list item
		if (val[se] !== '\n') {
			s += consumeListMarker(val, se) >= 0 ? '\n' : '\n\n';
		} else if (consumeLineBreak(val, se + 1) < 0 && consumeListMarker(val, se + 1) < 0) {
			s += '\n';
		}
	}
	replaceSelectedText(editor, s);
	editor.setSelectionRange(pos, pos);
};

/**
 * @param {null} dummy
 * @param {!HTMLTextAreaElement} editor
 * @param {!Event} ev
 */
var insertBr = function(dummy, editor, ev) {
	ev.preventDefault();
	initEditor(editor);
	unsetSelection(editor);
	var s = '<br>' + pageSelection,
		pos = ss + s.length;
	replaceSelectedText(editor, s);
	editor.setSelectionRange(pos, pos);
};

/**
 * @param {!Element} panel
 * @param {!HTMLTextAreaElement} editor
 */
var setClickActions = function(panel, editor) {
	var handlers = {
		bold: {func: insertInlineTag, params: ['*', '*']},
		underline: {func: insertInlineTag, params: ['+', '+']},
		strike: {func: insertInlineTag, params: ['-', '-', true]},
		italic: {func: insertInlineTag, params: ['_', '_']},
		godname: {func: insertInlineTag, params: ['"', '":пс']},
		link: {func: insertInlineTag, params: ['"', '":']},
		sup: {func: insertInlineTag, params: ['^', '^', true]},
		sub: {func: insertInlineTag, params: ['~', '~', true]},
		monospace: {func: insertInlineTag, params: ['@', '@', true]},
		bq: {func: insertBlockTag, params: 'bq'},
		bc: {func: insertBlockTag, params: 'bc'},
		pre: {func: insertBlockTag, params: 'pre'},
		ul: {func: insertList, params: '*'},
		ol: {func: insertList, params: '#'},
		br: {func: insertBr, params: null}
	};
	var buttons = panel.getElementsByClassName('formatting'),
		button, classes, h;
	for (var i = 0, len = buttons.length; i < len; i++) {
		button = buttons[i];
		classes = button.classList;
		for (var j = 0, jlen = classes.length; j < jlen; j++) {
			if ((h = handlers[classes[j]])) { // can omit .hasOwnProperty check since we generate them ourselves
				GUIp.common.addListener(button, 'click', h.func.bind(null, h.params, editor));
				break;
			}
		}
	}
};

/**
 * @param {boolean} onForum
 */
var addFormattingButtons = function(onForum) {
	var container, editor, panel;
	if (onForum) {
		container = $id('post_body_editor');
		editor = $id('post_body');
	} else {
		container = $id('post-content-inplaceeditor');
		editor = container.getElementsByClassName('editor_field')[0];
	}
	container.insertAdjacentHTML('afterbegin',
		'<div>' +
			'<button class="formatting button bold" title="' + worker.GUIp_i18n.bold_hint + '">' + worker.GUIp_i18n.bold + '</button>' +
			'<button class="formatting button underline" title="' + worker.GUIp_i18n.underline_hint + '">' + worker.GUIp_i18n.underline + '</button>' +
			'<button class="formatting button strike" title="' + worker.GUIp_i18n.strike_hint + '">' + worker.GUIp_i18n.strike + '</button>' +
			'<button class="formatting button italic" title="' + worker.GUIp_i18n.italic_hint + '">' + worker.GUIp_i18n.italic + '</button>' +
			'<button class="formatting bq" title="' + worker.GUIp_i18n.quote_hint + '">bq.</button>' +
			'<button class="formatting bc" title="' + worker.GUIp_i18n.code_hint + '">bc.</button>' +
			'<button class="formatting pre" title="' + worker.GUIp_i18n.pre_hint + '">pre.</button>' +
			(onForum && GUIp_locale === 'ru' ? '<button class="formatting button godname" title="Вставить ссылку на бога"></button>' : '') +
			'<button class="formatting button link" title="' + worker.GUIp_i18n.link_hint + '">a</button>' +
			'<button class="formatting button ul" title="' + worker.GUIp_i18n.unordered_list_hint + '">•</button>' +
			'<button class="formatting button ol" title="' + worker.GUIp_i18n.ordered_list_hint + '">1.</button>' +
			'<button class="formatting button br" title="' + worker.GUIp_i18n.br_hint + '"></button>' +
			'<button class="formatting button sup" title="' + worker.GUIp_i18n.sup_hint + '">X<sup>2</sup></button>' +
			'<button class="formatting button sub" title="' + worker.GUIp_i18n.sub_hint + '">X<sub>2</sub></button>' +
			'<button class="formatting button monospace" title="' + worker.GUIp_i18n.monospace_hint + '">' + worker.GUIp_i18n.monospace + '</button>' +
		'</div>'
	);
	panel = container.firstChild;
	if (!onForum) {
		// hide panel when previewing
		GUIp.common.newMutationObserver(function() {
			panel.classList[editor.offsetParent ? 'remove' : 'add']('hidden');
		}).observe(editor, {attributes: true, attributeFilter: ['class', 'style']});
	}
	setClickActions(panel, editor);
};

var fixGodnamePaste = function() {
	if (!worker.jQuery) {
		return;
	}
	worker.ReplyForm.add_name = function(name,e) {
		e.preventDefault();
		var pos = 0,
			editor, storedMsg;
		if (document.getElementById('edit').style.display === 'none' || !(editor = document.getElementById('edit_body'))) {
			editor = document.getElementById('post_body');
			if (document.getElementById('reply').style.display === 'none') {
				worker.ReplyForm.show();
				if (!editor.value && (storedMsg = localStorage.fmsgl)) {
					editor.value = storedMsg;
					editor.setSelectionRange(0, 0);
				}
			}
		}
		initEditor(editor);
		unsetSelection(editor);
		replaceSelectedText(editor, '*' + name + '*, ');
		pos = ss + name.length + 4;
		editor.setSelectionRange(pos, pos);
	};
	var godnameLinks = document.querySelectorAll('.vcard .gravatar a'),
		len = godnameLinks.length,
		handlers = Object.create(null),
		name = '',
		link, handler;
	if (len && $(godnameLinks[0]).off) {
		for (var i = 0; i < len; i++) {
			link = godnameLinks[i];
			$(link).off('click');
			name = link.dataset.gname;
			if (!(handler = handlers[name])) {
				handler = handlers[name] = GUIp.common.try2.bind(null, worker.ReplyForm.add_name, name);
			}
			link.addEventListener('click', handler);
		}
	}
};
