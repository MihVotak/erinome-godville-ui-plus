// ui_words
var ui_words = worker.GUIp.words = {};

ui_words.currentPhrase = '';
// gets words from phrases.js file and splits them into sections
ui_words.init = function() {
	var sect, text, customSects = ['pets','chosen_monsters','special_monsters','custom_craft','custom_informers'];
	if (ui_data.isFight) {
		customSects = ['ally_blacklist','custom_informers'];
	}
	this.base = worker.GUIp_words();
	for (sect in this.base.phrases) {
		text = ui_storage.get('CustomPhrases:' + sect);
		if (text) {
			this.base.phrases[sect] = text.split('||');
		}
	}
	for (sect in customSects) {
		text = ui_storage.get('CustomWords:' + customSects[sect]);
		if (text) {
			try {
				this.base[customSects[sect]] = JSON.parse(text,function(key,value) {
					return value === 'Infinity' ? Infinity : value;
				});
				if (customSects[sect] === 'custom_informers') {
					for (var i = 0, len = this.base.custom_informers.length; i < len; i++) {
						if (/[sldarqwn]/i.test(this.base.custom_informers[i].type)) {
							var sum = 0;
							for (var j = 0; j < this.base.custom_informers[i].type.length; j++) {
								switch (this.base.custom_informers[i].type[j].toUpperCase()) {
									case 'S': sum |= 1; break;
									case 'L': sum |= 16; break;
									case 'D': sum |= 32; break;
									case 'A': sum |= 64; break;
									case 'R': sum |= 128; break;
									case 'Q': sum |= 256; break;
									case 'W': sum |= 512; break;
									case 'N': sum |= 1024; break;
								}
							}
							this.base.custom_informers[i].type = sum;
						}
					}
					ui_informer.recompileCustomInformers(this.base.custom_informers);
				}
			} catch (error) {
				GUIp.common.error('error while parsing custom words section "' + customSects[sect] + '", resetting...');
				ui_storage.remove('CustomWords:' + customSects[sect]);
			}
		}
	}
};
ui_words._changeFirstLetter = function(text) {
	return text.charAt(0).toLowerCase() + text.slice(1);
};
ui_words._addHeroName = function(text) {
	if (!ui_storage.getFlag('Option:useHeroName')) { return text; }
	return ui_data.char_name + ', ' + ui_words._changeFirstLetter(text);
};
ui_words._addExclamation = function(text) {
	if (!ui_storage.getFlag('Option:useExclamations')) { return text; }
	return ui_utils.getRandomItem(this.base.phrases.exclamation) + ', ' + ui_words._changeFirstLetter(text);
};
// single phrase gen
ui_words._randomPhrase = function(sect) {
	return ui_utils.getRandomItem(this.base.phrases[sect]);
};
// single phrase gen for inverted direction
ui_words._randomPhraseInvDir = function(sect) {
	var replacement = [];
	switch (sect) {
		case 'go_north': sect = 'go_south'; replacement = ['↓','↑']; break;
		case 'go_south': sect = 'go_north'; replacement = ['↑','↓']; break;
		case 'go_west': sect = 'go_east'; replacement = ['→','←']; break;
		case 'go_east': sect = 'go_west'; replacement = ['←','→']; break;
	}
	if (ui_storage.getFlag('Option:disableInvertedArrows')) {
		return ui_utils.getRandomItem(this.base.phrases[sect]);
	} else {
		return ui_utils.getRandomItem(this.base.phrases[sect]).replace(replacement[0],replacement[1]);
	}
};
ui_words._longPhrase_recursion = function(source, len) {
	while (source.length) {
		var next = ui_utils.popRandomItem(source);
		var remainder = len - next.length - 2; // 2 for ', '
		if (remainder > 0) {
			return [next].concat(ui_words._longPhrase_recursion(source, remainder));
		}
	}
	return [];
};
// main phrase constructor
ui_words.longPhrase = function(sect, item_name, len) {
	if (ui_storage.getFlag('phrasesChanged')) {
		ui_words.init();
		ui_storage.set('phrasesChanged', 'false');
	}
	if (!ui_data.isFight && ['heal', 'pray', 'hit'].includes(sect)) {
		sect += '_field';
	}
	var prefix = ui_words._addHeroName(ui_words._addExclamation(''));
	var phrases;
	if (item_name) {
		phrases = [ui_words._randomPhrase(sect) + ' ' + item_name + '!'];
	} else if (sect.includes('go_')) {
		if (document.getElementById('map') && document.getElementById('map').textContent.match(/Противоречия|Disobedience/)) {
			phrases = [ui_words._randomPhraseInvDir(sect)];
		} else {
			phrases = [ui_words._randomPhrase(sect)];
		}
	} else if (ui_storage.getFlag('Option:useShortPhrases')) {
		phrases = [ui_words._randomPhrase(sect)];
	} else {
		phrases = ui_words._longPhrase_recursion(this.base.phrases[sect].slice(), (len || 100) - prefix.length);
	}
	this.currentPhrase = prefix ? prefix + ui_words._changeFirstLetter(phrases.join(' ')) : phrases.join(' ');
	return this.currentPhrase;
};
// inspect button phrase gen
ui_words.inspectPhrase = function(item_name) {
	return ui_words.longPhrase('inspect_prefix', item_name);
};
// craft button phrase gen
ui_words.craftPhrase = function(items) {
	return ui_words.longPhrase('craft_prefix', items);
};
// mnemonic voices processing
ui_words.mnemoVoice = function() {
	if (ui_storage.getFlag('phrasesChanged')) {
		ui_words.init();
		ui_storage.set('phrasesChanged', 'false');
	}
	if (!this.base.phrases.mnemonics.length) {
		return;
	}
	var i, len, result, voiceInput = ui_utils.voiceInput.value.split('//'),
		defResult = this.base.phrases.mnemonics[0].split('//')[0];
	if (!ui_utils.voiceInput.value) {
		ui_utils.setVoice('// ' + defResult.trim(), true);
		return;
	}
	if (!voiceInput[1]) {
		voiceInput.unshift('');
	}
	result = this.mnemoLookup(voiceInput[1].trim());
	if (result) {
		ui_utils.setVoice((voiceInput[0].trim() + ' // ' + result.trim()).trim(), true);
	};
};
// mnemonics lookup
ui_words.mnemoLookup = function(request) {
	var i, len, mnemoline;
	// searching through mnemonics, return first case-insensitive match
	for (i = 0, len = this.base.phrases.mnemonics.length; i < len; i++) {
		mnemoline = this.base.phrases.mnemonics[i].split('//');
		if (mnemoline[1] && (request.toLowerCase() === mnemoline[1].trim().toLowerCase())) {
			return mnemoline[0];
		}
	}
	// full text search, return one phrase after full-string match
	for (i = 0, len = this.base.phrases.mnemonics.length; i < len; i++) {
		if (request === this.base.phrases.mnemonics[i].split('//')[0].trim()) {
			if ((i + 1) < len) {
				return this.base.phrases.mnemonics[i + 1].split('//')[0];
			} else {
				return this.base.phrases.mnemonics[0].split('//')[0];
			}
		}
	}
	// partial text search, return first case-insensitive partial match
	for (i = 0, len = this.base.phrases.mnemonics.length; i < len; i++) {
		mnemoline = this.base.phrases.mnemonics[i].split('//');
		if (mnemoline[0].trim().toLowerCase().includes(request.toLowerCase())) {
			return mnemoline[0];
		}
	}
	return null;
};
// Checkers
ui_words.usableItemType = function(desc) {
	return this.base.usable_items.descriptions.indexOf(desc);
};
ui_words.usableItemTypeMatch = function(desc,itype) {
	return (desc === this.base.usable_items.descriptions[this.base.usable_items.types.indexOf(itype)]);
};
