// ui_mining
var ui_mining = GUIp.mining = {};

ui_mining.init = function() {
	var bossName;
	if (!ui_data.isMining) {
		return;
	}
	ui_mining.bitsPerByte = ui_stats.Bits_Per_Byte();
	for (var i = 0; i < 4; i++) {
		bossName = ui_stats.EnemySingle_Name(i + 1);
		if (bossName[0] === '@') {
			ui_mining.ownBoss = i;
			ui_mining._bossTitles[i] = '$&: ';
		} else {
			ui_mining._bossTitles[i] = (
				bossName.slice(4, -1) + ' (' + (ui_stats.EnemySingle_Godname(i + 1) || '?') + '): '
			).replace(/\$/g, '$$$$');
		}
	}
};

/*
	map legend:

	0x0 - ( )   empty
	0x1 - (?)   unknown
	0x2 - (#)   wall
	0x3 - (#11) wall containing bits
	0x4 - (1)   bit
	0x5 - (11)  bits
	0x6 - (+)   repair kit
	0x7 - (x)   pusher
	0x8 - (A)   boss
	0x9 - (B)   boss
	0xA - (C)   boss
	0xB - (D)   boss

	0x10 - (+-=) divine influence
*/

ui_mining._symbolCodes = {
	'#': 0x2,
	'1': 0x4,
	'+': 0x6,
	'x': 0x7,
	'A': 0x8,
	'B': 0x9,
	'C': 0xA,
	'D': 0xB
};

/**
 * @readonly
 * @type {number}
 */
ui_mining.ownBoss = -1;

/**
 * @readonly
 * @type {number}
 */
ui_mining.bitsPerByte = 8;

/**
 * @private
 * @type {!Array<string>}
 */
ui_mining._bossTitles = [];

/**
 * @param {!HTMLCollection} cells
 * @returns {!Array<number>}
 */
ui_mining.parseMap = function(cells) {
	var result = [],
		symbolCodes = ui_mining._symbolCodes,
		mod = 0x0,
		text = '',
		cell, classes;
	for (var i = 0, len = cells.length; i < len; i++) {
		cell = cells[i];
		classes = cell.classList;
		if (classes.contains('rmve')) {
			result[i] = 0x1; // ?
			continue;
		}
		mod = cell.getElementsByClassName('dm2')[0] ? 0x10 : 0x0; // +-=
		cell = cell.firstElementChild;
		text = cell ? cell.textContent : '';
		result[i] = mod | (
			text === '11' ? (
				classes.contains('dmw') ? 0x3 : 0x5 // #11 | 11
			) : text === '@' ? (
				0x8 | ui_mining.ownBoss // ABCD
			) : (
				symbolCodes[text] | 0
			)
		);
	}
	return result;
};

ui_mining._tmpArr = new Int8Array(4);

/**
 * @private
 * @param {!Array<number>} collected
 * @param {!Array<number>} oldMap
 * @param {!Array<number>} newMap
 */
ui_mining._migrateCollected = function(collected, oldMap, newMap) {
	var cur = 0x0,
		prev = 0x0,
		i = 0,
		len = oldMap.length,
		pickedUp = ui_mining._tmpArr;
	pickedUp[0] = pickedUp[1] = pickedUp[2] = pickedUp[3] = 0;
	// process dropped bits first
	for (i = 0; i < len; i++) {
		cur = newMap[i];
		if (cur <= 0x2) continue; // fast path
		if ((cur & 0xF) === 0x4) { // a bit
			prev = oldMap[i];
			if ((prev & 0xC) === 0x8 && collected[prev &= 0x3] % ui_mining.bitsPerByte) {
				// there was a boss, and now there is a bit
				collected[prev]--;
			}
		} else if ((cur & 0xC) === 0x8) { // a boss
			prev = oldMap[i] & 0xF;
			if (prev >= 0x3 && prev <= 0x5) {
				// there were bits, and now there is a boss
				pickedUp[cur & 0x3] = prev === 0x4 ? 1 : 2;
			}
		} else if ((cur &= 0xF) === 0x3 || cur === 0x5) { // bits
			prev = oldMap[i];
			if ((prev & 0xC) === 0x8) {
				// there was a boss, and now there are bits. time flowed backwards :(
				collected[prev & 0x3] -= 2; // can break a byte
			}
		}
	}
	// then process picked up bits
	for (i = 0; i < 4; i++) {
		collected[i] += pickedUp[i];
	}
};

/**
 * @private
 * @param {!Array<number>} newMap
 * @returns {{map: !Array<number>, collected: !Array<number>}}
 */
ui_mining._migrate = function(newMap) {
	var key = 'Log:' + ui_stats.logId() + ':data',
		data = GUIp.common.parseJSON(ui_storage.get(key)) || {
			map: [],
			collected: [0, 0, 0, 0]
		};
	ui_mining._migrateCollected(data.collected, data.map, newMap);
	data.collected[ui_mining.ownBoss] = ui_stats.Bits(); // luckily, at least for our own boss we have a reliable info source
	data.map = newMap;
	ui_storage.set(key, JSON.stringify(data));
	return data;
};

/**
 * @returns {?HTMLCollection}
 */
ui_mining.getStepMarks = function() {
	var bar = document.getElementById('turn_pbar');
	return bar && bar.getElementsByClassName('st_div');
};

ui_mining.updateMap = function() {
	var replaceWithAlpha = ui_storage.getList('Option:rangeMapSettings').includes('balph'),
		cells = document.getElementById('r_map').getElementsByClassName('rmc'),
		data = ui_mining._migrate(ui_mining.parseMap(cells)),
		map = data.map,
		stepMarks = ui_mining.getStepMarks(),
		hideStepMark = ui_mining._tmpArr,
		i = 0,
		len = 0,
		num = 0,
		bits = 0,
		cell;
	hideStepMark[0] = hideStepMark[1] = hideStepMark[2] = 1;
	for (i = 0, len = map.length; i < len; i++) {
		if (((num = map[i]) & 0xC) !== 0x8) {
			continue; // not a boss
		}
		num &= 0x3;
		hideStepMark[(num + 3 - ui_mining.ownBoss) & 0x3] = 0; // the boss is alive; do not hide
		bits = data.collected[num];
		cell = cells[i];
		cell.title = cell.title.replace(/(?:ваш|чужой) босс|(?:Your|Other) Boss|$/,
			ui_mining._bossTitles[num] +
			(bits >= ui_mining.bitsPerByte ? Math.floor(bits / ui_mining.bitsPerByte) + '+[' : '[') + bits % ui_mining.bitsPerByte + ']'
		);
		if (replaceWithAlpha && num === ui_mining.ownBoss && (cell = cell.firstElementChild)) {
			cell.textContent = String.fromCharCode(num + 65); // 'A'
		}
	}
	if (stepMarks) {
		for (i = 0, len = stepMarks.length; i < len; i++) {
			stepMarks[i].classList[hideStepMark[i] ? 'add' : 'remove']('hidden');
		}
	}
};
