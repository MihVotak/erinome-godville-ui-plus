// ui_informer
var ui_informer = worker.GUIp.informer = {};

/**
 * @private
 * @property {!Array<?GUIp.expr.CompiledExpr>} exprs
 * @property {!Array<(string|!Array<(string|!GUIp.expr.CompiledExpr>))>} titles
 */
ui_informer._compiled = {exprs: [], titles: []};

ui_informer._lgcc = '';
ui_informer._lgcid = '';
ui_informer._debugLastExpr = '';

ui_informer.init = function() {
	// container
	document.getElementById('main_wrapper').insertAdjacentHTML('afterbegin', '<div id="informer_bar"></div>');
	this.container = document.getElementById('informer_bar');
	// load
	ui_informer._load();
	// custom data
	ui_informer.initCustomInformersData();
	// forcefully update page title
	GUIp.common.setTimeout(function() { ui_utils.setTitle(ui_informer._getTitleNotices() + ui_data.docTitle); }, 100);
	// run custom informers at least once a minute, at xx:xx:00 (can be slightly postponed by a browser engine)
	ui_informer._initTimer();
};
ui_informer._initTimer = function() {
	GUIp.common.setTimeout(ui_informer._initTimerCallback, (60 - ui_utils.getPreciseTime(0, true).getSeconds()) * 1e3);
};
ui_informer._initTimerCallback = function() {
	ui_informer.updateCustomInformers();
	ui_informer._initTimer();
};
ui_informer._load = function() {
	this.activeInformers = JSON.parse(ui_storage.get('Option:activeInformers') || '{}');
	this.flags = JSON.parse(ui_storage.get('informer_flags') || '{}');
	for (var flag in this.flags) {
		if (this.flags[flag].state === 'e' || (flag.startsWith('ci ') && !this.getCustomInformer(flag))) {
			delete this.flags[flag];
		}
	}
	ui_informer._save();
};
ui_informer._save = function() {
	ui_storage.set('informer_flags', JSON.stringify(this.flags));
};
ui_informer._createLabel = function(flag) {
	var div = document.createElement('div');
	div.id = flag.replace(/ /g, '_');
	div.textContent = this.flags[flag].text ? this.flags[flag].text : flag.replace(/^ci /,'');
	GUIp.common.addListener(div, 'click', function(e) {
		ui_informer.hide(flag);
		ui_utils.hideNotification(flag);
		e.stopPropagation();
	});
	this.container.insertBefore(div,null);
};
ui_informer._deleteLabel = function(flag) {
	var label = document.getElementById(flag.replace(/ /g, '_'));
	if (label) {
		if (label.dataset.snInterval) {
			worker.clearInterval(label.dataset.snInterval);
		}
		this.container.removeChild(label);
	}
};
ui_informer._renameLabel = function(flag,text) {
	var label = document.getElementById(flag.replace(/ /g, '_'));
	if (label) {
		label.textContent = text;
	}
};
ui_informer._tick = function() {
	// iterate through all the flags and choose enabled ones
	var ci, activeFlags = [];
	for (var flag in this.flags) {
		if (this.flags[flag].state === 'e') {
			// custom informers with type 'N' (non-active) should never flash in document title
			if (flag.startsWith('ci ') && (ci = this.getCustomInformer(flag)) && (ci.type & 0x400)) {
				continue;
			}
			activeFlags.push(this.flags[flag].text || flag.replace(/^ci /,''));
		}
	}
	activeFlags.sort();
	// update title if there're active informers
	if (activeFlags.length) {
		ui_informer._updateTitle(activeFlags);
		this.tref = GUIp.common.setTimeout(ui_informer._tick.bind(ui_informer), 700);
	} else {
		ui_informer.clearTitle();
		this.tref = 0;
	}
};
ui_informer.clearTitle = function() {
	var ci;
	for (var flag in this.flags) {
		// it is fine to clear title when 'N'-type informer is still active
		if (flag.startsWith('ci ') && (ci = this.getCustomInformer(flag)) && (ci.type & 0x400)) {
			continue;
		}
		if (this.flags[flag].state === 'e') {
			return;
		}
	}
	ui_utils.setTitle(ui_informer._getTitleNotices() + ui_data.docTitle);
};
ui_informer._getTitleNotices = function() {
	var forbidden_title_notices = ui_storage.getList('Option:forbiddenTitleNotices');
	var titleNotices = (!forbidden_title_notices.includes('pm') ? ui_informer._getPMTitleNotice() : '') +
					   (!forbidden_title_notices.includes('gm') ? ui_informer._getGMTitleNotice() : '') +
					   (!forbidden_title_notices.includes('fi') ? ui_informer._getFITitleNotice() : '');
	return titleNotices ? titleNotices + ' ' : '';
};
ui_informer._getPMTitleNotice = function() {
	var pm = 0,
		pm_badge = document.querySelector('.fr_new_badge_pos');
	if (pm_badge && pm_badge.style.display !== 'none') {
		pm = +pm_badge.textContent;
	}
	var stars = document.querySelectorAll('.msgDock .fr_new_msg');
	for (var i = 0, len = stars.length; i < len; i++) {
		if (!stars[i].parentNode.getElementsByClassName('dockfrname')[0].textContent.match(/Гильдсовет|Guild Council/)) {
			pm++;
		}
	}
	return pm ? '[' + pm + ']' : '';
};
ui_informer._getGMTitleNotice = function() {
	var gm = document.getElementsByClassName('gc_new_badge')[0].style.display !== 'none',
		stars = document.querySelectorAll('.msgDock .fr_new_msg');
	for (var i = 0, len = stars.length; i < len; i++) {
		if (stars[i].parentNode.getElementsByClassName('dockfrname')[0].textContent.match(/Гильдсовет|Guild Council/)) {
			gm = true;
			break;
		}
	}
	return gm ? '[g]' : '';
};
ui_informer._getFITitleNotice = function() {
	return document.querySelector('#forum_informer_bar a') ? '[f]' : '';
};
ui_informer._updateTitle = function(activeFlags) {
	this.odd_tick = !this.odd_tick;
	var sep = this.odd_tick ? '!!!' : '...';
	this.tref_title = this._getTitleNotices() + sep + ' ' + activeFlags.join('! ') + ' ' + sep;
	ui_utils.setTitle(this.tref_title);
};
ui_informer.redrawTitle = function() {
	if (ui_storage.getFlag('Option:discardTitleChanges')) {
		ui_data.docTitle = ui_data.docTitle.replace(/\((\d+)([!@~\+])\)/,'($2)');
	}
	if (this.tref && this.tref_title) {
		ui_utils.setTitle(this.tref_title);
	} else {
		ui_utils.setTitle(this._getTitleNotices() + ui_data.docTitle);
	}
};
ui_informer.getInformerType = function(flag) {
	var type;
	if (flag.startsWith('ci ')) {
		var informer = this.getCustomInformer(flag);
		if (informer && informer.type > 1) {
			type = informer.type || 0;
		} else {
			type = this.activeInformers['custom_informers'] || 0;
		}
	} else {
		type = this.activeInformers[flag.replace(/ /g, '_')] || 0;
	}
	return type;
};
ui_informer.activateLabelNotification = function(flag,type) {
	if (!(type & 0x10)) {
		return;
	}
	ui_informer._createLabel(flag);
	if (!this.tref) {
		ui_informer._tick();
	}
};
ui_informer.activateDesktopNotification = function(flag,type) {
	if (!(type & 0x20)) {
		return;
	}
	if (ui_storage.getFlag('Option:enableInformerAlerts') && (worker.Notification && worker.Notification.permission === "granted" || worker.GUIp_browser === 'Chrome')) {
		var title = '[☆] ' + ui_data.god_name,
			text = (this.flags[flag].text ? this.flags[flag].text : flag.replace(/^ci /,'')),
			callback = function(){ui_informer.hide(flag);};
		ui_utils.showNotification(title,text,callback,flag);
	}
	if (!(type & 0x10)) {
		this.flags[flag].state = 'd';
	}
};
ui_informer.activateSoundNotification = function(flag,type) {
	if (!(type & 0xC0)) {
		return;
	}
	GUIp.common.playSound(ui_storage.get('Option:informerCustomSound') || 'arena');
	if (!(type & 0x10)) {
		this.flags[flag].state = 'd';
	} else if (type & 0x80) {
		var snLabel = document.getElementById(flag.replace(/ /g, '_'));
		if (snLabel) {
			snLabel.dataset.snInterval = GUIp.common.setInterval(GUIp.common.playSound.bind(null, ui_storage.get('Option:informerCustomSound') || 'arena'), 7e3);
		}
	}
};
ui_informer.update = function(flag, value, text) {
	if (value) {
		var infType = this.getInformerType(flag);
		if ((this.activeInformers[flag.replace(/ /g, '_')] || flag.startsWith('ci ')) && (flag === 'fight' || flag === 'low health' || flag.startsWith('ci ') || !(ui_data.isFight && !ui_data.isDungeon && !ui_data.isSail)) &&
			!(flag === 'much gold' && ui_stats.hasTemple() && ui_stats.townName()) &&
			!(flag === 'smelter' && this.flags['smelt!'] && this.flags['smelt!'].state === 'e') &&
			!(flag === 'transformer' && this.flags['transform!'] && this.flags['transform!'].state === 'e') &&
			!(ui_data.inShop && (infType & 0x800))) {
			if (this.flags[flag] === undefined) {
				this.flags[flag] = {state: 'e'};
				if (text) {
					this.flags[flag].text = text;
				}
				this.activateLabelNotification(flag,infType);
				this.activateDesktopNotification(flag,infType);
				this.activateSoundNotification(flag,infType);
				ui_informer._save();
			} else if (this.flags[flag].state === 's') {
				this.flags[flag].state = 'e';
				this.activateLabelNotification(flag,infType);
				ui_informer._save();
			}
		} else if (this.flags[flag] && this.flags[flag].state === 'e') {
			this.flags[flag].state = 's';
			ui_informer._deleteLabel(flag);
			ui_informer._save();
		}
	} else if (this.flags[flag] !== undefined) {
		delete this.flags[flag];
		ui_informer._deleteLabel(flag);
		ui_utils.hideNotification(flag);
		ui_informer._save();
	}
};
ui_informer.hide = function(flag) {
	if (this.flags[flag]) {
		this.flags[flag].state = 'd';
	}
	ui_informer._deleteLabel(flag);
	if (flag === 'selected town') {
		delete this.flags[flag];
		ui_improver.distanceInformerReset();
	}
	ui_informer._save();
};
ui_informer.getCustomInformer = function(title) {
	title = title.replace(/^ci /,'');
	return ui_words.base.custom_informers.find(function(informer) { return informer.title === title; });
};

/**
 * @private
 * @param {!Array<{text: string, e: !Error}>} errors
 */
ui_informer._reportCustomInformerErrors = function(errors) {
	var len = errors.length;
	if (!len) return;
	var s = '<ul>';
	for (var i = 0; i < len; i++) {
		s +=
			'<li><div class="e_custom_informer_text">' + GUIp.common.escapeHTML(errors[i].text) +
			'</div><div class="e_custom_informer_error">' + GUIp.common.escapeHTML(errors[i].e.toString()) +
			'</div></li>';
	}
	ui_utils.showMessage('custom_informers_errors', {
		title: GUIp_i18n.custom_informers_check,
		content: '<div style="text-align: left;">' + GUIp_i18n.custom_informers_non_trivial_errors + s + '</ul></div>'
	});
};

/**
 * @private
 * @param {!Array<{q: boolean, title: string, expr: string}>} informers
 */
ui_informer.recompileCustomInformers = function(informers) {
	var errors = [], ast, fragments, fr;
	this._compiled.exprs.length = this._compiled.titles.length = 0;
	for (var i = 0, len = informers.length; i < len; i++) {
		var informer = informers[i];
		this._compiled.exprs[i] = null;
		this._compiled.titles[i] = '';
		if (informer.q) continue;

		try {
			ast = ui_expr.compile(informer.expr, true);
		} catch (e) {
			errors.push({text: informer.expr, e: e});
			ast = null;
		}
		try {
			fragments = ui_expr.compileEmbedded(informer.title);
		} catch (e) {
			errors.push({text: informer.title, e: e});
			continue;
		}
		if (!ast) continue;

		this._compiled.exprs[i] = ast;
		this._compiled.titles[i] =
			fragments.length === 1 && typeof (fr = fragments[0]) === 'string' && !/\bgv\.\w/.test(fr) ? fr : fragments;
	}
	this._reportCustomInformerErrors(errors);
};

ui_informer.updateCustomInformers = function() {
	if (!this.activeInformers['custom_informers']) {
		return;
	}
	var cache = ui_expr.makeCache(this.CIDstate),
		errors = [],
		informer, state, title, computedTitle;
	for (var i = 0, len = ui_words.base.custom_informers.length; i < len; i++) {
		if (!this._compiled.exprs[i]) {
			continue;
		}

		informer = ui_words.base.custom_informers[i];
		// evaluate the condition
		try {
			state = !!ui_expr.eval(this._compiled.exprs[i], this.CIDstate, cache);
		} catch (e) {
			errors.push({text: informer.expr, e: e});
			// user-written code resuled in an exception; ignore that informer
			continue;
		}

		title = informer.title;
		if (!state) {
			computedTitle = null;
		} else if (typeof this._compiled.titles[i] === 'string') {
			computedTitle = this._compiled.titles[i];
		} else {
			// evaluate the dynamic title
			try {
				computedTitle = ui_expr.evalEmbedded(this._compiled.titles[i], this.CIDstate, cache);
			} catch (e) {
				errors.push({text: title, e: e});
				// if an informer has fired, we must notify the user even if the title is messy
				computedTitle = title;
			}

			// check if informer text has changed
			if (this.flags['ci ' + title] !== undefined && this.flags['ci ' + title].text !== computedTitle) {
				switch (this.flags['ci ' + title].state) {
				case 'e':
				case 's':
					if (informer.type & 0x200) {
						// remove everything with flag 'W'
						delete this.flags['ci ' + title];
						ui_informer._deleteLabel('ci ' + title);
					} else {
						// or just rename label if it exists
						this.flags['ci ' + title].text = computedTitle;
						ui_informer._renameLabel('ci ' + title, computedTitle);
					}
					break;
				case 'd':
					// kill current label and flag of revivable-type informer to regenerate it from scratch as a new one
					if (informer.type & 0x300) {
						delete this.flags['ci ' + title];
						ui_informer._deleteLabel('ci ' + title);
					}
					break;
				}
				ui_informer._save();
			}
		}
		if ((informer.type % 2) === 0) {
			this.update('ci ' + title, state, computedTitle);
		} else {
			if (state) {
				this.update('ci ' + title, true, computedTitle);
			} else if (this.flags['ci ' + title] && this.flags['ci ' + title].state === 'd') {
				this.update('ci ' + title, false, computedTitle);
			}
		}
	}
	// clear guildchat last message cache
	if (this._lgcc) {
		this._lgcc = '';
	}
	if (errors.length && !document.getElementById('msgcustom_informers_errors')) {
		this._reportCustomInformerErrors(errors);
	}
};

/**
 * @private
 * @param {!Array<string>} impl
 * @param {!Array<string>} iface
 * @returns {{status: string, method: string}}
 */
ui_informer._checkConsistency = function(impl, iface) {
	for (var i = 0, len = Math.max(impl.length, iface.length); i < len; i++) {
		if (impl[i] !== iface[i]) {
			if (impl[i] && (!iface[i] || impl[i] < iface[i])) {
				return {status: 'undeclared', method: impl[i]};
			} else {
				return {status: 'unimplemented', method: iface[i]};
			}
		}
	}
	return {status: 'ok', method: ''};
};

ui_informer.initCustomInformersData = function() {
	this.CIDstate = {
		get health() { return ui_stats.HP(); },
		get healthMax() { return ui_stats.Max_HP(); },
		get healthPrc() { return Math.floor(100 * this.health / (this.healthMax - 1e-6)); },
		get gold() { return ui_stats.Gold(); },
		get supplies() { return ui_stats.Map_Supplies(); },
		get suppliesMax() { return ui_stats.Map_MaxSupplies(); },
		get suppliesPrc() { return Math.floor(100 * this.supplies / (this.suppliesMax - 1e-6)); },
		get inventory() { return ui_stats.Inv(); },
		get inventoryMax() { return ui_stats.Max_Inv(); },
		get inventoryPrc() { return Math.floor(100 * this.inventory / (this.inventoryMax - 1e-6)); },
		inventoryHasItem: ui_inventory.hasItem.bind(ui_inventory),
		inventoryHasType: ui_inventory.hasType.bind(ui_inventory),
		inventoryCountLike: ui_inventory.countLike.bind(ui_inventory),
		get inventoryHealing() { return ui_inventory.getHealingItems(); },
		get inventoryUnsaleable() { return this.inventoryUnsellable; },
		get inventoryUnsellable() { return ui_inventory.getUnsellableItems(); },
		get exp() { return ui_stats.Exp(); },
		get expTrader() { return ui_stats.Trader_Exp(); },
		get portDistance() { return ui_improver.sailPortDistance || 0; },
		get auraName() { return ui_stats.auraName(); },
		get auraDuration() { return ui_stats.auraDuration(); },
		get bingoItems() { return ui_inventory.getBingoItems(); },
		get bingoSlotsLeft() {
			var bingoItems = ui_storage.get('BingoItems');
			return bingoItems ? bingoItems.split('|').length : 0;
		},
		get bingoTriesLeft() { return parseInt(ui_storage.get('BingoTries')) || 0; },
		get enemyHealth() { return ui_stats.Enemy_HP(); },
		get enemyHealthMax() { return ui_stats.Enemy_MaxHP(); },
		get enemyHealthPrc() { return Math.floor(100 * this.enemyHealth / (this.enemyHealthMax - 1e-6)); },
		get enemyCount() { return ui_stats.Enemy_Count(); },
		get enemyAliveCount() { return ui_stats.Enemy_AliveCount(); },
		get enemyAbilitiesCount() { return ui_stats.Enemy_AbilitiesCount(); },
		get enemyGold() { return ui_stats.Enemy_Gold(); },
		enemyHasAbility: ui_stats.Enemy_HasAbility.bind(ui_stats),
		enemyHasAbilityLoc: ui_stats.Enemy_HasAbilityLoc.bind(ui_stats),
		get alliesHealth() { return ui_stats.Hero_Alls_HP(); },
		get alliesHealthMax() { return ui_improver.sumAllsHP || ui_stats.Hero_Alls_MaxHP(); },
		get alliesHealthPrc() { return Math.floor(100 * this.alliesHealth / (this.alliesHealthMax - 1e-6)); },
		get alliesCount() { return ui_stats.Hero_Alls_Count(); },
		get alliesAliveCount() { return ui_stats.Hero_Alls_AliveCount(); },
		get alliesAliveHealthMax() { return ui_stats.Hero_Alls_AliveMaxHP(); },
		get lowHealth() { return ui_data.isDungeon ? ui_stats.lowHealth('dungeon') : (ui_data.isSail ? ui_stats.lowHealth('sail') : (ui_data.isFight ? ui_stats.lowHealth('fight') : false)); },
		get godpower() { return ui_stats.Godpower(); },
		get godpowerMax() { return ui_stats.Max_Godpower(); },
		get godpowerPrc() { return Math.floor(100 * this.godpower / this.godpowerMax); },
		get charges() { return ui_stats.Charges(); },
		get arenaAvailable() { return ui_stats.isArenaAvailable(); },
		get sparAvailable() { return ui_stats.isSparAvailable(); },
		get dungeonAvailable() { return ui_stats.isDungeonAvailable(); },
		get sailAvailable() { return ui_stats.isSailAvailable(); },
		get miningAvailable() { return ui_stats.isMiningAvailable(); },
		get arenaSendDelay() { return ui_stats.sendDelayArena(); },
		get sparSendDelay() { return ui_stats.sendDelaySpar(); },
		get dungeonSendDelay() { return ui_stats.sendDelayDungeon(); },
		get sailSendDelay() { return ui_stats.sendDelaySail(); },
		get miningSendDelay() { return ui_stats.sendDelayMine(); },
		get fightMode() { return ui_stats.fightType(); },
		get fightType() { return ui_stats.fightType(); },
		get fightStep() { return ui_stats.currentStep(); },
		get sailConditions() {
			if (!ui_improver.islandsMap.manager) return '';
			return Object.keys(ui_improver.islandsMap.manager.conditions).toString();
		},
		get cargo() { return ui_stats.cargo(); },
		get pushReadiness() { return ui_stats.Push_Readiness(); },
		get bits() { return ui_stats.Bits() - ui_stats.Bytes() * ui_mining.bitsPerByte; },
		get bytes() { return ui_stats.Bytes(); },
		get bitsPerByte() { return ui_stats.Bits_Per_Byte(); },
		get bookBytes() { return ui_stats.Book_Bytes() - ui_stats.Book_Words()*4; },
		get bookWords() { return ui_stats.Book_Words(); },
		get inBossFight() { return ui_stats.isBossFight(); },
		get inFight() { return ui_stats.isFight(); },
		get inShop() { return ui_data.inShop; },
		get inTown() { return ui_stats.townName().length > 0; },
		get nearestTown() { return ui_improver.dailyForecast.includes('gvroads') ? (worker.GUIp_locale === 'ru' ? 'Годвилль' : 'Godville') : ui_stats.nearbyTown(); },
		get currentTown() { return ui_stats.townName(); },
		get mileStones() { return ui_stats.mileStones(); },
		get poiMileStones() { return ui_stats.poiMileStones(); },
		get poiDistance() { return ui_stats.poiDistance(); },
		get lastNews() { return ui_stats.lastNews(); },
		get lastDiary() {
			var lastDiary = ui_stats.lastDiaryRealEntry(0, true);
			return lastDiary ? lastDiary.textContent : '';
		},
		get lastDiaryVoice() {
			var lastDiary = ui_stats.lastDiary();
			for (var i = 0; i < lastDiary.length; i++) {
				if (lastDiary[i].includes('☣')) {
					return lastDiary[i];
				}
			}
			return '';
		},
		get lastGuildChat() {
			if (ui_informer._lgcc) {
				return ui_informer._lgcc;
			}
			var msgsc, msgs = ui_utils.getLastGCM();
			if (!msgs.length) {
				return '';
			}
			msgsc = msgs.length - 1;
			if (!ui_informer._lgcid) {
				ui_informer._lgcid = JSON.stringify(msgs[msgsc]);
				return '';
			}
			var found = false, filtered = '';
			for (var i = 0, len = msgs.length; i < len; i++) {
				if (JSON.stringify(msgs[i]) === ui_informer._lgcid) {
					found = true;
					continue;
				}
				if (!found || msgs[i].a === ui_data.god_name) {
					continue;
				}
				filtered += msgs[i].a + ': ' + msgs[i].c + '\n';
			}
			if (!found) {
				for (var i = 0, len = msgs.length; i < len; i++) {
					if (msgs[i].a !== ui_data.god_name) {
						filtered += msgs[i].a + ': ' + msgs[i].c + '\n';
					}
				}
			}
			ui_informer._lgcid = JSON.stringify(msgs[msgsc]);
			return filtered && (ui_informer._lgcc = filtered);
		},
		get isGoingBack() { return ui_stats.isGoingBack(); },
		get isGoingForth() { return ui_improver.detectors.stateGTF.res; },
		get isGoingGodville() { return this.isGoingToGodville; },
		get isGoingToGodville() { return ui_improver.detectors.stateGTG.res; },
		get isFishing() { return ui_improver.detectors.stateFishing.res; },
		get isTrading() { return ui_improver.detectors.stateTP.res; },
		isForecast: function(type) {
			if (!GUIp.common.forecastRegexes.hasOwnProperty(type)) {
				if (type === 'nolaying') { // legacy name
					type = 'noconversion';
				} else {
					throw new Error('unknown category in gv.isForecast: "' + type + '"');
				}
			}
			return ui_improver.dailyForecast.includes(type);
		},
		get dailyForecast() { return ui_improver.dailyForecastText; },
		get hasTemple() { return ui_stats.hasTemple(); },
		get hasArk() { return  ui_stats.hasArk(); },
		get monstersKilled() { return +ui_storage.get('Logger:Monster') || 0; },
		get currentMonster() { return ui_stats.monsterName(); },
		get chosenMonster() { return ui_stats.chosenMonster(); },
		get specialMonster() { return ui_stats.specialMonster(); },
		get strongMonster() { return ui_improver.detectors.stateSM.res; },
		get tamableMonster() { return ui_stats.tamableMonster(); },
		get wantedMonster() { return ui_stats.wantedMonster(); },
		get questName() { return ui_stats.Task_Name(); },
		get questNumber() { return ui_stats.Task_Number(); },
		get questProgress() { return ui_stats.Task(); },
		get sideJobName() { return ui_stats.Side_Job_Name(); },
		get sideJobDuration() { return ui_stats.Side_Job_Duration(); },
		get sideJobProgress() { return ui_stats.Side_Job(); },
		get petKnockedOut() { return ui_stats.petIsKnockedOut(); },
		get expTimeout() {
			return Math.max(0, Math.ceil((ui_timers.getGuaranteeInfo().conversion.optimistic[0] - Date.now()) / 60e3 - 1e-6));
		},
		get logTimeout() {
			return Math.max(0, Math.ceil((ui_timers.getGuaranteeInfo().dungeon.optimistic[0] - Date.now()) / 60e3 - 1e-6));
		},
		get byteTimeout() {
			return Math.max(0, Math.ceil((ui_timers.getGuaranteeInfo().mining.optimistic[0] - Date.now()) / 60e3 - 1e-6));
		},
		get byteDoubleTimeout() {
			return Math.max(0, Math.ceil((ui_timers.getGuaranteeInfo().mining.optimistic[1] - Date.now()) / 60e3 - 1e-6));
		},
		get sparTimeout() {
			return Math.max(0, Math.ceil((ui_timers.getGuaranteeInfo().spar.optimistic[0] - Date.now()) / 60e3 - 1e-6));
		},
		get getSeconds() { return ui_utils.getPreciseTime().getSeconds(); },
		get getMinutes() { return ui_utils.getPreciseTime().getMinutes(); },
		get getHours() { return ui_utils.getPreciseTime().getHours(); },
		get getHoursUTC() { return ui_utils.getPreciseTime().getUTCHours(); },
		get getHoursMSK() { return ui_utils.getPreciseTime(10800e3).getUTCHours(); },
		get getDay() { return ui_utils.getPreciseTime().getDay() || 7; },
		get getDayUTC() { return ui_utils.getPreciseTime().getUTCDay() || 7; },
		get getDayMSK() { return ui_utils.getPreciseTime(10800e3).getUTCDay() || 7; },
		get voiceCooldown() { return Math.max(0,Math.ceil((ui_timeout.finishDate - Date.now()) / 1000 - 1e-6)); },
		get windowFocused() { return ui_utils.windowFocused; }
	};

	var result = this._checkConsistency(Object.keys(this.CIDstate).sort(), GUIp.common.expr.gvAPI);
	if (result.status !== 'ok') {
		ui_utils.showMessage('gv_api_consistency_error', {
			title: GUIp_i18n.error_message_title,
			content: '<strong>' + result.method + (
				result.status === 'undeclared' ? (
					'</strong> is not declared in <strong>GUIp.common.expr.gvAPI</strong> <em>(common/expr/gv_api.js)</em>.'
				) : '</strong> is not implemented in <strong>GUIp.informer.CIDstate</strong> <em>(superhero/informer.js)</em>.'
			)
		});
	}
};

ui_informer.checkCustomExpression = function() {
	var n = ui_words.base.custom_informers.length;
	var input = worker.prompt(worker.GUIp_i18n.custom_informers_input, this._debugLastExpr || (
		n ? ui_words.base.custom_informers[Math.floor(Math.random() * n)].expr : 'gv.healthPrc > 70 || gv.godpower == gv.godpowerMax'
	));
	if (!input) return;
	this._debugLastExpr = input;

	ui_utils.showMessage('custom_expression_check', {
		title: GUIp_i18n.custom_informers_check,
		content:
			'<div style="text-align: left;">' +
				'<div>' + GUIp_i18n.custom_informers_check_expr +
					'<span class="e_custom_informer_text">' + GUIp.common.escapeHTML(input) + '</span>' +
				'</div><br />' +
				ui_infdebug.formatExprHTML(input, this.CIDstate, ui_expr.makeCache(this.CIDstate)) +
			'</div>'
	});
};
