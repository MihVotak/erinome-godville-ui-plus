// ui_infdebug
var ui_infdebug = GUIp.infdebug = {};

/**
 * @private
 * @param {*} value
 * @param {boolean} [verbose]
 * @returns {string}
 */
ui_infdebug._formatValue = function(value, verbose) {
	var type = typeof value, s = '';
	if (type === 'string') {
		// value = value.replace(/\n/g, '\\n').replace(/[\\"]/g, '\\$&');
		if (verbose || value.length <= 25) {
			return '&#34;' + GUIp.common.escapeHTML(value) + '&#34;';
		}
		return (
			'<abbr title="&#34;' + GUIp.common.escapeHTML(value) + '&#34;">&#34;' +
			GUIp.common.escapeHTML(value.slice(0, 24)) + '…&#34;</abbr>'
		);
	}
	if (value) {
		if (Array.isArray(value)) {
			s = value.map(ui_infdebug._formatValueVerbose).join(', ');
			if (verbose || !s) {
				return '[' + s + ']';
			} else {
				return '<abbr title="[' + s.replace(/<[^>]*>/g, '') + ']">array(' + value.length + ')</abbr>';
			}
		}
		if (type === 'function') {
			return value.name ? GUIp.common.escapeHTML(value.name) : 'function';
		}
		if (type === 'object') {
			var keys = Object.keys(value).sort(), len = keys.length;
			for (var i = 0; i < len; i++) {
				if (i === 25) {
					s += '    …\n';
					break;
				}
				var key = keys[i];
				if (/^\w+$/.test(key)) {
					s += '    ' + key + ',\n';
				} else {
					s += '    &#34;' + GUIp.common.escapeHTML(key) + '&#34;,\n';
				}
			}
			return '<abbr title="{\n' + s + '}">object(' + len + ')</abbr>';
		}
	} else if (type === 'undefined') {
		return '<span style="color: red;">undefined</span>';
	}
	return String(value); // no need to escape: the value is either boolean, number, or null
};

/**
 * @private
 * @param {*} value
 * @returns {string}
 */
ui_infdebug._formatValueVerbose = function(value) {
	return ui_infdebug._formatValue(value, true);
};

/**
 * @private
 * @param {*} value
 * @returns {string}
 */
ui_infdebug._formatProperty = function(value) {
	var s = String(value);
	if (/^[A-Za-z_]\w*$/.test(s)) {
		return '.' + s;
	} else {
		return '[</strong>' + ui_infdebug._formatValue(value) + '<strong>]';
	}
};

/**
 * @private
 * @param {{eResult: *, computed: boolean, object: {type: string, eResult: *}, property: {type: string, eResult: *}}} n
 * @returns {string}
 */
ui_infdebug._formatMember = function(n) {
	var result, valueType = typeof n.object.value;
	if (/*n.object.type === 'Literal' &&*/ (valueType === 'object' || valueType === 'function')) {
		result = GUIp.common.escapeHTML(n.object.raw);
	} else {
		result = ui_infdebug._formatValue(n.object.eResult);
	}
	return result + ui_infdebug._formatProperty(n.property.name || n.property.eResult);
};

/**
 * @param {!Array<{type: string, eResult: *}>} trace
 * @returns {string}
 */
ui_infdebug.formatTraceHTML = function(trace) {
	var s = '', line;
	for (var i = 0, len = trace.length; i < len; i++) {
		var n = trace[i];

		switch (n.type) {
		case 'Literal':
		case 'E_GVLiteral':
		case 'ArrayExpression':
			continue;

		case 'ConditionalExpression':
			line =
				ui_infdebug._formatValue(n.test.eResult) + ' <strong>?</strong>&nbsp;' +
				ui_infdebug._formatValue(n.consequent.eResult) + ' <strong>:</strong>&nbsp;' +
				ui_infdebug._formatValue(n.alternate.eResult);
			break;

		case 'UnaryExpression':
			if (typeof n.argument.eResult === 'number' && (n.operator === '-' || n.operator === '+')) {
				continue;
			}
			line =
				'<strong>' + GUIp.common.escapeHTML(n.operator) + '</strong>' +
				ui_infdebug._formatValue(n.argument.eResult);
			break;

		case 'BinaryExpression':
		case 'LogicalExpression':
			line =
				ui_infdebug._formatValue(n.left.eResult) +
				' <strong>' + GUIp.common.escapeHTML(n.operator) + '</strong>&nbsp;' +
				ui_infdebug._formatValue(n.right.eResult);
			break;

		case 'MemberExpression':
			line = '<strong>' + ui_infdebug._formatMember(n) + '</strong>';
			break;

		case 'E_GVExpression':
			line = '<strong>gv' + ui_infdebug._formatProperty(n.property.eResult) + '</strong>';
			break;

		case 'CallExpression':
			line = '<strong>';
			if (n.callee.type === 'MemberExpression') {
				line += ui_infdebug._formatMember(n.callee);
			} else {
				line += ui_infdebug._formatValue(n.callee.eResult);
			}
			line += '(</strong>';
			for (var j = 0, jlen = n.arguments.length; j < jlen; j++) {
				if (j) line += ', ';
				line += ui_infdebug._formatValue(n.arguments[j].eResult);
			}
			line += '<strong>)</strong>';
			break;

		case 'E_MatchExpression':
			line =
				ui_infdebug._formatValue(n.text.eResult) +
				' <strong>' + (n.negated ? '!' : '') + (n.insensitive ? '~*' : '~') + '</strong>&nbsp;' +
				ui_infdebug._formatValue(n.pattern.value !== undefined ? n.pattern.value : n.pattern.eResult);
			break;

		default:
			throw new Error('expression type "' + n.type + '" is unsupported in custom informers');
		}

		s += '<li>' + line + ' →&nbsp;' + ui_infdebug._formatValue(n.eResult, true) + '</li>';
	}
	return s;
};

/**
 * @private
 * @param {string} msg
 * @returns {string}
 */
ui_infdebug._formatError = function(msg) {
	return '<div>' + GUIp_i18n.custom_informers_error + ': ' + GUIp.common.escapeHTML(msg) + '.</div>';
};

/**
 * @param {!GUIp.expr.DebugInfo} debug
 * @returns {string}
 */
ui_infdebug.formatDebugHTML = function(debug) {
	var s = '<ol>' + ui_infdebug.formatTraceHTML(debug.trace) + '</ol><br />';
	if (debug.error != null) {
		s += ui_infdebug._formatError(debug.error);
	} else {
		s += '<div>' + GUIp_i18n.custom_informers_check_result + '<strong>' + !!debug.result + '</strong></div>';
	}
	return s;
};

/**
 * @param {string} text
 * @param {!Object} gv
 * @param {function(string): *} gvCache
 * @returns {string}
 */
ui_infdebug.formatExprHTML = function(text, gv, gvCache) {
	var ast;
	try {
		ast = ui_expr.compile(text, true);
	} catch (e) {
		return ui_infdebug._formatError(e.message);
	}
	return ui_infdebug.formatDebugHTML(ui_expr.debug(ast, gv, gvCache));
};
