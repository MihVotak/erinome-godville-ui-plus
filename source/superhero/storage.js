// ui_storage
var ui_storage = worker.GUIp.storage = {};

ui_storage._backend = localStorage;

var ui_tmpstorage = GUIp.tmpstorage = Object.create(ui_storage);
ui_tmpstorage._backend = sessionStorage;

ui_tmpstorage.wipeOut = function() {
	this._delete(/(?:)/);
};

ui_storage.init = function() {
	this.migrate();
	GUIp.common.addListener(worker, 'storage', ui_improver.processExternalChanges);
	if (ui_data.god_name !== sessionStorage.eGUI_CurrentUser) {
		// paranoid mode
		ui_tmpstorage.wipeOut();
		sessionStorage.eGUI_CurrentUser = ui_data.god_name;
	}
};

ui_storage._getKey = function(key) {
	return 'eGUI_' + ui_data.god_name + ':' + key;
};
// gets diff with a value
ui_storage.diff = function(id, value) {
	var diff = null;
	var old = this.get(id);
	if (old !== null && value !== null) {
		diff = value - old;
	}
	return diff;
};
// stores a value
ui_storage.set = function(id, value) {
	return (this._backend[this._getKey(id)] = value);
};
// reads a value
ui_storage.get = function(id) {
	return this._backend.getItem(this._getKey(id)); // .getItem returns null if the key was not found
};
ui_storage.getFlag = function(id) {
	return this.get(id) === 'true';
};
ui_storage.getList = function(id) {
	var s = this.get(id);
	return s ? s.split(',') : []; // because ''.split(',') is ['']
};
// deletes single item from storage
ui_storage.remove = function(id) {
	return this._backend.removeItem(this._getKey(id));
};
// stores value and gets diff with old
ui_storage.set_with_diff = function(id, value) {
	var diff = this.diff(id, value);
	this.set(id, value);
	return diff;
};
// list keys with specified prefix
ui_storage.list = function(prefix) {
	var lines = [],
		prefix = 'eGUI_' + ui_data.god_name + ':' + (prefix || ''),
		keys = Object.keys(this._backend),
		key = '';
	for (var i = 0, len = keys.length; i < len; i++) {
		key = keys[i];
		if (key.startsWith(prefix)) {
			lines.push(key.slice(key.indexOf(':') + 1));
		}
	}
	return lines;
};
// dumps all values related to current god_name
ui_storage.dump = function(selector) {
	var lines = [],
		prefix = 'eGUI_' + (selector ? ui_data.god_name + ':' + selector : ''),
		keys = Object.keys(this._backend),
		key = '';
	for (var i = 0, len = keys.length; i < len; i++) {
		key = keys[i];
		if (key.startsWith(prefix)) {
			lines.push(key + ' = ' + this._backend[key]);
		}
	}
	lines.sort();
	worker.console.info('Godville UI+: Storage:\n' + lines.join('\n'));
};
// resets saved options
ui_storage.clear = function(what) {
	if (!what || !/^(?:eGUI|Godville|All)$/.test(what)) {
		if (worker.GUIp_locale === 'ru') {
			worker.console.log('Godville UI+: использование storage.clear:\n' +
							   'storage.clear("eGUI") для удаления только настроек Godville UI+;\n' +
							   'storage.clear("Godville") для удаления настроек Годвилля, сохранив настройки Godville UI+;\n' +
							   'storage.clear("All") для удаления всех настроек.');
		} else {
			worker.console.log('Godville UI+: storage.clear usage:\n' +
							   'storage.clear("eGUI") to remove Godville UI+ settings only;\n' +
							   'storage.clear("Godville") to remove Godville settings and keep Godville UI+ settings;\n' +
							   'storage.clear("All") to remove all settings.');
		}
		return;
	}
	var keys = Object.keys(this._backend),
		key = '';
	for (var i = 0, len = keys.length; i < len; i++) {
		key = keys[i];
		if (what === 'eGUI' && key.startsWith('eGUI_') ||
			what === 'Godville' && !key.startsWith('eGUI_') ||
			what === 'All') {
			this._backend.removeItem(key);
		}
	}
	location.reload();
};
ui_storage._rename = function(from, to) {
	var keys = Object.keys(this._backend),
		key = '';
	for (var i = 0, len = keys.length; i < len; i++) {
		key = keys[i];
		if (from.test(key)) {
			this._backend[key.replace(from, to)] = this._backend[key];
			this._backend.removeItem(key);
		}
	}
};
ui_storage._delete = function(regexp) {
	var keys = Object.keys(this._backend),
		key = '';
	for (var i = 0, len = keys.length; i < len; i++) {
		key = keys[i];
		if (key.startsWith('eGUI_') && regexp.test(key)) {
			this._backend.removeItem(key);
		}
	}
};
ui_storage._iterate = function(callback) {
	var keys = Object.keys(this._backend),
		key = '',
		colon = 0;
	for (var i = 0, len = keys.length; i < len; i++) {
		key = keys[i];
		if (!key.startsWith('eGUI_')) continue;
		colon = key.indexOf(':', 5);
		callback.call(this, key, key.slice(colon >= 0 ? colon + 1 : 5), colon >= 6 ? key.slice(5, colon) : '');
	}
};
ui_storage.migrate = function() {
	var mid = localStorage.eGUI_migrated,
		midc = false,
		updateTo = function(date) {
			if (mid >= date) return false;
			mid = date;
			midc = true;
			return true;
		};
	if (!mid) {
		ui_storage._rename(/^GUIp_/, 'eGUI_');
		mid = localStorage.eGUI_migrated;
		if (!mid) {
			mid = '141115';
			midc = true;
		}
	}
	if (updateTo('150510')) {
		ui_storage._delete(/:Stats:/);
	}
	if (updateTo('151003')) {
		var topics = Object.create(null);
		ui_storage._iterate(function(key, localKey, godn) {
			if (!/Forum\d/.test(localKey)) {
				return;
			}
			Object.assign(topics[godn] || (topics[godn] = {}), JSON.parse(localStorage.getItem(key)));
			localStorage.removeItem(key);
		});
		for (var godn in topics) {
			localStorage.setItem('eGUI_' + godn + ':ForumSubscriptions', JSON.stringify(topics[godn]));
		}
	}
	if (updateTo('160310')) {
		var allInformers = ['full_godpower','much_gold','dead','low_health','fight','arena_available','dungeon_available','sail_available','selected_town','wanted_monster','special_monster','tamable_monster','chosen_monster','pet_knocked_out','close_to_boss','close_to_rival','guild_quest','mini_quest','custom_informers','arena_box','aura_box','black_box','treasure_box','boss_box','charge_box','coolstory_box','friend_box','gift_box','good_box','heal_box','invite','raidboss_box','quest_box','smelter','teleporter','temper_box','to_arena_box','transformer'];
		ui_storage._iterate(function(key, localKey, godn) {
			if (localKey === 'Option:forbiddenInformers') {
				var oldInformers = localStorage.getItem(key).split(','),
					newInformers = {};
				for (var i = 0, len = allInformers.length; i < len; i++) {
					if (!oldInformers.includes(allInformers[i])) {
						newInformers[allInformers[i]] = 48;
					}
				}
				if (newInformers['tamable_monster']) {
					newInformers['tamable_monster'] = 112;
				}
				localStorage.removeItem(key);
				localStorage.setItem('eGUI_' + godn + ':Option:activeInformers', JSON.stringify(newInformers));
			}
		});
	}
	if (updateTo('160320')) {
		ui_storage._delete(/:LEMRestrictions:/);
	}
	if (updateTo('170200')) {
		var oldValue, customWords = [['custom_informers','title'],['custom_craft','t'],['ally_blacklist','n']];
		ui_storage._iterate(function(key, localKey) {
			for (var i = 0, len = customWords.length; i < len; i++) {
				if (localKey === 'CustomWords:' + customWords[i][0]) {
					var prop = customWords[i][1];
					oldValue = JSON.parse(localStorage.getItem(key));
					for (var j = 0, len2 = oldValue.length; j < len2; j++) {
						if (oldValue[j][prop] && oldValue[j][prop][0] === '#') {
							oldValue[j].q = true;
							oldValue[j][prop] = oldValue[j][prop].slice(1);
						}
					}
					localStorage.setItem(key,JSON.stringify(oldValue));
				}
			}
		});
	}
	if (updateTo('171010')) {
		localStorage.setItem('LogDB:lastUpdate', 0);
		localStorage.setItem('LogDB:lastSerial', 0);
	}
	if (updateTo('190825')) {
		ui_storage._iterate(function(key, localKey, godn) {
			if (localKey === 'ThirdEye:LastLog' || localKey === 'ThirdEye:PenultLog') {
				var date = new Date(localStorage.getItem(key));
				if (date.getTime()) {
					var res = JSON.parse(localStorage.getItem('eGUI_' + godn + ':ThirdEye:DungeonResults') || '[]');
					res.push(date.getTime());
					res = res.sort().slice(-2);
					localStorage.setItem('eGUI_' + godn + ':ThirdEye:DungeonResults',JSON.stringify(res));
					localStorage.removeItem(key);
				}
			}
		});
	}
	if (updateTo('190830')) {
		ui_storage._iterate(function(key, localKey, godn) {
			if (localKey === 'Option:disableLayingTimer') {
				if (localStorage.getItem(key) === 'true') {
					localStorage.setItem('eGUI_' + godn + ':Option:disabledTimers','laying');
				}
				localStorage.removeItem(key);
			}
		});
	}
	if (updateTo('190902')) {
		ui_storage._delete(/:ThirdEye:/);
	}
	if (updateTo('200118')) {
		ui_storage._iterate(function(key, localKey) {
			if (localKey.startsWith('Logger:HTML:') && localStorage.getItem(key).length > 3000) {
				localStorage.removeItem(key);
			}
		});
	}
	if (updateTo('200412')) {
		ui_storage._iterate(function(key, localKey) {
			if (localKey !== 'Option:disabledTimers' &&
				localKey !== 'ThirdEye:Activities' &&
				localKey !== 'ThirdEye:ActivityStatuses'
			) return;
			localStorage.setItem(key, localStorage.getItem(key).replace(/\blaying\b/g, 'conversion'));
		});
	}
	if (midc) {
		localStorage.eGUI_migrated = mid;
	}
};
