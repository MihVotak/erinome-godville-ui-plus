// ui_stats
var ui_stats = worker.GUIp.stats = {};

ui_stats.Ark_F = function() {
	var a = parseInt(ui_utils.getStat('#hk_ark_m .l_val','pointed',2));
	return !isNaN(a) ? a : null;
};
ui_stats.Ark_M = function() {
	var a = parseInt(ui_utils.getStat('#hk_ark_m .l_val','pointed',1));
	return !isNaN(a) ? a : null;
};
ui_stats.Bricks = function() {
	var a = ui_utils.getStat('#hk_bricks_cnt .l_val','dec');
	return parseInt(a === 1000 ? 1000 : a * 10) || 0;
};
ui_stats.Bits = function() {
	var a, b = ui_utils.getStat('#hk_bl .l_val','text') || '';
	if ((a = /(?:(\d+)\+)?\[(.*?)\]/.exec(b))) {
		return (+a[1] || 0) * a[2].length  + (a[2].match(/1/g) || []).length;
	}
	return 0;
};
ui_stats.Bytes = function() {
	var a = (ui_utils.getStat('#hk_bl .l_val','text') || '').match(/(\d+)\+/);
	return a && +a[1] || 0;
};
ui_stats.Bits_Per_Byte = function() {
	var a, b = ui_utils.getStat('#hk_bl .l_val', 'text') || '';
	return (a = /\[.*?\]/.exec(b)) ? a[0].length - 2 : 8;
};
ui_stats.Book_Bytes = function() {
	var a, b = ui_utils.getStat('#hk_wrd .l_val','text') || '';
	if (a = b.match(/^(?:(.*?)% \+ )?\[(.*?)\]$/)) {
		return (+a[1]*10 || 0)*4 + 4 - (a[2].match(/\./g) || []).length;
	}
	return 0;
};
ui_stats.Book_Words = function() {
	var a = (ui_utils.getStat('#hk_wrd .l_val','text') || '').match(/^(.*?)% \+ \[(.*?)\]$/);
	return a && +a[1]*10 || 0;
};
ui_stats.Charges =
ui_stats.Map_Charges =
ui_stats.Hero_Charges = function() {
	return ui_utils.getStat('#cntrl .acc_val','num') || 0;
};
ui_stats.Death = function() {
	return ui_utils.getStat('#hk_death_count .l_val','num') || 0;
};
ui_stats.Enemy_Bossname = function() {
	return ui_utils.getStat('#o_hk_name .l_val a','text') || '';
};
ui_stats.Enemy_Godname = function() {
	return ui_utils.getStat('#o_hk_godname .l_val a','text') || '';
};
ui_stats.Enemy_Gold = function() {
	return ui_utils.getStat('#o_hk_gold_we .l_val','numre') || 0;
};
ui_stats.Enemy_HP = function() {
	var a, b = document.querySelectorAll('#opps .opp_h'),
		hp = 0;
	if (!b.length) {
		return +ui_utils.getStat('#o_hl1 .l_val','slashed',1) || 0;
	}
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			hp += +a[1] || 0;
		}
	}
	return hp;
};
ui_stats.Enemy_MaxHP = function() {
	var a, b = document.querySelectorAll('#opps .opp_h'),
		mhp = 0;
	if (!b.length) {
		return +ui_utils.getStat('#o_hl1 .l_val','slashed',2) || 0;
	}
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			mhp += +a[2] || 0;
		}
	}
	return mhp;
};
ui_stats.Enemy_Inv = function() {
	return +ui_utils.getStat('#o_hk_inventory_num .l_val','slashed',1) || 0;
};
ui_stats.EnemySingle_HP = function(enemy) {
	var a, b = document.querySelectorAll('#opps .opp_h, #bosses .opp_h');
	if (b[enemy-1] && (a = b[enemy-1].textContent.match(/^(.+?) \/ (.+?)$/))) {
		return +a[1] || 0;
	}
	return 0;
};
ui_stats.EnemySingle_MaxHP = function(enemy) {
	var a, b = document.querySelectorAll('#opps .opp_h, #bosses .opp_h');
	if (b[enemy-1] && (a = b[enemy-1].textContent.match(/^(.+?) \/ (.+?)$/))) {
		return +a[2] || 0;
	}
	return 0;
};
ui_stats.EnemySingle_Name = function(enemy) {
	var a, b = document.querySelectorAll('#opps .opp_n:not(.opp_ng), #bosses .opp_n:not(.opp_ng)');
	return b[enemy-1] && b[enemy-1].textContent || '';
};
ui_stats.EnemySingle_Godname = function(enemy) {
	var a = document.querySelectorAll('#bosses .opp_ng')[enemy - 1];
	return a ? a.textContent.replace(/\s*➤/, '').slice(1, -1) : ''; // enemies can be friends as well
};
ui_stats.Enemy_AbilitiesText = function() {
	var a = document.querySelector('#o_hk_gold_we + .line:not(#o_hk_death_count) .l_val');
	return a && a.textContent || '';
};
ui_stats.Enemy_AbilitiesCount = function() {
	var a = this.Enemy_AbilitiesText();
	return a && a.split(',').length || 0;
};
ui_stats.Enemy_HasAbility = function(ability) {
	return !!this.Enemy_AbilitiesText().match(new worker.RegExp(ability,'i')) || false;
};
ui_stats.Enemy_HasAbilityLoc = function(ability) {
	return this.Enemy_HasAbility(ability);
};
ui_stats.Enemy_Count = function() {
	var a = document.querySelectorAll('#opps .opp_n:not(.opp_ng), #bosses .opp_n:not(.opp_ng)');
	return a && a.length || 0;
};
ui_stats.Enemy_AliveCount = function() {
	var a, b = document.querySelectorAll('#opps .opp_h, #bosses .opp_h'),
		alive = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			alive++;
		}
	}
	return alive;
};
ui_stats.Equip1 = function() {
	return ui_utils.getStat('#eq_0 .eq_level','num') || 0;
};
ui_stats.Equip2 = function() {
	return ui_utils.getStat('#eq_1 .eq_level','num') || 0;
};
ui_stats.Equip3 = function() {
	return ui_utils.getStat('#eq_2 .eq_level','num') || 0;
};
ui_stats.Equip4 = function() {
	return ui_utils.getStat('#eq_3 .eq_level','num') || 0;
};
ui_stats.Equip5 = function() {
	return ui_utils.getStat('#eq_4 .eq_level','num') || 0;
};
ui_stats.Equip6 = function() {
	return ui_utils.getStat('#eq_5 .eq_level','num') || 0;
};
ui_stats.Equip7 = function() {
	return ui_utils.getStat('#eq_6 .eq_level','num') || 0;
};
ui_stats._isEquipBold = function(selector) {
	var a = document.querySelector(selector);
	return !!(a && a.classList.contains('eq_b'));
};
ui_stats.Equip1_IsBold = ui_stats._isEquipBold.bind(null, '#eq_0 .eq_name');
ui_stats.Equip2_IsBold = ui_stats._isEquipBold.bind(null, '#eq_1 .eq_name');
ui_stats.Equip3_IsBold = ui_stats._isEquipBold.bind(null, '#eq_2 .eq_name');
ui_stats.Equip4_IsBold = ui_stats._isEquipBold.bind(null, '#eq_3 .eq_name');
ui_stats.Equip5_IsBold = ui_stats._isEquipBold.bind(null, '#eq_4 .eq_name');
ui_stats.Equip6_IsBold = ui_stats._isEquipBold.bind(null, '#eq_5 .eq_name');
ui_stats.Equip7_IsBold = ui_stats._isEquipBold.bind(null, '#eq_6 .eq_name');
ui_stats.Exp =
ui_stats.Map_Exp =
ui_stats.Hero_Exp = function() {
	return ui_utils.getStat('#hk_level .p_bar','titlenumre') || 0;
};
ui_stats.Godpower = function() {
	return ui_utils.getStat('#cntrl .gp_val','num') || 0;
};
ui_stats.Gold =
ui_stats.Map_Gold =
ui_stats.Hero_Gold = function() {
	return ui_utils.getStat('#hk_gold_we .l_val','numre') || 0;
};
ui_stats.Map_Alls_HP =
ui_stats.Hero_Alls_HP = function() {
	var a, b = document.querySelectorAll('#alls .opp_h'),
		hp = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			hp += +a[1] || 0;
		}
	}
	return hp;
};
ui_stats.Map_Ally_HP =
ui_stats.Hero_Ally_HP = function(ally) {
	var a, b = document.querySelectorAll('#alls .opp_h');
	if (b[ally-1]) {
		if (a = b[ally-1].textContent.match(/^(.+?) \/ (.+?)$/)) {
			return +a[1] || 0;
		}
		if (a = b[ally-1].textContent.match(/(defeated|повержен)/)) {
			return 1;
		}
		if (a = b[ally-1].textContent.match(/(exited|evacuated|вышел|отбуксирован)/)) {
			return null;
		}
	}
	return 0;
};
ui_stats.Hero_Alls_MaxHP = function() {
	var a, b = document.querySelectorAll('#alls .opp_h'),
		mhp = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			mhp += +a[2] || 0;
		}
	}
	return mhp;
};
ui_stats.Map_Ally_MaxHP =
ui_stats.Hero_Ally_MaxHP = function(ally) {
	var a, b = document.querySelectorAll('#alls .opp_h');
	if (b[ally-1] && (a = b[ally-1].textContent.match(/^(.+?) \/ (.+?)$/))) {
		return +a[2] || 0;
	}
	return 0;
};
ui_stats.Hero_Alls_Count = function() {
	var a = document.querySelectorAll('#alls .opp_n:not(.opp_ng)');
	return a && a.length || 0;
};
ui_stats.Hero_Alls_AliveCount = function() {
	var a, b = document.querySelectorAll('#alls .opp_h'),
		alive = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			alive++;
		}
	}
	return alive;
};
ui_stats.Hero_Alls_AliveMaxHP = function() {
	var a, b = document.querySelectorAll('#alls .opp_h'),
		mhp = 0;
	for (var i = 0, len = b.length; i < len; i++) {
		if (a = b[i].textContent.match(/^(.+?) \/ (.+?)$/)) {
			mhp += +a[2] || 0;
		}
	}
	return mhp;
};
ui_stats.Hero_Ally_Name = function(ally) {
	var a, b = document.querySelectorAll('#alls .opp_n:not(.opp_ng)');
	if (ui_data.isSail) {
		return b[ally-1] && b[ally-1].textContent.slice(3) || '';
	} else {
		return b[ally-1] && b[ally-1].textContent || '';
	}
};
ui_stats.Hero_Ally_Names = function() {
	return Array.from(document.querySelectorAll('#alls .opp_n:not(.opp_ng)'),
		ui_data.isSail ? function(a) { return a.textContent.slice(3); } : function(a) { return a.textContent; }
	);
};
ui_stats.HP =
ui_stats.Map_HP =
ui_stats.Hero_HP = function() {
	return +ui_utils.getStat((ui_data.isMining ? '#hk_bhp' : '#hk_health')+' .l_val','slashed',1) || 0;
};
ui_stats.Inv =
ui_stats.Map_Inv =
ui_stats.Hero_Inv = function() {
	return +ui_utils.getStat('#hk_inventory_num .l_val','slashed',1) || 0;
};
ui_stats.Map_Supplies = function() {
	return +ui_utils.getStat('#hk_water .l_val','slashed',1) || 0;
};
ui_stats.Map_MaxSupplies = function() {
	return +ui_utils.getStat('#hk_water .l_val','slashed',2) || 0;
};
ui_stats.Lab_F = function() {
	var a = parseInt(ui_utils.getStat('.bps_mf .l_val','pointed',2));
	return !isNaN(a) ? a : null;
};
ui_stats.Lab_M = function() {
	var a = parseInt(ui_utils.getStat('.bps_mf .l_val','pointed',1));
	return !isNaN(a) ? a : null;
};
ui_stats.Level =
ui_stats.Map_Level = function() {
	return ui_utils.getStat('#hk_level .l_val','num') || 0;
};
ui_stats.Logs = function() {
	return ui_utils.getStat('#hk_wood .l_val','dec') * 10 || 0;
};
ui_stats.Max_Godpower = function() {
	return ui_data.hasShop ? 200 : 100;
};
ui_stats.Max_HP = function() {
	return +ui_utils.getStat((ui_data.isMining ? '#hk_bhp' : '#hk_health')+' .l_val','slashed',2) || 0;
};
ui_stats.Max_Inv = function() {
	return +ui_utils.getStat('#hk_inventory_num .l_val','slashed',2) || 0;
};
ui_stats.Monster = function() {
	return ui_utils.getStat('#hk_monsters_killed .l_val','num') || 0;
};
ui_stats.Pet_Level = function() {
	var a = document.querySelector('#hk_pet_class + div .l_val');
	return (a && a.parentNode.style.display !== 'none' && parseInt(a.textContent)) || 0;
};
ui_stats.Pet_NameType = function() {
	var b = document.querySelector('#hk_pet_class .l_val');
	if (!b || b.parentNode.style.display === 'none') {
		return ':';
	}
	var pName = ui_utils.getStat('#hk_pet_name .l_val','text') || '',
		pType = b.textContent;
	return (pName.match(/^(.*?)(\ «.+»)?(\ “.+”)?(\ ❌)?$/) || [null,''])[1] + ':' + pType;
};
ui_stats.Push_Readiness = function() {
	return ui_utils.getStat('#hk_bls .l_val','num') || 0;
};
ui_stats.Task = function() {
	return ui_utils.getStat('#hk_quests_completed .p_bar','titlenumre') || 0;
};
ui_stats.Task_Name = function() {
	return ui_utils.getStat('#hk_quests_completed .q_name','text') || '';
};
ui_stats.Task_Number = function() {
	return +(ui_utils.getStat('#hk_quests_completed .l_val','text') || '').slice(1) || 0;
};
ui_stats.Side_Job = function() {
	var a = document.querySelector('#hk_quests_completed + .line .p_bar');
	return (a && !ui_utils.isHidden(a) && ui_utils.parseStat(a, 'titlenumre')) || 0;
};
ui_stats.Side_Job_Name = function() {
	var a = document.querySelector('#hk_quests_completed + .line .q_name');
	return (a && !ui_utils.isHidden(a) && a.textContent) || '';
};
ui_stats.Side_Job_Duration = function() {
	var m = /(\d+):(\d+)/.exec(ui_utils.getStat('#hk_quests_completed + .line .l_val','text') || '');
	return m ? +m[1] * 3600 + +m[2] * 60 : 0;
};
ui_stats.Trader_Exp = function() {
	return ui_utils.getStat('#hk_t_level .p_bar','titlenumre') || 0;
};
ui_stats.Trader_Level = function() {
	return ui_utils.getStat('#hk_t_level .l_val','num') || 0;
};
ui_stats.Savings = function() {
	var a, b = ui_utils.getStat('#hk_retirement .l_val','text') || '';
	if (a = b.match(/^((\d+)M,? ?)?(\d+)?k?$/i)) {
		return 1000 * (a[2] || 0) + 1 * (a[3] || 0)
	} else {
		return parseInt(b) || 0;
	}
};

ui_stats.auraName = function() {
	var a, b = document.getElementById('hk_distance');
	if (b && (b = b.previousSibling) && !ui_utils.isHidden(b) && (a = /^(?:Аура|Aura)(.*?) \(.*?\)$/.exec(b.textContent))) {
		return a[1] || ''
	}
	return '';
};
ui_stats.auraDuration = function() {
	var a, b = document.getElementById('hk_distance');
	if (b && b.previousSibling && (a = b.previousSibling.textContent.match(/^(Аура|Aura).*?\((\d+):(\d+)\)$/))) {
		return (parseInt(a[2]) * 3600 + parseInt(a[3]) * 60) || 0;
	}
	return 0;
};
ui_stats.currentStep = function() {
	var a, b = document.getElementById('m_fight_log') || document.getElementById('r_map');
	return b && (b = b.getElementsByTagName('h2')[0]) && (a = /\((?:шаг|step) (\d+)\)$/.exec(b.textContent)) ? +a[1] : 0;
};
ui_stats.cargo = function() {
	return ui_utils.getStat('#hk_cargo .l_val', 'text') || '';
};
ui_stats.charName = function() {
	if (ui_data.isSail) {
		if (this._sailCharName) {
			return this._sailCharName;
		}
		var n, pt = document.querySelector('.dir_arrow');
		if (pt && (n = pt.classList.toString().match(/pl(\d)/))) {
			var a = document.querySelectorAll('#alls .opp_n:not(.opp_ng)');
			if (a[+n[1]-1] && a[+n[1]-1].textContent.slice(3)) {
				this._sailCharName = a[+n[1]-1].textContent.slice(3);
				return this._sailCharName;
			}
		}
	}
	return ui_utils.getStat('#hk_name .l_val','text') || '';
};
ui_stats.inShop = function() {
	var inShop = document.querySelectorAll('#trader .line a');
	return inShop[1] && inShop[1].style.display !== 'none' || false;
};
ui_stats.checkShop = function() {
	var a = document.querySelector('#trader > .block_content');
	return !!(a && a.firstChild);
};
ui_stats.fightType = function() {
	if (this.isFight()) {
		var mfl = document.querySelector('#m_fight_log h2');
		if (document.getElementById('map') && document.getElementById('map').style.display !== 'none') {
			return 'dungeon';
		} else if (document.getElementById('s_map') && document.getElementById('s_map').style.display !== 'none') {
			return 'sail';
		} else if (document.getElementById('r_map') && document.getElementById('r_map').style.display !== 'none') {
			return 'mining';
		} else if (mfl && /Тренировочный бой|Sparring Chronicle/.test(mfl.textContent)) {
			return 'spar';
		} else if (mfl && /Вести с арены|Arena Journal/.test(mfl.textContent)) {
			return 'arena';
		} else if (this.Enemy_Godname().length > 0) {
			return 'player';
		} else if (this.Hero_Alls_Count() === 0 && this.Enemy_Count() > 1) {
			return 'multi_monster';
		} else {
			return 'monster';
		}
	}
	return '';
};
ui_stats.godName = function() {
	var result, a, b = ui_utils.getStat('#hk_name a, #hk_godname a','href') || '';
	if (a = b.match(/\/([^\/]+)$/)) {
		result = decodeURIComponent(a[1]) || '';
	}
	if (!result) {
		if (a = ui_data.docTitle.match(/(?:\((?:\d+)?[!@~\+]\) )?(.*?)(?: и е(го|ё) геро| and h(is|er) hero)/)) {
			return a[1].trim();
		}
		return GUIp.common.getCurrentGodname();
	}
	return result;
};
ui_stats.guildName = function() {
	return ui_utils.getStat('#hk_clan a','text') || '';
};
ui_stats.goldTextLength = function() {
	return (ui_utils.getStat('#hk_gold_we .l_val','text') || '').length;
};
ui_stats.hasArk = function() {
	return this.Logs() >= 1000;
};
ui_stats.hasTemple = function() {
	return this.Bricks() === 1000;
}
ui_stats.heroHasPet = function() {
	var a = document.querySelector('#hk_pet_class .l_val');
	return !!(a && a.parentNode.style.display !== 'none' && a.textContent);
};
ui_stats._isActivityAvailable = function(selector) {
	var a = document.querySelector(selector);
	return !!a && a.classList.contains('div_link') && a.style.display !== 'none';
};
ui_stats.isArenaAvailable   = ui_stats._isActivityAvailable.bind(ui_stats, '.arena_link_wrap a');
ui_stats.isSparAvailable    = ui_stats._isActivityAvailable.bind(ui_stats, '.e_challenge_button a');
ui_stats.isDungeonAvailable = ui_stats._isActivityAvailable.bind(ui_stats, '.e_dungeon_button a');
ui_stats.isSailAvailable    = ui_stats._isActivityAvailable.bind(ui_stats, '.e_sail_button a');
ui_stats.isMiningAvailable    = ui_stats._isActivityAvailable.bind(ui_stats, '.e_mining_button a');
ui_stats.isFight = function() {
	return /^\((\d+)?[!@~\+]\) /.test(ui_data.docTitle);
};
ui_stats.isBossFight = function() {
	return this.fightType() === 'monster';
};
ui_stats.isDungeon = function() {
	return this.fightType() === 'dungeon';
};
ui_stats.isSail = function() {
	return this.fightType() === 'sail';
};
ui_stats.isMining = function() {
	return this.fightType() === 'mining';
};
ui_stats.isMale = function() {
	return !/героиня|heroine/i.test((document.querySelector('#stats h2, #m_info h2') || '').textContent + ui_data.docTitle);
};
ui_stats.isGoingBack = function() {
	return /до столицы|to Pass/i.test(ui_utils.getStat('#hk_distance .l_capt','text') || '');
};
ui_stats.lastDiaryItems = function() {
	var a = Array.from(document.querySelectorAll('#diary .d_msg')),
		b = document.querySelector('#diary .sort_ch');
	if (b && b.textContent === '▲') {
		a.reverse();
	}
	return a;
};
ui_stats.lastDiaryRealEntry = function(skip, skipInfl) {
	var a = ui_stats.lastDiaryItems();
	skip >>= 0;
	for (var i = 0, j = 0, len = a.length; i < len; i++) {
		if (a[i].textContent.includes('☣')) {
			this._lastDiaryVoice = a[i].textContent.replace(/ \([^(]+\)$/,'');
			continue;
		}
		if (this._lastDiaryVoice === a[i].textContent || (skipInfl && a[i].classList.contains('m_infl'))) {
			continue;
		}
		if (skip === j) {
			return a[i];
		}
		j++;
	}
	return null;
};
ui_stats.lastDiaryIsInfl = function(skip) {
	var a = ui_stats.lastDiaryRealEntry(skip);
	return a && a.classList.contains('m_infl') || false;
};
ui_stats.lastDiary = function() {
	return ui_stats.lastDiaryItems().map(function(a) { return a.textContent; });
};
ui_stats.lastNews = function() {
	return ui_utils.getStat('.f_news.line','text') || '';
};
ui_stats.logId = function() {
	var a, b = ui_utils.getStat('#fbclink','href') || '';
	if (!b.endsWith('/superhero') && (a = b.match(/\/([^\/\?]+)(\?.+)?$/))) {
		return decodeURIComponent(a[1]) || '';
	}
	return '';
};
ui_stats.mileStones = function() {
	return ui_utils.getStat('#hk_distance .l_val','num') || 0;
};
ui_stats.monsterName = function() {
	var a = document.querySelector('#news .line .l_val');
	return a && a.parentNode.style.display !== 'none' && a.textContent || '';
};
ui_stats.nearbyTown = function() {
	var a = (ui_utils.getStat('#hk_distance .l_val','title') || '').match(/: (.+?)(?: \(.*?\))?$/);
	return a && a[1] || '';
};
ui_stats.poiMileStones = function() {
	var a = document.querySelectorAll('#hmap_svg g.poi:not([style$="none;"]) title');
	if (!a.length && this._poiMileStonesCached) {
		if (!document.querySelector('#hmap g:not(.poi):not(.tl):not([style$="none;"]) title')) {
			return this._poiMileStonesCached;
		} else {
			delete this._poiMileStonesCached;
		}
	}
	a = Array.from(a).map(function(b) { return +(b.textContent.match(/(\d+)/) || '')[1] });
	if (a.length > 1) {
		var c = ui_stats.mileStones();
		a.sort(function(d,e) { return Math.abs(c - d) - Math.abs(c - e);});
	}
	a = a[0];
	if (a) {
		this._poiMileStonesCached = a;
	}
	return a || Infinity;
};
ui_stats.poiDistance = function() {
	return ui_stats.poiMileStones() - ui_stats.mileStones();
};
ui_stats.petIsKnockedOut = function() {
	return (ui_utils.getStat('#hk_pet_name .l_val','text') || '').endsWith(' ❌');
};
ui_stats.mProgress = function() {
	var a = ui_utils.getStat('#news .p_bar.monster_pb','title');
	return a && parseInt((a.match(/ (?:—|-) (\d+)%/) || [])[1]) || 0;

};
ui_stats.sProgress = function() {
	var a = ui_utils.getStat('#news .p_bar.n_pbar','title');
	return a && parseInt((a.match(/ (?:—|-) (\d+)%/) || [])[1]) || 0;
};
ui_stats.progressDiff = function() {
	return Math.abs(ui_stats.sProgress() - ui_stats.Task());
};
ui_stats.sendDelay = function(selector, text) {
	var nodes = document.querySelectorAll(selector),
		sendToDesc, sendToStr, m;
	for (var i = 0, len = nodes.length; i < len; i++) {
		sendToDesc = nodes[i];
		sendToStr = sendToDesc.textContent;
		if (text.test(sendToStr)) {
			if (sendToDesc.style.display !== 'none' && (m = /(?:(\d+)\s*[чh]\s+)?(\d+)\s*(?:мин|m)/.exec(sendToStr))) {
				return ((+m[1] || 0) * 60 + +m[2]) * 60;
			}
			return 0;
		}
	}
	return 0;
};
ui_stats.sendDelayArena = function() { return this.sendDelay('#cntrl2 span.to_arena',/Арена откроется через|Arena available in/); };
ui_stats.sendDelayDungeon = function() { return this.sendDelay('#cntrl2 div.arena_msg',/Подземелье откроется через|Dungeon available in/); };
ui_stats.sendDelaySail = function() { return this.sendDelay('#cntrl2 div.arena_msg',/Отплыть можно через|Sail available in/); };
ui_stats.sendDelaySpar = function() { return this.sendDelay('#cntrl2 div.arena_msg',/Тренировка через|Sparring available in/); };
ui_stats.sendDelayMine = function() { return this.sendDelay('#cntrl2 div.arena_msg',/Босс освободится через|Boss returns in/); };
ui_stats.storedPets = function() {
	return ui_data.storedPets || [];
};
ui_stats.townName = function() {
	var a = ui_utils.getStat('#hk_distance .l_val','text') || '';
	return isNaN(parseInt(a)) && a || '';
};
ui_stats.chosenMonster = function() {
	var monster = ui_stats.monsterName();
	if (monster && ui_words.base.chosen_monsters.length && (new worker.RegExp(ui_words.base.chosen_monsters.join('|'),'i')).test(monster)) {
		return true;
	}
	return false;
};
ui_stats.specialMonster = function() {
	var monster = ui_stats.monsterName();
	if (monster && ui_words.base.special_monsters.length && (new worker.RegExp(ui_words.base.special_monsters.join('|'),'i')).test(monster)) {
		return true;
	}
	return false;
};
ui_stats.tamableMonster = function() {
	var monster = ui_stats.monsterName();
	if (monster && !ui_stats.heroHasPet() && ui_words.base.pets.length) {
		var pet, hero_level = ui_stats.Level(),
			has_ark = ui_stats.hasArk(),
			stored = ui_stats.storedPets();
		monster = monster.toLowerCase();
		for (var i = 0, len = ui_words.base.pets.length; i < len; i++) {
			pet = ui_words.base.pets[i];
			if (monster === pet.name.toLowerCase() && !stored.includes(pet.name.toLowerCase()) && hero_level >= pet.min_level && hero_level <= (pet.max_level || pet.min_level + (has_ark ? 29 : 14))) {
				return true;
			}
		}
	}
	return false;
};
ui_stats.wantedMonster = function() {
	return /(☠|WANTED) /.test(ui_stats.monsterName());
};
ui_stats.lowHealth = function(location) {
	var step, hp = ui_stats.HP();
	switch (location) {
		case 'dungeon':
			return hp < 130 && hp > 1;
		case 'sail':
			// ark health level should be actually checked only when the ark is on the map since it is getting changed
			// to hero's health level when exiting sailing mode, possibly leading to false triggering
			if (ui_improver.islandsMap.manager && ui_improver.islandsMap.manager.model.arks[ui_improver.islandsMap.ownArk]) {
				return hp < 25 && hp > 0;
			}
			return false;
		case 'fight':
			if (ui_stats.Enemy_HP() <= 0) {
				return false; // the fight is over, no need to worry any more
			}
			var health_lim, health_ph, damage_lim = 102,
				allies_count = ui_stats.Hero_Alls_Count();
			if (ui_stats.fightType() === 'multi_monster') { // corovan
				health_lim = ui_stats.Max_HP() * 0.05 * ui_stats.Enemy_AliveCount();
			} else if (allies_count === 0) { // single enemy
				health_lim = ui_stats.Max_HP() * 0.15;
			} else { // raid boss or dungeon boss
				if (allies_count < 3) {
					damage_lim -= 10;
				} else if (allies_count > 4) {
					damage_lim += 10;
				}
				health_ph = ((ui_improver.alliesHP.sum || ui_stats.Hero_Alls_MaxHP()) + ui_stats.Max_HP()) / (allies_count + 1);
				health_lim = Math.min(health_ph * 0.24, damage_lim);
				if (ui_stats.Enemy_HasAbility("nimble|бойкий")) {
					health_lim = health_lim * 1.9; // nimble boss can cause almost double damage, but it would be very rarely exactly doubled
				}
				if (ui_stats.Enemy_HasAbility("summoning|зовущий") && ui_stats.Enemy_AliveCount()) {
					if (ui_stats.EnemySingle_HP(1) < 1) { // in case primary boss is dead
						health_lim = 0;
					}
					if (ui_stats.EnemySingle_HP(2) > 0) { // in case secondary boss is present and alive
						health_lim += Math.min(health_ph * 0.24, damage_lim) * 0.3;
					}
				}
				health_lim += 30; // add some safety buffer
			}
			if ((step = this.currentStep()) > 60) { // damage increases in long fights
				health_lim *= (step - 60) * 0.02 + 1;
			}
			return hp < health_lim && hp > 1;
	}
	return false;
};
