// ui_timers
var ui_timers = worker.GUIp.timers = {};

/**
 * @private
 * @type {!Array<string>}
 */
ui_timers._enabledTimers = [];

/**
 * Index into _enabledTimers. If the array is empty, _curTimer may have arbitrary non-negative value.
 *
 * @private
 * @type {number}
 */
ui_timers._curTimer = 0;

/**
 * @private
 * @type {?Array<!GUIp.common.activities.DiaryEntry>}
 */
ui_timers._fallbackThirdEye = null;

ui_timers.init = function() {
	var container, timer, i;
	if ((ui_stats.hasTemple() && !ui_data.isSail) || ui_data.isMining) {
		if (!(container = document.querySelector('#m_fight_log .block_h .l_slot, #diary .block_h .l_slot, #r_map .block_h .l_slot'))) {
			// seems that in come cases it may fail to find a place for timers immediately
			GUIp.common.setTimeout(ui_timers.init, 1e3);
			return;
		}
		ui_timers.setDisabledTimers(ui_storage.get('Option:disabledTimers') || '');
		if (!GUIp.common.activities.readThirdEyeFromLS(ui_data.god_name).length) {
			ui_timers._fallbackThirdEye = []; // enable fallback
			GUIp.common.setTimeout(ui_timers._requestThirdEyeUpdate, 200 + 300 * Math.random());
		}
		container.insertAdjacentHTML('beforeend', '<div id="imp_timer" class="fr_new_badge"></div>');
		timer = container.lastChild;
		if (ui_data.isDungeon || ui_storage.get('Logger:Location') === 'Dungeon' || (ui_data.isFight && ui_stats.Hero_Alls_Count() >= 4)) {
			if ((i = ui_timers._enabledTimers.indexOf('dungeon')) >= 0) {
				ui_timers._curTimer = i;
			}
		} else if (ui_data.isMining && (i = ui_timers._enabledTimers.indexOf('mining')) >= 0) {
			ui_timers._curTimer = i;
		}
		GUIp.common.addListener(timer, 'click', ui_timers._toggleTimers);
	}
	ui_timers.redraw();
	GUIp.common.setInterval(ui_timers.redraw, 60e3);
};

/**
 * @param {string} disabled - A comma-separated list of timers.
 */
ui_timers.setDisabledTimers = function(disabled) {
	var enabled = [], i, timer;
	if (!disabled.includes('conversion')) {
		enabled[0] = 'conversion';
	}
	if (!disabled.includes('dungeon')) {
		enabled.push('dungeon');
	}
	if (!disabled.includes('mining') && (ui_data.isMining || +ui_storage.get('Logger:Book_Bytes'))) {
		enabled.push('mining');
	}
	if ((i = enabled.indexOf(ui_timers._enabledTimers[ui_timers._curTimer])) >= 0) {
		ui_timers._curTimer = i;
	} else if (enabled.length) {
		ui_timers._curTimer %= enabled.length;
	}
	ui_timers._enabledTimers = enabled;
	if ((timer = document.getElementById('imp_timer'))) {
		ui_utils.hideElem(timer, !enabled.length);
		timer.style.cursor = enabled.length === 1 ? 'default' : '';
	}
};

/**
 * @private
 * @returns {!Array<!GUIp.common.activities.DiaryEntry>}
 */
ui_timers._readThirdEyeFromHTML = function() {
	var lines = document.querySelectorAll('.tep .d_line'),
		result = [],
		now = Date.now(),
		offset = 0,
		entry, elements, element, m;
	for (var i = 0, len = lines.length; i < len; i++) {
		entry = {date: 0, type: 'regular', msg: '', logID: ''};
		elements = lines[i].querySelectorAll('div, span a');
		for (var j = 0, jlen = elements.length; j < jlen; j++) {
			element = elements[j];
			if (element.classList.contains('diary_cs_t')) {
				switch (element.textContent) {
					case 'Сегодня':
					case 'Today':
						offset = 0;
						break;
					case 'Вчера':
					case 'Yesterday':
						offset = 86400e3;
						break;
					case 'Позавчера':
					// case '2 days ago':
						offset = 172800e3;
						break;
					default:
						offset = parseInt(element.textContent) * 86400e3 || 0;
						break;
				}
			} else if (element.classList.contains('d_time')) {
				if ((m = /(\d+):(\d+)\s*([AP]M)?/i.exec(element.textContent))) {
					entry.date = GUIp.common.setTime(new Date(now - offset), +m[1], +m[2], (m[3] || '').toUpperCase());
				}
			} else if (element.classList.contains('d_msg')) {
				if (element.classList.contains('m_infl')) {
					entry.type = 'influence';
				}
				entry.msg = element.textContent;
			} else if (element.classList.contains('div_link')) {
				entry.logID = element.href.slice(element.href.lastIndexOf('/') + 1);
			}
		}
		if (entry.date && entry.msg) {
			result.push(entry);
		}
	}
	return result.sort(ui_utils.byDate);
};

/**
 * @private
 * @returns {!Array<!GUIp.common.activities.DiaryEntry>}
 */
ui_timers._readThirdEye = function() {
	return ui_timers._fallbackThirdEye || GUIp.common.activities.readThirdEyeFromLS(ui_data.god_name);
};

/**
 * @private
 * @param {!Array<number>} gaps
 * @param {number} threshold
 */
ui_timers._dropOldGaps = function(gaps, threshold) {
	var n = gaps.length;
	while (n > 1 && gaps[1] <= threshold) {
		gaps.shift();
		gaps.shift();
		n -= 2;
	}
	if (n && threshold > gaps[0]) {
		gaps[0] = threshold;
	}
};

/**
 * @private
 * @param {!Array<!GUIp.common.activities.DiaryEntry>} te
 * @returns {{gaps: !Array<number>, latest: number}}
 */
ui_timers._updateGaps = function(te) {
	var gaps = GUIp.common.parseJSON(ui_storage.get('ThirdEye:Gaps')) || [],
		threshold = Date.now() - GUIp.common.activities.storageTime,
		latest = +ui_storage.get('ThirdEye:Latest') || threshold;
	if (te.length) {
		if (latest < te[0].date) {
			gaps.push(latest, te[0].date);
		}
		ui_storage.set('ThirdEye:Latest', te[te.length - 1].date);
	}
	ui_timers._dropOldGaps(gaps, threshold);
	ui_storage.set('ThirdEye:Gaps', '[' + gaps + ']');
	return {gaps: gaps, latest: latest};
};

/**
 * @private
 * @returns {!Array<!GUIp.common.activities.Activity>}
 */
ui_timers._loadActivities = function() {
	return GUIp.common.activities.load(ui_storage);
};

/**
 * @private
 * @param {!Array<!GUIp.common.activities.Activity>} activities
 */
ui_timers._saveActivities = function(activities) {
	GUIp.common.activities.save(ui_storage, activities);
};

/**
 * @private
 * @returns {!Object<string, !GUIp.common.activities.ActivityStatus>}
 */
ui_timers._loadActivityStatuses = function() {
	return GUIp.common.activities.loadStatuses(ui_storage);
};

/**
 * @private
 * @param {!Object<string, !GUIp.common.activities.ActivityStatus>} statuses
 */
ui_timers._saveActivityStatuses = function(statuses) {
	GUIp.common.activities.saveStatuses(ui_storage, statuses);
};

/**
 * Points in the future when a timer is expired.
 *
 * @private
 * @type {!Object<string, {optimistic: ?Float64Array, pessimistic: number}>}
 */
ui_timers._guaranteeCache = {
	spar:       {optimistic: null, pessimistic: Infinity},
	dungeon:    {optimistic: null, pessimistic: Infinity},
	mining:     {optimistic: null, pessimistic: Infinity},
	conversion: {optimistic: null, pessimistic: Infinity}
};

ui_timers._invalidateGuarantee = function() {
	var g = ui_timers._guaranteeCache;
	g.spar.optimistic = g.dungeon.optimistic = g.mining.optimistic = g.conversion.optimistic = null;
	g.spar.pessimistic = g.dungeon.pessimistic = g.mining.pessimistic = g.conversion.pessimistic = Infinity;
};

/**
 * @private
 * @const
 * @type {!Object<string, function(string): number>}
 */
ui_timers._diaryParsers = {
	spar: function(msg) {
		if (!/^(?:Выдержка из хроники тренировочного боя|Notes from the sparring fight):/i.test(msg)) {
			return -1;
		}
		var pos = msg.search(/ получает порцию опыта| gets experience points for today/i);
		return +(pos >= 0 && msg.endsWith(ui_data.char_name, pos));
	},
	dungeon: function(msg) {
		if (!/^(?:Выдержка из хроники подземелья|Notes from the dungeon):/i.test(msg)) {
			return -1;
		}
		return (msg.match(/бревно для ковчега|ещё одно бревно|log for the ark/gi) || []).length;
	},
	mining: function(msg) {
		if (!/^(?:Заметки с полигона|Notes from the datamine):/i.test(msg)) {
			return -1;
		}
		return /два слога|two glyphs/i.test(msg) ? 2 : /три слога|three glyphs/i.test(msg) ? 3 : +/ слог[^а-яё]| glyph\b/i.test(msg);
	},
	conversion: function(msg) {
		return /^(?:Возложил.+?алтарь|Выставила? тридцать золотых столбиков|(?:Asking for.+?|I )?Placed.+?bags of gold.+?altar.+?experience)/i.test(msg) ? 1 : -1;
	}
};

/**
 * @private
 * @param {string} msg
 * @param {?{type: string, value: number}} [result]
 * @returns {{type: string, value: number}}
 */
ui_timers._parseDiaryMsg = function(msg, result) {
	var type = '',
		value = -1;
	for (var i = 0, len = GUIp.common.activities.guaranteeArr.length; i < len; i++) {
		type = GUIp.common.activities.guaranteeArr[i].type;
		value = ui_timers._diaryParsers[type](msg);
		if (value >= 0) {
			if (!result) return {type: type, value: value};
			result.type = type;
			result.value = value;
			return result;
		}
	}
	if (!result) return {type: '', value: -1};
	result.type = '';
	result.value = -1;
	return result;
};

/**
 * @private
 * @param {!Array<!GUIp.common.activities.DiaryEntry>} te
 * @param {number} latest
 * @returns {{activities: ?Array<!GUIp.common.activities.Activity>, statuses: ?Array<!GUIp.common.activities.ActivityStatus>}}
 */
ui_timers._updateThirdEye = function(te, latest) {
	var len = te.length;
	if (len >= 2 && te[len - 1].date === latest && te[len - 2].date < latest) {
		return {activities: null, statuses: null}; // avoid parsing the Third Eye and loading activities when possible
	}
	var activities = null,
		dict = null,
		statuses = null,
		changed = false,
		parsed = null,
		type = '',
		entry, act;
	for (var i = len - 1; i >= 0; i--) {
		entry = te[i];
		if (entry.date < latest) { // strictly less than
			break;
		}
		if (entry.type !== 'regular') continue;
		parsed = ui_timers._parseDiaryMsg(entry.msg, parsed);
		if ((type = parsed.type)) {
			if (!activities) {
				activities = ui_timers._loadActivities();
				dict = GUIp.common.activities.createActivitiesDict(activities);
				statuses = ui_timers._loadActivityStatuses();
			}
			GUIp.common.activities.updateStatus(statuses[type], true, entry.date);
			if ((act = dict[entry.logID || entry.date])) {
				if (parsed.value === act.result && entry.date === act.date) {
					continue; // no changes
				}
				act.result = parsed.value;
				// if we discovered that activity from its log page, it might have wrong date (off by a minute)
				act.date = entry.date;
			} else {
				activities.push({type: type, date: entry.date, result: parsed.value, logID: entry.logID});
			}
			changed = true;
		}
	}
	if (statuses) {
		ui_timers._invalidateGuarantee();
		if (changed) {
			ui_timers._saveActivities(activities.sort(ui_utils.byDate));
		}
		ui_timers._saveActivityStatuses(statuses);
	}
	return {activities: activities, statuses: statuses};
};

/**
 * @private
 * @returns {{te: !Array<!GUIp.common.activities.DiaryEntry>, gaps: !Array<number>, activities: ?Array<!GUIp.common.activities.Activity>, statuses: ?Array<!GUIp.common.activities.ActivityStatus>}}
 */
ui_timers._readAndUpdateThirdEye = function() {
	var te = ui_timers._readThirdEye(),
		tmp = ui_timers._updateGaps(te),
		tmp1 = ui_timers._updateThirdEye(te, tmp.latest);
	return {
		te: te,
		gaps: tmp.gaps,
		activities: tmp1.activities,
		statuses: tmp1.statuses
	};
};

ui_timers._requestThirdEyeUpdate = function() {
	var button = document.getElementById('imp_button'),
		container = document.getElementById('imp_e_popover_c');
	if (!button || !container) return;
	var unreadCnt = button.classList.contains('fr_new_badge') ? button.textContent : '';
	try {
		$(button).trigger($.Event('click', {originalEvent: {}}));
		container.parentNode.parentNode.style.display = 'none';
	} catch (e) { GUIp.common.error(e); };
	GUIp.common.setTimeout(function() {
		$(button).trigger($.Event('click', {originalEvent: {}}));
		if (unreadCnt) {
			button.textContent = unreadCnt;
			button.classList.add('fr_new_badge');
		}
	}, 300);
};

ui_timers.updateThirdEyeFromHTML = function() {
	if (ui_timers._fallbackThirdEye) {
		ui_timers._fallbackThirdEye = ui_timers._readThirdEyeFromHTML();
		ui_timers._readAndUpdateThirdEye();
	}
};

/**
 * @private
 * @param {!Array<!GUIp.common.activities.Activity>} activities
 * @returns {!Object<string, {successfulDates: !Float64Array, indeterminateDate: number}>}
 */
ui_timers._getGuaranteeSummary = function(activities) {
	var summary = {
		spar:       {successfulDates: new Float64Array(GUIp.common.activities.guarantee.spar.number),       indeterminateDate: 0},
		dungeon:    {successfulDates: new Float64Array(GUIp.common.activities.guarantee.dungeon.number),    indeterminateDate: 0},
		mining:     {successfulDates: new Float64Array(GUIp.common.activities.guarantee.mining.number),     indeterminateDate: 0},
		conversion: {successfulDates: new Float64Array(GUIp.common.activities.guarantee.conversion.number), indeterminateDate: 0}
	};
	var indices = {
		spar:       GUIp.common.activities.guarantee.spar.number,
		dungeon:    GUIp.common.activities.guarantee.dungeon.number,
		mining:     GUIp.common.activities.guarantee.mining.number,
		conversion: GUIp.common.activities.guarantee.conversion.number
	};
	var now = Date.now(),
		threshold = now - GUIp.common.activities.maxTimeout,
		i, j, act, s, result;
	for (i = activities.length - 1; i >= 0; i--) {
		act = activities[i];
		if (act.date < threshold) {
			break;
		} else if (act.date < now - GUIp.common.activities.guarantee[act.type].timeout || !(j = indices[act.type])) {
			continue; // too old or the guarantee is filled up
		}
		s = summary[act.type];
		result = act.result;
		if (result > 0) {
			do {
				s.successfulDates[--j] = act.date;
			} while (j && --result);
			indices[act.type] = j;
		} else if (result && !s.indeterminateDate) {
			s.indeterminateDate = act.date;
		}
	}
	return summary;
};

/**
 * @private
 * @param {!Array<number>} gaps
 * @param {?Array<!GUIp.common.activities.Activity>} activities
 * @param {?Array<!GUIp.common.activities.ActivityStatus>} statuses
 * @returns {!Object<string, {optimistic: !Float64Array, pessimistic: number}>}
 */
ui_timers._calcGuarantee = function(gaps, activities, statuses) {
	var g = ui_timers._guaranteeCache;
	if (g.spar.optimistic) return g;
	var summary = ui_timers._getGuaranteeSummary(activities || ui_timers._loadActivities()),
		latestGap = gaps.length ? gaps[gaps.length - 1] : 0,
		data, s, gg;
	if (!statuses) {
		statuses = ui_timers._loadActivityStatuses();
	}
	for (var i = 0, len = GUIp.common.activities.guaranteeArr.length; i < len; i++) {
		data = GUIp.common.activities.guaranteeArr[i];
		s = summary[data.type];
		for (var j = 0; j < data.number; j++) {
			s.successfulDates[j] += data.timeout;
		}
		gg = g[data.type];
		gg.optimistic = s.successfulDates;
		gg.pessimistic = statuses[data.type].reliable ? Math.max(s.indeterminateDate, latestGap) + data.timeout : Infinity;
	}
	return g;
};

/**
 * @returns {!Object<string, {optimistic: !Float64Array, pessimistic: number}>}
 */
ui_timers.getGuaranteeInfo = function() {
	return ui_timers._guaranteeCache.spar.optimistic ? (
		ui_timers._guaranteeCache
	) : ui_timers._calcGuarantee(GUIp.common.parseJSON(ui_storage.get('ThirdEye:Gaps')) || [], null, null);
};

/**
 * @private
 * @param {number} time
 * @returns {string}
 */
ui_timers._formatRemaining = function(time) {
	return GUIp.common.formatTime(Math.ceil(time / 60e3 - 1e-6), 'remaining');
};

/**
 * @private
 * @param {number} timeElapsed
 * @returns {string}
 */
ui_timers._calculateExp = function(timeElapsed) {
	var baseExp = Math.min(timeElapsed / 64800e3, 2), // 18h
		amountMultiplier = [1, 2, 2.5],
		levelMultiplier = (ui_storage.getFlag('Logger:Equip3_IsBold') ? 1.5 : 1) / (
			Math.floor(Math.max(+ui_storage.get('Logger:Level') || 0, 75) * 0.04 + 1e-6) - 2
		),
		lines = ['', '', ''];
	for (var i = 1; i <= 3; i++) {
		lines[i - 1] = i + '0k gld → ' + ((i + baseExp * amountMultiplier[i - 1]) * levelMultiplier).toFixed(1) + '% exp';
	}
	return lines.join('\n');
};

/**
 * @private
 * @typedef {Object} GUIp.timers._Design
 * @property {number} midTimeout - Time till (the first) guarantee, under which the timer turns yellow.
 * @property {number} timerPhases - Number of guaranteed resources that should have timers.
 * @property {string} guaranteedLabel - Text to be displayed when enough resources are guaranteed.
 * @property {function(string): string} decorateTime - Transformer for the time string.
 * @property {function(!Float64Array): string} getTitle
 * @property {function(!Float64Array, number): string} getTitleUnsure
 * @property {function(!Float64Array): string} getTitleUnreliable
 */

/**
 * @private
 * @const
 * @type {!Object<string, !GUIp.timers._Design>}
 */
ui_timers._design = {
	dungeon: {
		midTimeout: 1800e3, // 30m
		timerPhases: 1,
		guaranteedLabel: '木',
		decorateTime: function(time) { return '¦' + time + '¦'; },
		getTitle: function(remaining) {
			return remaining[0] ? GUIp_i18n.log_isnt_guaranteed : GUIp_i18n.log_is_guaranteed;
		},
		getTitleUnsure: function(remainingOpt, remainingPess) {
			return GUIp_i18n.fmt(remainingOpt[0] ? 'log_isnt_guaranteed_old_time' : 'log_old_time', ui_timers._formatRemaining(remainingPess));
		},
		getTitleUnreliable: function(remaining) {
			return remaining[0] ? GUIp_i18n.log_isnt_guaranteed_requires_te : GUIp_i18n.log_requires_te;
		}
	},
	mining: {
		midTimeout: 780e3, // 13m
		timerPhases: 2,
		guaranteedLabel: '字',
		decorateTime: function(time) { return '⁞' + time + '⁞'; },
		getTitle: function(remaining) {
			return remaining[0] ? (
				GUIp_i18n.byte_isnt_guaranteed
			) : remaining[1] ? (
				GUIp_i18n.byte_is_guaranteed
			) : remaining[2] ? (
				GUIp_i18n.two_bytes_are_guaranteed
			) : (
				GUIp_i18n.three_bytes_are_guaranteed
			);
		},
		getTitleUnsure: function(remainingOpt, remainingPess) {
			return remainingOpt[0] ? (
				GUIp_i18n.fmt('byte_isnt_guaranteed_old_time', ui_timers._formatRemaining(remainingPess))
			) : GUIp_i18n.fmt('bytes_old_time', ui_timers._formatRemaining(remainingPess), this.getTitle(remainingOpt));
		},
		getTitleUnreliable: function(remaining) {
			return remaining[0] ? GUIp_i18n.byte_isnt_guaranteed_requires_te : GUIp_i18n.byte_requires_te;
		}
	},
	conversion: {
		midTimeout: 64800e3, // 18h
		timerPhases: 1,
		guaranteedLabel: '✓',
		decorateTime: function(time) { return time; },
		getTitle: function(remaining) {
			return ui_timers._calculateExp(GUIp.common.activities.guarantee.conversion.timeout - remaining[0]);
		},
		getTitleUnsure: function(remainingOpt, remainingPess) {
			return GUIp_i18n.fmt('gte_old_penalty',
				ui_timers._calculateExp(GUIp.common.activities.guarantee.conversion.timeout - remainingOpt[0]),
				ui_timers._formatRemaining(remainingPess)
			);
		},
		getTitleUnreliable: function(remaining) {
			return GUIp_i18n.fmt('gte_requires_te',
				ui_timers._calculateExp(GUIp.common.activities.guarantee.conversion.timeout - remaining[0])
			);
		}
	}
};

ui_timers.redraw = function() {
	var timer = document.getElementById('imp_timer'),
		tmp = ui_timers._readAndUpdateThirdEye(); // evaluated unconditionally for custom informers' variables to work
	if (!timer || !ui_timers._enabledTimers.length) return;
	var type = ui_timers._enabledTimers[ui_timers._curTimer],
		g = ui_timers._calcGuarantee(tmp.gaps, tmp.activities, tmp.statuses)[type],
		now = Date.now(),
		remainingOpt = g.optimistic.map(function(opt) { return Math.max(opt - now, 0); }),
		remainingPess = Math.max(g.pessimistic - now, 0),
		design = ui_timers._design[type],
		fontSize = '';
	timer.className = timer.className.replace(/\b(?:blue|green|yellow|red|grey|darkgray)\b/g, '');

	if (remainingOpt[0]) {
		timer.textContent = design.decorateTime(ui_timers._formatRemaining(remainingOpt[0]));
		timer.classList.add(remainingOpt[0] <= design.midTimeout ? 'yellow' : 'red');
	} else if (remainingPess === Infinity) {
		timer.textContent = '?';
		timer.classList.add('darkgray');
	} else if (design.timerPhases >= 2 && remainingOpt[1]) {
		timer.textContent = design.decorateTime(ui_timers._formatRemaining(remainingOpt[1]));
		timer.classList.add(remainingPess ? 'grey' : 'green');
	} else {
		timer.textContent = design.guaranteedLabel;
		timer.classList.add(remainingPess ? 'grey' : design.timerPhases === 1 ? 'green' : 'blue');
		fontSize = 'unset';
	}
	timer.style.fontSize = fontSize;

	timer.title = remainingPess === Infinity ? (
		design.getTitleUnreliable(remainingOpt)
	) : remainingPess > remainingOpt[0] ? (
		design.getTitleUnsure(remainingOpt, remainingPess)
	) : (
		design.getTitle(remainingOpt)
	);
};

/**
 * @private
 * @param {!Event} ev
 */
ui_timers._toggleTimers = function(ev) {
	ev.stopPropagation();
	if (ui_timers._enabledTimers.length <= 1) return;
	ui_timers._curTimer = (ui_timers._curTimer + 1) % ui_timers._enabledTimers.length;
	var timer = $('#imp_timer');
	timer.fadeOut(500, GUIp.common.try2.bind(null, function() {
		ui_timers.redraw();
		timer.fadeIn(500);
	}));
};

/**
 * @param {!Array<!GUIp.common.activities.DiaryEntry>} diary
 */
ui_timers.updateDiary = function(diary) {
	var activities = null,
		dict = null,
		statuses = null,
		changed = false,
		changedStatuses = false,
		type = '',
		value = -1,
		nextParsed = {type: '', value: -1},
		multiple = false,
		entry, nextEntry;
	var processEntry = function() {
		var tmp, act;
		if (!activities) {
			tmp = ui_timers._readAndUpdateThirdEye();
			activities = tmp.activities || ui_timers._loadActivities();
			dict = GUIp.common.activities.createActivitiesDict(activities);
			statuses = tmp.statuses;
		}
		if ((act = dict[entry.logID || entry.date])) {
			if (value === act.result && entry.date === act.date) {
				return; // no changes
			}
			act.result = value;
			// if we discovered that activity from its log page, it might have wrong date (off by a minute)
			act.date = entry.date;
		} else {
			activities.push({type: type, date: entry.date, result: value, logID: entry.logID});
			// we should have seen this in the Third Eye, but we haven't. seems that this type of activities
			// is disabled in the Third Eye's settings
			if (!statuses) {
				statuses = ui_timers._loadActivityStatuses();
			}
			GUIp.common.activities.updateStatus(statuses[type], false, entry.date);
			changedStatuses = true;
		}
		changed = true;
	};

	for (var i = 0, len = diary.length; i < len; i++) {
		nextEntry = diary[i];
		if (nextEntry.type === 'regular') {
			nextParsed = ui_timers._parseDiaryMsg(nextEntry.msg, nextParsed);
		} else {
			nextParsed.type = '';
			nextParsed.value = -1;
		}
		multiple = type === nextParsed.type;
		if (type && (type === 'conversion' || (!multiple && entry.logID))) {
			// there should be 2 diary entries in a row for 'spar', 'dungeon', or 'mining': both match our regexes, but
			// the earlier indicates the start of an activity and the later does the end. we should ignore the former.
			// and if an entry does not have an associated link, we certainly should ignore it.
			GUIp.common.debug('timers: processing "' + type + '" ' + value + ' (multiple=' + multiple + '):', entry.msg);
			processEntry();
		} else if (type) {
			GUIp.common.debug('timers: ignoring "' + type + '" ' + value + ' (multiple=' + multiple + '):', entry.msg);
		}
		entry = nextEntry;
		type = nextParsed.type;
		value = nextParsed.value;
	}
	if (type && (type === 'conversion' || (multiple && entry.logID))) {
		// if the latest entry in the diary is unpaired, then it indicates the start of an activity that is just
		// about to begin. so we ignore it as well.
		GUIp.common.debug('timers: processing "' + type + '" ' + value + ' (multiple=' + multiple + '; latest):', entry.msg);
		processEntry();
	} else if (type) {
		GUIp.common.debug('timers: ignoring "' + type + '" ' + value + ' (multiple=' + multiple + '; latest):', entry.msg);
	}

	if (changed) {
		ui_timers._invalidateGuarantee();
		ui_timers._saveActivities(activities.sort(ui_utils.byDate));
		if (changedStatuses) {
			ui_timers._saveActivityStatuses(statuses);
		}
		// we intentionally do not call redraw() here so that old conversion timer's values can be observed for a while
	}
};

/**
 * @param {!Array<!GUIp.common.activities.LastFightsEntry>} fights
 * @returns {!Array<!GUIp.common.activities.Activity>}
 */
ui_timers.updateLastFights = function(fights) {
	var tmp = ui_timers._readAndUpdateThirdEye(),
		activities = tmp.activities || ui_timers._loadActivities();
	if (GUIp.common.activities.updateLastFights(
		ui_storage, tmp.te, tmp.gaps, activities, tmp.statuses || ui_timers._loadActivityStatuses(), fights
	)) {
		ui_timers._invalidateGuarantee();
	}
	if (ui_timers._enabledTimers.length && ui_timers._enabledTimers[ui_timers._curTimer] !== 'conversion') {
		ui_timers.redraw();
	}
	return activities;
};

ui_timers.processActivitiesExternal = function() {
	ui_timers._invalidateGuarantee();
	if (ui_timers._enabledTimers.length && ui_timers._enabledTimers[ui_timers._curTimer] !== 'conversion') {
		ui_timers.redraw();
	}
};
