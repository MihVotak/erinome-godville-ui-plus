(function(worker) {
'use strict';

worker.GUIp = worker.GUIp || {};

//! include './data.js';
//! include './utils.js';
//! include './timeout.js';
//! include './help.js';
//! include './storage.js';
//! include './expr.js';
//! include './words.js';
//! include './stats.js';
//! include './logger.js';
//! include './informer_debug.js';
//! include './informer.js';
//! include './forum.js';
//! include './mining.js';
//! include './dragger.js';
//! include './improver.js';
//! include './inventory.js';
//! include './timers.js';
//! include './observers.js';
//! include './starter.js';
//! include 'mixins/module.js';

registerModuleAsync('superhero', ui_starter.waitForContents, ui_starter);

})(this);
