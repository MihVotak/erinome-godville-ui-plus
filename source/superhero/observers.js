// ui_observers
var ui_observers = worker.GUIp.observers = {};

ui_observers.init = function() {
	for (var key in this) {
		if (this[key].condition) {
			ui_observers.start(this[key]);
		}
	}
};
ui_observers.start = function(obj) {
	for (var i = 0, len = obj.target.length; i < len; i++) {
		var target = document.querySelector(obj.target[i]);
		if (target) {
			GUIp.common.newMutationObserver(obj.func).observe(target, obj.config);
		}
	}
};
ui_observers.mutationChecker = function(mutations, check, callback) {
	if (!mutations.some(check)) {
		return false;
	}
	callback();
	return true;
};
ui_observers.chats = {
	condition: true,
	config: {childList: true},
	func: function(mutations) {
		for (var i = 0, len = mutations.length; i < len; i++) {
			var mutation = mutations[i];
			for (var j = 0, jlen = mutation.addedNodes.length; j < jlen; j++) {
				var newNode = mutation.addedNodes[j];
				if (newNode.classList.contains('e_moved')) {
					continue;
				}
				newNode.classList.add('e_moved');
				mutation.target.appendChild(newNode); // move chat window to the left
				var msgArea = newNode.querySelector('.frMsgArea');
				if (msgArea) {
					msgArea.scrollTop = msgArea.scrollTopMax || msgArea.scrollHeight;
				}
				ui_improver.processNewChat(newNode);
			}
		}
		ui_observers.mutationChecker(mutations, function(mutation) {
			return mutation.addedNodes.length || mutation.removedNodes.length;
		}, function() {
			ui_improver.chatsFix();
			ui_informer.clearTitle();
		});
	},
	target: ['.chat_ph']
};
ui_observers.clearTitle = {
	condition: true,
	config: {
		childList: true,
		attributes: true,
		subtree: true,
		attributeFilter: ['style']
	},
	func: function(mutations) {
		var isFocused = document.hasFocus && document.hasFocus();
		ui_observers.mutationChecker(mutations, function(mutation) {
			return isFocused && mutation.addedNodes.length && mutation.addedNodes[0].classList && mutation.addedNodes[0].classList.contains('fr_new_msg')
		}, function() { GUIp.common.setTimeout(ui_improver.checkGCMark.bind(ui_improver,'observe'), 50); });
		ui_observers.mutationChecker(mutations, function(mutation) {
			return mutation.target.classList && (mutation.target.classList.contains('fr_new_badge_pos') || mutation.target.classList.contains('frmsg_i'));
		}, function() { GUIp.common.setTimeout(ui_utils.pmNotification.bind(ui_utils), 100); });
		ui_observers.mutationChecker(mutations, function(mutation) {
			return mutation.target.className.match(/fr_new_(?:msg|badge)/) ||
				  (mutation.target.className.includes('dockfrname_w') && (mutation.removedNodes.length && mutation.removedNodes[0].className.includes('fr_new_msg') || mutation.addedNodes.length && mutation.addedNodes[0].className.includes('fr_new_msg')));
		}, ui_informer.clearTitle.bind(ui_informer));
	},
	target: ['.msgDockWrapper']
};
ui_observers.voiceform = {
	condition: true,
	config: {
		attributes: true,
		attributeFilter: ['style']
	},
	func: function(mutations) {
		for (var i = 0, len = mutations.length; i < len; i++) {
			if (mutations[i].target.style.display) {
				ui_improver.improvementDebounce();
				break;
			}
		}
	},
	target: ['#cntrl .voice_line']
};
ui_observers.refresher = {
	condition: (worker.GUIp_browser !== 'Opera'),
	config: {
		attributes: true,
		characterData: true,
		childList: true,
		subtree: true
	},
	func: function(mutations) {
		var toReset = false;
		for (var i = 0, len = mutations.length; i < len; i++) {
			var tgt = mutations[i].target,
				id = tgt.id,
				cl = tgt.className;
			if (!(id && id.match && id.match(/logger|pet_badge|equip_badge/)) &&
				!(cl && cl.match && cl.match(/voice_generator|inspect_button|m_hover|craft_button/))) {
				toReset = true;
				break;
			}
		}
		if (toReset) {
			worker.clearInterval(ui_improver.softRefreshInt);
			worker.clearInterval(ui_improver.hardRefreshInt);
			if (!ui_storage.getFlag('Option:disablePageRefresh')) {
				ui_improver.softRefreshInt = GUIp.common.setInterval(ui_improver.softRefresh, (ui_data.isFight || ui_data.isDungeon) ? 9e3 : 18e4);
				ui_improver.hardRefreshInt = GUIp.common.setInterval(ui_improver.hardRefresh, (ui_data.isFight || ui_data.isDungeon) ? 25e3 : 50e4);
			}
		}
	},
	target: ['#main_wrapper']
};
ui_observers.diary = {
	get condition() {
		return !ui_data.isFight && !ui_data.isDungeon;
	},
	config: {childList: true},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes.length;	}, function() { ui_improver.improveDiary(); ui_improver.improvementDebounce(); });
	},
	target: ['#diary .d_content']
};
ui_observers.news = {
	get condition() {
		return !ui_data.isFight && !ui_data.isDungeon;
	},
	config: {childList: true, characterData: true, subtree: true},
	func: function() { ui_improver.improvementDebounce(); },
	target: ['.f_news']
};
ui_observers.wup_detector = {
	condition: true,
	config: {childList: true},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) {
			return mutation.addedNodes.length && mutation.addedNodes[0].classList && mutation.addedNodes[0].classList.contains('wup');
		}, function() {
			if (document.querySelector('.wup.in .tep') && ui_storage.getFlag('Option:tePosFix')) {
				ui_utils.hideElem(document.querySelector('.wup.in'),true);
				GUIp.common.setTimeout(function() { ui_improver.thirdEyePositionFix(); },0);
			}
			if (ui_improver.improveLabInt) { worker.clearInterval(ui_improver.improveLabInt); ui_improver.improveLabInt = null; }
			if (!ui_data.isFight) {
				ui_timers.updateThirdEyeFromHTML();
				ui_improver.improveLastFights();
				ui_improver.improveLastVoices();
				ui_improver.improveStoredPets();
				ui_improver.improveSparMenu();
			}
			ui_improver.improveLabInt = GUIp.common.setInterval(function() { ui_improver.improveLab(); },200);
		});
	},
	target: ['body']
};
ui_observers.chronicles = {
	get condition() {
		return ui_data.isDungeon;
	},
	config: {childList: true},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes.length; }, function() { ui_improver.improveChronicles(); ui_improver.improvementDebounce(); });
	},
	target: ['#m_fight_log .d_content']
};
ui_observers.map_colorization = {
	get condition() {
		return ui_data.isDungeon;
	},
	config: {
		childList: true,
		subtree: true
	},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes.length && !(mutation.target.classList && mutation.target.classList.contains('restoredExcl')); }, ui_improver.colorDungeonMap.bind(ui_improver));
	},
	target: ['#map .block_content']
};
ui_observers.s_chronicles = {
	get condition() {
		return ui_data.isSail;
	},
	config: {childList: true},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes.length; }, function() { ui_improver.improveSailChronicles(); ui_improver.improvementDebounce(); });
	},
	target: ['#m_fight_log h2']
};
ui_observers.h_map = {
	get condition() {
		return !ui_data.isFight;
	},
	config: {
		childList: true,
		subtree: true
	},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes[0] && mutation.addedNodes[0].tagName === 'g'; }, ui_improver.nearbyTownsFix.bind(ui_improver)) ||
			ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes[0] && mutation.target.tagName === 'title'; }, ui_improver.nearbyTownsFix.bind(ui_improver,true));
	},
	target: ['#hmap_svg']
};
ui_observers.mining_map = {
	get condition() {
		return ui_data.isMining;
	},
	config: {
		childList: true,
		subtree: true
	},
	func: function(mutations, observer) {
		ui_observers.mutationChecker(mutations, function(mutation) { return mutation.addedNodes.length; }, function() {
			ui_improver.improvementDebounce();
			ui_mining.updateMap();
			observer.takeRecords();
		});
	},
	target: ['.wrmap']
};
ui_observers.theme_switcher = {
	condition: true,
	config: {childList: true},
	func: function(mutations) {
		var href = '', nodes, theme;
		for (var i = 0, len = mutations.length; i < len; i++) {
			nodes = mutations[i].addedNodes;
			if (nodes.length && (href = nodes[0].href) && (theme = /\/stylesheets\/+(th_.*?)\.css/.exec(href))) {
				ui_utils.switchTheme(theme[1]);
				break;
			}
		}
	},
	target: ['head']
};
ui_observers.node_insertion = {
	condition: true,
	config: {
		childList: true,
		subtree: true
	},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) {
			// to prevent improving WHEN ENTERING FUCKING TEXT IN FUCKING TEXTAREA
			return mutation.addedNodes.length && mutation.addedNodes[0].nodeType !== 3;
		}, ui_improver.improvementDebounce);
	},
	target: ['body']
};
