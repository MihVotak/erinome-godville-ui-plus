// ui_improver
var ui_improver = worker.GUIp.improver = {};

ui_improver.isFirstTime = true;
ui_improver.voiceSubmitted = false;
ui_improver.clockToggling = false;
ui_improver.clock = null;
ui_improver.wantedMonsters = null;
ui_improver.wantedItems = null;
ui_improver.bingoItems = null;
ui_improver.dailyForecast = [];
ui_improver.dailyForecastText = '';
ui_improver.improvementDebounce = null;
// dungeon & sailing
ui_improver.chronicles = {};
ui_improver.directionlessMoveIndex = 0;
ui_improver.wormholeMoveIndex = 0;
ui_improver.alliesHP = {};
ui_improver.corrections = {n: 'north', e: 'east', s: 'south', w: 'west'};
ui_improver.dungeonXHRCount = 0;
ui_improver.needLog = true;
// resresher
ui_improver.softRefreshInt = 0;
ui_improver.hardRefreshInt = 0;

ui_improver.init = function() {
	ui_improver.improvementDebounce = GUIp.common.debounce(250, function improvementDebounce() {
		ui_improver.improve();
		if (ui_data.isFight) {
			ui_logger.update();
		}
	});
};
ui_improver.softRefresh = function() {
	GUIp.common.info('soft reloading...');
	document.getElementById('d_refresh') && document.getElementById('d_refresh').click();
};
ui_improver.hardRefresh = function() {
	GUIp.common.warn('hard reloading...');
	location.reload();
};
ui_improver.improve = function() {
	this.improveInProcess = true;

	if (this.isFirstTime) {
		if (GUIp_browser === 'Opera') {
			document.body.classList.add('e_opera');
		}
		if (!ui_data.isFight && !ui_data.isDungeon && !ui_data.isSail) {
			ui_improver.improveDiary();
			ui_improver.distanceInformerInit();
			ui_improver.improveShop();
		}
		if (ui_data.isDungeon) {
			ui_improver.initStreaming(document.querySelector('#m_fight_log .block_content'));
			var heroNames = ui_stats.Hero_Ally_Names();
			heroNames.push(ui_data.char_name);
			GUIp.common.setExtraDiscardData(heroNames);
			GUIp.common.getDungeonPhrases(ui_improver.improveChronicles.bind(ui_improver),null);
			GUIp.common.dmapExclCache = JSON.parse(ui_storage.get('Log:' + ui_stats.logId() + ':excl') || '{}');
		}
		if (ui_data.isSail) {
			var mapSettings = ui_storage.getList('Option:islandsMapSettings');
			if (mapSettings.includes('widen')) {
				ui_improver.sailPageResize = true;
				ui_improver.whenWindowResize();
			}
			ui_improver.initSailing();
		}
		if (ui_data.isMining) {
			ui_mining.updateMap();
		}
		if (ui_data.isFight) {
			GUIp.common.setTimeout(function() { ui_improver.improvePlayers(); },250);
			if (ui_improver.alliesHP.sum === undefined) {
				try {
					if (!ui_storage.get('Log:' + ui_stats.logId() + ':allhp')) {
						ui_improver.alliesHP = {sum: 0};
						for (var i = 1; i <= 5; i++) {
							ui_improver.alliesHP[i] = ui_stats.Hero_Ally_MaxHP(i);
							if (ui_stats.Hero_Ally_Name(i)[0] !== '+') {
								ui_improver.alliesHP.sum += ui_improver.alliesHP[i];
							}
						}
						ui_storage.set('Log:' + ui_stats.logId() + ':allhp', JSON.stringify(ui_improver.alliesHP));
					} else {
						ui_improver.alliesHP = JSON.parse(ui_storage.get('Log:' + ui_stats.logId() + ':allhp'));
					}
				} catch (e) {}
			}
		}
		ui_improver.initTouchDragging();
	}
	ui_improver.improveStats();
	ui_improver.improveVoiceDialog();
	if (!ui_data.isFight) {
		ui_improver.improveNews();
		ui_improver.improveEquip();
		ui_improver.improveSkills();
		ui_improver.improvePet();
		ui_improver.improveSendButtons();
		ui_improver.detectors.detectField();
		if (ui_data.hasShop) {
			ui_improver.detectors.detectLS();
		}
	} else {
		ui_improver.improveHP();
	}
	if (ui_data.isDungeon) {
		ui_improver.improveDungeon();
	}
	ui_improver.improveInterface();
	ui_improver.improveChat();
	ui_improver.calculateButtonsVisibility();
	this.isFirstTime = false;
	this.improveInProcess = false;

	ui_informer.update('fight', ui_data.isFight && !ui_data.isDungeon && !ui_data.isSail);
	ui_informer.update('arena available', ui_stats.isArenaAvailable());
	ui_informer.update('dungeon available', ui_stats.isDungeonAvailable());
	ui_informer.update('sail available', ui_stats.isSailAvailable());

	ui_informer.updateCustomInformers();
};
ui_improver.processExternalChanges = function(event) {
	// we're interested in keys related to current godname only
	if (event.key.slice(0,event.key.indexOf(':') + 1) !== ui_storage._getKey('')) {
		return;
	}
	// convert some special values
	var newValue = event.newValue;
	switch (newValue) {
		case 'true': newValue = true; break;
		case 'false': newValue = false; break;
		case 'null': newValue = null; break;
	}
	// strip prefix from actual key name and switch by eGUI+ storage keys
	var key = event.key.slice(event.key.indexOf(':') + 1);
	switch (key) {
		case 'UserCss':
			GUIp.common.addCSSFromString(newValue);
			break;
		case 'Option:disableDieButton':
		case 'Option:disableVoiceGenerators':
		case 'Option:fixedCraftButtons':
			ui_improver.calculateButtonsVisibility(true);
			break;
		case 'Option:disabledTimers':
			ui_timers.setDisabledTimers(newValue || '');
			ui_timers.redraw();
			break;
		case 'Option:disableLogger':
			ui_logger.disable(newValue);
			break;
		case 'Option:enableInformerAlerts':
		case 'Option:enablePmAlerts':
			if (newValue && worker.Notification && worker.Notification.permission !== "granted" && worker.GUIp_browser !== 'Chrome') {
				worker.Notification.requestPermission();
			}
			break;
		case 'Option:forbiddenCraft':
			ui_inventory.forbiddenCraft = newValue || '';
			break;
		case 'Option:activeInformers':
			ui_informer.activeInformers = JSON.parse(newValue || '{}');
			break;
		case 'Option:freezeVoiceButton':
			ui_improver.freezeVoiceButton = newValue || '';
			break;
		case 'Option:relocateCraftButtons':
			var craftButtons = document.querySelector('.craft_button.span');
			if (craftButtons) {
				craftButtons.parentNode.insertBefore(craftButtons, newValue ? craftButtons.parentNode.firstChild : null);
			}
		case 'Option:relocateDuelButtons':
			ui_improver.improveSendButtons(true);
			break;
		case 'Option:dungeonMapSettings':
			if (ui_data.isDungeon) {
				ui_improver.improveDungeon(true);
				ui_utils.hideElem(document.querySelector('.dmapDimensions'), !(newValue || '').includes('dims'));
			}
			break;
		case 'Option:useBackground':
			ui_improver.improveInterface(true);
			break;
		case 'Option:discardTitleChanges':
			if (newValue) {
				ui_informer.redrawTitle();
			}
			break;
		case 'CustomWords:pets':
		case 'CustomWords:chosen_monsters':
		case 'CustomWords:special_monsters':
		case 'CustomWords:custom_informers':
			ui_words.init();
			break;
		case 'CustomWords:ally_blacklist':
			ui_words.init();
			ui_improver.improvePlayers();
			break;
		case 'CustomWords:custom_craft':
			if (!ui_data.isFight) {
				ui_words.init();
				ui_inventory.rebuildCustomCraft();
				ui_improver.calculateButtonsVisibility(true);
			}
			break;
		case 'ThirdEye:Activities':
			ui_improver.redrawLastFights(newValue || '[]');
			// break;
		case 'ThirdEye:ActivityStatuses':
			ui_timers.processActivitiesExternal();
			break;
		case 'BingoItems':
			ui_improver.bingoItems = newValue ? new RegExp(newValue,'i') : null;
			if (document.querySelector('#inventory li.improved')) {
				ui_inventory._update();
			}
			break;
		case 'DailyForecast':
			ui_improver.dailyForecast = newValue ? newValue.split(',') : [];
			break;
		case 'DailyForecastText':
			ui_improver.dailyForecastText = newValue || '';
			ui_improver.showDailyForecast();
			break;
		default:
			if (key.startsWith('ForumInformersNotify')) {
				ui_forum.informersNotify([{key:key,data:JSON.parse(newValue)}]);
			}
	}
};
ui_improver.improveVoiceDialog = function() {
	// If playing in pure ZPG mode there won't be present voice input block at all;
	if (!document.getElementById('ve_wrap')) {
		return;
	}
	// Add voicegens and show timeout bar after saying
	if (this.isFirstTime) {
		this.freezeVoiceButton = ui_storage.get('Option:freezeVoiceButton') || '';
		ui_utils.updateVoiceSubmitState();

		var voiceSubmit = document.getElementById('voice_submit');
		document.getElementById('ve_wrap').insertAdjacentHTML('afterbegin',
			'<div id="clear_voice_input" class="div_link_nu gvl_popover hidden" title="' + worker.GUIp_i18n.clear_voice_input + '">×</div>'
		);
		GUIp.common.addListener(document.getElementById('clear_voice_input'), 'click', function() {
			ui_utils.setVoice('');
		});
		worker.$(document).on('change keypress paste focus textInput input', '#godvoice, #god_phrase', function() {
			GUIp.common.try2(function(target) {
				ui_utils.updateVoiceSubmitState();
				ui_utils.hideElem(document.getElementById('clear_voice_input'), !target.value);
			}, this);
		});
		GUIp.common.addListener(document.getElementById('ve_wrap'), 'keypress', function(e) {
			if (e.which !== 13) return;
			if (voiceSubmit.disabled) {
				e.preventDefault();
				e.stopPropagation();
			} else {
				ui_improver.voiceSubmitted = true;
			}
		}, true);
		GUIp.common.addListener(voiceSubmit, 'click', function() {
			ui_improver.voiceSubmitted = true;
			voiceSubmit.blur();
		});
		// prevent Godville from re-enabling the button when we want to keep it disabled
		GUIp.common.newMutationObserver(ui_utils.updateVoiceSubmitState.bind(ui_utils))
			.observe(voiceSubmit, {attributes: true, attributeFilter: ['disabled']});

		if (!ui_utils.isAlreadyImproved(document.getElementById('cntrl'))) {
			var gp_label = document.getElementsByClassName('gp_label')[0];
			gp_label.classList.add('l_capt');
			document.getElementsByClassName('gp_val')[0].classList.add('l_val');
			if (ui_words.base.phrases.mnemonics.length) {
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.mnemo_button, 'mnemonics', worker.GUIp_i18n.mnemo_title);
			}
			if (ui_data.isDungeon) {
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.east, 'go_east', worker.GUIp_i18n.fmt('ask_to_go_east', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.west, 'go_west', worker.GUIp_i18n.fmt('ask_to_go_west', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.south, 'go_south', worker.GUIp_i18n.fmt('ask_to_go_south', ui_data.char_sex[0]));
				ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.north, 'go_north', worker.GUIp_i18n.fmt('ask_to_go_north', ui_data.char_sex[0]));
				if (GUIp.common.isAndroid && document.getElementById('map')) {
					GUIp.common.addListener(document.getElementById('map'), 'click', ui_utils.mapVoicegen);
					GUIp.common.tooltipCells();
				}
			} else if (!ui_data.isSail) {
				if (ui_data.isFight) {
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.defend, 'defend', worker.GUIp_i18n.fmt('ask_to_defend', ui_data.char_sex[0]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.pray, 'pray', worker.GUIp_i18n.fmt('ask_to_pray', ui_data.char_sex[0]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.heal, 'heal', worker.GUIp_i18n.fmt('ask_to_heal', ui_data.char_sex[1]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.hit, 'hit', worker.GUIp_i18n.fmt('ask_to_hit', ui_data.char_sex[1]));
				} else {
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.sacrifice, 'sacrifice', worker.GUIp_i18n.fmt('ask_to_sacrifice', ui_data.char_sex[1]));
					ui_utils.addVoicegen(gp_label, worker.GUIp_i18n.pray, 'pray', worker.GUIp_i18n.fmt('ask_to_pray', ui_data.char_sex[0]));
				}
			}
		}
	}
	//hide_charge_button
	var charge_button = document.querySelector('#cntrl .hch_link');
	if (charge_button) {
		charge_button.style.visibility = ui_storage.getFlag('Option:hideChargeButton') ? 'hidden' : '';
	}
	ui_informer.update('full godpower', ui_stats.Godpower() === ui_stats.Max_Godpower() && !ui_data.isFight);
};
ui_improver.improveNews = function() {
	if (!ui_utils.isAlreadyImproved(document.getElementById('news'))) {
		ui_utils.addVoicegen(document.querySelector('#news .l_capt'), worker.GUIp_i18n.hit, 'hit', worker.GUIp_i18n.fmt('ask_to_hit', ui_data.char_sex[1]));
	}
	ui_informer.update('wanted monster', ui_stats.wantedMonster());
	ui_informer.update('special monster', ui_stats.specialMonster());
	ui_informer.update('tamable monster', ui_stats.tamableMonster());
	ui_informer.update('chosen monster', ui_stats.chosenMonster());
};
ui_improver.improveDungeon = function(forcedUpdate) {
	if (this.isFirstTime || forcedUpdate) {
		var control = document.getElementById('m_control'),
			map = document.getElementById('map'),
			right_block = document.getElementById('a_right_block');
		if (ui_storage.getList('Option:dungeonMapSettings').includes('rloc')) {
			if (!document.querySelector('#a_right_block #map')) {
				map.parentNode.insertBefore(control, map);
				right_block.insertBefore(map, null);
				document.querySelector('#m_control .block_title').textContent = worker.GUIp_locale === 'ru' ? 'Пульт вмешательства в личную жизнь' : 'Remote Control';
			}
		} else if (forcedUpdate) {
			if (!document.querySelector('#a_central_block #map')) {
				control.parentNode.insertBefore(map, control);
				right_block.insertBefore(control, null);
				document.querySelector('#m_control .block_title').textContent = worker.GUIp_locale === 'ru' ? 'Пульт' : 'Remote';
			}
		}
	}
	this.updateDungeonVoicegen(); // todo: place this into dungeon colorization? also run this from here but only at first time?
};
ui_improver.updateDungeonVoicegen = function() {
	if (document.querySelectorAll('#map .dml').length) {
		var i, j, chronolen = +worker.Object.keys(this.chronicles).reverse()[0],
			$box = worker.$('#cntrl .voice_generator'),
			$boxML = worker.$('#map .dml'),
			$boxMC = worker.$('#map .dmc'),
			kRow = $boxML.length,
			kColumn = $boxML[0].textContent.length,
			isJumping = document.getElementById('map').textContent.match(/Прыгучести|Jumping|Загадки|Mystery/);
		if (!$box.length) {
			return;
		}
		for (i = 0; i < 4; i++) {
			$box[i].style.visibility = 'hidden';
		}
		for (var si = 0; si < kRow; si++) {
			for (var sj = 0; sj < kColumn; sj++) {
				if ($boxMC[si * kColumn + sj].textContent !== '@') {
					continue;
				}
				var isMoveLoss = [];
				for (i = 0; i < 4; i++) {
					isMoveLoss[i] = chronolen > i && this.chronicles[chronolen - i].marks.includes('trapMoveLoss');
				}
				var directionsShouldBeShown = !isMoveLoss[0] || (isMoveLoss[1] && (!isMoveLoss[2] || isMoveLoss[3]));
				if (directionsShouldBeShown) {
					if ($boxMC[(si - 1) * kColumn + sj].textContent !== '#' || isJumping && (si === 1 || si !== 1 && $boxMC[(si - 2) * kColumn + sj].textContent !== '#')) {
						$box[0].style.visibility = ''; // Север
					}
					if ($boxMC[(si + 1) *kColumn + sj].textContent !== '#' || isJumping && (si === kRow - 2 || si !== kRow - 2 && $boxMC[(si + 2) *kColumn + sj].textContent !== '#')) {
						$box[1].style.visibility = ''; // Юг
					}
					if ($boxMC[si * kColumn + sj - 1].textContent !== '#' || isJumping && $boxMC[si * kColumn + sj - 2].textContent !== '#') {
						$box[2].style.visibility = ''; // Запад
					}
					if ($boxMC[si * kColumn + sj + 1].textContent !== '#' || isJumping && $boxMC[si * kColumn + sj + 2].textContent !== '#') {
						$box[3].style.visibility = ''; // Восток
					}
					if (isJumping && ui_storage.getFlag('Option:jumpingOverrideDirections')) {
						for (i = 0; i < 4; i++) {
							$box[i].style.visibility = '';
						}
					}
				}
			}
		}
	}
};
ui_improver._toFixedPoint = function(m0) {
	return (+m0).toFixed(1);
};
ui_improver.improveStats = function() {
	var i, brNode;
	if ((brNode = document.querySelector('#hk_bricks_cnt .l_val'))) {
		brNode.textContent = brNode.textContent.replace(/[\d.]+/, this._toFixedPoint);
	}
	if (ui_data.isDungeon) {
		if (ui_storage.get('Logger:Location') === 'Field') {
			ui_storage.set('Logger:Location', 'Dungeon');
			ui_storage.set('Logger:Map_HP', ui_stats.HP());
			ui_storage.set('Logger:Map_Exp', ui_stats.Exp());
			ui_storage.set('Logger:Map_Level', ui_stats.Level());
			ui_storage.set('Logger:Map_Gold', ui_storage.get('Logger:Gold'));
			ui_storage.set('Logger:Map_Inv', ui_stats.Inv());
			ui_storage.set('Logger:Map_Charges',ui_stats.Charges());
			ui_storage.set('Logger:Map_Alls_HP', ui_stats.Map_Alls_HP());
			for (i = 1; i <= 5; i++) {
				ui_storage.set('Logger:Map_Ally'+i+'_HP', ui_stats.Map_Ally_HP(i));
			}
		}
		ui_informer.update('low health', ui_stats.lowHealth('dungeon'));
		return;
	}
	if (ui_data.isSail) {
		if (ui_storage.get('Logger:Location') === 'Field') {
			ui_storage.set('Logger:Location', 'Sail');
			ui_storage.set('Logger:Map_HP', ui_stats.HP());
			ui_storage.set('Logger:Map_Charges',ui_stats.Charges());
			ui_storage.set('Logger:Map_Supplies',ui_stats.Map_Supplies());
			for (i = 1; i <= 4; i++) {
				ui_storage.set('Logger:Map_Ally'+i+'_HP', ui_stats.Map_Ally_HP(i));
			}
			for (i = 1; i <= 4; i++) {
				ui_storage.set('Logger:Enemy'+i+'_Name', '');
			}
		}
		ui_informer.update('low health', ui_stats.lowHealth('sail'));
		return;
	}
	if (ui_data.isMining) {
		if (ui_storage.get('Logger:Location') === 'Field') {
			ui_storage.set('Logger:Location', 'Mining');
			ui_storage.set('Logger:Hero_HP', ui_stats.HP());
			ui_storage.set('Logger:Map_Charges',ui_stats.Charges());
			ui_storage.set('Logger:Bits',ui_stats.Bits());
			ui_storage.set('Logger:Bytes',ui_stats.Bytes());
			ui_storage.set('Logger:Push_Readiness',ui_stats.Push_Readiness());
			for (i = 1; i <= 4; i++) {
				ui_storage.set('Logger:Enemy'+i+'_HP', ui_stats.EnemySingle_HP(i));
			}
		}
		return;
	}
	if (ui_data.isFight) {
		if (this.isFirstTime) {
			ui_storage.set('Logger:Hero_HP', ui_stats.HP());
			ui_storage.set('Logger:Hero_Gold', ui_stats.Gold());
			ui_storage.set('Logger:Hero_Inv', ui_stats.Inv());
			ui_storage.set('Logger:Hero_Charges', ui_stats.Charges());
			ui_storage.set('Logger:Enemy_HP', ui_stats.Enemy_HP());
			ui_storage.set('Logger:Enemy_Gold', ui_stats.Enemy_Gold());
			ui_storage.set('Logger:Enemy_Inv', ui_stats.Enemy_Inv());
			ui_storage.set('Logger:Hero_Alls_HP', ui_stats.Hero_Alls_HP());
			for (i = 1; i <= 11; i++) {
				ui_storage.set('Logger:Hero_Ally'+i+'_HP', ui_stats.Hero_Ally_HP(i));
			}
			for (i = 1; i <= 6; i++) {
				ui_storage.set('Logger:Enemy'+i+'_HP', ui_stats.EnemySingle_HP(i));
			}
			ui_storage.set('Logger:Enemy_AliveCount', ui_stats.Enemy_AliveCount());
		}
		ui_informer.update('low health', ui_stats.lowHealth('fight'));
		return;
	}
	if (ui_data.hasShop) {
		if (ui_stats.inShop()) {
			if (ui_storage.get('Logger:Location') === 'Field') {
				ui_storage.set('Logger:Location', 'Store');
				ui_logger.suppressOldStats();
				ui_logger.update(ui_logger.fieldWatchers);
				ui_storage.set('Logger:Hero_Gold', ui_stats.Gold());
				ui_storage.set('Logger:Hero_Inv', ui_stats.Inv());
			}
			ui_data.inShop = true;
		} else if (ui_data.inShop) {
			ui_logger.update(ui_logger.shopWatchers);
			ui_data.inShop = false;
		}
	}
	if (!ui_data.inShop && ui_storage.get('Logger:Location') !== 'Field') {
		ui_storage.set('Logger:Location', 'Field');
	}
	if (!ui_utils.isAlreadyImproved(document.getElementById('stats'))) {
		// Add voicegens
		ui_utils.addVoicegen(document.querySelector('#hk_level .l_capt'), worker.GUIp_i18n.study, 'exp', worker.GUIp_i18n.fmt('ask_to_study', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_health .l_capt'), worker.GUIp_i18n.heal, 'heal', worker.GUIp_i18n.fmt('ask_to_heal', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_gold_we .l_capt'), worker.GUIp_i18n.dig, 'dig', worker.GUIp_i18n.fmt('ask_to_dig', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_quests_completed .l_capt'), worker.GUIp_i18n.cancel_task, 'cancel_task', worker.GUIp_i18n.fmt('ask_to_cancel_task', ui_data.char_sex[0]));
		ui_utils.addVoicegen(document.querySelector('#hk_quests_completed .l_capt'), worker.GUIp_i18n.do_task, 'do_task', worker.GUIp_i18n.fmt('ask_to_do_task', ui_data.char_sex[1]));
		ui_utils.addVoicegen(document.querySelector('#hk_death_count .l_capt'), worker.GUIp_i18n.die, 'die', worker.GUIp_i18n.fmt('ask_to_die', ui_data.char_sex[0]));
	}
	if (!document.querySelector('#hk_distance .voice_generator')) {
		ui_utils.addVoicegen(document.querySelector('#hk_distance .l_capt'), document.querySelector('#main_wrapper.page_wrapper_5c') ? '回' : worker.GUIp_i18n.return, 'town', worker.GUIp_i18n.fmt('ask_to_return', ui_data.char_sex[0]));
	}

	ui_informer.update('much gold', ui_stats.Gold() >= (ui_stats.hasTemple() ? 10000 : 3000));
	ui_informer.update('dead', ui_stats.HP() === 0);
	var questName = ui_stats.Task_Name();
	ui_informer.update('guild quest', questName.match(/членом гильдии|member of the guild/) && !questName.match(/\((отменено|cancelled)\)/));
	ui_informer.update('mini quest', questName.match(/\((мини|mini)\)/) && !questName.match(/\((отменено|cancelled)\)/));

	var townInformer = false;
	if (!ui_stats.isGoingBack() && ui_stats.townName() === '') {
		if (this.informTown === ui_stats.nearbyTown()) {
			townInformer = true;
		}
	} else if (this.informTown === ui_stats.townName()) {
		delete ui_improver.informTown;
		ui_storage.remove('townInformer');
		ui_improver.distanceInformerUpdate();
	}
	ui_informer.update('selected town', townInformer);

	// shovel imaging
	var digVoice = document.querySelector('#hk_gold_we .voice_generator');
	if (this.isFirstTime) {
		if (worker.GUIp_browser !== 'Opera') {
			digVoice.style.backgroundImage = 'url(' + worker.GUIp_getResource('images/shovel.png') + ')';
		} else {
			worker.GUIp_getResource('images/shovel.png',digVoice);
		}
	}
	if (ui_stats.goldTextLength() > 16 - 2*document.getElementsByClassName('page_wrapper_5c').length) {
		digVoice.classList.add('shovel');
		if (ui_stats.goldTextLength() > 20 - 3*document.getElementsByClassName('page_wrapper_5c').length) {
			digVoice.classList.add('compact');
		} else {
			digVoice.classList.remove('compact');
		}
	} else {
		digVoice.classList.remove('shovel');
	}
	// improve title of side quest bar with exact values
	var sjName = ui_stats.Side_Job_Name();
	if (sjName) {
		var sjReq = +(/\d+/.exec(sjName) || '')[0] || (/дважды|twice/.test(sjName) ? 2 : 0),
			sjBar = document.querySelector('#hk_quests_completed + .line .p_bar');
		if (sjBar && sjReq > 0) {
			sjBar.title = sjBar.title.replace(/( \(\d+\/\d+\))/,'') + ' (' + Math.round(ui_stats.Side_Job() / 100 * sjReq) + '/' + sjReq + ')';
		}
	}
};
ui_improver.improveShop = function() {
	var trdNode = document.getElementById('trader'), node;
	if (trdNode && !ui_utils.isAlreadyImproved(trdNode)) {
		var a, p, co = (trdNode.querySelectorAll('.line > a') || [])[2];
		if (co) {
			GUIp.common.addListener(co, 'click', function() {
				a = document.querySelector('#tordr + div');
				if (!a || !a.textContent.length) {
					return;
				}
				if (!a.classList.contains('hidden')) {
					p = a.textContent;
					GUIp.common.setTimeout(function() {
						if (p !== a.textContent) {
							return;
						}
						a.classList.add('hidden');
					},100);
				} else {
					a.classList.remove('hidden');
				}
			});
		}
		if ((node = trdNode.getElementsByClassName('l_slot')[0])) {
			node.insertAdjacentHTML('beforeend', '<div id="trademode_timer" class="fr_new_badge"></div>');
			ui_utils.hideElem(document.getElementById('trademode_timer'), true);
		}
	}
};
ui_improver.improvePet = function() {
	var petBadge = document.getElementById('pet_badge'),
		petLevelLabel = document.querySelector('#hk_pet_class + div .l_val'),
		level = ui_stats.Pet_Level(),
		petRes  = (level * 0.4 + 0.5).toFixed(1) + 'k',
		petRes2 = (level * 0.8 + 0.5).toFixed(1) + 'k';
	if (ui_stats.petIsKnockedOut()) {
		if (!ui_utils.isAlreadyImproved(document.getElementById('pet'))) {
			document.querySelector('#pet .r_slot').insertAdjacentHTML('afterbegin', '<div id="pet_badge" class="fr_new_badge e_badge_pos hidden">0</div>');
			petBadge = document.getElementById('pet_badge');
		}
		if (document.querySelector('#pet .block_content').style.display !== 'none') {
			petBadge.title = worker.GUIp_i18n.badge_pet2;
			petBadge.textContent = petRes;
		} else {
			petBadge.title = worker.GUIp_i18n.badge_pet1;
			petBadge.textContent = ui_utils.findLabel(worker.$('#pet'), worker.GUIp_i18n.pet_status_label).siblings('.l_val').text().replace(/[^0-9:]/g, '');
		}
		ui_utils.hideElem(petBadge, false);
	} else if (petBadge) {
		ui_utils.hideElem(petBadge, true);
	}
	if (petLevelLabel) {
		petLevelLabel.title = '↑ ' + petRes + (worker.GUIp_locale === 'ru' ? '\n⇈ ' + petRes2 : '');
	}
	// knock out informer
	ui_informer.update('pet knocked out', ui_stats.petIsKnockedOut());
};
ui_improver.describeEquipBoldness = function(mutations, observer) {
	var names = document.querySelectorAll('#equipment .eq_name'),
		name, eq;
	for (var i = 0, len = names.length; i < len; i++) {
		name = names[i];
		eq = name.parentNode;
		if (name.classList.contains('eq_b')) {
			eq.classList.add('e_eq_bold');
			name.title = GUIp_i18n.equip_boldness[i] ? eq.firstElementChild.textContent + ': ' + GUIp_i18n.equip_boldness[i] : '';
		} else {
			eq.classList.remove('e_eq_bold');
			name.title = '';
		}
	}
	observer.takeRecords();
};
ui_improver.improveEquip = function() {
	var observer;
	if (!ui_utils.isAlreadyImproved(document.getElementById('equipment'))) {
		document.querySelector('#equipment .r_slot').insertAdjacentHTML('afterbegin', '<div id="equip_badge" class="fr_new_badge e_badge_pos">0</div>');
		observer = GUIp.common.newMutationObserver(ui_improver.describeEquipBoldness);
		observer.observe(document.getElementById('equipment'), {subtree: true, attributes: true, attributeFilter: ['class']});
		ui_improver.describeEquipBoldness(null, observer);
	}
	var equipBadge = document.getElementById('equip_badge'),
		averageEquipLevel = 0;
	for (var i = 1; i <= 7; i++) {
		averageEquipLevel += ui_stats['Equip' + i]();
	}
	averageEquipLevel = averageEquipLevel / 7 - ui_stats.Level();
	averageEquipLevel = (averageEquipLevel >= 0 ? '+' : '') + averageEquipLevel.toFixed(1);
	if (equipBadge.textContent !== averageEquipLevel) {
		equipBadge.title = worker.GUIp_i18n.badge_equip;
		equipBadge.textContent = averageEquipLevel;
	}
};
ui_improver.improveSkills = function() {
	var block = document.getElementById('skills');
	if (!ui_utils.isAlreadyImproved(block)) {
		block.getElementsByClassName('r_slot')[0].insertAdjacentHTML('afterbegin',
			'<div id="skill_badge" class="fr_new_badge e_badge_pos"></div>'
		);
	}
	var skillBadge = document.getElementById('skill_badge'),
		skillList = block.getElementsByClassName('skill_info'),
		minSkillLevel = Infinity,
		minSkillPrice = '',
		skill, m, level, price;
	for (var i = 0, len = skillList.length; i < len; i++) {
		skill = skillList[i];
		if (!(m = /\d+/.exec(skill.textContent))) {
			continue;
		}
		level = +m[0];
		price = ((level + 1) / 2).toFixed(1) + 'k';
		skill.title = '↑ ' + price;
		if (level < minSkillLevel) {
			minSkillLevel = level;
			minSkillPrice = price;
		}
	}
	if (minSkillPrice && skillBadge.textContent !== minSkillPrice) {
		skillBadge.title = worker.GUIp_i18n.badge_skill;
		skillBadge.textContent = minSkillPrice;
	}
	ui_utils.hideElem(skillBadge, !minSkillPrice);
};
ui_improver.improvePlayers = function() {
	var player, matched = false,
		players = document.querySelectorAll('#alls .opp_n.opp_ng span, #bosses .opp_n.opp_ng span, #o_hk_godname .l_val a'),
		marks = ui_words.base.ally_blacklist || [];
	for (var i = 0, len = players.length; i < len; i++) {
		matched = false;
		player = players[i].nodeName === 'A' ? players[i].textContent : (players[i].textContent.match(/\((.*?)\)/) || [])[1];
		for (var j = 0, len2 = marks.length; j < len2; j++) {
			if (marks[j].q) {
				continue;
			}
			if (player === marks[j].n) {
				matched = true;
				if (!players[i].classList.contains('e_player_marked')) {
					players[i].dataset.etitle = players[i].title;
					players[i].dataset.ecss = players[i].style.cssText;
				}
				players[i].classList.add('e_player_marked');
				players[i].title = worker.GUIp_i18n.player_marked + (marks[j].r.length ? ': \n' + marks[j].r : '');
				players[i].style.cssText = players[i].dataset.ecss + ((marks[j].s && marks[j].s.length) ? ' ' + marks[j].s : '');
			}
		}
		if (!matched && players[i].classList.contains('e_player_marked')) {
			players[i].classList.remove('e_player_marked');
			players[i].title = players[i].dataset.etitle ? players[i].dataset.etitle : '';
			players[i].style.cssText = players[i].dataset.ecss ? players[i].dataset.ecss : '';
		}
	}
};
ui_improver.improveHP = function() {
	var hp, hps = document.querySelectorAll('#alls .opp_h, #opps .opp_h, #bosses .opp_h');
	for (var i = 0, len = hps.length; i < len; i++) {
		if ((hp = /^(\d+) \/ (\d+)$/.exec(hps[i].textContent))) {
			hps[i].title = +(+hp[1] / +hp[2] * 100).toFixed(2) + '%';
		} else {
			hps[i].title = '';
		}
	}
};
ui_improver.improveSendButtons = function(forcedUpdate) {
	var sendToButtons, pants = document.querySelector('#pantheons .block_content'),
		curGP = ui_stats.Godpower();
	if (this.isFirstTime) {
		sendToButtons = document.querySelectorAll('#cntrl div.chf_link_wrap a');
		for (var i = 0, len = sendToButtons.length; i < len; i++) {
			if (sendToButtons[i].textContent.match(/(Послать на тренировку|Spar a Friend)/i)) {
				sendToButtons[i].parentNode.classList.add("e_challenge_button");
			} else if (sendToButtons[i].textContent.match(/(Направить в подземелье|Drop to Dungeon)/i)) {
				sendToButtons[i].parentNode.classList.add("e_dungeon_button");
			} else if (sendToButtons[i].textContent.match(/(Снарядить в плавание|Set Sail)/i)) {
				sendToButtons[i].parentNode.classList.add("e_sail_button");
			} else if (sendToButtons[i].textContent.match(/(Посетить полигон|Explore Datamine)/i)) {
				sendToButtons[i].parentNode.classList.add("e_mining_button");
			}
		}
		if (ui_stats.Level() >= 14) {
			// personal statistics
			pants.insertAdjacentHTML('afterbegin', '<div class="guip p_group_sep"></div>');
			var insertStatsLnk = function() {
				pants.insertAdjacentHTML('beforeend', '<div class="guip_stats_lnk p_group_sep"></div>');
				pants.insertAdjacentHTML('beforeend',
					'<div class="guip_stats_lnk"><div class="line"><a class="no_link div_link"' +
					' href="//stats.' + worker.location.host + '/me" target="_blank"' +
					' title="' + worker.GUIp_i18n.statistics_title + '">' +
						worker.GUIp_i18n.statistics +
					'</a></div></div>'
				);
			};
			var panthLines = pants.getElementsByClassName('panth_line');
			if (panthLines[0]) {
				insertStatsLnk();
			} else {
				var i = 0, tid = GUIp.common.setInterval(function() {
					if (panthLines[0] || i++ >= 50) {
						insertStatsLnk();
						worker.clearInterval(tid);
					}
				},100);
			}
		}
	}
	sendToButtons = document.querySelectorAll('a.to_arena');
	for (var lim, i = 0, len = sendToButtons.length; i < len; i++) {
		lim = 50;
		if (!i && ui_improver.dailyForecast.includes('arena')) {
			lim = 25;
		}
		if (curGP < lim) {
			sendToButtons[i].classList.add('e_low_gp');
		} else {
			sendToButtons[i].classList.remove('e_low_gp');
		}
	}
	sendToButtons = document.querySelectorAll('#cntrl div.chf_link_wrap a');
	var sendToDelay, sendToStr, sendToDesc = document.querySelectorAll('#cntrl2 div.arena_msg, #cntrl2 span.to_arena');
	for (var i = 0, len = sendToDesc.length; i < len; i++) {
		if (sendToDesc[i].style.display === 'none') {
			continue;
		}
		if ((!sendToDesc[i].title.length || (sendToDesc[i].dataset.expires < Date.now() + 5000)) && (sendToStr = sendToDesc[i].textContent.match(/(Подземелье откроется через|Отплыть можно через|Арена откроется через|Тренировка через|Полигон откроется через|Босс освободится через|Arena available in|Dungeon available in|Sail available in|Sparring available in|Datamine available in|Boss ready in) (?:(\d+)(?:h| ч) )?(?:(\d+)(?:m| мин))/))) {
			sendToDelay = ((sendToStr[2] !== undefined ? +sendToStr[2] : 0) * 60 + +sendToStr[3]) * 60;
			sendToStr = sendToStr[1].replace(/ через/,' в').replace(/ in/,' at');
			sendToDesc[i].dataset.expires = Date.now() + sendToDelay * 1000;
			sendToDesc[i].title = sendToStr + ' ' + GUIp.common.formatTime(new Date(+sendToDesc[i].dataset.expires),'simpletime');
		}
	}
	if (this.isFirstTime || forcedUpdate) {
		var relocated, buttonInPantheons, relocateDuelButtons = ui_storage.getList('Option:relocateDuelButtons');
		relocated = relocateDuelButtons.includes('arena');
		buttonInPantheons = document.querySelector('#pantheons .arena_link_wrap');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('arena_link_wrap')[0], pants.firstChild);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelector('#control .arena_msg'));
		}
		relocated = relocateDuelButtons.includes('chf');
		buttonInPantheons = document.querySelector('#pantheons .e_challenge_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_challenge_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelector('#control .arena_msg').nextSibling);
		}
		relocated = relocateDuelButtons.includes('dun');
		buttonInPantheons = document.querySelector('#pantheons .e_dungeon_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_dungeon_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelectorAll('#control .arena_msg')[1]);
		}
		relocated = relocateDuelButtons.includes('sail');
		buttonInPantheons = document.querySelector('#pantheons .e_sail_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_sail_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelectorAll('#control .arena_msg')[2]);
		}
		relocated = relocateDuelButtons.includes('min');
		buttonInPantheons = document.querySelector('#pantheons .e_mining_button');
		if (relocated && !buttonInPantheons) {
			pants.insertBefore(document.getElementsByClassName('e_mining_button')[0], document.getElementsByClassName('guip p_group_sep')[0]);
		} else if (!relocated && buttonInPantheons) {
			document.getElementById('cntrl2').insertBefore(buttonInPantheons, document.querySelectorAll('#control .arena_msg')[3]);
		}
	}
};
/**
 * @returns {!Array<!GUIp.common.activities.DiaryEntry>}
 */
ui_improver.readDiaryOnce = function() {
	var root = document.getElementById('diary'),
		messages = root.querySelectorAll('.d_msg:not(.parsed)'),
		sortButton = root.getElementsByClassName('sort_ch')[0],
		result = [],
		ampm = worker.ampm === '12h',
		today = new Date,
		nearFuture = +today + 300e3, // 5m
		msg, time, m, hours, date, link;
	for (var i = 0, len = messages.length; i < len; i++) {
		msg = messages[i];
		msg.classList.add('parsed');
		time = msg.parentNode.getElementsByClassName('d_time')[0];
		if (!time || !(m = /(\d+):(\d+)/.exec(time.textContent))) {
			GUIp.common.warn('cannot parse diary: no time for "' + msg.textContent + '"');
			continue;
		}
		if (ampm) {
			// there are no AM or PM marks. we have to guess
			hours = +m[1];
			date = today.setHours(hours >= 12 ? hours - 12 : hours, +m[2], 0, 0);
			if (date > nearFuture) {
				date -= 86400e3; // 24h
			}
			if (date + 43200e3 <= nearFuture) { // 12h
				date += 43200e3; // 12h
			}
		} else {
			date = today.setHours(+m[1], +m[2], 0, 0);
			if (date > nearFuture) {
				date -= 86400e3; // 24h
			}
		}
		if (msg.classList.contains('m_infl')) {
			result.push({date: date, type: 'influence', msg: msg.textContent, logID: ''});
		} else if (msg.getElementsByClassName('vote_links_b')[0]) {
			result.push({date: date, type: 'foreignVoice', msg: msg.textContent, logID: ''});
		} else {
			link = msg.getElementsByTagName('a')[0]; // weird, it has .vote_link class
			result.push({
				date: date,
				type: 'regular',
				msg: msg.textContent,
				logID: link ? link.href.slice(link.href.lastIndexOf('/') + 1) : ''
			});
		}
	}
	if (!sortButton || sortButton.textContent === '▼') {
		// we cannot simply sort the entries by their date, because some of their dates might be identical
		// and we must keep them in the correct order in that case
		result.reverse();
	}
	return result;
};
ui_improver.improveDiary = function() {
	var diary = ui_improver.readDiaryOnce(),
		len = diary.length,
		infl = false,
		junk = 0;
	if (len && !ui_improver.isFirstTime && ui_improver.voiceSubmitted) {
		// run voice timeout
		for (var i = 0; i < len; i++) {
			switch (diary[i].type) {
				case 'influence': infl = true; break;
				case 'foreignVoice': junk++; break;
			}
		}
		if (infl) {
			if (len - junk >= 2) {
				ui_timeout.start();
			}
			ui_improver.voiceSubmitted = false;
		}
	}
	ui_timers.updateDiary(diary);
};
ui_improver.detectors = {};
ui_improver.detectors.detectLS = function() {
	var tmTimer = document.getElementById('trademode_timer');
	if (!ui_data.inShop || !this.stateLS || !tmTimer) {
		this.stateLS = {bp: -1, bt: 0, cp: -1, ct: 0, tpp: 0};
		ui_utils.hideElem(tmTimer, true);
		return;
	}
	var updateTimer = function() {
		if (this.stateLS.tpp && tmTimer) {
			tmTimer.title = '[' + GUIp.common.formatTime(new Date(+this.stateLS.bt - (this.stateLS.bp - 0.5) * this.stateLS.tpp),'simpletime') + '~' + GUIp.common.formatTime(new Date(+this.stateLS.ct + (100.5 - cp) * this.stateLS.tpp),'simpletime') + ']';
			tmTimer.textContent = GUIp.common.formatTime((+this.stateLS.ct - Date.now() + (100.5 - cp) * this.stateLS.tpp) / 60000,'remaining');
			ui_utils.hideElem(tmTimer, false);
		}
	};
	var cp = ui_stats.sProgress();
	if (this.stateLS.bp < 0) {
		// check cached data if any
		try {
			var cache = JSON.parse(ui_storage.get('Cache:stateLS'));
			if (Math.abs(cache.ct + (100.5 - cache.cp) * cache.tpp - Date.now() - (100.5 - cp) * cache.tpp) < 1800e3) {
				this.stateLS = cache;
				updateTimer.call(this);
				return;
			}
		} catch(e) {}
		this.stateLS.bp = cp;
		this.stateLS.bt = Date.now();
		return;
	}
	if (this.stateLS.bp < cp && this.stateLS.cp < 0) {
		this.stateLS.bp = this.stateLS.cp = cp;
		this.stateLS.bt = Date.now();
		return;
	}
	if (this.stateLS.bp < cp && this.stateLS.cp < cp) {
		this.stateLS.cp = cp;
		this.stateLS.ct = Date.now();
		this.stateLS.tpp = (this.stateLS.ct - this.stateLS.bt)/(this.stateLS.cp - this.stateLS.bp);
		// cache obtained values
		ui_storage.set('Cache:stateLS',JSON.stringify(this.stateLS));
	}
	updateTimer.call(this);
};
ui_improver.detectors.stateTP = {cnt: 0, res: false};
ui_improver.detectors.stateFishing = {pre: false, res: false};
ui_improver.detectors.stateGTF = {cnt: 0, res: false};
ui_improver.detectors.stateGTG = {init: -1, initTown: "", res: false};
ui_improver.detectors.stateSM = {res: false};
ui_improver.detectors.stateField = {inv: 0, gld: 0, dst: 0, intown: false, nbtown: "", task: "", monster: null, ldiary: ""};
Object.defineProperty(ui_improver.detectors, 'stateGB', { get: function() { return this.stateGTG; } });
ui_improver.detectors.detectField = function() {
	var sp = ui_stats.sProgress(),
		inv = ui_stats.Inv(),
		gld = ui_stats.Gold(),
		intown = !!ui_stats.townName(),
		nbtown = ui_stats.nearbyTown(),
		monster = ui_stats.monsterName().replace(/^(Undead|Восставший) /,''),
		milestones = ui_stats.mileStones(),
		task = ui_stats.Task_Name().replace(/ \((выполнено|completed|отменено|cancelled|эпик|epic)\)/g,''),
		ldiary = ui_stats.lastDiary()[0] || '';
	// going to capital
	if (ui_stats.isGoingBack()) {
		if (this.stateGTG.init < 0) {
			if (/\((?:выполнено|отменено|completed|cancelled)\)/.test(ui_stats.Task_Name()) || milestones < (GUIp_locale === 'ru' ? 3 : 5) ||
				ui_improver.dailyForecast.includes('gvroads') || (this.stateField.nbtown && nbtown !== this.stateField.nbtown && sp <= 85)) {
				this.stateGTG.res = true;
			}
			this.stateGTG.init = this.stateField.dst ? this.stateField.dst : milestones;
			this.stateGTG.initTown = this.stateField.nbtown ? this.stateField.nbtown : nbtown;
		} else if (this.stateGTG.initTown && this.stateGTG.initTown !== nbtown) {
			this.stateGTG.res = true;
		}
	} else if (ui_improver.detectors.stateGTG.init > -1) {
		ui_improver.detectors.stateGTG = {init: -1, initTown: "", res: false};
	}
	// go to fields
	if (monster || intown || milestones < this.stateField.dst || this.stateGTF.res && sp < this.stateField.sp) {
		this.stateGTF.cnt = 0;
		this.stateGTF.res = false;
	} else if (this.stateField.intown && !intown && !ui_stats.lastDiaryIsInfl()) {
		this.stateGTF.res = true;
	} else if (milestones > this.stateField.dst && ui_stats.progressDiff() > 2 && !(/\((мини|mini|гильд|guild)\)/.test(task))) {
		this.stateGTF.cnt++;
		if (this.stateGTF.cnt >= 2) {
			this.stateGTF.res = true;
		}
	}
	// fishing
	if (this.stateFishing.res || this.stateFishing.pre) {
		if (sp < this.stateField.sp || milestones != this.stateField.dst || inv < this.stateField.inv) {
			this.stateFishing.res = false;
			this.stateFishing.pre = false;
		} else if (this.stateFishing.pre && sp > 0 && sp < 35 && ui_stats.HP() > 0) { // dead people aren't fishing
			if (ui_storage.getFlag('Option:enableDebugMode')) {
				GUIp.common.debug('fishing might be detected (p1)');
				console.log(ui_stats.lastDiary());
			}
			this.stateFishing.res = true;
			this.stateFishing.pre = false;
		}
	} else if (sp < this.stateField.sp && sp < 5 && (this.stateField.inv - inv === 1 || (((new Date()).getMonth() < 2 || (new Date()).getMonth() > 10) && ui_stats.progressDiff() > 1)) && task === this.stateField.task && !(intown || this.stateField.intown || monster || ui_stats.isGoingBack()) && !ui_stats.lastDiaryIsInfl()) {
		if (sp > 0) {
			if (ui_storage.getFlag('Option:enableDebugMode')) {
				GUIp.common.debug('fishing might be detected (p2)');
				console.log(ui_stats.lastDiary());
			}
			this.stateFishing.res = true;
		} else if (ui_stats.lastDiaryIsInfl(1) || !(ui_stats.lastDiaryIsInfl(2) && /(«|“).*?(копай|золот|клад|dig|gold|treasure).*?(»|”)/i.test(ui_stats.lastDiary()[2] || ''))) {
			// ^ the condition above should prevent digging-boss-waiting phase to accidently trigger fishing detector
			this.stateFishing.pre = true;
		}
	}
	// trading process
	if (monster || milestones != this.stateField.dst || gld < this.stateField.gld || !intown && this.stateField.intown || sp < this.stateField.sp || ui_data.inShop) {
		this.stateTP.cnt = 0;
		this.stateTP.res = false;
	} else if (inv < this.stateField.inv && gld >= this.stateField.gld && sp === 0) {
		if (++this.stateTP.cnt >= 2) {
			this.stateTP.res = true;
		}
	}
	// strong monster
	if (monster && this.stateField.monster !== null && monster !== this.stateField.monster && ldiary !== this.stateField.ldiary && ldiary.includes(monster)) {
		this.stateSM.res = true;
	} else if (this.stateSM.res && (!monster || monster !== this.stateField.monster)) {
		this.stateSM.res = false;
	}
	// common
	this.stateField.inv = inv;
	this.stateField.gld = gld;
	this.stateField.dst = milestones;
	this.stateField.intown = intown;
	this.stateField.nbtown = nbtown;
	this.stateField.task = task;
	this.stateField.monster = monster;
	this.stateField.ldiary = ldiary;
	if (!monster) {
		this.stateField.sp = sp;
	}
};
ui_improver.distanceInformerInit = function() {
	var dstSelected, dstTown, dstContent, dstContentInner,
		dstLine = worker.$('#hk_distance .l_capt'),
		dstSaved = ui_storage.get('townInformer');
	if (dstLine) {
		dstLine.addClass('edst_header');
		dstLine.wup({
			title: worker.GUIp_i18n.town_informer,
			placement: 'bottom',
			width: 320,
			onShow: function(t) {
				try {
					var shift, pos = parseInt(t[0].style.left);
					if (pos < 5) {
						shift = 10 - pos;
						t[0].style.left = '10px';
						t[0].firstChild.style.left = (parseInt(t[0].firstChild.style.left) - shift) + 'px';
					}
				} catch (e) {}
			},
			content: function(t) {
				dstContent = $('<div></div>');
				dstSelected = worker.$('<div class="edst_headline"></div>');
				GUIp.common.addListener(dstSelected[0], 'click', function(ev) {
					ev.preventDefault();
					ui_improver.distanceInformerReset();
				});
				dstContent.append(dstSelected);
				dstContentInner = $('<div class="edst_towns"></div>');
				var towns, towns_keys, dist, name, desc, sh, pricemod, title = [],
					currentTown = ui_stats.townName();
				try {
					towns = JSON.parse(localStorage.town_c);
					towns_keys = Object.keys(towns.t).sort(function(a, b) { return a - b; });
					for (var i = 1, len = towns_keys.length; i < len; i++) {
						dist = +towns_keys[i];
						name = towns.t[dist];
						if (ui_improver.dailyForecast.includes('gvroads') && currentTown !== name) {
							continue;
						}
						title.length = 0;
						dstTown = worker.$('<div class="edst_tline chf_line"></div>').html('<div class="l">' + name + '</div><div class="r">(' + dist + (towns_keys[i+1] ? '&ndash;' + (+towns_keys[i+1] - 1) : '+') + ')</div>');
						if (currentTown === name) {
							dstTown.addClass('e_current_town');
							title.push(worker.GUIp_i18n.town_current);
						}
						if ((desc = towns.d[name])) {
							title.push(worker.GUIp_i18n.town_banner + desc.replace(/\s*,\s*/, worker.GUIp_i18n.town_as_well_as));
						}
						if (pricemod = ui_words.base.town_list.find(function(a) { return (a.skillsDiscount || a.skillsMarkup) && a.name === name; })) {
							title.push(worker.GUIp_i18n.fmt(pricemod.skillsDiscount ? 'town_skills_discount' : 'town_skills_markup', pricemod.skillsDiscount || pricemod.skillsMarkup));
						}
						if ((sh = towns.sh.indexOf(dist)) > -1) {
							dstTown.addClass('e_shifted_town');
							if (towns.t[towns.sh[sh^0x01]]) {
								title.push(worker.GUIp_i18n.fmt('town_shifted', towns.t[towns.sh[sh^0x01]]));
							}
						}
						if (title.length) {
							dstTown.prop('title', title.join('. ') + '.');
						}
						if (!ui_improver.dailyForecast.includes('gvroads')) {
							GUIp.common.addListener(dstTown[0], 'click', function(ev) {
								ev.preventDefault();
								ui_improver.distanceInformerSet(this);
							}.bind({name: name}));
						}
						dstTown.appendTo(dstContentInner);
					}
					if (ui_improver.dailyForecast.includes('gvroads')) {
						dstTown = worker.$('<div class="edst_tline chf_line"></div>').html('<div class="c">' + worker.GUIp_i18n.town_informer_gvroads + '</div>');
						dstTown.appendTo(dstContentInner);
					}
				} catch (e) {}
				dstContent.append(dstContentInner);
				ui_improver.distanceInformerUpdate(dstLine,dstSelected);
				dstLine.wup("hide");
				return dstContent;
			}
		});
		if (dstSaved) {
			ui_improver.informTown = dstSaved;
			ui_improver.distanceInformerUpdate(dstLine,dstSelected);
		}
	}
};
ui_improver.distanceInformerUpdate = function(dstLine,dstSelected) {
	dstLine = dstLine || worker.$('#hk_distance .l_capt'),
		dstSelected = dstSelected || worker.$('.edst_headline');
	var town = document.querySelector('.e_selected_town');
	if (town) {
		town.classList.remove('e_selected_town');
	}
	if (ui_improver.informTown) {
		dstSelected.html(worker.GUIp_i18n.town_informer_curtown + ': <strong>' + ui_improver.informTown + '</strong> [x]');
		dstSelected.attr('title',worker.GUIp_i18n.town_informer_reset);
		dstLine.addClass('edst_header_active');
		dstSelected.addClass('edst_headline_active');
		ui_improver.nearbyTownsFix();
	} else {
		dstSelected.text(worker.GUIp_i18n.town_informer_choose);
		dstSelected.attr('title','');
		dstLine.removeClass('edst_header_active');
		dstSelected.removeClass('edst_headline_active');
	}
};
ui_improver.distanceInformerSet = function(town) {
	ui_storage.set('townInformer',town.name);
	ui_improver.informTown = town.name;
	ui_improver.distanceInformerUpdate();
};
ui_improver.distanceInformerReset = function() {
	if (ui_improver.informTown) {
		delete ui_improver.informTown;
		ui_storage.remove('townInformer');
		ui_improver.distanceInformerUpdate();
	}
};
ui_improver.showDailyForecast = function() {
	var dfcTimer, forecastLink = document.getElementById('e_forecast'),
		news = document.getElementById('m_hero').previousElementSibling,
		forecastTitle = worker.GUIp_i18n.daily_forecast + (
			this.dailyForecast.length ? ': [' + this.dailyForecast.join(', ') + ']\n' : ':\n'
		) + this.dailyForecastText;
	if (forecastLink) {
		if (this.dailyForecastText) {
			forecastLink.title = forecastTitle;
		} else {
			forecastLink.parentNode.removeChild(forecastLink);
		}
	} else if (news && this.dailyForecastText) {
		forecastLink = document.createElement('a');
		forecastLink.id = 'e_forecast';
		forecastLink.textContent = '❅';
		forecastLink.href = '/news';
		forecastLink.title = forecastTitle;
		if (GUIp.common.isAndroid) {
			GUIp.common.addListener(forecastLink, 'click', function(ev) {
				ev.preventDefault();
				worker.alert(this.title);
			});
		} else {
			GUIp.common.addListener(forecastLink, 'click', function(ev) { ev.preventDefault(); });
		}
		news.insertBefore(forecastLink, null);
	}
	if (!ui_utils.isAlreadyImproved(news)) {
		GUIp.common.addListener(news, 'mousedown', function(e) {
			if (e.button !== 0) { return; }
			dfcTimer = GUIp.common.setTimeout(function() {
				e.preventDefault();
				ui_data._getWantedMonster(true);
				worker.alert(worker.GUIp_i18n.daily_forecast_update_notice);
			},1200);
		});
		GUIp.common.addListener(news, 'mouseup', function(e) {
			if (e.button !== 0) { return; }
			if (dfcTimer) {
				worker.clearTimeout(dfcTimer);
			}
		});
	}
};
ui_improver._onSubCheckboxClick = function() {
	// this: HTMLInputElement
	// see markup below
	var mask = this.dataset.eMask;
	if (!mask) return;
	var subscriptions = GUIp.common.parseJSON(ui_storage.get('ForumSubscriptions')) || {},
		tid = this.parentNode.dataset.eTid,
		sub = subscriptions[tid];
	if (!sub) return;

	if (this.checked) {
		sub.notifications |= mask;
	} else {
		sub.notifications &= ~mask;
	}
	ui_storage.set('ForumSubscriptions', JSON.stringify(subscriptions));
};
ui_improver.showSubsLink = function() {
	var forumLink, subsLink = document.getElementById('e_subs');
	if (!subsLink) {
		forumLink = document.querySelector('#menu_bar a[href="/forums"]');
		if (forumLink) {
			var subs_target = '/forums/show/1/#guip_subscriptions';
			subsLink = document.createElement('a');
			subsLink.id = 'e_subs';
			subsLink.className = 'em_font';
			subsLink.textContent = '▶';
			subsLink.href = subs_target;
			subsLink.title = worker.GUIp_i18n.forum_subs;
			forumLink.parentElement.insertBefore(document.createTextNode(' '),null)
			forumLink.parentElement.insertBefore(subsLink,null);
			worker.$('#e_subs').wup({
				title: worker.GUIp_i18n.forum_subs,
				placement: 'bottom',
				onShow: function(t) {
					try {
						var shift, pos = parseInt(t[0].style.left);
						if (pos < 5) {
							shift = 10 - pos;
							t[0].style.left = '10px';
							t[0].firstChild.style.left = (parseInt(t[0].firstChild.style.left) - shift) + 'px';
						}
					} catch (e) {}
				},
				content: function(t) {
					var fsubsContent = $('<div class="esubs_content"></div>');
					var fsubsLine, subscriptions = JSON.parse(ui_storage.get('ForumSubscriptions')) || {},
						informers = JSON.parse(ui_storage.get('ForumInformers')) || {},
						topics = worker.Object.keys(subscriptions),
						tid, sub, page, title;
					topics.sort(function(a,b) { return subscriptions[b].date - subscriptions[a].date; });
					for (var i = 0, len = topics.length; i < len; i++) {
						tid = topics[i];
						sub = subscriptions[tid];
						page = Math.ceil(sub.posts / 25);
						title = GUIp.common.escapeHTML(sub.name);
						fsubsLine = worker.$('<div class="esubs_line"></div>').html(
							'<div class="title">' +
								'<img alt="Comment" class="' + (tid in informers ? 'green' : 'grey') +
									'" src="/images/forum/clearbits/comment.gif" />' +
								' <a href="/forums/show_topic/' + tid + '" title="' + title + '" target="_blank">' +
									title +
								'</a>' +
							'</div>' +
							'<div class="info">' +
								GUIp.common.formatTime(new Date(sub.date),'simpledate') +
								', ' +
								GUIp.common.formatTime(new Date(sub.date),'simpletime') +
								'<span class="em_font"> ➠ </span>' +
								'<a href="/forums/show_topic/' + tid + '?page=' + page +
									'#guip_' + (sub.posts + 25 - page*25) +
									'" title="' + worker.GUIp_i18n.forum_subs_info + (sub.by || '&lt;?&gt;') +
									'" target="_blank">' +
									(sub.by || '&lt;?&gt;') +
								'</a>' +
							'</div>' +
							'<div class="options" data-e-tid="' + tid + '">' +
								(worker.GUIp_browser !== 'Opera' ?
									'<input type="checkbox" data-e-mask="1" ' +
									(sub.notifications & 0x1 ? ' checked' : '') +
									' title="' + GUIp_i18n.forum_subs_desktop_notif + '" />'
								: '') +
								'<input type="checkbox" data-e-mask="2" ' +
									(sub.notifications & 0x2 ? ' checked' : '') +
									' title="' + GUIp_i18n.forum_subs_sound_notif + '" />' +
							'</div>' +
							'<div style="clear: both;"></div>'
						);
						fsubsLine.appendTo(fsubsContent);
					}
					fsubsLine = worker.$('<div class="esubs_line"></div>').html('<div class="c"><a href="' + subs_target + '" target="_blank">' + worker.GUIp_i18n.forum_subs_full + '</a></div>');
					fsubsLine.appendTo(fsubsContent);
					fsubsContent.on('click', 'input', function() {
						GUIp.common.try2.call(this, ui_improver._onSubCheckboxClick);
					});
					worker.$('#e_subs').wup("hide");
					return fsubsContent;
				}
			});
		}
	}
};
ui_improver.parseChronicles = function(xhr) {
	this.needLog = false;
	var lastNotParsed, texts = [], infls = [],
		step = 1,
		step_max = +worker.Object.keys(this.chronicles)[0],
		matches = xhr.responseText.match(/<div class="new_line[^"]+"[^>]*style='[^']*'>[^]*?<div class="text_content .*?">[^]+?<\/div>/g);
	worker.chronicles = matches;
	worker.response = xhr.responseText;
	if (!matches) {
		GUIp.common.warn('initial parsing chronicles from the campaign page failed, map colorization disabled!');
		return;
	}
	for (var i = 0; step <= step_max; i++) {
		if (!matches[i]) { break; }
		lastNotParsed = true;
		if (!/<div class="text_content (opp_)?infl">/.test(matches[i])) {
			texts.push(matches[i].match(/<div class="text_content ">([^]+?)<\/div>/)[1].trim().replace(/&#39;/g, "'"));
		} else {
			infls.push(matches[i].match(/<div class="text_content (?:opp_)?infl">([^]+?)(<span|<\/div>)/)[1].trim().replace(/&#39;/g, "'"));
		}
		if (/<div class="new_line[^"]+"[^>]*style='[^']+'>/.test(matches[i]) && i > 0) {
			GUIp.common.parseSingleChronicle.call(ui_improver, texts, infls, step, true);
			lastNotParsed = false;
			texts = [];
			infls = [];
			step++;
		}
	}
	if (lastNotParsed) {
		GUIp.common.parseSingleChronicle.call(ui_improver, texts, infls, step, true);
	}
	var alliesMatch, alliesRE = /<a href=["']\/gods\/.*?>(.*?)<\/a>[^]+?<span id=["']hp\d["']>\d+<\/span> \/ (\d+)/g,
		alliesHP = {sum: 0};
	while (alliesMatch = alliesRE.exec(xhr.responseText)) {
		for (var i = 1; i <= 5; i++) {
			if (ui_stats.Hero_Ally_Name(i) === alliesMatch[1]) {
				alliesHP[i] = +alliesMatch[2];
				alliesHP.sum += alliesHP[i];
				break;
			}
		}
	}
	if (alliesHP.sum > 0 && (Object.keys(alliesHP).length === ui_stats.Hero_Alls_Count() + 1)) {
		ui_improver.alliesHP = alliesHP;
		ui_storage.set('Log:' + ui_stats.logId() + ':allhp', JSON.stringify(ui_improver.alliesHP));
	}
	ui_improver.colorDungeonMap();
};
ui_improver.deleteInvalidChronicles = function() {
	var isHiddenChronicles = true,
		chronicles = document.querySelectorAll('#m_fight_log .line.d_line');
	for (var i = chronicles.length - 1; i >= 0; i--) {
		if (isHiddenChronicles) {
			if (chronicles[i].style.display !== 'none') {
				isHiddenChronicles = false;
			}
		} else {
			if (chronicles[i].style.display === 'none') {
				chronicles[i].parentNode.removeChild(chronicles[i]);
			}
		}
	}
};
ui_improver.improveChronicles = function() {
	if (!GUIp.common[GUIp.common.dungeonPhrases[GUIp.common.dungeonPhrases.length - 1] + 'RegExp']) {
		GUIp.common.getDungeonPhrases(ui_improver.improveChronicles.bind(ui_improver),null);
	} else {
		//ui_improver.deleteInvalidChronicles();
		var i, len, lastNotParsed, mapText, texts = [], infls = [],
			chronicles = document.querySelectorAll('#m_fight_log .d_msg:not(.parsed)'),
			ch_down = document.querySelector('.sort_ch').textContent === '▼',
			step = ui_stats.currentStep();
		for (len = chronicles.length, i = ch_down ? 0 : len - 1; (ch_down ? i < len : i >= 0) && step; ch_down ? i++ : i--) {
			lastNotParsed = true;
			if (!chronicles[i].className.includes('m_infl')) {
				texts = [chronicles[i].textContent].concat(texts);
			} else {
				infls = [chronicles[i].textContent].concat(infls);
			}
			if (chronicles[i].parentNode.className.includes('turn_separator')) {
				GUIp.common.parseSingleChronicle.call(ui_improver, texts, infls, step);
				lastNotParsed = false;
				texts = [];
				infls = [];
				step--;
			}
			if (!chronicles[i].className.includes('m_infl')) {
				if (GUIp.common.bossHintRegExp.test(chronicles[i].textContent)) {
					chronicles[i].parentNode.classList.add('bossHint');
				}
				if (/voice from above announced that all bosses in|голос откуда-то сверху сообщил, что ни единого живого босса/.test(chronicles[i].textContent)) {
					chronicles[i].innerHTML = chronicles[i].innerHTML.replace(/(A pleasant voice from above announced that all bosses in this dungeon have perished and wished the intruders to burn in hell\.|Приятный голос откуда-то сверху сообщил, что ни единого живого босса в этом подземелье не осталось, и пожелал виновникам гореть в аду\.)/,function(m,p1) { return '<strong>' + p1 + '</strong>'; });
				}
			}
			chronicles[i].classList.add('parsed');
		}
		if (lastNotParsed) {
			GUIp.common.parseSingleChronicle.call(ui_improver, texts, infls, step);
		}

		if (!this.initial) {
			this.initial = true;
		}

		if (this.needLog) {
			if (worker.Object.keys(this.chronicles)[0] === '1' && chronicles.length < 20) {
				this.needLog = false;
				ui_improver.colorDungeonMap();
			} else if (this.dungeonXHRCount < 3) {
				this.dungeonXHRCount++;
				GUIp.common.requestLog(ui_stats.logId(), ui_improver.parseChronicles.bind(ui_improver));
			}
		}
		// informer
		if (worker.Object.keys(this.chronicles).length) {
			ui_informer.update('close to boss', this.chronicles[worker.Object.keys(this.chronicles).reverse()[0]].marks.includes('bossHint'));
		}

		if (ui_storage.get('Log:current') !== ui_stats.logId()) {
			ui_storage.set('Log:current', ui_stats.logId());
			ui_storage.set('Log:' + ui_stats.logId() + ':corrections', '');
			ui_storage.set('Log:' + ui_stats.logId() + ':wormholes', '[]');
			// detect dungeons of pledge and dungeons of mysterious pledge
			mapText = (document.getElementById('map') || {}).textContent;
			if (/Залога|Pledge/i.test(mapText)) {
				ui_logger.appendLogs(-1);
				ui_logger.finishBatch();
			} else if (/Загадки|Mystery/i.test(mapText)) {
				GUIp.common.getDomainXHR('/gods/api/' + encodeURIComponent(ui_data.god_name), function(xhr) {
					var logs = +ui_storage.get('Logger:Logs');
					try {
						logs = JSON.parse(xhr.responseText).wood_cnt - logs;
						if (!logs) return;
					} catch (e) {
						GUIp.common.warn('got invalid JSON from /gods/api/:', xhr.responseText);
						return;
					}
					// we calculate a difference instead of just showing "wd-1" in case user has opened a dungeon
					// after being inactive for some time
					ui_logger.appendLogs(logs);
					ui_logger.finishBatch();
				}, function(xhr) {
					GUIp.common.warn('failed to load /gods/api/ (' + xhr.status + '):', xhr.responseText);
				});
			}
		}
		ui_storage.set('Log:' + ui_stats.logId() + ':steps', ui_stats.currentStep());
		ui_storage.set('Log:' + ui_stats.logId() + ':map', JSON.stringify(ui_improver.getDungeonMap()));

		// stream the dungeon
		if (this.streamingManager.active) {
			this.streamingManager.uploadStep();
		}
	}
};

ui_improver.getDungeonMapNodes = function() {
	var result = [], rows = document.querySelectorAll('#map .dml');
	for (var i = 0, len = rows.length; i < len; i++) {
		result.push(Array.from(rows[i].getElementsByClassName('dmc')));
	}
	return result;
};

ui_improver.getDungeonMap = function() {
	var result = [], cells, row, rows = document.querySelectorAll('#map .dml');
	for (var i = 0, len = rows.length; i < len; i++) {
		row = [];
		cells = rows[i].querySelectorAll('.dmc');
		for (var j = 0, len2 = cells.length; j < len2; j++) {
			row.push(cells[j].textContent.trim());
		}
		result.push(row);
	}
	return result;
};

ui_improver.colorDungeonMap = function() {
	if (!ui_data.isDungeon) {
		return;
	}
	var step, mapCells, currentCell, trapMoveLossCount = 0,
		coords = GUIp.common.calculateExitXY(),
		heroesCoords = GUIp.common.calculateXY(GUIp.common.getOwnCell()),
		steps = worker.Object.keys(this.chronicles),
		steps_max = steps.length;
	if (steps_max !== ui_stats.currentStep()) {
		GUIp.common.warn('step count mismatch: parsed=' + steps_max + ', required=' + ui_stats.currentStep());
		return;
	}
	// describe map
	mapCells = document.querySelectorAll('#map .dml');
	for (step = 1; step <= steps_max; step++) {
		if (!this.chronicles[step]) {
			GUIp.common.error('data for step #' + step + ' is missing, colorizing map failed');
			return;
		}
		// directionless step
		if (this.chronicles[step].directionless) {
			var shortCorrection = (ui_storage.get('Log:' + ui_stats.logId() + ':corrections') || '')[this.directionlessMoveIndex++];
			if (shortCorrection) {
				this.chronicles[step].direction = this.corrections[shortCorrection];
			} else {
				var corrections = GUIp.common.calculateDirectionlessMove.call(ui_improver, '#map', coords, step);
				this.chronicles[step].direction = this.corrections[corrections[0]];
				ui_storage.set('Log:' + ui_stats.logId() + ':corrections',(ui_storage.get('Log:' + ui_stats.logId() + ':corrections') || '') + corrections);
			}
			this.chronicles[step].directionless = false;
		}
		// normal step
		GUIp.common.moveCoords(coords, this.chronicles[step]);
		// wormhole jump
		if (this.chronicles[step].wormhole) {
			if (this.chronicles[step].wormholedst === null) {
				var wormholeDst = JSON.parse(ui_storage.get('Log:' + ui_stats.logId() + ':wormholes')) || [];
				if (wormholeDst[this.wormholeMoveIndex]) {
					this.chronicles[step].wormholedst = wormholeDst[this.wormholeMoveIndex];
				} else if (step === steps_max) {
					GUIp.common.debug('getting wormhole target from actual coords mismatch');
					this.chronicles[step].wormholedst = [heroesCoords.y - coords.y, heroesCoords.x - coords.x];
					wormholeDst.push(this.chronicles[step].wormholedst);
					ui_storage.set('Log:' + ui_stats.logId() + ':wormholes',JSON.stringify(wormholeDst));
				} else {
					var result = GUIp.common.calculateWormholeMove.call(ui_improver, '#map', coords, step, true);
					if (!result.wm.length) {
						GUIp.common.debug('wcheck disabled, trying again');
						result = GUIp.common.calculateWormholeMove.call(ui_improver, '#map', coords, step, false);
					}
					if (result.wm.length) {
						GUIp.common.debug('found possible targets: [' + JSON.stringify(result) + ']');
						this.chronicles[step].wormholedst = result.wm[0];
						wormholeDst = wormholeDst.concat(result.wm);
						if (result.dm.length) {
							ui_storage.set('Log:' + ui_stats.logId() + ':corrections',(ui_storage.get('Log:' + ui_stats.logId() + ':corrections') || '') + result.dm);
						}
						ui_storage.set('Log:' + ui_stats.logId() + ':wormholes',JSON.stringify(wormholeDst));
					} else {
						GUIp.common.error('unknown wormhole destination!');
					}
				}
				this.wormholeMoveIndex++;
			}
			if (this.chronicles[step].wormholedst !== null) {
				if (mapCells[coords.y] && mapCells[coords.y].children[coords.x]) {
					currentCell = mapCells[coords.y].children[coords.x];
					GUIp.common.describeCell(currentCell,step,steps_max,this.chronicles[step],trapMoveLossCount,true);
				}
				coords.y += this.chronicles[step].wormholedst[0];
				coords.x += this.chronicles[step].wormholedst[1];
			}
		}
		if (!mapCells[coords.y] || !mapCells[coords.y].children[coords.x]) {
			break;
		}
		currentCell = mapCells[coords.y].children[coords.x];
		if (currentCell.textContent.trim() === '#') {
			GUIp.common.error(
				'parsed chronicle does not match the map at step #' + step +
				': either direction ("' + this.chronicles[step].direction + '") is invalid or map is out of sync!');
			break;
		}
		if (currentCell.textContent.trim() === '✖') {
			this.chronicles[step].chamber = true;
		}
		if (this.chronicles[step].pointers.length > 0) {
			currentCell.dataset.pointers = this.chronicles[step].pointers.join(' ');
		}
		trapMoveLossCount = GUIp.common.describeCell(currentCell,step,steps_max,this.chronicles[step],trapMoveLossCount);
	}
	if (heroesCoords.x !== coords.x || heroesCoords.y !== coords.y) {
		GUIp.common.error(
			'chronicle processing failed, coords diff: x: ' + (heroesCoords.x - coords.x) + ', y: ' + (heroesCoords.y - coords.y));
		if (ui_utils.hasShownInfoMessage !== true) {
			ui_utils.showMessage('info', {
				title: worker.GUIp_i18n.coords_error_title,
				content: '<div>' + worker.GUIp_i18n.coords_error_desc + ': [x:' + (heroesCoords.x - coords.x) + ', y:' + (heroesCoords.y - coords.y) + '].</div>'
			});
			ui_utils.hasShownInfoMessage = true;
		}
		if (this.wormholeMoveIndex) {
			this.wormholeMoveIndex = 0;
			ui_storage.set('Log:' + ui_stats.logId() + ':wormholes','[]');
		}
	}
	if (!this.nookOffset) {
		this.nookOffset = GUIp.common.calculateNookOffset();
	}
	// update broadcast link
	ui_utils.updateBroadcastLink(document.getElementById('e_broadcast'));
	ui_utils.updateBroadcastLink(document.querySelector('div.fightblink a'));
	// highlight treasury on the map
	GUIp.common.improveMap.call(this,'map');
	// map settings
	var dmapSettings = ui_storage.getList('Option:dungeonMapSettings');
	// mark exclamations
	if (!this.isFirstTime && dmapSettings.includes('excl') && (/Заметности|Clarity|Загадки|Mystery/i.test((document.getElementById('map') || {}).textContent) || Object.keys(GUIp.common.dmapExclCache).length)) {
		GUIp.common.dmapExcl();
		if (Object.keys(GUIp.common.dmapExclCache).length) {
			ui_storage.set('Log:' + ui_stats.logId() + ':excl', JSON.stringify(GUIp.common.dmapExclCache));
		}
	}
	// print dungeon coords
	GUIp.common.dmapCoords();
	// print dungeon dimensions
	if (document.querySelector('#map .block_title')) {
		var mapDimensions;
		if (!(mapDimensions = document.querySelector('.dmapDimensions'))) {
			mapDimensions = document.createElement('span');
			mapDimensions.classList.add('dmapDimensions');
			ui_utils.hideElem(mapDimensions, !dmapSettings.includes('dims'));
			document.querySelector('#map .block_title').insertBefore(mapDimensions,document.querySelector('#map .block_title #e_broadcast'));
		}
		mapDimensions.textContent = GUIp.common.dmapDimensions();
	}
};

/** @namespace */
ui_improver.reporter = {
	// module loading order is arbitrary, so we cannot simply assign a field
	/** @type {string} */
	get url() { return (worker.GUIp_locale === 'ru' ? GUIp.common.erinome_url : GUIp.common.erinome_url_en) + '/reporter'; },
	/** @type {string} */
	get urlMS() { return GUIp.common.erinome_url + '/mapStreamer'; },

	/**
	 * @param {string} logID
	 * @returns {string}
	 */
	getLogURL: function(logID) {
		return ui_data.isDungeon ? '/duels/log/' + logID + '?sort=desc&estreaming=1' : this.url + '/duels/log/' + logID;
	},

	_getHTML: function(id) {
		var node = document.getElementById(id);
		return node ? node.outerHTML : '';
	},

	/** @const {number} */
	protocolVersion: 3,

	/**
	 * @returns {!Object<string, (string|number)>}
	 */
	collect: function() {
		if (ui_data.isDungeon) {
			return {
				id: ui_stats.logId(),
				step: ui_stats.currentStep(),
				map: JSON.stringify(ui_improver.getDungeonMap()),
				lang: worker.GUIp_locale
			};
		}
		// https://github.com/Godvillers/ReporterServer/blob/master/docs/api.md
		var ru = worker.GUIp_locale === 'ru',
			data = ['alls', 's_map', 'm_fight_log'].map(this._getHTML).join('<&>');
		return {
			protocolVersion: this.protocolVersion,
			agent: 'eGUI+/' + ui_data.currentVersion,
			link: GUIp.common.resolveURL(ui_utils.getStat('#fbclink', 'href') || worker.location.href),
			stepDuration: ru ? 20 : 23,
			timezone: -new Date().getTimezoneOffset(),
			step: ui_stats.currentStep(),
			playerNumber: ui_improver.islandsMap.ownArk + 1,
			cargo: ui_stats.cargo(),
			data: worker.base64js.fromByteArray(worker.pako.deflate(data)),
			clientData: '{"erinome":' + JSON.stringify({
				conditions: Object.keys(ui_improver.islandsMap.manager.conditions)
			}) + '}'
		};
	},

	/**
	 * @param {!Object} data
	 * @param {function(!Object)} onsuccess
	 * @param {function(!Object)} onfailure
	 */
	send: function(data, onsuccess, onfailure) {
		GUIp.common.postXHR((ui_data.isDungeon ? this.urlMS + '/port' : this.url + '/send'), data, 'form-data', onsuccess, onfailure);
	},

	/**
	 * @param {function(!Object)} onsuccess
	 * @param {function(!Object)} onfailure
	 */
	fetchAPIInfo: function(onsuccess, onfailure) {
		GUIp.common.getXHR(this.url + '/api.json', onsuccess, onfailure);
	},

	/**
	 * @param {string} text
	 * @returns {?Object<string, *>}
	 */
	parseAPIInfo: function(text) {
		try {
			var api = JSON.parse(text);
			return api.currentVersion >= api.minSupportedVersion ? api : null;
		} catch (e) {
			return null;
		}
	},

	/**
	 * @param {!Object} api
	 * @returns {boolean}
	 */
	checkVersion: function(api) {
		return this.protocolVersion >= api.minSupportedVersion && this.protocolVersion <= api.currentVersion;
	}
};

ui_improver.streamingManager = {
	onsuccess: [],
	onfailure: [],
	onincompatibility: [],
	lastUploadedStep: 0,

	get active() {
		return ui_storage.get('Log:streaming') === ui_stats.logId();
	},

	set active(value) {
		ui_storage.set('Log:streaming', value ? ui_stats.logId() : '');
	},

	_runCallbacks: function(callbacks) {
		callbacks.forEach(function(f) { f(); });
	},

	uploadStep: function() {
		var self = this,
			reporter = ui_improver.reporter,
			step = ui_stats.currentStep();
		if (step <= this.lastUploadedStep) return;

		reporter.send(reporter.collect(), function() {
			// requests are asynchronous, so perform an extra check
			if (step > self.lastUploadedStep) {
				self.lastUploadedStep = step;
				self._runCallbacks(self.onsuccess);
			}
		}, function(xhr) {
			GUIp.common.error('cannot stream step ' + step + ' (' + xhr.status + '):', xhr.responseText);
			// investigate the problem
			if (xhr.status === 501 /*Not Implemented*/) {
				// stop trying to stream
				self.active = false;
				// apparently, we are using an outdated protocol - let's check that
				reporter.fetchAPIInfo(function(xhr) {
					var api = reporter.parseAPIInfo(xhr.responseText);
					if (api == null) {
						// got invalid JSON?
						GUIp.common.error("cannot parse Reporter API: '" + xhr.responseText + "'");
						self._runCallbacks(self.onfailure);
						return;
					}
					GUIp.common.info('version = ' + reporter.protocolVersion + ', api =', api);
					if (reporter.checkVersion(api)) {
						// our version seems to be OK, but we still got 501. Something mystical
						self._runCallbacks(self.onfailure);
					} else {
						// yes, we're indeed too ancient
						self._runCallbacks(self.onincompatibility);
					}
					// as said before, don't try again
				}, function(xhr) {
					GUIp.common.error('cannot fetch Reporter API:', xhr.status);
					self._runCallbacks(self.onfailure);
				});
				return;
			}
			// try again later
			GUIp.common.setTimeout(function() {
				if (step === ui_stats.currentStep()) {
					self.uploadStep();
				}
			}, 5e3);
		});
	}
};

ui_improver.initStreaming = function(container) {
	if (!container) {
		GUIp.common.error('streaming link injection failed');
		return;
	}

	container.insertAdjacentHTML('beforeend',
		'<div id="e_streaming">' +
			'<a href="#">' + worker.GUIp_i18n.start_streaming + '</a>' +
			'<span class="hidden">' + worker.GUIp_i18n.streaming_now + ' <span class="em_font">❌</span></span>' +
			'<a href="' + this.reporter.getLogURL(ui_stats.logId()) + '" target="_blank">' +
				worker.GUIp_i18n.streaming_now +
			'</a>' +
			' <a class="e_cancel em_font" href="#" title="' + worker.GUIp_i18n.cancel_streaming + '">❌</a>' +
		'</div>'
	);
	container = document.getElementById('e_streaming');
	var startLink = container.firstChild,
		placeholder = startLink.nextSibling,
		streamingLink = placeholder.nextSibling,
		stopLink = container.lastChild, // streamingLink.nextSibling is a text node containing whitespace
		established = this.streamingManager.active;
	if (established) {
		startLink.classList.add('hidden');
		// streamingManager.uploadStep is called from elsewhere
	} else {
		streamingLink.classList.add('hidden');
		stopLink.classList.add('hidden');
	}

	GUIp.common.addListener(startLink, 'click', function(ev) {
		ev.preventDefault();
		startLink.classList.add('hidden');
		if (!established) {
			placeholder.classList.remove('hidden');
		} else {
			streamingLink.classList.remove('hidden');
			stopLink.classList.remove('hidden');
		}
		ui_improver.streamingManager.active = true;
		ui_improver.streamingManager.uploadStep();
	});

	GUIp.common.addListener(stopLink, 'click', function(ev) {
		ev.preventDefault();
		startLink.classList.remove('hidden');
		streamingLink.classList.add('hidden');
		stopLink.classList.add('hidden');
		ui_improver.streamingManager.active = false;
	});

	this.streamingManager.onsuccess.push(function() {
		if (!established) {
			established = true;
			placeholder.classList.add('hidden');
			streamingLink.classList.remove('hidden');
			stopLink.classList.remove('hidden');
		}
	});

	var handler = function(msg) {
		container.textContent = msg;
		container.classList.add('error');
	};
	this.streamingManager.onincompatibility.push(handler.bind(null, worker.GUIp_i18n.streaming_is_unsupported));
	this.streamingManager.onfailure.push(handler.bind(null, worker.GUIp_i18n.streaming_failed));
};

/** @namespace */
ui_improver.islandsMap = {
	/** @type {?GUIp.common.islandsMap.Manager} */
	manager: null,

	/**
	 * @readonly
	 * @type {number}
	 */
	ownArk: -1,

	_saveData: function() {
		var logID = ui_stats.logId(),
			steps = ui_stats.currentStep(),
			prefix = 'Log:' + logID;
		if (steps === parseInt(ui_storage.get(prefix + ':steps'))) {
			return;
		}
		ui_storage.set('Log:current', logID);
		ui_storage.set(prefix + ':steps', steps);
		ui_storage.set(prefix + ':map',     '[' + this.manager.rawModel.map + ']');
		ui_storage.set(prefix + ':arks',    '[' + this.manager.rawModel.arks + ']');
		ui_storage.set(prefix + ':visited', '[' + this.manager.rawModel.visited + ']');
		ui_storage.set(prefix + ':pois',    '[' + this.manager.rawModel.pois + ']');
	},

	/**
	 * @private
	 * @param {string} key
	 * @returns {!Array<number>}
	 */
	_loadArray: function(key) {
		var arr = GUIp.common.parseJSON(ui_storage.get(key));
		return GUIp.common.isIntegralArray(arr) ? arr : [];
	},

	_tryLoadData: function() {
		var prefix = 'Log:' + ui_stats.logId(),
			map = this._loadArray(prefix + ':map');
		if (!map.length) return;
		var rawModel = new GUIp.common.islandsMap.conv.RawModel(
			map,
			this._loadArray(prefix + ':arks'),
			this._loadArray(prefix + ':visited'),
			this._loadArray(prefix + ':pois')
		);
		var loadedStep = parseInt(ui_storage.get(prefix + ':steps')),
			currentStep = ui_stats.currentStep();
		if (loadedStep === currentStep) {
			// we've just loaded a relevant model, nice
			this.manager.rawModel = rawModel;
		} else if (loadedStep && loadedStep < currentStep) {
			// a model is outdated, but it can be useful for data migration
			this.manager.model = GUIp.common.islandsMap.conv.decode(rawModel);
		}
	},

	_extractConditions: function(mapContainer) {
		var logID = ui_stats.logId(),
			key = 'Log:' + logID + ':conds',
			conditions = ui_storage.get(key);
		if (conditions != null) {
			this.manager.conditions = conditions ? GUIp.common.makeHashSet(conditions.split(',')) : Object.create(null);
			return;
		}

		conditions = GUIp.common.sailing.tryExtractConditions();
		if (conditions != null) {
			this.manager.conditions = GUIp.common.sailing.parseConditions(conditions);
			ui_storage.set(key, Object.keys(this.manager.conditions));
		} else {
			// TODO: retry on failure
			GUIp.common.requestLog(logID, function(xhr) {
				this.manager.conditions = GUIp.common.sailing.parseConditions(
					GUIp.common.sailing.extractConditionsFromHTML(xhr.responseText)
				);
				ui_storage.set(key, Object.keys(this.manager.conditions));
				this.manager.rawModel = null; // force rescanning the map
				this.updateMap(mapContainer);
			}.bind(this));
		}
	},

	_loadPOIColors: function() {
		if (!ui_storage.getList('Option:islandsMapSettings').includes('rndc')) {
			return;
		}

		var key = 'Log:' + ui_stats.logId() + ':poiColors',
			colors = GUIp.common.parseJSON(ui_storage.get(key)),
			colorizer = GUIp.common.islandsMap.vtrans.poiColorizer;
		if (GUIp.common.isIntegralArray(colors)) {
			colorizer.colors = colors;
		} else {
			GUIp.common.shuffleArray(colorizer.colors);
			ui_storage.set(key, '[' + colorizer.colors + ']');
		}
	},

	/**
	 * @param {string} type
	 */
	/*
	changeHintDrawer: function(type) {
		var imap = GUIp.common.islandsMap;
		imap.vtrans.hintManager.drawer = imap.vtrans.createHintDrawer(
			this.manager.model,
			this.manager.view,
			type,
			{whirlpoolZoneRadius: imap.defaults.getWhirlpoolZoneRadius(this.manager.conditions)}
		);
	},
	*/

	_checkBeasties: function() {
		if (!GUIp.common.sailing.phrases.beastiesCount) {
			return;
		}
		var enemies = document.querySelectorAll('#opps .opp_n');
		for (var i = 0, len = enemies.length; i < len; i++) {
			var maxHP = ui_stats.EnemySingle_MaxHP(i + 1);
			if (maxHP >= 100) {
				continue; // an ark, apparently
			}
			var text = enemies[i].textContent.trim();
			if (text && !GUIp.common.sailing.isBeastie(text) && !enemies[i].classList.contains('improved')) {
				enemies[i].insertAdjacentHTML('beforeend',
					'<span class="e_beareport"> ' +
					'<a title="' + worker.GUIp_i18n.sea_monster_report +
					'" href="' + GUIp.common.erinome_url +
						'/beastiereport?name=' + encodeURIComponent(text) +
						'&hp=' + maxHP +
						'&locale=' + worker.GUIp_locale +
					'" target="_blank">[?]</a></span>'
				);
				enemies[i].classList.add('improved');
			}
		}
	},

	/**
	 * @param {!Element} mapContainer
	 */
	updateMap: function(mapContainer) {
		var newSVG = mapContainer.getElementsByTagName('svg')[0];
		if (!newSVG) {
			throw new Error('the SVG is gone');
		}
		// update our structures
		this.manager.replaceSVG(newSVG);

		var model = this.manager.model,
			settings = ui_storage.getList('Option:islandsMapSettings');
		// apply changes to the SVG
		GUIp.common.islandsMap.defaults.vTransBindAll(model, this.manager.view, this.manager.conditions, settings);
		// detect missing beasties in our lists
		this._checkBeasties();
		// stream the sailing
		if (ui_improver.streamingManager.active) {
			// wait until beasties in the chronicle block are highlighted
			GUIp.common.setTimeout(function() { ui_improver.streamingManager.uploadStep(); }, 0);
		}

		var currentStep = ui_stats.currentStep(),
			rivalsNearby = false;
		if (model.arks[this.ownArk]) {
			var ownArkPos = model.arks[this.ownArk].pos;
			// remember distance to the port for custom informers
			ui_improver.sailPortDistance = GUIp.common.islandsMap.vec.dist(ownArkPos, model.port ? model.port.pos : 0x0);
			// show distances to port & rim
			var blockTitle = mapContainer.getElementsByClassName('block_title')[0];
			if (blockTitle) {
				var distInfo = blockTitle.getElementsByClassName('e_dist_info')[0];
				if (!distInfo) {
					// opera 12.x can't insert multiple nodes in insertAdjacentHTML at once
					// when any ancestor node has DOMNodeInserted event attached
					blockTitle.appendChild(document.createTextNode(' '));
					blockTitle.insertAdjacentHTML('beforeend', '<span class="e_dist_info"></span>');
					distInfo = blockTitle.lastChild;
				}
				distInfo.innerHTML = ui_utils.formatSailingDistInfo(model, ownArkPos, this.manager.conditions, settings);
			}
			// check distances to other arks
			rivalsNearby = currentStep >= 5 && model.arks.some(function(ark) {
				return ark && ark.pos !== ownArkPos && GUIp.common.islandsMap.vec.dist(ark.pos, ownArkPos) <= 3;
			});
		}
		ui_informer.update('close to rival', rivalsNearby);
	},

	/**
	 * @param {!Element} mapContainer
	 */
	initMap: function(mapContainer) {
		this.manager = new GUIp.common.islandsMap.Manager;
		this._tryLoadData(); // instead of parsing each time the page refreshes

		// get own ark number
		var name = ui_stats.charName();
		this.ownArk = [1, 2, 3, 4].findIndex(function(i) { return ui_stats.Hero_Ally_Name(i) === name; });

		// extract conditions
		this._extractConditions(mapContainer);

		this.manager.onPreUpdate.push(function(mger) {
			GUIp.common.islandsMap.defaults.vTransUnbindAll(mger.model, mger.view);
		});
		this.manager.onPostEncode.push(this._saveData.bind(this));

		// do initial processing
		this._loadPOIColors();
		this.updateMap(mapContainer);

		// watch for changes
		GUIp.common.islandsMap.observer.create(mapContainer, function() {
			try {
				this.manager.rawModel = null; // force rescanning the map
				this.updateMap(mapContainer);
			} catch (e) {
				GUIp.common.error('cannot update the map:', e);
			}
		}, this);
	}
};

ui_improver.improveSailChronicles = function() {
	GUIp.common.sailing.describeBeastiesOnPage('.d_msg:not(.parsed)', 'parsed', ui_storage.getList('Option:islandsMapSettings').includes('bhp'));
};

ui_improver.initSailing = function() {
	var mapContainer = GUIp.common.sailing.tryFindMapBlock();
	if (!mapContainer) {
		GUIp.common.setTimeout(ui_improver.initSailing, 250);
		return;
	}
	var rulerContainer = mapContainer.querySelector('.block_h .l_slot');
	if (!rulerContainer) {
		GUIp.common.error('ruler injection failed');
	} else {
		rulerContainer.insertAdjacentHTML('beforeend',
			'<span id="e_ruler_button" title="' + worker.GUIp_i18n.sail_ruler + '">📏</span>'
		);
		GUIp.common.islandsMap.vtrans.rulerManager.init(rulerContainer.lastChild);
	}
	ui_improver.initStreaming(document.querySelector('#m_fight_log .block_content'));
	ui_improver.islandsMap.initMap(mapContainer);
	ui_improver.improveSailChronicles();
};

ui_improver.whenWindowResize = function() {
	ui_improver.chatsFix();
	// sail mode column resizing
	if (ui_improver.sailPageResize) {
		var sccwidth,
			sclwidth = worker.$('#a_left_block').outerWidth(true),
			scrwidth = worker.$('#a_right_block').outerWidth(true),
			scl2width = worker.$('#a_left_left_block'),
			scr2width = worker.$('#a_right_right_block'),
			maxwidth = worker.$(worker).width() * 0.85;
		scl2width = scl2width[0] && scl2width[0].offsetWidth ? scl2width.outerWidth(true) : 0;
		scr2width = scr2width[0] && scr2width[0].offsetWidth ? scr2width.outerWidth(true) : 0;
		sccwidth = Math.max(Math.min((maxwidth - sclwidth - scl2width - scrwidth - scr2width),932),448); // todo: get rid of hardcoded values?
		worker.$('#a_central_block').width(sccwidth);
		sccwidth = worker.$('#a_central_block').outerWidth(true);
		//worker.$('#main_wrapper').width(sccwidth + sclwidth + scl2width + scrwidth + scr2width + 20);
		worker.$('.c_col .block_title').each(function() { worker.$(this).width(sccwidth - 115); });
	}
	// body widening
	worker.$('body').width(worker.$(worker).width() < worker.$('#main_wrapper').width() ? worker.$('#main_wrapper').width() : '');
};
ui_improver._clockUpdate = function() {
	worker.$('#control .block_title').text(ui_utils.formatClock(ui_utils.getPreciseTime(ui_improver.clock.offset)));
	if (Date.now() > ui_improver.clock.switchOffTime) {
		ui_improver._clockToggle();
	}
};
ui_improver._clockToggle = function(e) {
	if (e) {
		e.stopPropagation();
	}
	if (ui_improver.clockToggling) {
		return;
	}
	ui_improver.clockToggling = true;
	var $clock = worker.$('#control .block_title');
	if (ui_improver.clock) {
		worker.clearInterval(ui_improver.clock.updateTimer);
		$clock.fadeOut(500, GUIp.common.try2.bind(ui_improver.clock.prevText, function() {
			$clock.removeClass('e_clock_inaccurate');
			$clock.text(this).fadeIn(500);
			$clock.prop('title', worker.GUIp_i18n.show_godville_clock);
			ui_improver.clockToggling = false;
		}));
		ui_improver.clock = null;
	} else {
		ui_improver.clock = {
			prevText: $clock.text(),
			switchOffTime: Date.now() + 300e3, // 5 min
			offset: ui_storage.getFlag('Option:localtimeGodvilleClock') ? (
				new Date().getTimezoneOffset() * -60e3
			) : (ui_storage.get('Option:offsetGodvilleClock') || 3) * 3600e3,
			updateTimer: 0
		};
		var ok = false;
		Promise.all([
			ui_utils.syncClock().then(function(success) {
				ok = success;
			}),
			new Promise(function(resolve) {
				$clock.fadeOut(500, GUIp.common.try2.bind(null, function() {
					$clock.fadeIn(500);
					ui_improver.clockToggling = false;
					if (!ok) {
						$clock.text('--:--:--').addClass('e_clock_inaccurate');
					}
					$clock.prop('title', worker.GUIp_i18n.hide_godville_clock);
					resolve();
				}));
			})
		]).then(function() {
			if (!ui_improver.clock || ui_improver.clock.updateTimer) {
				return; // user hid the clock before it got synced
			}
			if (ok) {
				ui_improver._clockUpdate();
				ui_improver.clock.updateTimer = GUIp.common.setTimeout(function() {
					ui_improver._clockUpdate();
					ui_improver.clock.updateTimer = GUIp.common.setInterval(ui_improver._clockUpdate, 1e3);
				}, 1e3 - ui_utils.getPreciseTime().getMilliseconds());
			} else {
				// ui_improver.clockToggling = false;
				ui_improver._clockToggle();
			}
		}).catch(GUIp.common.onUnhandledException);
	}
};

ui_improver.improveInterface = function(forcedUpdate) {
	var node;
	if (this.isFirstTime) {
		worker.$('a[href="#"]').removeAttr('href');
		ui_improver.whenWindowResize();
		GUIp.common.addListener(worker, 'resize', GUIp.common.debounce(250, ui_improver.whenWindowResize));
		if (ui_storage.getFlag('Option:themeOverride')) {
			ui_utils.switchTheme(ui_storage.get('ui_s'),true);
		} else {
			ui_utils.switchTheme();
		}
		GUIp.common.addListener(worker, 'focus', function() {
			ui_improver.checkGCMark('focus');
			ui_informer.updateCustomInformers();
			// forcefully fire keyup event on focus to prevent possible issues with alt key processing
			worker.$(document).trigger(worker.$.Event('keyup', {originalEvent: {}}));
		});
		// cache private messages
		ui_utils.pmNotificationInit();
		// experimental keep-screen-awake feature for Firefox Mobile
		if (GUIp.common.isAndroid && worker.navigator.requestWakeLock) {
			ui_improver.createWakelock();
		}
		// generate nearby town information on usual place
		if (!ui_data.isFight) {
			ui_improver.nearbyTownsFix();
		}
		if (!ui_data.isFight && !ui_storage.getFlag('Option:disableGodvilleClock') && document.querySelector('#control .block_title')) {
			var controlTitle = document.querySelector('#control .block_title');
			controlTitle.title = worker.GUIp_i18n.show_godville_clock;
			controlTitle.style.cursor = 'pointer';
			GUIp.common.addListener(controlTitle, 'click', ui_improver._clockToggle);
		}
		var encButton = worker.$('#cntrl .enc_link'),
			punButton = worker.$('#cntrl .pun_link'),
			mirButton = worker.$('#cntrl .mir_link');
		if (ui_storage.getFlag('Option:improveDischargeButton')) {
			var dischargeHandlers, dischargeButton = worker.$('#acc_links_wrap .dch_link');
			encButton.click(function() { ui_improver.last_infl = 'enc'; });
			punButton.click(function() { ui_improver.last_infl = 'pun'; });
			mirButton.click(function() { ui_improver.last_infl = 'mir'; });
			if (dischargeButton.length) {
				dischargeHandlers = worker.$._data(dischargeButton[0],'events');
				if (dischargeHandlers && dischargeHandlers.click && dischargeHandlers.click.length === 1) {
					dischargeButton.click(function() {
						try {
							var limit = ui_stats.Max_Godpower() - 50;
							if (ui_improver.dailyForecast.includes('accu70')) {
								limit -= 20;
							}
							if (worker.$('#voice_submit').attr('disabled') === 'disabled') {
								limit += 5;
							}
							if (!worker.$('#cntrl .enc_link').hasClass('div_link')) {
								if (ui_improver.last_infl === 'mir') {
									limit += 50;
								} else {
									limit += 25;
								}
							}
							GUIp.common.debug('acc_discharge dynamic limit =', limit);
							localStorage.setItem('gp_thre',limit);
						} catch (e) {
							GUIp.common.error('setting discharge dynamic limit failed:', e);
						}
					});
					dischargeHandlers.click.reverse();
				}
			}
		}
		if (!ui_data.isFight) {
			ui_utils.addGPConfirmation(encButton, 25,
				worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n[ui_stats.isMale() ? 'to_encourage_hero' : 'to_encourage_heroine']));
			ui_utils.addGPConfirmation(punButton, 25,
				worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n[ui_stats.isMale() ? 'to_punish_hero' : 'to_punish_heroine']));
			ui_utils.addGPConfirmation(mirButton, 50,
				worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n.to_make_a_miracle));
		}
		if (worker.GUIp_browser === 'Firefox') {
			try {
				worker.$(document).bind('click', function(e) {
					if (e.which !== 1) {
						e.stopImmediatePropagation();
					}
				});
				var rmClickFix, clickHandlers = worker.$._data(document,'events');
				if (clickHandlers.click && clickHandlers.click.length) {
					rmClickFix = clickHandlers.click.pop();
					clickHandlers.click.splice(clickHandlers.click.delegateCount || 0,0,rmClickFix);
				}
			} catch (e) {
				GUIp.common.error('failed to init rmclickfix workaround:', e);
			}
		}
		GUIp.common.addListener(worker, 'beforeunload', function() {
			// save unsent personal messages
			var node = document.getElementsByClassName('chat_ph')[0], text;
			if (node) {
				var textareas = node.getElementsByTagName('textarea'),
					names = node.getElementsByClassName('dockfrname');
				for (var i = 0, len = Math.min(textareas.length, names.length); i < len; i++) {
					node = textareas[i];
					if (!node.disabled) {
						if ((text = node.value)) {
							ui_tmpstorage.set('PM:' + names[i].textContent, text);
						} else {
							ui_tmpstorage.remove('PM:' + names[i].textContent);
						}
					}
				}
			}
			if (worker.GUIp_browser === 'Chrome') {
				// when the page is closing chrome leaves notifications created by it with removed onclick handlers,
				// making it impossible for user to close them by clicking at arbitrary place on the notification
				GUIp.common.postErinomeMessage({type: 'notifyHideAll'});
			}
		});
		if ((node = document.getElementById('logout'))) {
			GUIp.common.addListener(node, 'click', ui_tmpstorage.wipeOut.bind(ui_tmpstorage));
		}
		// workaround for title-related issues
		Object.defineProperty(document,'title', {
			set: function(val) {
				try {
					ui_data.docTitle = val;
					ui_informer.redrawTitle();
					return val;
				} catch (e) { return val; }
			},
			get: function() {
				try {
					return document.querySelector('title').childNodes[0].nodeValue;
				} catch (e) { return ''; }
			},
			configurable: true
		});
		ui_informer.redrawTitle();
		GUIp.common.addCSSFromString(ui_storage.get('UserCss'));
	}
	if (ui_data.isFight && !document.getElementById('e_broadcast')) {
		var qselector = document.querySelector(ui_data.isDungeon ? '#map .block_title, #control .block_title, #m_control .block_title' : '#control .block_title, #m_control .block_title');
		if (qselector) {
			if ((ui_data.isSail || ui_data.isMining) && worker.GUIp_locale === 'en') {
				qselector.textContent = 'Remote';
			}
			qselector.insertAdjacentHTML('beforeend', '<a id="e_broadcast" class="broadcast" href="/duels/log/' + ui_stats.logId() + ui_utils.generateBroadcastLinkXtra() + '" target="_blank">' + worker.GUIp_i18n.broadcast + '</a>');
		}
	}
	if (!document.getElementById('e_custom_informers_setup') && ui_informer.activeInformers.custom_informers) {
		ui_utils.generateLightboxSetup('custom_informers','#stats .block_title, #m_info .block_title, #b_info .block_title',function() { ui_words.init(); });
	}
	if (!ui_data.isFight && !document.getElementById('e_custom_craft_setup') && ui_storage.getFlag('Option:enableCustomCraft')) {
		ui_utils.generateLightboxSetup('custom_craft','#inventory .block_title',function() { ui_words.init(); ui_inventory.rebuildCustomCraft(); ui_improver.calculateButtonsVisibility(true); });
	}
	if (ui_data.isFight && !document.getElementById('e_ally_blacklist_setup') && document.querySelector('#alls .block_title, #bosses .block_title, #o_info .block_title')) {
		ui_utils.generateLightboxSetup('ally_blacklist',(document.querySelector('#o_info .l_val a[href*="/gods/"]') ? '#o_info' : '#alls')+' .block_title, #bosses .block_title',function() { ui_words.init(); ui_improver.improvePlayers(); });
	}
	if (this.isFirstTime || forcedUpdate) {
		GUIp.common.setPageBackground(ui_storage.get('Option:useBackground'));
	}
};
/**
 * @param {!Element} root
 * @returns {!Array<!GUIp.common.activities.LastFightsEntry>}
 */
ui_improver.getLastFights = function(root) {
	var ftypes = root.getElementsByClassName('wl_ftype');
	if (!ftypes.length) return [];
	root = ftypes[0].parentNode.parentNode;
	var dates = root.getElementsByClassName('wl_date'),
		result = [],
		ftype, link, href;
	for (var i = 0, len = Math.min(ftypes.length, dates.length); i < len; i++) {
		ftype = ftypes[i];
		link = ftype.getElementsByTagName('a')[0];
		href = link.href;
		result[i] = {
			date: +GUIp.common.parseDateTime(dates[i].textContent),
			type: GUIp.common.activities.parseFightType(link.textContent),
			logID: href.slice(href.lastIndexOf('/') + 1),
			success: ftype.textContent.includes('✓')
		};
	}
	return result.sort(ui_utils.byDate);
};
ui_improver.improveLastFights = function() {
	var popover = document.getElementById('lf_popover_c');
	if (!popover || ui_utils.isAlreadyImproved(popover)) {
		return;
	}
	var wupStyle = $(popover).closest('.wup')[0].style;
	wupStyle.width = (parseInt(wupStyle.width) + 30) + 'px';
	ui_utils.observeUntil(popover, {childList: true, subtree: true}, function() {
		var fights = ui_improver.getLastFights(popover);
		if (fights.length) return fights;
	}).then(function(fights) {
		var activities = ui_timers.updateLastFights(fights),
			byID = Object.create(null),
			links = popover.querySelectorAll('.wl_ftype.wl_stats a'),
			desc = null,
			i, len, act, link, id;
		for (i = 0, len = activities.length; i < len; i++) {
			act = activities[i];
			if (act.type === 'dungeon' || act.type === 'mining' || act.type === 'spar') {
				byID[act.logID] = act;
			}
		}
		for (i = 0, len = links.length; i < len; i++) {
			link = links[i];
			id = link.href;
			id = id.slice(id.lastIndexOf('/') + 1);
			if (!(act = byID[id]) || !act.result) {
				continue;
			}
			desc = GUIp.common.activities.describe(act, desc);
			link.parentNode.insertAdjacentHTML('afterend',
				'<div class="e_fight_result ' + desc.class + '" title="' + desc.title + '" data-e-id="' + id + '">' +
					desc.content +
				'</div>'
			);
		}
	}).catch(GUIp.common.onUnhandledException);
};
/**
 * @param {string} activitiesStr
 */
ui_improver.redrawLastFights = function(activitiesStr) {
	var root = document.getElementById('lf_popover_c'),
		desc = null,
		nodes, activities, byID, i, len, act, node;
	if (!root) return;
	nodes = Array.from(root.getElementsByClassName('e_fight_result_unknown'));
	if (!nodes.length) return;
	activities = JSON.parse(activitiesStr);
	byID = Object.create(null);
	for (i = 0, len = activities.length; i < len; i++) {
		act = activities[i];
		byID[act.logID] = act;
	}
	for (i = 0, len = nodes.length; i < len; i++) {
		node = nodes[i];
		if (!(act = byID[node.dataset.eId]) || act.result < 0) {
			continue;
		}
		desc = GUIp.common.activities.describe(act, desc);
		node.className = 'e_fight_result ' + desc.class;
		node.title = desc.title;
		node.textContent = desc.content;
	}
};
ui_improver.improveLastVoices = function() {
	if (!document.querySelector('#gv_popover_c .gv_list_empty')) {
		return;
	}
	var lvContent = document.querySelectorAll('.lv_text .div_link_nu');
	if (!lvContent.length) {
		return;
	}
	var handler = ui_utils.triggerChangeOnVoiceInput.bind(ui_utils);
	for (var i = 0, len = lvContent.length; i < len; i++) {
		GUIp.common.addListener(lvContent[i], 'click', handler);
	}
};
ui_improver.improveStoredPets = function() {
	var wtitle = document.querySelector('.wup-inner > .wup-title');
	if (!wtitle || !/В ковчеге|In the ark/.test(wtitle.textContent)) {
		return;
	}
	var splinks = wtitle.parentNode.querySelectorAll('.wup-content .wup_line > a:not(.no_link)');
	if (!splinks.length) {
		return;
	}
	var sp = [];
	for (var i = 0, len = splinks.length; i < len; i++) {
		sp.push(splinks[i].textContent.toLowerCase());
	}
	ui_storage.set('charStoredPets',JSON.stringify(sp));
	ui_data.storedPets = sp;
};
ui_improver.improveLab = function() {
	var wtitle = document.querySelector('.wup-inner > .wup-title');
	if (!wtitle || !/В лаборатории|In the Lab/.test(wtitle.textContent)) {
		worker.clearInterval(ui_improver.improveLabInt);
		return;
	}
	var wcontent = wtitle.parentNode.querySelector('.wup-content .bps_mf.wup_line');
	if (!wcontent || wcontent.classList.contains('improved')) {
		return;
	}
	wcontent.classList.add('improved');
	// improve boss parts list
	var itm, bSrc, bDst, prefix,
		bpLines = wtitle.parentNode.querySelectorAll('.wup-content .wup_line.bps_line'),
		inventory = ui_inventory._getInventory();
	for (var i = 0, len = bpLines.length; i < len; i++) {
		itm = bpLines[i].querySelector('div.bps_val');
		bSrc = itm.textContent.replace(/^(.*?)\ \(.+\)$/,"$1");
		GUIp.common.addListener(itm, 'click', function() {
			this.classList.toggle('e_match_part');
			for (var j = 0, len2 = bpLines.length; j < len2; j++) {
				var part = bpLines[j].querySelector('div.bps_val')
				if (part) {
					part.classList[this.textContent === part.textContent && this.classList.contains('e_match_part') ? 'add' : 'remove']('e_match_part');
				}
			}
		});
		if (ui_data.isBoss) {
			if ((bSrc === ui_stats.Enemy_Bossname() || (bSrc.includes(ui_stats.Enemy_Bossname()) && ui_stats.Enemy_HasAbility(/зовущий|summoning/)))) {
				itm.classList.add('e_current_boss');
			}
		} else {
			prefix = GUIp_i18n.bp_types[i] + GUIp_i18n.bp_prefix;
			for (var itemname, j = 0, len2 = inventory.length; j < len2; j++) {
				if (!ui_inventory._isBold(inventory[j])) {
					continue;
				}
				itemname = ui_inventory._getItemName(inventory[j]).replace(/^влюблённое /,'');
				if (itemname.toLowerCase().startsWith(prefix)) {
					bDst = itemname.slice(prefix.length);
					if (bDst[0] === bDst[0].toLowerCase()) {
						continue; // the first letter is lowercase -> probably it's not a boss part at all
					}
					if (bDst === bSrc) {
						continue; // exactly the same part; ignore
					}
					if (!itm.title.length) {
						itm.title = worker.GUIp_i18n.bp_others;
					}
					itm.title += '\n · ' + itemname;
					itm.classList.add('e_new_part');
				}
			}
		}
	}
	// update logger with lab creatures
	ui_logger.update(ui_logger.labWatchers);
};
ui_improver.improveSparMenu = function() {
	var lines, names, content = document.getElementById('chf_popover_c');
	if (!content) {
		return;
	}
	lines = content.getElementsByClassName('chf_line');
	for (var i = 0, len = lines.length; i < len; i++) {
		names = lines[i].firstChild.textContent.match(/^(.+?) \((.+?)\)/);
		if (!names || names.length < 3) {
			continue;
		}
		lines[i].insertAdjacentHTML('beforeend','<div class="e_chfr"><a class="div_link em_font">[✉]</a><a class="div_link em_font">[➠]</a></div>');
		(function(name){
			GUIp.common.addListener(lines[i].lastChild.firstChild, 'click', function(e) {
				ui_utils.openChatWith(name);
				e.stopPropagation();
			});
			lines[i].lastChild.firstChild.title = worker.GUIp_i18n.open_pchat;
			GUIp.common.addListener(lines[i].lastChild.lastChild, 'click', function(e) {
				worker.open("/gods/" + encodeURIComponent(name));
				e.stopPropagation();
			});
			lines[i].lastChild.lastChild.title = worker.GUIp_i18n.open_ppage;
		})(names[1]);
	}
};
ui_improver.calculateButtonsVisibility = function(forcedUpdate) {
	var i, j, len, baseCond = !ui_utils.isHidden(document.getElementById('godvoice') || {}) && !ui_storage.getFlag('Option:disableVoiceGenerators') && ui_stats.HP() > 0,
		isMonster = ui_stats.monsterName();
	if (!ui_data.isFight) {
		// inspect buttons
		var inspBtns = document.getElementsByClassName('inspect_button'),
			inspBtnsBefore = [], inspBtnsAfter = [];
		for (i = 0, len = inspBtns.length; i < len; i++) {
			inspBtnsBefore[i] = !inspBtns[i].classList.contains('hidden');
			inspBtnsAfter[i] = baseCond && !isMonster;
		}
		ui_improver.setButtonsVisibility(inspBtns, inspBtnsBefore, inspBtnsAfter);
		// craft buttons
		if (this.isFirstTime || forcedUpdate) {
			this.crftBtns = [document.getElementsByClassName('craft_button b_b')[0],
							 document.getElementsByClassName('craft_button b_r')[0],
							 document.getElementsByClassName('craft_button r_r')[0],
							 document.getElementsByClassName('craft_button span')[0],
							 document.getElementsByClassName('craft_group b_b')[0],
							 document.getElementsByClassName('craft_group b_r')[0],
							 document.getElementsByClassName('craft_group r_r')[0]
							];
			this.crftCustom = [[],[],[]];
			for (i = 0; i < 3; i++) {
				var ccrbs = this.crftBtns[i+4].getElementsByClassName('craft_button');
				for (j = 0, len = ccrbs.length; j < len; j++) {
					this.crftCustom[i].push(this.crftBtns.length);
					this.crftBtns.push(ccrbs[j]);
				}
			}
		}
		var crftBtnsBefore = [], crftBtnsAfter = [],
			crftFixed = ui_storage.getFlag('Option:fixedCraftButtons');
		for (i = 0, len = this.crftBtns.length; i < len; i++) {
			crftBtnsBefore[i] = !this.crftBtns[i].classList.contains('hidden');
			crftBtnsAfter[i] = !(!crftFixed && (!baseCond || isMonster));
		}
		crftBtnsAfter[0] = crftBtnsAfter[0] && ui_inventory.b_b.length;
		crftBtnsAfter[1] = crftBtnsAfter[1] && ui_inventory.b_r.length;
		crftBtnsAfter[2] = crftBtnsAfter[2] && ui_inventory.r_r.length;
		crftBtnsAfter[3] = crftBtnsAfter[0] || crftBtnsAfter[1] || crftBtnsAfter[2];

		for (i = 7, len = this.crftBtns.length; i < len; i++) {
			crftBtnsAfter[i] = crftBtnsAfter[i] && ui_inventory[this.crftBtns[i].id] && ui_inventory[this.crftBtns[i].id].length;
		}
		for (i = 0; i < 3; i++) {
			var crftGroupActive = false;
			for (j = 0, len = this.crftCustom[i].length; j < len; j++) {
				if (crftBtnsAfter[this.crftCustom[i][j]]) {
					crftGroupActive = true;
					break;
				}
			}
			crftBtnsAfter[i+4] = crftBtnsAfter[i+4] && crftGroupActive && crftBtnsAfter[i];
		}
		ui_improver.setButtonsVisibility(this.crftBtns, crftBtnsBefore, crftBtnsAfter);

		if (crftFixed) {
			for (i = 0, len = this.crftBtns.length; i < len; i++) {
				if (baseCond && !isMonster && !ui_data.inShop) {
					this.crftBtns[i].classList.remove('crb_inactive');
				} else {
					this.crftBtns[i].classList.add('crb_inactive');
				}
			}
		}
		// if we're in trader mode then try to mark some buttons as unavailable
		if (ui_data.hasShop) {
			ui_improver.switchButtonsForStore(!!ui_data.inShop);
		}
	}
	// voice generators
	if (this.isFirstTime) {
		this.voicegens = document.getElementsByClassName('voice_generator');
		this.voicegenClasses = [];
		for (i = 0, len = this.voicegens.length; i < len; i++) {
			this.voicegenClasses[i] = this.voicegens[i].className;
		}
	}
	var voicegensBefore = [], voicegensAfter = [],
		specialConds, specialClasses;
	if (!ui_data.isFight) {
		var isGoingBack = ui_stats.isGoingBack(),
			isTown = ui_stats.townName(),
			isSearching = ui_improver.detectors.stateGTF.res || ui_stats.lastNews().includes('дорогу'),
			isResting = /^(Переводит дух|Сушит вещи|Catching h.. breath|Drying h.. clothes)\.\.\.$/.test(ui_stats.lastNews()),
			dieIsDisabled = ui_storage.getFlag('Option:disableDieButton'),
			isFullGP = ui_stats.Godpower() === ui_stats.Max_Godpower(),
			isFullHP = ui_stats.HP() === ui_stats.Max_HP(),
			canQuestBeAffected = !/\((?:выполнено|completed|отменено|cancelled)\)/.test(ui_stats.Task_Name());
		specialClasses = ['heal', 'do_task', 'cancel_task', 'die', 'exp', 'dig', 'town', 'pray', 'sacrifice'];
		specialConds = [isMonster || isGoingBack || isTown || isSearching || isFullHP,				// heal
						isMonster || isGoingBack || isTown || isSearching || !canQuestBeAffected,	// do_task
																			 !canQuestBeAffected,	// cancel_task
						isMonster ||				isTown ||				 dieIsDisabled,			// die
						isMonster,																	// exp
						isMonster ||				isTown,											// dig
						isMonster || isGoingBack || isTown || isSearching || isResting,				// town
						isMonster ||										 isFullGP,				// pray
													isTown 											// sacrifice
					   ];
	}
	baseCond = baseCond && !worker.$('.r_blocked:visible').length;
	for (i = 0, len = this.voicegens.length; i < len; i++) {
		voicegensBefore[i] = !this.voicegens[i].classList.contains('hidden');
		voicegensAfter[i] = baseCond;
		if (baseCond && !ui_data.isFight) {
			for (var j = 0, len2 = specialConds.length; j < len2; j++) {
				if (specialConds[j] && this.voicegenClasses[i] && this.voicegenClasses[i].match(specialClasses[j])) {
					voicegensAfter[i] = false;
				}
			}
		}
	}
	ui_improver.setButtonsVisibility(this.voicegens, voicegensBefore, voicegensAfter);
};
ui_improver.setButtonsVisibility = function(btns, before, after) {
	for (var i = 0, len = btns.length; i < len; i++) {
		if (before[i] && !after[i]) {
			ui_utils.hideElem(btns[i], true);
		}
		if (!before[i] && after[i]) {
			ui_utils.hideElem(btns[i], false);
		}
	}
};
ui_improver.switchButtonsForStore = function(disable) {
	var activityButtons = document.querySelectorAll('#cntrl1 a, #cntrl2 a, a.voice_generator, a.craft_button, a.inspect_button');
	for (var i = 0, len = activityButtons.length; i < len; i++) {
		activityButtons[i].classList[disable ? 'add' : 'remove']('crb_inactive');
	}
	if (document.getElementById('voice_submit')) {
		document.getElementById('voice_submit').style.color = disable ? 'silver' : '';
	}
};
ui_improver.improveTownAbbrs = function(towns) {
	try {
		var ntname, abbr;
		for (var i = 0, len = towns.length; i < len; i++) {
			if (ntname = ui_words.base.town_list.find(function(a) { return a.abbr && a.name === towns[i].name; })) {
				abbr = towns[i].g.lastChild;
				if (abbr.textContent !== ntname.abbr) {
					abbr.textContent = ntname.abbr;
				}
			}
		}
	} catch(e) {}
};
ui_improver.nearbyTownsFix = function(onlyDistance) {
	if (!document.getElementById('hmap')) {
		return;
	}
	try {
		if (worker.GUIp_browser === 'Opera') {
			ui_improver.operaWMapFix();
		}
		var tmr, pos, town, towns, lval = document.querySelector('#hk_distance .l_val');
		pos = +document.querySelector('#hmap_svg g.loc_dir:not([style$="none;"]) + title').textContent.match(/(\d+)/)[1];
		towns = Array.from(document.querySelectorAll('#hmap_svg g.tl title, #hmap_svg g.sl title'))
				.map(function(a){ var b = a.textContent.match(/^(.*?) \((\d+)\)/); return {name:b[1], dst:+b[2], g: a.parentNode}; })
				.sort(function(a,b) { return a.dst - b.dst; });
		if (!onlyDistance) {
			var switchTI = function(town) {
				if (ui_improver.informTown === town.name) {
					ui_improver.distanceInformerReset();
				} else if (!ui_improver.dailyForecast.includes('gvroads')) {
					ui_improver.distanceInformerSet(town);
				}
			};
			for (var i = 0, len = towns.length; i < len; i++) {
				if (!ui_utils.isAlreadyImproved(towns[i].g)) {
					GUIp.common.addListener(towns[i].g, 'mousedown', function(town, e) {
						if (e.which === 2) {
							switchTI(town);
							e.preventDefault();
							return;
						}
						tmr = GUIp.common.setTimeout(function() {
							switchTI(town);
							if (towns[0].g.parentNode) towns[0].g.parentNode.classList.add('block_once');
							tmr = 0;
						},1e3);
					}.bind(null, towns[i]));
					GUIp.common.addListener(towns[i].g, 'mouseup', function() {
						if (tmr) {
							worker.clearTimeout(tmr);
						}
					});
				}
				if (ui_improver.informTown === towns[i].name) {
					towns[i].g.classList.add('e_selected_town');
				}
			}
			if (towns.length && !towns[0].g.parentNode.classList.contains('improved')) {
				GUIp.common.addListener(towns[0].g.parentNode, 'click', function(e) {
					if (this.classList.contains('block_once')) {
						this.classList.remove('block_once');
						e.stopPropagation();
					}
				}, true);
				towns[0].g.parentNode.classList.add('improved');
			}
			if (ui_storage.getFlag('Option:improveTownAbbrs')) {
				ui_improver.improveTownAbbrs(towns);
			}
		}
		town = towns.filter(function(b) { return b.dst <= pos; }).pop();
		if (!lval.title.includes(town.name)) {
			lval.title = worker.GUIp_i18n.fmt('nearby_town', town.name, town.dst);
		}
	} catch(e) {}
};
ui_improver.operaWMapFix = function() {
	if (ui_improver.operaWMapObserver) {
		ui_improver.operaWMapObserver.disconnect();
	} else {
		ui_improver.operaWMapObserver = GUIp.common.newMutationObserver(function(mutations) {
			var f = false;
			for (i = 0, len = mutations.length; i < len; i++) {
				if (mutations[i].attributeName === 'd' || mutations[i].attributeName === 'to' && mutations[i].target.nodeName === 'animateTransform' && mutations[i].target.getAttribute('type') === 'translate') {
					mutations[i].target.removeAttribute('oprfix');
					GUIp.common.debug('removing oprfix from "' + mutations[i].attributeName + '"')
					f = true;
				}
			}
			if (f) {
				ui_improver.operaWMapFix();
			}
		});
	}
	// info popups positioned at the bottom most of the time are glitched and not repainted properly, placing them at the top kinda "resolves" this issue
	var hinfos = document.querySelectorAll('.hmap_info');
	for (var i = 0, len = hinfos.length; i < len; i++) {
		hinfos[i].style.bottom = 'inherit';
		hinfos[i].style.top = '0';
	}
	// if the map is placed at central column then it's not required to apply rescaling to it
	if (document.querySelector('.c_col #hmap')) {
		return;
	}
	var svg = document.getElementById('hmap_svg');
	var hscale = function(a) { return (a*0.65).toFixed(2); }
	var paths = svg.querySelectorAll('path');
	for (var j = 0, i = 0; i < paths.length; i++) {
		if (paths[i].getAttribute('d') && !paths[i].getAttribute('oprfix')) {
			paths[i].setAttribute('d',paths[i].getAttribute('d').replace(/\d+(\.\d+)?/g,hscale));
			paths[i].setAttribute('oprfix','1');
			j++;
		}
		if (paths[i].classList.contains('tmap')) {
			paths[i].style.strokeWidth = hscale(2);
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' paths');
	var gs = svg.querySelectorAll('g[transform^="translate"]');
	for (var j = 0, i = 0, len = gs.length; i < len; i++) {
		if (!gs[i].getAttribute('oprfix')) {
			gs[i].setAttribute('transform',gs[i].getAttribute('transform').replace(/\d+(\.\d+)?/g,hscale));
			gs[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' gs');
	var circles = svg.querySelectorAll('circle');
	for (var j = 0, i = 0, len = circles.length; i < len; i++) {
		if (!circles[i].getAttribute('oprfix')) {
			circles[i].setAttribute('r',hscale(circles[i].getAttribute('r')));
			circles[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' circles');
	var texts = svg.querySelectorAll('text');
	for (var j = 0, i = 0, len = texts.length; i < len; i++) {
		if (!texts[i].getAttribute('oprfix')) {
			texts[i].setAttribute('dy',hscale(texts[i].getAttribute('dy')));
			texts[i].setAttribute('font-size',hscale(texts[i].getAttribute('font-size')));
			texts[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' texts');
	var anims = svg.querySelectorAll('animateTransform[type="translate"]');
	for (var j = 0, i = 0, len = anims.length; i < len; i++) {
		if (!anims[i].getAttribute('oprfix')) {
			anims[i].setAttribute('from',anims[i].getAttribute('from').replace(/\d+(\.\d+)?/g,hscale));
			anims[i].setAttribute('to',anims[i].getAttribute('to').replace(/\d+(\.\d+)?/g,hscale));
			anims[i].setAttribute('oprfix','1');
			j++;
		}
	}
	if (j) GUIp.common.info('world map: scaled '+j+' anims');
	svg.style.transform = 'scale(1)';
	ui_improver.operaWMapObserver.observe(svg,{ subtree: true, attributes: true, attributeFilter: ['d','to']});
};
ui_improver.thirdEyePositionFix = function() {
	var owtop, oatop, wtop, atop,
		content = document.querySelector('.wup.in .tep'),
		wup = document.querySelector('.wup.in');
	try {
		owtop = wup.style.top;
		oatop = wup.firstChild.style.top;
		wup.style.top = '-2000px';
		ui_utils.hideElem(wup,false);
		content.style.maxHeight = (worker.innerHeight - 150) + 'px';
		wtop = worker.innerHeight + worker.scrollY - wup.clientHeight - 75;
		atop = document.getElementById('imp_button').getBoundingClientRect().top + worker.scrollY - wtop + 11;
		if (atop < 50) {
			wtop -= 50 - atop;
			atop = 50;
		}
		if (atop > wup.clientHeight) {
			wtop += atop - wup.clientHeight + 10;
			atop -= atop - wup.clientHeight + 15;
		}
		if (wtop < 0) wtop = 0;
		wup.firstChild.style.top = atop + 'px';
		wup.style.top = wtop + 'px';
	} catch (e) {
		wup && wup.firstChild && (wup.firstChild.style.top = oatop);
		wup && (wup.style.top = owtop);
	}
};
ui_improver.chatsFix = function() {
	var chats = document.getElementsByClassName('frDockCell'),
		len = chats.length;
	// fix overlapping issues when opening a huge number of chats
	for (var i = 0; i < len; i++) {
		var chat = chats[i];
		chat.classList.remove('left');
		chat.style.zIndex = len - i;
		if (chat.getBoundingClientRect().right < 350) {
			chat.classList.add('left');
		}
	}
	// padding for page settings link
	var paddingBottom = len ? chats[0].getBoundingClientRect().bottom - chats[len - 1].getBoundingClientRect().top : worker.GUIp_browser === 'Opera' ? 27 : 0,
		isBottom = worker.scrollY >= document.documentElement.scrollHeight - document.documentElement.clientHeight - 10;
	paddingBottom = Math.floor(paddingBottom*10)/10 + 10;
	paddingBottom = paddingBottom < 0 ? 0 : paddingBottom + 'px';
	document.getElementsByClassName('reset_layout')[0].style.paddingBottom = paddingBottom;
	if (isBottom) {
		worker.scrollTo(0, document.documentElement.scrollHeight - document.documentElement.clientHeight);
	}
};
ui_improver.improveChat = function() {
	var chats = document.getElementsByClassName('chat_ph')[0];
	if (!chats || ui_utils.isAlreadyImproved(chats)) {
		return;
	}
	// by default, guildmate's name is appended to the end of the message when clicked.
	// we insert it into current cursor's position instead
	GUIp.common.addListener(chats, 'click', function(ev) {
		var node = ev.target;
		// ignore unless a godname is clicked
		if (!node.classList.contains('gc_fr_god')) {
			return;
		}
		var godName = node.textContent;
		// find the textarea
		while (!(node = node.parentNode).classList.contains('frMsgBlock')) { }
		node = node.querySelector('.frInputArea textarea');
		// insert godname into it
		var text = node.value,
			pos = node.selectionDirection === 'backward' ? node.selectionStart : node.selectionEnd;
		node.value = text.slice(0, pos) + '@' + godName + ', ' + text.slice(pos);
		node.focus();
		node.selectionStart = node.selectionEnd = pos + godName.length + 3;
		// avoid executing original code
		ev.stopPropagation();
	}, true);
};
ui_improver._getChatMsgText = function(msg) {
	var author = msg.getElementsByClassName('gc_fr_god')[0],
		result = author ? author.textContent : '',
		classes;
	// there might be hyperlinks in the message
	for (var child = msg.firstChild; child && !((classes = child.classList) && classes.contains('fr_msg_meta')); child = child.nextSibling) {
		result += child.textContent;
	}
	return result;
};
ui_improver._fixChatScrolling = function(msgArea) {
	var msgs = msgArea.getElementsByClassName('fr_msg_l'),
		latestMsgText = '',
		latestMsgOffset = 0,
		scroll = msgArea.scrollTop;
	if (msgs.length) {
		latestMsgText = this._getChatMsgText(msgs[msgs.length - 1]);
		latestMsgOffset = msgs[msgs.length - 1].offsetTop;
	}

	msgArea.addEventListener('scroll', function() {
		// executed without a try-catch wrapper for speed; extra careful here
		scroll = this.scrollTop;
	});

	GUIp.common.newMutationObserver(function() {
		var i = msgs.length - 1;
		if (i < 0) return;
		var j = i,
			newLatestMsg = msgs[i],
			newLatestMsgText = ui_improver._getChatMsgText(newLatestMsg),
			latestMsg = newLatestMsg;
		// look for the message we've seen the last time
		if (newLatestMsgText !== latestMsgText) {
			while (i-- && ui_improver._getChatMsgText((latestMsg = msgs[i])) !== latestMsgText) { }
			latestMsgText = newLatestMsgText;
		}

		var newLatestMsgOffset = newLatestMsg.offsetTop;
		if (i < 0 || (j - i <= 5 && scroll >= latestMsgOffset + latestMsg.offsetHeight - msgArea.offsetHeight - 3)) {
			// we've lost it or were already at the bottom; scroll down to the bottom
			// but guild council preserves its scrolling when more than 5 messages arrive at once, so do we
			msgArea.scrollTop = 2147483647;
		} else {
			// restore scrolling with respect to new messages' height
			msgArea.scrollTop = scroll + (latestMsg.offsetTop - latestMsgOffset);
		}
		latestMsgOffset = newLatestMsgOffset;
	}).observe(msgArea, {childList: true, subtree: true});
};
ui_improver.processNewChat = function(chat) {
	var node = chat.getElementsByClassName('dockfrname')[0],
		key, saved, header;
	if (!node || !(key = node.textContent) || !(node = chat.querySelector('.frInputArea textarea'))) {
		return;
	}

	key = 'PM:' + key;
	if ((saved = ui_tmpstorage.get(key))) {
		node.value = saved;
		node.dispatchEvent(new Event('change', {bubbles: true})); // ask Godville to resize the textarea
		if (node.disabled) {
			// Guild Council window is initially disabled, and when it gets enabled, the text is cleared
			GUIp.common.newMutationObserver(function(mutations, observer) {
				if (!node.disabled) {
					node.value = saved;
					observer.disconnect();
				}
			}).observe(node, {attributes: true, attributeFilter: ['disabled']});
		}
	}

	var saveText = function() {
		var text = node.value;
		if (text) {
			ui_tmpstorage.set(key, text);
		} else {
			ui_tmpstorage.remove(key);
		}
	};
	if (GUIp_browser !== 'Opera') {
		GUIp.common.addListener(node, 'blur', saveText);
	} else {
		// beforeunload doesn't fire in Opera, so we need to save the text periodically
		var oprTimer = 0;
		GUIp.common.addListener(node, 'blur', function() {
			saveText();
			worker.clearTimeout(oprTimer);
			oprTimer = 0;
		});
		var oprTimerCallback = function() {
			saveText();
			oprTimer = 0;
		};
		node.addEventListener('keydown', function() {
			// executed without a try-catch wrapper for speed; extra careful here
			if (!oprTimer) {
				oprTimer = GUIp.common.setTimeout(oprTimerCallback, 3e3);
			}
		});
	}

	// wait until chat is loaded
	if ((header = chat.getElementsByClassName('fr_chat_header')[0])) {
		ui_utils.observeUntil(header, {characterData: true, childList: true}, function() {
			return header.textContent.trim() || undefined;
		}).then(function(friendName) {
			var pos = friendName.search(/ и е(?:го|ё) | and h[ie][sr] hero/);
			if (pos >= 0) {
				friendName = friendName.slice(0, pos);
			}
			// hide desktop notification when opening the chat
			// unfortunately, .dockfrname contains just abbreviated name
			ui_utils.hideNotification('pm_' + friendName);
			GUIp.common.addListener(node, 'focus', ui_utils.hideNotification.bind(ui_utils, 'pm_' + friendName));
			// scroll position is reset every time a new message appears. try to workaround that
			var msgArea = chat.getElementsByClassName('frMsgArea')[0];
			if (msgArea) {
				ui_improver._fixChatScrolling(msgArea);
			}
		}).catch(GUIp.common.onUnhandledException);
	}
};
ui_improver.checkGCMark = function(csource) {
	var gc_tab = document.querySelector('.msgDock.frDockCell.frbutton_pressed .dockfrname');
	if (gc_tab && gc_tab.textContent.match(/Гильдсовет|Guild Council/) && gc_tab.parentNode.getElementsByClassName('fr_new_msg').length) {
		worker.$('.frbutton_pressed textarea').triggerHandler('focus');
	}
};
ui_improver.createWakelock = function() {
	var wakelockSwitch = document.createElement('div');
	wakelockSwitch.id = 'wakelock_switch';
	wakelockSwitch.textContent = '💡';
	GUIp.common.addListener(wakelockSwitch, 'click', ui_improver.switchWakelock);
	document.getElementById('main_wrapper').insertBefore(wakelockSwitch,null);
	if (ui_storage.getFlag('wakelockEnabled')) {
		wakelockSwitch.click();
	}
};
ui_improver.switchWakelock = function() {
	var wakelockSwitch = document.getElementById('wakelock_switch');
	if (ui_improver.wakeLock) {
		ui_improver.wakeLock.unlock();
		ui_improver.wakeLock = null;
		ui_storage.set('wakelockEnabled',false);
		wakelockSwitch.classList.remove('wakelock_enabled');
	} else {
		ui_improver.wakeLock = worker.navigator.requestWakeLock('screen');
		if (ui_improver.wakeLock) {
			ui_storage.set('wakelockEnabled',true);
			wakelockSwitch.classList.add('wakelock_enabled');
		}
	}
};
ui_improver.initTouchDragging = function() {
	Array.prototype.forEach.call(document.getElementsByClassName('b_handle'), ui_dragger.register);
};
ui_improver.activity = function() {
	if (!ui_logger.updating) {
		ui_logger.updating = true;
		GUIp.common.setTimeout(function() {
			ui_logger.updating = false;
		}, 500);
		ui_logger.update();
	}
};
