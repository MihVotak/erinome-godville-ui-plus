// ui_data
var ui_data = worker.GUIp.data = {};

// base variables initialization
ui_data.init = function() {
	ui_data._initVariables();
	// init mobile cookies
	GUIp.common.forceDesktopPage();
	// desktop notifications permissions
	if ((ui_storage.getFlag('Option:enableInformerAlerts') || ui_storage.getFlag('Option:enablePmAlerts')) && worker.GUIp_browser !== 'Chrome' && worker.Notification && worker.Notification.permission !== "granted") {
		worker.Notification.requestPermission();
	}
	ui_data._getWantedMonster();
	GUIp.common.setInterval(ui_data._getWantedMonster, 5*60*1000);
};
ui_data._initVariables = function() {
	this.currentVersion = '@VERSION@';
	this.docTitle = document.title;
	this.isFight = ui_stats.isFight();
	this.isDungeon = ui_stats.isDungeon();
	this.isSail = ui_stats.isSail();
	this.isMining = ui_stats.isMining();
	this.isBoss = ui_stats.fightType() === 'monster';
	this.god_name = ui_stats.godName();
	this.char_name = ui_stats.charName();
	this.char_sex = ui_stats.isMale() ? worker.GUIp_i18n.hero : worker.GUIp_i18n.heroine;
	GUIp.common.setCurrentGodname(this.god_name);
	ui_storage.set('charIsMale', ui_stats.isMale());
	if (ui_stats.checkShop()) {
		ui_storage.set('charHasShop',true);
		this.hasShop = true;
	} else {
		this.hasShop = ui_storage.getFlag('charHasShop') || false;
	}
	this.inShop = false;
	this.storedPets = JSON.parse(ui_storage.get('charStoredPets')) || [];
	if (!ui_storage.get('Option:activeInformers')) {
		// default preset of informers
		var informersPreset = {
			full_godpower:48, much_gold:48, dead:48, low_health:48, fight:48, selected_town:48, wanted_monster:48, special_monster:16, tamable_monster:112, chosen_monster:48,pet_knocked_out:48, close_to_boss:48, close_to_rival:48, guild_quest:16, custom_informers:48,
			treasure_box:48, charge_box:48, gift_box:48, good_box:48
		};
		if (!ui_stats.hasTemple()) {
			informersPreset['smelter'] = informersPreset['smelt!'] = informersPreset['transformer'] = informersPreset['transform!'] = 48;
		}
		ui_storage.set('Option:activeInformers',JSON.stringify(informersPreset));
		ui_storage.set('Option:disableDieButton',true);
		ui_storage.set('Option:useShortPhrases',true);
		ui_storage.set('Option:enableInformerAlerts',true);
		ui_storage.set('Option:enablePmAlerts',true);
	}
	if (!ui_storage.get('ForumSubscriptions')) {
		ui_storage.set('ForumSubscriptions', '{}');
		ui_storage.set('ForumInformers', '{}');
	}
	document.body.classList.add('superhero');
	document.body.classList.add(this.isDungeon ? 'dungeon' : this.isSail ? 'sail' : this.isMining ? 'mining' : this.isFight ? 'fight' : 'field');
	if (ui_stats.hasTemple()) {
		document.body.classList.add('has_temple');
	}
	if (this.isFight) {
		var abilities = ui_stats.Enemy_AbilitiesText().toLowerCase();
		this._parseBossAbs(abilities);
		if (/mutating|мутирующий/i.test(abilities)) {
			GUIp.common.newMutationObserver(this._parseBossAbs.bind(this, null)).observe(document.querySelector('#o_hk_gold_we + .line:not(#o_hk_death_count) .l_val'),{childList: true, subtree: true});
		}
	} else {
		this.lastFieldInit = ui_storage.set_with_diff('lastFieldInit',Date.now());
	}
	ui_utils.voiceInput = document.getElementById('god_phrase') || document.getElementById('godvoice');
};
ui_data._parseBossAbs = function(abilities) {
	abilities = abilities || ui_stats.Enemy_AbilitiesText().toLowerCase();
	var abilitiesList = {'золотоносный':'auriferous','глушащий':'deafening','лучезарный':'enlightened','взрывной':'explosive','неверующий':'faithless','мощный':'hulking','паразитирующий':'leeching','бойкий':'nimble','ушастый':'overhearing','тащущий':'pickpocketing','спешащий':'scurrying','творящий':'skilled','драпающий':'sneaky','транжирящий':'squandering','зовущий':'summoning','пробивающий':'sweeping','мутирующий':'mutating'},
		abilitiesVals = Object.values(abilitiesList);
	Array.from(document.body.classList).forEach(function(a) {
		if (a.startsWith('boss_')) {
			document.body.classList.remove(a);
		}
	});
	abilities.split(',').forEach(function(a) {
		a = a.trim();
		if (abilitiesList[a] || abilitiesVals.includes(a)) {
			document.body.classList.add('boss_' + (abilitiesList[a] || a));
		}
	});
};
ui_data._getWantedMonster = function(forced) {
	if (forced || isNaN(ui_storage.get('WantedMonster:Date')) ||
		ui_utils.dateToMoscowTimeZone(+ui_storage.get('WantedMonster:Date')) < ui_utils.dateToMoscowTimeZone(Date.now())) {
		GUIp.common.getDomainXHR('/news', ui_data._parseWantedMonster);
	} else {
		ui_improver.wantedMonsters = new RegExp('^(' + ui_storage.get('WantedMonster:Value') + ')$','i');
		ui_improver.wantedItems = new RegExp('^(' + ui_storage.get('WantedItem:Value') + ')$','i');
		ui_improver.bingoItems = ui_storage.get('BingoItems') ? new RegExp(ui_storage.get('BingoItems'),'i') : null;
		ui_improver.dailyForecast = ui_storage.getList('DailyForecast');
		ui_improver.dailyForecastText = ui_storage.get('DailyForecastText') || '';
		ui_improver.showDailyForecast();
	}
};
ui_data._parseWantedMonster = function(xhr) {
	var temp, newsDate;
	if (temp = xhr.responseText.match(/<div id="date"[^>]*>[^<]*<span>(\d+)<\/span>/)) {
		newsDate = temp[1] * 86400000 + ((worker.GUIp_locale === 'ru') ? 1195560000000 : 1273492800000);
	} else {
		newsDate = Date.now();
	}
	temp = xhr.responseText.match(/(?:Разыскиваются|Wanted)[^]+?>([^<]+?)<\/a>[^]+?>([^<]+?)<\/a>/)
	if (temp) {
		temp = temp[1] + '|' + temp[2];
		ui_storage.set('WantedMonster:Date', newsDate);
		ui_storage.set('WantedMonster:Value', temp);
		ui_improver.wantedMonsters = new worker.RegExp('^(' + temp + ')$','i');
	} else {
		ui_improver.wantedMonsters = null;
	}
	temp = xhr.responseText.match(/(?:Куплю-продам|Buy and Sell)[^]+?\/index\.php\/[^]+?>([^<]+?)<\/a>[^]+?\/index\.php\/[^]+?>([^<]+?)<\/a>/);
	if (temp) {
		temp = temp[1] + '|' + temp[2];
		ui_storage.set('WantedItem:Value', temp);
		ui_improver.wantedItems = new worker.RegExp('^(' + temp + ')$','i');
	} else {
		ui_improver.wantedItems = null;
	}
	temp = xhr.responseText.match(/(?:Астропрогноз|Daily Forecast)[^]+?<p>([^<]+?)<\/p>(?:[^<]+?<p>([^<]+?)<\/p>)?/);
	if (temp) {
		var forecastData, forecastText, forecast = [];
		forecastText = temp[1] ? (temp[1] + (temp[2] ? '\n' + temp[2] : '')).replace(/&#0149;/g,'•') : '';
		temp = temp[1] + '|' + temp[2];
		ui_improver.dailyForecast = GUIp.common.parseForecasts(temp);
		ui_improver.dailyForecastText = forecastText;
		ui_storage.set('DailyForecast', ui_improver.dailyForecast);
		ui_storage.set('DailyForecastText', ui_improver.dailyForecastText);
	} else {
		ui_improver.dailyForecast = [];
		ui_improver.dailyForecastText = '';
	}
	ui_improver.showDailyForecast();
	var bingoMatch, bingoItems = [], bingoRE = /<td><span>(.*?)<\/span><\/td>/g;
	while ((bingoMatch = bingoRE.exec(xhr.responseText))) {
		bingoItems.push(bingoMatch[1]);
	}
	ui_storage.set('BingoItems', bingoItems.join('|'));
	bingoMatch = /<span\s[^<>]*\bid\s*=\s*["']?b_cnt\b[^<>]*>(.*?)<\//.exec(xhr.responseText); // span#b_cnt
	ui_storage.set('BingoTries', (bingoMatch && parseInt(bingoMatch[1])) || 0);
	ui_improver.bingoItems = bingoItems.length ? new RegExp(bingoItems.join('|'),'i') : null;
	if (document.querySelector('#inventory li.improved')) {
		ui_inventory._update();
	}
};
