// ui_inventory
var ui_inventory = worker.GUIp.inventory = {};

ui_inventory.observer = {
	config: {
		childList: true,
		attributes: true,
		subtree: true,
		attributeFilter: ['style']
	},
	func: function(mutations) {
		ui_observers.mutationChecker(mutations, function(mutation) {
			return mutation.target.tagName.toLowerCase() === 'li' && mutation.type === "attributes" &&
				   mutation.target.style.display === 'none' && mutation.target.parentNode ||
				   mutation.target.tagName.toLowerCase() === 'ul' && mutation.addedNodes.length;
		}, ui_inventory._update.bind(ui_inventory));
	},
	target: ['#inventory ul']
};
ui_inventory.bossPartRE = /[]/; // never matches
ui_inventory.init = function() {
	if (ui_data.isFight) {
		return;
	}
	this.forbiddenCraft = ui_storage.get('Option:forbiddenCraft') || '';
	this.bossPartRE = new RegExp('^(?:' + GUIp_i18n.bp_types.join('|') + ')' + GUIp_i18n.bp_prefix, 'i');
	this._createCraftButtons();
	this._update();
	ui_observers.start(ui_inventory.observer);
};
ui_inventory.rebuildCustomCraft = function() {
	var groupType, rebuiltGroup, craftGroups = document.getElementsByClassName('craft_group');
	for (var i = 0, len = craftGroups.length; i < len; i++)
	{
		while (craftGroups[i].firstChild) {
			craftGroups[i].removeChild(craftGroups[i].firstChild);
		}
		groupType = craftGroups[i].className.match(/(b_b|b_r|r_r)/g);
		if (groupType) {
			rebuiltGroup = ui_inventory._createCraftSubGroup(groupType[0]);
			rebuiltGroup.className = craftGroups[i].className;
			craftGroups[i].parentNode.replaceChild(rebuiltGroup,craftGroups[i]);
		}
	}
	ui_inventory._update();
};
ui_inventory._createCraftButtons = function() {
	var customGroup, relocateButtons = ui_storage.getFlag('Option:relocateCraftButtons'),
		invContent = document.querySelector('#inventory .block_content'),
		craftContent = document.createElement('span');
	if (!invContent) {
		return;
	}
	craftContent.className = 'craft_button span';
	craftContent.insertBefore(ui_inventory._createCraftButton(worker.GUIp_i18n.b_b, 'b_b', worker.GUIp_i18n.b_b_hint), null);
	craftContent.insertBefore(ui_inventory._createCraftSubGroup('b_b'), null);
	craftContent.insertBefore(ui_inventory._createCraftButton(worker.GUIp_i18n.b_r, 'b_r', worker.GUIp_i18n.b_r_hint), null);
	craftContent.insertBefore(ui_inventory._createCraftSubGroup('b_r'), null);
	craftContent.insertBefore(ui_inventory._createCraftButton(worker.GUIp_i18n.r_r, 'r_r', worker.GUIp_i18n.r_r_hint), null);
	craftContent.insertBefore(ui_inventory._createCraftSubGroup('r_r'), null);
	invContent.insertBefore(craftContent, relocateButtons ? invContent.firstChild : null);
};
ui_inventory._createInspectButton = function(item_name) {
	var a = document.createElement('a');
	a.className = 'inspect_button' + (ui_data.inShop ? ' crb_inactive' : '');
	a.title = worker.GUIp_i18n.fmt('ask_to_inspect', ui_data.char_sex[0], item_name);
	a.textContent = '?';
	GUIp.common.addListener(a, 'click', ui_inventory._inspectButtonClick.bind(null, item_name));
	return a;
};
ui_inventory._inspectButtonClick = function(item_name, ev) {
	ev.preventDefault();
	ui_utils.setVoice(ui_words.inspectPhrase(worker.GUIp_i18n.trophy + item_name));
};
ui_inventory._createCraftButton = function(combo, combo_list, hint) {
	var a = document.createElement('a');
	if (!combo_list.includes('customCraft-')) {
		a.className = 'craft_button ' + combo_list + (ui_data.inShop ? ' crb_inactive' : '');
		a.title = worker.GUIp_i18n.fmt('ask_to_craft', ui_data.char_sex[0], hint);
	} else {
		a.className = 'craft_button' + (ui_data.inShop ? ' crb_inactive' : '');
		a.id = combo_list;
		a.title = worker.GUIp_i18n.fmt('ask_to_craft_a', ui_data.char_sex[0], hint);
	}
	a.innerHTML = combo;
	GUIp.common.addListener(a, 'click', ui_inventory._craftButtonClick.bind(a, combo_list));
	return a;
};
ui_inventory._craftButtonClick = function(combo_list, ev) {
	ev.preventDefault();
	var rand = Math.floor(Math.random()*ui_inventory[combo_list].length),
		items = ui_inventory[combo_list][rand];
	if (Math.random() < 0.5) {
		items.reverse();
	}
	ui_utils.setVoice(ui_words.craftPhrase(items[0] + worker.GUIp_i18n.and + items[1]));
};
ui_inventory._createCraftSubGroup = function(combo_group) {
	var span = document.createElement('span'),
		customCraftGroups = ui_inventory._customCraftCheckGrp(combo_group);
	span.className = 'craft_group ' + combo_group + ' hidden';
	span.insertBefore(document.createElement('wbr'), null);
	span.insertBefore(document.createTextNode('('), null);
	for (var i = 0, len = customCraftGroups.length; i < len; i++) {
		span.insertBefore(document.createElement('wbr'), null);
		span.insertBefore(ui_inventory._createCraftButton(customCraftGroups[i].t, 'customCraft-'+combo_group+'-'+customCraftGroups[i].i, customCraftGroups[i].d), null);
	}
	span.insertBefore(document.createTextNode(')'), null);
	span.insertBefore(document.createElement('wbr'), null);
	return span;
};
ui_inventory._customCraftCheckGrp = function(grp) {
	var grpm, result = [];
	switch (grp) {
		case 'b_b': grpm = new worker.RegExp('ж|b','i'); break;
		case 'b_r': grpm = new worker.RegExp('с|m','i'); break;
		default: grpm = new worker.RegExp('н|r','i');
	}
	for (var i = 0, len = ui_words.base.custom_craft.length; i < len; i++) {
		var customCombo = ui_words.base.custom_craft[i];
		if (customCombo.q) {
			continue;
		}
		if (customCombo.g.match(grpm)) {
			result.push(customCombo);
		}
	}
	return result;
};
ui_inventory._update = function() {
	var i, len, item, count, usable, gp, flags = [],
		bold_items = 0,
		bingo_tries = parseInt(ui_storage.get('BingoTries')) || 0,
		trophy_type = {};
	// there's no inventory li-s in sail mode, but state.inventory is present nevertheless
	if (ui_data.isSail || ui_data.isMining) {
		return;
	}
	for (i = 0, len = ui_words.base.usable_items.types.length; i < len; i++) {
		flags[i] = false;
	}
	// Parse items
	var inventory = ui_inventory._getInventory();
	for (i = 0, len = inventory.length; i < len; i++) {
		item = ui_inventory._getItemName(inventory[i]);
		count = ui_inventory._getCount(inventory[i]);
		// color items and add buttons
		if ((usable = inventory[i].querySelector('a.item_act_link_div'))) { // usable item
			var m = /^(.*?)\s*\((.*?)\)$/.exec(usable.title) || [, usable.title, usable.title],
				description = m[1],
				bold = ui_inventory._isBold(inventory[i]),
				sect = ui_words.usableItemType(description),
				// cannot be undefined because we've already succeeded with a .querySelector search
				ilink = inventory[i].getElementsByTagName('a')[0];
			if (bold) {
				bold_items += count;
			}
			if (sect !== -1) {
				flags[sect] = true;
				inventory[i].classList.add('type-' + ui_words.base.usable_items.types[sect].replace(/\ /g,'-'));
			} else if (!ui_utils.hasShownInfoMessage) {
				ui_utils.hasShownInfoMessage = true;
				ui_utils.showMessage('info', {
					title: worker.GUIp_i18n.unknown_item_type_title,
					content: '<div>' + worker.GUIp_i18n.unknown_item_type_content + '<b>"' + description + '</b>"</div>'
				});
			}
			if (!(this.forbiddenCraft.includes('usable') || (this.forbiddenCraft.includes('b_b') && this.forbiddenCraft.includes('b_r'))) &&
				!ui_words.usableItemTypeMatch(description, 'to arena box') && !ui_words.usableItemTypeMatch(description, 'temper box')) {
				trophy_type[item] = {a:true, b:bold, c:count};
			}
			// confirm dialog on item activation for firefox on Android
			if (GUIp.common.isAndroid) {
				var ilink2;
				if (!inventory[i].getElementsByTagName('a')[1]) {
					ilink2 = document.createElement('a');
					GUIp.common.addListener(ilink2, 'click', function(link) {
						if (!link.classList.contains('div_link') || worker.confirm(link.title + '. ' + worker.GUIp_i18n.confirm_item_activate)) {
							link.click();
						}
					}.bind(null, ilink));
					ilink.parentNode.insertBefore(ilink2,ilink.nextSibling);
					ilink.style.display = 'none';
				} else {
					ilink2 = inventory[i].getElementsByTagName('a')[1];
				}
				if (ilink2) {
					ilink2.title = ilink.title;
					ilink2.className = ilink.className;
					ilink2.textContent = '★';
				}
			} else if (!inventory[i].classList.contains('improved') && (m = /\d+\s*%/.exec(m[2])) && (gp = parseInt(m[0]))) {
				ui_utils.addGPConfirmation(worker.$(ilink), gp,
					worker.GUIp_i18n.fmt('do_you_want', worker.GUIp_i18n.to_activate + item));
			}
			if (ui_improver.dailyForecast.includes('cheapactivatables')) {
				inventory[i].classList.add('usable_item_fcc');
				inventory[i].classList.remove('usable_item_fce');
			} else if (ui_improver.dailyForecast.includes('expensiveactivatables')) {
				inventory[i].classList.add('usable_item_fce');
				inventory[i].classList.remove('usable_item_fcc');
			} else {
				inventory[i].classList.remove('usable_item_fcc,usable_item_fce');
			}
		} else if (ui_inventory._isHealing(inventory[i])) { // healing item
			// if item quantity has increased, it seems that class needs to be re-added again
			inventory[i].classList.add('heal_item');
			if (!(this.forbiddenCraft.includes('heal') || (this.forbiddenCraft.includes('b_r') && this.forbiddenCraft.includes('r_r')))) {
				trophy_type[item] = {b:false,c:count};
			}
		} else {
			if (ui_inventory._isBold(inventory[i])) { // bold item
				bold_items += count;
				if (!(this.forbiddenCraft.includes('b_b') && this.forbiddenCraft.includes('b_r')) && ui_inventory._isCraftable(inventory[i])) {
					trophy_type[item] = {b:true,c:count};
				}
			} else {
				if (!(this.forbiddenCraft.includes('b_r') && this.forbiddenCraft.includes('r_r')) && ui_inventory._isCraftable(inventory[i])) {
					trophy_type[item] = {b:false,c:count};
				}
			}
			if (!inventory[i].classList.contains('improved')) {
				inventory[i].insertBefore(ui_inventory._createInspectButton(item), null);
			}
		}
		if (ui_improver.wantedItems && item.match(ui_improver.wantedItems)) {
			inventory[i].classList.add('wanted_item');
		} else {
			inventory[i].classList.remove('wanted_item');
		}
		if (bingo_tries > 0 && (item === '❄' || (ui_improver.bingoItems && ui_improver.bingoItems.test(item)))) {
			inventory[i].classList.add('bingo_item');
		} else {
			inventory[i].classList.remove('bingo_item');
		}
		inventory[i].classList.add('improved');
	}

	for (i = 0, len = flags.length; i < len; i++) {
		ui_informer.update(ui_words.base.usable_items.types[i], flags[i]);
	}
	ui_informer.update('transform!', flags[ui_words.base.usable_items.types.indexOf('transformer')] && bold_items >= 2);
	ui_informer.update('smelt!', flags[ui_words.base.usable_items.types.indexOf('smelter')] && ui_stats.Gold() >= 3000);

	ui_inventory._updateCraftCombos(trophy_type);
};
ui_inventory._updateCraftCombos = function(trophy_type) {
	// Склейка трофеев, формирование списков
	ui_inventory.b_b = [];
	ui_inventory.b_r = [];
	ui_inventory.r_r = [];
	var item_names = worker.Object.keys(trophy_type).sort(),
		ccraft_enabled = ui_storage.getFlag('Option:enableCustomCraft');
	if (item_names.length) {
		for (var i = 0, len = item_names.length; i < len; i++) {
			for (var j = i + 1; j < len && i < len - 1; j++) {
				if (item_names[i][0] === item_names[j][0]) {
					if (trophy_type[item_names[i]].b && trophy_type[item_names[j]].b) {
						if (!this.forbiddenCraft.includes('b_b')) {
							ui_inventory.b_b.push([item_names[i], item_names[j]]);
						}
					} else if (!trophy_type[item_names[i]].b && !trophy_type[item_names[j]].b) {
						if (!this.forbiddenCraft.includes('r_r')) {
							ui_inventory.r_r.push([item_names[i], item_names[j]]);
						}
					} else {
						if (!this.forbiddenCraft.includes('b_r')) {
							ui_inventory.b_r.push([item_names[i], item_names[j]]);
						}
					}
				} else {
					break;
				}
			}
			if (trophy_type[item_names[i]].c > 1) {
				if (trophy_type[item_names[i]].b) {
					ui_inventory.b_b.push([item_names[i], item_names[i]]);
				} else {
					ui_inventory.r_r.push([item_names[i], item_names[i]]);
				}
			}
		}
		for (var i = 0, len = ui_words.base.custom_craft.length; i < len; i++) {
			var customCombo = ui_words.base.custom_craft[i];
			if (customCombo.g.match(/ж|b/i)) {
				ui_inventory['customCraft-b_b-'+customCombo.i] = [];
				if (ccraft_enabled) {
					for (var j = 0, len2 = ui_inventory.b_b.length; j < len2; j++) {
						if (customCombo.l.includes(ui_inventory.b_b[j][0][0].toLowerCase())) {
							if (customCombo.g.match(/а|a/i) && !(trophy_type[ui_inventory.b_b[j][0]].a || trophy_type[ui_inventory.b_b[j][1]].a)) {
								continue;
							}
							ui_inventory['customCraft-b_b-'+customCombo.i].push(ui_inventory.b_b[j]);
						}
					}
				}
			}
			if (customCombo.g.match(/с|m/i)) {
				ui_inventory['customCraft-b_r-'+customCombo.i] = [];
				if (ccraft_enabled) {
					for (var j = 0, len2 = ui_inventory.b_r.length; j < len2; j++) {
						if (customCombo.l.includes(ui_inventory.b_r[j][0][0].toLowerCase())) {
							if (customCombo.g.match(/а|a/i) && !(trophy_type[ui_inventory.b_r[j][0]].a || trophy_type[ui_inventory.b_r[j][1]].a)) {
								continue;
							}
							ui_inventory['customCraft-b_r-'+customCombo.i].push(ui_inventory.b_r[j]);
						}
					}
				}
			}
			if (customCombo.g.match(/н|r/i)) {
				ui_inventory['customCraft-r_r-'+customCombo.i] = [];
				if (ccraft_enabled) {
					for (var j = 0, len2 = ui_inventory.r_r.length; j < len2; j++) {
						if (customCombo.l.includes(ui_inventory.r_r[j][0][0].toLowerCase())) {
							if (customCombo.g.match(/а|a/i) && !(trophy_type[ui_inventory.r_r[j][0]].a || trophy_type[ui_inventory.r_r[j][1]].a)) {
								continue;
							}
							ui_inventory['customCraft-r_r-'+customCombo.i].push(ui_inventory.r_r[j]);
						}
					}
				}
			}
		}
	}
	// fixme! this shouldn't be called until everything in improver has finished loading
	if (!ui_improver.isFirstTime) {
		ui_improver.calculateButtonsVisibility();
	}
};

/**
 * @returns {!Array<!HTMLLIElement>}
 */
ui_inventory._getInventory = function() {
	return Array.prototype.filter.call(
		document.querySelectorAll('#inv_block_content li'),
		function(item) {
			return item.style.display !== 'none';
		}
	);
};

/**
 * @param {!HTMLElement} item
 * @returns {string}
 */
ui_inventory._getItemName = function(item) {
	return item.firstChild.textContent;
};

/**
 * @param {!HTMLElement} item
 * @returns {boolean}
 */
ui_inventory._isHealing = function(item) {
	return item.style.fontStyle === 'italic';
};

/**
 * @param {!HTMLElement} item
 * @returns {boolean}
 */
ui_inventory._isBold = function(item) {
	var value = item.style.fontWeight;
	return value === 'bold' || !!+value;
};

/**
 * @param {!HTMLElement} item
 * @returns {boolean} Whether the item could be activated (godpower is *not* taken into account).
 */
ui_inventory._isUsable = function(item) {
	return item.getElementsByClassName('item_act_link_div').length !== 0;
};

/**
 * @param {!HTMLElement} item
 * @returns {boolean} Whether the item could be used to craft something.
 */
ui_inventory._isCraftable = function(item) {
	var name = this._getItemName(item);
	return !this.bossPartRE.test(name) && !/ошмёток босса |триббла|шкуру разыскиваемого |старую шмотку|золотой кирпич|ареналин|босскоин|shred of the |tribble$|proof of a wanted |old piece of equipment|gold(?:en)? brick|arena ticket|arenalin|bosscoin/i.test(name);
};

/**
 * @param {!HTMLElement} item
 * @returns {boolean} Whether the item could (not) be sold to a trader.
 */
ui_inventory._isUnsellable = function(item) {
	var name = this._getItemName(item);
	if (ui_improver.detectors.stateTP.res && this.bossPartRE.test(name) && !ui_stats.townName()) {
		return true; // boss parts cannot be sold to a field trader
	}
	return ui_inventory._isHealing(item) || /триббла|шкуру разыскиваемого |tribble$|proof of a wanted /i.test(name);
};

/**
 * @param {!HTMLElement} item
 * @returns {number} Amount of stacked items, or 1.
 */
ui_inventory._getCount = function(item) {
	var field = item.childNodes[1], count;
	if (!field) {
		return 1;
	}
	count = /\((\d+)\s*(?:шт|pcs)\)$/.exec(field.textContent);
	return (count && +count[1]) || 1;
};

/**
 * @param {string} name
 * @returns {boolean}
 */
ui_inventory.hasItem = function(name) {
	name = (name || '').toLowerCase();
	return this._getInventory().some(function(item) {
		return ui_inventory._getItemName(item).toLowerCase() === name;
	});
};

/**
 * Check if there is an activatable of the given type (e.g. 'coolstory-box') in the inventory.
 *
 * @param {string} type
 * @returns {boolean}
 */
ui_inventory.hasType = function(type) {
	if (!ui_words.base.usable_items.types.includes(String.prototype.replace.call(type, /-/g, ' '))) {
		throw new Error('unknown category in gv.inventoryHasType: "' + type + '"');
	}
	type = 'type-' + type;
	return this._getInventory().some(function(item) {
		return item.classList.contains(type);
	});
};

/**
 * @returns {number}
 */
ui_inventory.getHealingItems = function() {
	return this._getInventory().reduce(function(result, item) {
		return result + (ui_inventory._isHealing(item) ? ui_inventory._getCount(item) : 0);
	}, 0);
};

/**
 * @returns {number}
 */
ui_inventory.getUnsellableItems = function() {
	return this._getInventory().reduce(function(result, item) {
		return result + (ui_inventory._isUnsellable(item) ? ui_inventory._getCount(item) : 0);
	}, 0);
};

/**
 * @returns {number}
 */
ui_inventory.getBingoItems = function() {
	var pattern = ui_storage.get('BingoItems'),
		bonus = 0,
		graph = [], filteredSlots = [],
		slots, slotsCount, inventory, item, itemName, itemCount;
	if (!pattern) return 0;
	slots = pattern.split('|');
	slotsCount = slots.length;
	inventory = this._getInventory();
	for (var i = 0, len = inventory.length; i < len; i++) {
		item = inventory[i];
		itemName = this._getItemName(item).toLowerCase();
		if (itemName === '❄') {
			// snowflakes are always at the end of the list, so they don't ban slots
			bonus += this._getCount(item);
			continue;
		}
		for (var j = 0; j < slotsCount; j++) {
			if (itemName.includes(slots[j])) {
				filteredSlots.push(j);
			}
		}
		if (filteredSlots.length) {
			itemCount = this._getCount(item);
			while (itemCount-- > 0) {
				graph.push(filteredSlots);
			}
			filteredSlots = [];
		}
	}
	return Math.min(GUIp.common.findBipartiteMatching(graph, slotsCount) + bonus, slotsCount);
};

/**
 * Count how many items match the regex (case-insensitive) and satisfy the constraints.
 * `constraints` are a string composed of the following letters:
 *
 * + 'h' - Healing.
 * + 'H' - Non-healing.
 * + 'b' - Any bold.
 * + 'B' - Non-bold.
 * + 'a' - Any activatable.
 * + 'A' - Non-activatable.
 * + 'c' - Craftable.
 * + 'C' - Uncraftable.
 * + 's' - Sellable.
 * + 'S' - Unsellable.
 *
 * @param {string}  regex
 * @param {(string|number)} [constraints]
 * @returns {number}
 */
ui_inventory.countLike = function(regex, constraints) {
	var i, len, mask = 0x0, unrecognized = '';
	regex = new RegExp(regex || '', 'i');
	if (typeof constraints === 'string') {
		for (i = 0, len = constraints.length; i < len; i++) {
			switch (constraints.charCodeAt(i)) {
				case 72:  /*H*/ mask |= 0x1; break;
				case 104: /*h*/ mask |= 0x2; break;
				case 66:  /*B*/ mask |= 0x4; break;
				case 98:  /*b*/ mask |= 0x8; break;
				case 65:  /*A*/ mask |= 0x10; break;
				case 97:  /*a*/ mask |= 0x20; break;
				case 67:  /*C*/ mask |= 0x40; break;
				case 99:  /*c*/ mask |= 0x80; break;
				case 83:  /*S*/ mask |= 0x100; break;
				case 115: /*s*/ mask |= 0x200; break;
				// codes above once were a public API and must not be changed
				default: unrecognized += constraints[i];
			}
		}
		if (unrecognized) {
			throw new Error('unknown constraints in gv.inventoryCountLike: "' + unrecognized + '"');
		}
		constraints = mask;
	} else {
		constraints >>>= 0;
		if (constraints >= 0x400) {
			throw new Error('unknown constraints in gv.inventoryCountLike: ' + constraints);
		}
	}

	var result = 0, inventory = this._getInventory();
	for (i = 0, len = inventory.length; i < len; i++) {
		var item = inventory[i];
		if (!regex.test(ui_inventory._getItemName(item))) {
			continue;
		}
		mask =  this._isHealing(item)    ? 0x2   : 0x1;
		mask |= this._isBold(item)       ? 0x8   : 0x4;
		mask |= this._isUsable(item)     ? 0x20  : 0x10;
		mask |= this._isCraftable(item)  ? 0x80  : 0x40;
		mask |= this._isUnsellable(item) ? 0x100 : 0x200; // not a typo
		if ((mask & constraints) === constraints) {
			result += this._getCount(item);
		}
	}
	return result;
};
