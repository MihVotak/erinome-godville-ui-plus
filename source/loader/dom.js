/** @namespace */
var dom = {};

/**
 * @param {string} type
 * @param {(string|number)} id
 * @returns {!Element}
 */
dom.createElement = function(type, id) {
	var elem = document.createElement(type);
	elem.id = 'godville-ui-plus-' + id;
	elem.charset = 'UTF-8';
	return elem;
};

/**
 * @param {(string|number)} id
 * @param {string} src
 * @returns {!HTMLScriptElement}
 */
dom.createScript = function(id, src) {
	var script = dom.createElement('script', id);
	script.type = 'text/javascript';
	script.src = src;
	return script;
};

/**
 * @param {(string|number)} id
 * @param {string} href
 * @returns {!HTMLLinkElement}
 */
dom.createCSSLink = function(id, href) {
	var link = dom.createElement('link', id);
	link.rel = 'stylesheet';
	link.type = 'text/css';
	link.href = href;
	return link;
};

/**
 * @param {string} prefix
 * @returns {function(string, function())}
 */
dom.createModuleLoader = function(prefix) {
	var serial = 0;
	return function(src, onload) {
		var script = dom.createScript(serial++, prefix + src);
		script.onload = onload;
		document.head.appendChild(script);
	};
};

/**
 * @param {string} prefix
 * @returns {function(string)}
 */
dom.createStyleSheetLoader = function(prefix) {
	var serial = 0;
	return function(href) {
		document.head.appendChild(dom.createCSSLink('css-' + serial++, prefix + href));
	};
};
