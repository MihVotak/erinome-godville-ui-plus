(function(window) {
'use strict';

//! include './utils.js';
//! include './xhrs.js';
//! include './modules.js';
//! include './initializer.js';
//! include './dom.js';

var prefix = window.chrome.extension.getURL('');
localStorage.setItem('eGUI_prefix', prefix);

modules.MODULES.browser = {src: 'guip_chrome.js'};

// last chrome available for XP doesn't natively support Object.values
if (!Object.values) {
	modules.MODULES.object  = {src: 'Object.js'};
	modules.MODULES.superhero.deps.push('object');
	modules.MODULES.log.deps.push('object');
}

var messageSender = chrome.runtime.sendMessage.bind(chrome.runtime);

initializer.registerErinomeListener(initializer.createErinomeListener({
	webxhr: xhrs.processWebXHR,
	playsound: messageSender,
	makefocus: messageSender,
	notify: messageSender,
	notifyHide: messageSender,
	notifyHideAll: messageSender
}));
initializer.init(dom.createModuleLoader(prefix), dom.createStyleSheetLoader(prefix));

})(this);
